<!DOCTYPE html>
<html>
<head>
<title>Discogs cost calculator</title>
  <style>
  p { margin:0; color:blue; }
	div,p { margin-left:10px; }
	p#totalprice {color:red }
	.container {display: table;}
	.row  {display: table-row;}
	.left, .right, .middle {display: table-cell;}
  </style>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
</head>
<body>
  <h1>Album cost calculator</h1>
  
  <p>Use the discogs API to grab the cost of your albums. <a href="http://www.discogs.com/developers"><img src="http://www.discogs.com/favicon.ico" /></a> </p>
  <p>Enter a list of albums here and it will try to look them up</p><br />
  <form action="javascript:alert('script probably failed if you see this');">
    <div>
      <textarea name="comments" rows="5" cols="50"></textarea>

      <input type="submit" />
    </div>
	
	<div class="container" id="container"></div>
  </form>
  <div id="errinfo"></div>
  <div id="totalprice"></div>
  <br />
<script>


//global
var totalprice=0;

// recalculate sums
function ccb() {
	totalprice=0;
	$('#container input:checked').each(function() {
		var cbid=$(this).attr('id');
		var id = Number(cbid.replace(/[^0-9\.]+/g,""));
		var text=$('#price'+id).html();
		var price = Number(text.replace(/[^0-9\.]+/g,""));
		totalprice=totalprice+price;
	});

	$('#totalprice').html('<p id="totalprice">Total cost of albums is $'+totalprice+'!</p>');
}
//http://www.actiononline.biz/web/code/how-to-getelementsbyclass-in-javascript-the-code/
function getElementsByClass(theHTML, theClass) {
    //Create Array of All HTML Tags
    var allHTMLTags = theHTML.getElementsByTagName('*');
    var classElements = [];
    var i;

    //Loop through all tags using a for loop
    for (i = 0; i < allHTMLTags.length; i++) {

      //Get all tags with the specified class name.
      if (allHTMLTags[i].className == theClass) {
        classElements.push(allHTMLTags[i]);
      }
    }
    return classElements;
}

// get price from proxy, use closure to "burn" id number
function getprice(id) {
	return function(json) {
		var tempDiv = document.createElement('div');
		tempDiv.innerHTML=json.contents;
		
		var pret=getElementsByClass(tempDiv, 'price');
		var price=$(pret).html();
		if(price!=null){
			var number = Number(pricea.replace(/[^0-9\.]+/g,""));
			totalprice=totalprice+number;
			$('#'+id).append('<p id=price'+id+'>'+price+'</p>');
			$('#totalprice').html('<p id="totalprice">Total cost of albums is $'+totalprice+'!</p>');

			$('#'+id).append('<input type="checkbox" id="cb'+id+'" checked="checked"/> ');
			$('#cb'+id).click(ccb);
		}
		else
		{
			$('#'+id).append('<p>Unknown cost</p>');
		}
	}
}

// dump error
function err(str)
{
  $('#errinfo').fadeIn("slow");
  $("#errinfo").html(str);
}

//submit handler
$("form").submit(function() {
	var text = $('[name=comments]').val();
	var lines = text.split('\n');
	totalprice=0;
	$('#container').empty();
	$('#totalprice').html('<p id="totalprice">Calculating cost...</p>');
	$.each(lines, function(){
		var title=this;
		console.log('processing '+title);
		$.ajax({
			url: 'http://api.discogs.com/database/search',
			data: {"title": title},
			dataType: 'jsonp',
			success: (function (json) {
				var discogs_search=json.data;
				$.each(discogs_search.results, function() {
					console.log('found '+this.title);
					$('#container').append('<div class="row" id='+this.id+'><div class="left"><img src='+this.thumb+'></img></div><div class="middle"><a href=http://www.discogs.com/'+this.uri+'>'+this.title+'</a></div></div><br />');
					var proxy = 'ba-simple-proxy.php?url=';
					var myurl = proxy + encodeURIComponent('http://www.discogs.com/release/'+this.id);
					$.ajax({
					  type: "GET",
					  url: myurl,
					  processData: true,
					  data: {},
					  dataType: "json",
					  success: getprice(this.id)
					}).error(err);
				});
			}),
			jsonp: 'callback'
		});
	});
	return false;
});


</script>

</body>
</html>
