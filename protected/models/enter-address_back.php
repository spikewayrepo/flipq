<div class="container enter-address-wrap">

<div class="section-label">
    <a class="section-label-a">
      <span class="bold">
      <?php echo t("Enter your location below")?></span>
      <b></b>
    </a>     
</div>  

<form id="frm-modal-enter-address" class="frm-modal-enter-address" method="POST" onsubmit="return false;" >
<?php echo CHtml::hiddenField('action','setAddress');?> 
<?php echo CHtml::hiddenField('web_session_id',
isset($this->data['web_session_id'])?$this->data['web_session_id']:''
);?>

<div class="row">
  <div class="col-md-12 ">
    <?php
		$fq_search_adrress=Cookie::getCookie('fq_search_address');

?>
<div id="exstinglocation">
    <p  class="font24 text-black margin-bottom20">Are you from <strong><span id="ipCity"><?php print $fq_search_adrress ?></span>?</strong></p>
<p>
                    <a class="btn btn-xs btn-orange margin-right10" href="javascript:void(0);" id="btnYes">Yes</a>
                    <a class="btn btn-xs btn-white margin-left10" href="javascript:void(0);" id="btnNo">No</a>
                </p>
</div>
<input type="text" id="client_address" name="client_address" value="<?php isset($_SESSION['fq_search_address'])?$_SESSION['fq_search_address']:'' ?>" data-validation="required" class="grey-inputs valid" placeholder="Enter a location" autocomplete="on">

    <?php /* 
echo CHtml::textField('client_address',
	 isset($_SESSION['kr_search_address'])?$_SESSION['kr_search_address']:''
	 ,array(
	 'class'=>"grey-inputs",
	 'data-validation'=>"required"
	 ))
*/
?>

  </div> 
</div> <!--row-->

<div class="row food-item-actions top10">
  <!--<div class="col-md-6 "></div>-->
  <div class="col-md-6 ">
     <input type="submit" class="green-button inline" id="client_address_submit" value="<?php echo t("Submit")?>">
  </div>
  <div class="col-md-6 ">
     <input type="button" class="orange-button inline" id="client_address_not_shown" value="<?php echo t("Not Show Again")?>">
  </div>
</div>
 </form>



</div> <!--container-->

<script type="text/javascript">
$.validate({ 	
	language : jsLanguageValidator,
	language : jsLanguageValidator,
    form : '#frm-modal-enter-address',    
    onError : function() {      
    },
    onSuccess : function() {     
      form_submit('frm-modal-enter-address');
	  close_fb();
      return false;
    }  
})

jQuery(document).ready(function() {
	var google_auto_address= $("#google_auto_address").val();	
	if ( google_auto_address =="yes") {		
	} else {
		$("#client_address").geocomplete({
		    country: $("#admin_country_set").val()
		});	
	}


$('#btnYes').click(function(){
	     $('#client_address').val($.cookie("fq_search_address"));
          close_fb();
		 $('#client_address_submit').click();
		 //close_fb();

	});
	
$('#client_address_not_shown').click(function(){
	//alert("Hi");
	var date= new Date();
	//date.setHours(23,59,59,0);
	$.cookie("Notshowdialog", true, {expires: 1});
	close_fb();
	//location.reload();
	//alert($.cookie("Notshowdialog"));
	});	

$('#btnNo').click(function(){
    $('#client_address').show();
    $('#exstinglocation').hide();
	});
});
</script>
<?php

die();
