<div class="container enter-address-wrap">

<div class="section-label">
    <a class="section-label-a">
      <span class="bold">
      <?php echo t("Enter your location below")?></span>
      <b></b>
    </a>     
</div>  

<form id="frm-modal-enter-address" class="frm-modal-enter-address" method="POST" onsubmit="return false;" >
<?php echo CHtml::hiddenField('action','setAddress');?> 
<?php echo CHtml::hiddenField('web_session_id',
isset($this->data['web_session_id'])?$this->data['web_session_id']:''
);?>

<div class="row">
  <div class="col-md-12 ">
    <?php
		$fq_search_adrress=Cookie::getCookie('fq_search_address');

?>
		<div id="exstinglocation">
		    <p  class="font24 text-black margin-bottom20">Are you from <strong><span id="ipCity"><?php print $fq_search_adrress ?></span>?</strong></p>
		<p>
				    <a class="btn btn-xs btn-orange margin-right10" href="javascript:void(0);" id="btnYes">Yes</a>
				    <a class="btn btn-xs btn-white margin-left10" href="javascript:void(0);" id="btnNo">No</a>
				</p>
		</div>

		<input type="text" id="client_address" name="client_address" value="<?php isset($_SESSION['fq_search_address'])?$_SESSION['fq_search_address']:'' ?>" data-validation="required" class="grey-inputs valid" placeholder="Enter a location" autocomplete="on">

    
  </div> 
</div> <!--row-->

<div class="row food-item-actions top10">
  <!--<div class="col-md-6 "></div>-->
  <div class="col-md-12 ">
     <input type="submit" class="green-button inline" id="client_address_submit" value="<?php echo t("Submit")?>">
  </div>
  <div class="col-md-6 " style="display:none">
     <input type="button" class="orange-button inline" id="client_address_not_shown" value="<?php echo t("Not Show Again")?>">
  </div>
</div>
 </form>



</div> <!--container-->

<script type="text/javascript">

</script>
<?php

//die();
