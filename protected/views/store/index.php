<?php 
$kr_searchh_adrress = FunctionsV3::getSessionAddress();

$home_search_text=Yii::app()->functions->getOptionAdmin('home_search_text');
if (empty($home_search_text)){
	$home_search_text=Yii::t("default","Find restaurants near you");
}



 ?>




<section class="banner">

	<?php 

	if ( $home_search_mode=="address" || $home_search_mode=="") 
	{ 
		if ( $enabled_advance_search=="yes")
		{
			$this->renderPartial('/front/advance_search',array(
				'home_search_text'=>$home_search_text,
				'kr_search_adrress'=>$kr_search_adrress,
				'placholder_search'=>$placholder_search,
				'home_search_subtext'=>$home_search_subtext,
				'theme_search_merchant_name'=>getOptionA('theme_search_merchant_name'),
				'theme_search_street_name'=>getOptionA('theme_search_street_name'),
				'theme_search_cuisine'=>getOptionA('theme_search_cuisine'),
				'theme_search_foodname'=>getOptionA('theme_search_foodname'),
				'theme_search_merchant_address'=>getOptionA('theme_search_merchant_address'),
			));
		} else
			$this->renderPartial('/front/single_search',array(
				'home_search_text'=>$home_search_text,
				'kr_search_adrress'=>$kr_search_adrress,
				'placholder_search'=>$placholder_search,
				'home_search_subtext'=>$home_search_subtext
			));
	} else {
		$this->renderPartial('/front/search_postcode',array(
			'home_search_text'=>$home_search_text,
			'placholder_search'=>$placholder_search,
			'home_search_subtext'=>t("Enter your post code")
		));
	} ?>
	
</section>

<?php
$this->renderPartial('/front/icons');
      $happy_hours = FunctionsV3::getNoofHappy();
      $deals = FunctionsV3::getNoofDeals();
      $fdeals = FunctionsV3::getNoofDeals( '' , 'FLD');
      $restraunts = FunctionsV3::getNoofRestra();
if($_SESSION['fq_search_address'])
{
	$client_lat_long = Yii::app()->functions->geodecodeAddress($_SESSION['fq_search_address']);
} else {
	$place= "Nehru Place, New Delhi, Delhi, India";
	$client_lat_long = Yii::app()->functions->geodecodeAddress($place);
}
$rtype['data']= $restratype;
$this->renderPartial('/front/image_grid',$rtype);
?>
									<!---------------- COUNTERS START HERE ------------------->
									
<section class="counters">
	
	<div class="container">   
		
		<div class="row">
			
			<div class="col-sm-3 col-xs-6 text-center count_home">
				<p class="count"><?php echo $happy_hours; ?></p>
				<p>Extended Happy Hours</p>
			</div>
			
			<div class="col-sm-3 col-xs-6 text-center count_home">
				<p class="count"><?php echo $deals; ?></p>
				<p>Deals</p>
			</div>
			
			<div class="col-sm-3 col-xs-6 text-center count_home">
				<p class="count"><?php echo $restraunts; ?></p>
				<p>Restaurants</p>
			</div>
	
			<div class="col-sm-3 col-xs-6 text-center count_home">
				<p class="count"><?php echo $fdeals; ?></p>
				<p>Flash Deals</p>
			</div>

		</div>
	
	</div> <!-- Container --> 
	
</section> <!-- Counters -->



<?php
	if($mid=FunctionsV3::getMerchantIDbyDeals('FLD'))
	{
		$data['result']= $mid;
		$this->renderPartial('/front/flash_deals', $data);
	}
	?>									
 
		

