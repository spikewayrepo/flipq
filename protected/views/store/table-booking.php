<?php
/*POINTS PROGRAM*/
if (FunctionsV3::hasModuleAddon("pointsprogram")){
	unset($_SESSION['pts_redeem_amt']);
	unset($_SESSION['pts_redeem_points']);
}

$merchant_photo_bg=getOption($merchant_id,'merchant_photo_bg');
if ( !file_exists(FunctionsV3::uploadPath()."/$merchant_photo_bg")){
	$merchant_photo_bg='';
} 

/*RENDER MENU HEADER FILE*/
$ratings=Yii::app()->functions->getRatings($merchant_id);   
$merchant_info=array(   
  'merchant_id'=>$merchant_id ,
  'minimum_order'=>$data['minimum_order'],
  'ratings'=>$ratings,
  'merchant_address'=>$data['merchant_address'],
  'cuisine'=>$data['cuisine'],
  'restaurant_name'=>$data['restaurant_name'],
  'background'=>$merchant_photo_bg,
  'merchant_website'=>$merchant_website,
  'merchant_logo'=>FunctionsV3::getMerchantLogo($merchant_id)
);
$this->renderPartial('/front/menu-header',$merchant_info);

/*ADD MERCHANT INFO AS JSON */
$cs = Yii::app()->getClientScript();
$cs->registerScript(
  'merchant_information',
  "var merchant_information =".json_encode($merchant_info)."",
  CClientScript::POS_HEAD
);		

/*PROGRESS ORDER BAR*/
$this->renderPartial('/front/order-progress-bar',array(
   'step'=>3,
   'show_bar'=>true
));

$now=date('Y-m-d');
$now_time='';
$checkout=FunctionsV3::isMerchantcanCheckout($merchant_id); 
$menu=Yii::app()->functions->getMerchantMenu($merchant_id); 


echo CHtml::hiddenField('is_merchant_open',isset($checkout['code'])?$checkout['code']:'' );

/*hidden TEXT*/
echo CHtml::hiddenField('restaurant_slug',$data['restaurant_slug']);
echo CHtml::hiddenField('merchant_id',$merchant_id);
echo CHtml::hiddenField('is_client_login',Yii::app()->functions->isClientLogin());

echo CHtml::hiddenField('website_disbaled_auto_cart',
Yii::app()->functions->getOptionAdmin('website_disbaled_auto_cart'));

$hide_foodprice=Yii::app()->functions->getOptionAdmin('website_hide_foodprice');
echo CHtml::hiddenField('hide_foodprice',$hide_foodprice);

echo CHtml::hiddenField('accept_booking_sameday',getOption($merchant_id
,'accept_booking_sameday'));

echo CHtml::hiddenField('customer_ask_address',getOptionA('customer_ask_address'));

echo CHtml::hiddenField('merchant_required_delivery_time',
  Yii::app()->functions->getOption("merchant_required_delivery_time",$merchant_id));   
  
/** add minimum order for pickup status*/
$merchant_minimum_order_pickup=Yii::app()->functions->getOption('merchant_minimum_order_pickup',$merchant_id);
if (!empty($merchant_minimum_order_pickup)){
	  echo CHtml::hiddenField('merchant_minimum_order_pickup',$merchant_minimum_order_pickup);
	  
	  echo CHtml::hiddenField('merchant_minimum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_minimum_order_pickup)));
}
 
$merchant_maximum_order_pickup=Yii::app()->functions->getOption('merchant_maximum_order_pickup',$merchant_id);
if (!empty($merchant_maximum_order_pickup)){
	  echo CHtml::hiddenField('merchant_maximum_order_pickup',$merchant_maximum_order_pickup);
	  
	  echo CHtml::hiddenField('merchant_maximum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_maximum_order_pickup)));
}  

/*add minimum and max for delivery*/
$minimum_order=Yii::app()->functions->getOption('merchant_minimum_order',$merchant_id);
if (!empty($minimum_order)){
	echo CHtml::hiddenField('minimum_order',unPrettyPrice($minimum_order));
	echo CHtml::hiddenField('minimum_order_pretty',
	 displayPrice(baseCurrency(),prettyFormat($minimum_order))
	);
}
$merchant_maximum_order=Yii::app()->functions->getOption("merchant_maximum_order",$merchant_id);
 if (is_numeric($merchant_maximum_order)){
 	echo CHtml::hiddenField('merchant_maximum_order',unPrettyPrice($merchant_maximum_order));
    echo CHtml::hiddenField('merchant_maximum_order_pretty',baseCurrency().prettyFormat($merchant_maximum_order));
 }

$is_ok_delivered=1;
if (is_numeric($merchant_delivery_distance)){
	if ( $distance>$merchant_delivery_distance){
		$is_ok_delivered=2;
		/*check if distance type is feet and meters*/
		if($distance_type=="ft" || $distance_type=="mm" || $distance_type=="mt"){
			$is_ok_delivered=1;
		}
	}
} 

echo CHtml::hiddenField('is_ok_delivered',$is_ok_delivered);
echo CHtml::hiddenField('merchant_delivery_miles',$merchant_delivery_distance);
echo CHtml::hiddenField('unit_distance',$distance_type);
echo CHtml::hiddenField('from_address', FunctionsV3::getSessionAddress() );

echo CHtml::hiddenField('merchant_close_store',getOption($merchant_id,'merchant_close_store'));
/*$close_msg=getOption($merchant_id,'merchant_close_msg');
if(empty($close_msg)){
	$close_msg=t("This restaurant is closed now. Please check the opening times.");
}*/
echo CHtml::hiddenField('merchant_close_msg',
isset($checkout['msg'])?$checkout['msg']:t("Sorry merchant is closed."));

echo CHtml::hiddenField('disabled_website_ordering',getOptionA('disabled_website_ordering'));
echo CHtml::hiddenField('web_session_id',session_id());

echo CHtml::hiddenField('merchant_map_latitude',$data['latitude']);
echo CHtml::hiddenField('merchant_map_longtitude',$data['lontitude']);
echo CHtml::hiddenField('restaurant_name',$data['restaurant_name']);


/*add meta tag for image*/
Yii::app()->clientScript->registerMetaTag(
Yii::app()->getBaseUrl(true).FunctionsV3::getMerchantLogo($merchant_id)
,'og:image');
?>

<div class="sections section-menu section-grey2">
<div class="container">
  <div class="row">

     <div class="col-md-8 border menu-left-content">
         
        <div class="tabs-wrapper" id="menu-tab-wrapper">
	    <ul id="tabs">




			<li>
		       <span><?php echo t("Extended happy hours")?></span>
		       <i class="ion-ios-information-outline"></i>
		    </li>


		    <li class="">
		       <span><?php echo t("Overview")?></span>
		       <i class="ion-ios-information-outline"></i>
		    </li>



		     <?php if ( $promo['enabled']==2 && $theme_promo_tab==""):?>
		      <li>
		       <span><?php echo t("Deals")?></span>
		       <i class="ion-pizza"></i>
		      </li>
		    <?php endif;?>



		     <li>
		       <span><?php echo t("Menu")?></span>
		       <i class="ion-fork"></i>
		    </li>


		    <?php if ($theme_reviews_tab==""):?>
		    <li class="view-reviews">
		       <span><?php echo t("Reviews")?></span>
		       <i class="ion-ios-star-half"></i>
		    </li>
		    <?php endif;?>
		    
		    <?php if ($theme_map_tab==""):?>
		    <li class="view-merchant-map">
		       <span><?php echo t("Map")?></span>
		       <i class="ion-ios-navigate-outline"></i>
		    </li>
		    <?php endif;?>
		    
		    <?php if ($booking_enabled):?>
		      <li class="active">
		      <span><?php echo t("Book a Table")?></span>
		      <i class="ion-coffee"></i>
		      </li>
		    <?php endif;?>
		    
		    <?php if ($photo_enabled):?>
		      <li class="view-merchant-photos">
		       <span><?php echo t("Photos")?></span>
		       <i class="ion-images"></i>
		      </li>
		    <?php endif;?>
		    
		 
		    
		    
		</ul>
		
		<ul id="tab">
		
		<!--Overview-->




	    <li>
	        <div class="row">
				<div class="col-xs-12 col-sm-12 col-lg-12  col-md-12  border">
					<div class="panel-group" id="accordion">
					<?php			
						$happyhourlist = Yii::app()->functions->getHappyHoursList();
						if( is_array($happyhourlist)){
							foreach($happyhourlist as $key){
								//print_r($key);
								echo '<div class="panel panel-default"><div class="panel-heading" style="background-color: #000;"><h4 class="panel-title">
									  <a data-toggle="collapse" data-parent="#accordion" href="#collapse" >'.$key['happyhours_name'].' </a>
									  </h4></div>';
								$menubyhappy=Yii::app()->functions->getItemByHappyhours($key['happyhours_name'],false,$merchant_id); 
								#$itemsdata = Yii::app()->functions->getFoodItemByHappyhours($key,$merchant_id);
								echo '<div id="collapse'.$key['happyhours_name'].'" class="panel-collapse collapse in">';
								$this->renderPartial('/front/menu-merchant-happy',array(
										  'merchant_id'=>$merchant_id,
										  'menu'=> $menubyhappy,
										  'disabled_addcart'=>$disabled_addcart,
										  'tc'=>$tc
										));	  
								echo '</div>';
								//print_r($itemsdata);
							}
							echo '</div>';

						} ?>
					</div>
				</div>
			</div>
        </li>
		   
		   <!--END HAPPY HOURS-->
		   
		   <!--OVERVIEW-->		   
	    <li>
	        <div class="row">
			<div class="col-xs-12 col-sm-4 col-lg-4  col-md-4  border">
                             <div class="mbot">
                                     <div class="res-main-phone p-relative phone-details clearfix" title="Flechazo, Marathahalli phone number" id="resinfo-phone">
                                     <div id="phoneNoString" class="phone">
                                        <h2 aria-label="Phone number" role="heading" tabindex="0" class="mb5 ">Phone number</h2>
                                        <span class="tel res-tel">
                    <span class="fontsize2 bold zgreen"><span itemprop="telephone" class="tel" aria-label="<?php print $data['contact_phone'] ?>" tabindex="0"><?php print $data['contact_phone'] ?></span></span><br>                </span>
            

                    </div>
            
    </div>
    <span class="mb5 res-main-phone-reservation">
        <span title="Table reservations" class=" grey-text">
            Table booking recommended        </span>
    </span>



<div class="mbot">
                            <div class="res-info-group clearfix">
        <h2 class="mt0 mb5" tabindex="0">Cuisines</h2>
				<div class="res-info-cuisines clearfix">

<?php echo FunctionsV3::displayCuisine($data['cuisine']);?>
				</div>    
                           </div>
                        </div>




    <div class="clear"></div>

                    </div>




              

	                      <?php //echo getOption($merchant_id,'merchant_information')?>
                        </div>
			<div class="col-xs-12 col-sm-4 col-lg-4  col-md-4  border">

		  		    <?php if ($theme_hours_tab==""):?>
				     <div class="mbot">
						<div class="res-info-group clearfix">
							<div class="res-info-detail">
								<div tabindex="0" class="clearfix">
									<h2 class="mt0 mb5 inlineblock"><?php echo t("Opening Hours")?>
									</h2>
									 <!-- Trigger the modal with a button -->
									<a class="opening-hours" style="border:0 px, color:red," data-toggle="modal" data-target="#myModal">See Opening Hours</a>
									<div class="modal fade" id="myModal" role="dialog">
										<div class="modal-dialog modal-sm">
											<div class="modal-content">
												<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Opening Hours</h4>
												</div>
											<div class="modal-body">
											<?php
											$this->renderPartial('/front/merchant-hours',array(
											'merchant_id'=>$merchant_id
											)); ?>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                     </div>
				    <?php endif;?>
					
					
					<div class="mbot">
                    <div class="res-info-group clearfix">
        <h2 class="mt0 mb5" tabindex="0">Address</h2>
				<div class="res-info-cuisines clearfix">


<?php echo $data['merchant_address']?>
				</div>    
                    </div>
                    </div>
					
            </div>
						
						
	<div class="col-xs-12 col-sm-4 col-lg-4  col-md-4  border">
		<div class="mbot">
            <div class="res-info-group clearfix">
				<h2 class="mt0 mb51" tabindex="0">Highlights</h2>
					<div class="res-info-cuisines clearfix">
						<?php echo FunctionsV3::displayHighlights($data['highlights']);?>
					</div>
			</div>
        </div>
    </div>
						
						
						
  


	        </div>
	    </li>

	    <!--END OVERVIEW-->

		<!--DEALS-->
	    <?php if ( $promo['enabled']==2 && $theme_promo_tab==""):?>
	    <li>
	    <?php $this->renderPartial('/front/merchant-deals',array(
	      'merchant_id'=>$merchant_id,
	      'promo'=>$promo
	    )); ?>        
	    </li>
	    <?php endif;?>
	    <!--END DEALS-->

			
			<!--MENU---->

	    <li>
	        <div class="row">
			 <div class="col-md-4 col-xs-4 border category-list">
				<div class="theiaStickySidebar">
				 <?php 
				 $this->renderPartial('/front/menu-category',array(
				  'merchant_id'=>$merchant_id,
				  'menu'=>$menu			  
				 ));
				 ?>
				</div>
			 </div> <!--col-->
			 <div class="col-md-8 col-xs-8 border" id="menu-list-wrapper">
			 <?php 
			 $admin_activated_menu=getOptionA('admin_activated_menu');
			 $admin_menu_allowed_merchant=getOptionA('admin_menu_allowed_merchant');
			 if ($admin_menu_allowed_merchant==2){			 	 
			 	 $temp_activated_menu=getOption($merchant_id,'merchant_activated_menu');
			 	 if(!empty($temp_activated_menu)){
			 	 	 $admin_activated_menu=$temp_activated_menu;
			 	 }
			 }			 
			 switch ($admin_activated_menu)
			 {
			 	case 1:
			 		$this->renderPartial('/front/menu-merchant-2',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'disabled_addcart'=>$disabled_addcart
					));
			 		break;
			 		
			 	case 2:
			 		$this->renderPartial('/front/menu-merchant-3',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'disabled_addcart'=>$disabled_addcart
					));
			 		break;
			 			
			 	default:	
				 	$this->renderPartial('/front/menu-merchant-1',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'disabled_addcart'=>$disabled_addcart,
					  'tc'=>$tc
					));
			    break;
			 }			 
			 ?>			
			 </div> <!--col-->
			</div> <!--row-->
	    </li>
	    <!--END MENU-->

	    
	    <!--MERCHANT REVIEW-->
	    <?php if ($theme_reviews_tab==""):?>
	    <li class="review-tab-content">	       	     
	    <?php $this->renderPartial('/front/merchant-review',array(
	      'merchant_id'=>$merchant_id
	    )); ?>           
	    </li>
	    <?php endif;?>
	    <!--END MERCHANT REVIEW-->
	    
	    <!--MERCHANT MAP-->
	    <?php if ($theme_map_tab==""):?>
	    <li>	        	
	    <?php $this->renderPartial('/front/merchant-map'); ?>        
	    </li>
	    <?php endif;?>
	    <!--END MERCHANT MAP-->
	    
	    <!--BOOK A TABLE-->
	    <?php if ($booking_enabled):?>
	    <li class="active">
	    <?php $this->renderPartial('/front/merchant-book-table',array(
	      'merchant_id'=>$merchant_id
	    )); ?>        
	    </li>
	    <?php endif;?>
	    <!--END BOOK A TABLE-->
	    
	    <!--PHOTOS-->
	    <?php if ($photo_enabled):?>
	    <li>
	    <?php 
	    $gallery=Yii::app()->functions->getOption("merchant_gallery",$merchant_id);
        $gallery=!empty($gallery)?json_decode($gallery):false;
	    $this->renderPartial('/front/merchant-photos',array(
	      'merchant_id'=>$merchant_id,
	      'gallery'=>$gallery
	    )); ?>        
	    </li>
	    <?php endif;?>
	    <!--END PHOTOS-->
	    
	    <!--INFORMATION-->
	  
	    
	    <!--PROMOS-->
	    <?php /*if ( $promo['enabled']==2 && $theme_promo_tab==""):?>
	    <li>
	    <?php $this->renderPartial('/front/merchant-deals',array(
	      'merchant_id'=>$merchant_id,
	      'promo'=>$promo
	    )); ?>        
	    </li>
	    <?php endif; */?>
	    <!--END PROMOS-->
	    
	    
	   </ul>
	   </div>
     
     </div> <!-- menu-left-content-->
     
     <?php if (getOptionA('disabled_website_ordering')!="yes"):?>
     <div id="menu-right-content" class="col-md-4 border menu-right-content <?php echo $disabled_addcart=="yes"?"hide":''?>" >
     
     <div class="theiaStickySidebar">
      <div class="box-grey rounded  relative">
      
        <!--<div class="star-float"></div>-->
      
        <!--DELIVERY INFO-->
        <div class="inner center">
         <button type="button" class="close modal-close-btn" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button> 
                   
            <?php /* if ($data['service']==3):?>
            <p class="bold"><?php echo t("Distance Information")?></p>
            <?php else :?>
	        <p class="bold"><?php echo t("Delivery Information")?></p>
	        <?php endif; */ ?>
	        <?php /* ?>
	        <p>
	        <?php 
	        if ($distance){
	        	echo t("Distance").": ".number_format($distance,1)." $distance_type";
	        } else echo  t("Distance").": ".t("not available");
	        ?>
	        </p>
	        
	        <p class="delivery-fee-wrap">
	        <?php echo t("Delivery Est")?>: <?php echo FunctionsV3::getDeliveryEstimation($merchant_id)?></p>
	        
	        <p class="delivery-fee-wrap">
	        <?php 
	        if (!empty($merchant_delivery_distance)){
	        	echo t("Delivery Distance Covered").": ".$merchant_delivery_distance." $distance_type_orig";
	        } else echo  t("Delivery Distance Covered").": ".t("not available");
	        ?>
	        </p>
	        
	        <p class="delivery-fee-wrap">
	        <?php 
	        if ($delivery_fee){
	             echo t("Delivery Fee").": ".FunctionsV3::prettyPrice($delivery_fee);
	        } else echo  t("Delivery Fee").": ".t("Free Delivery");
	        ?>
	        </p>
	        
	        <a href="javascript:;" class="top10 green-color change-address block text-center">
	        [<?php echo t("Change Your Address here")?>]
	        </a>
               <?php */ ?>
	        
        </div>
        <!--END DELIVERY INFO-->
        
        <!--CART-->
        <div class="inner line-top relative">
        
           <i class="order-icon your-order-icon"></i>
           
           <p class="bold center"><?php echo t("Your Order")?></p>
           
           <div class="item-order-wrap"></div>
           
           <!--VOUCHER STARTS HERE-->
           <?php //Widgets::applyVoucher($merchant_id);?>
           <!--VOUCHER STARTS HERE-->
           
           <!--MAX AND MIN ORDR-->
           <?php if ($minimum_order>0):?>
           <div class="delivery-min">
              <p class="small center"><?php echo Yii::t("default","Subtotal must exceed")?> 
              <?php echo displayPrice(baseCurrency(),prettyFormat($minimum_order,$merchant_id))?>
           </div>
           <?php endif;?>
           
           <?php if ($merchant_minimum_order_pickup>0):?>
           <div class="pickup-min">
              <p class="small center"><?php echo Yii::t("default","Subtotal must exceed")?> 
              <?php echo displayPrice(baseCurrency(),prettyFormat($merchant_minimum_order_pickup,$merchant_id))?>
           </div>
           <?php endif;?>
              
	        <a href="javascript:;" class="clear-cart">[<?php echo t("Clear Order")?>]</a>
           
        </div> <!--inner-->
        <!--END CART-->
        
        <!--DELIVERY OPTIONS-->
        
        <div class="inner line-top relative delivery-option center">
           <i class="order-icon delivery-option-icon"></i>
           <p class="bold"><?php echo t("ORDER TIME")?></p>
           
           <?php  /*echo CHtml::dropDownList('delivery_type',$now,
           (array)Yii::app()->functions->DeliveryOptions($merchant_id),array(
             'class'=>'grey-fields'
           ))
*/

?>
           
           <?php echo CHtml::hiddenField('delivery_date',$now)?>
           <?php echo CHtml::textField('delivery_date1',
            FormatDateTime($now,false),array('class'=>"j_date grey-fields",'data-id'=>'delivery_date')) ;  ?>
                 
          
           <div class="delivery_asap_wrap"> 
	         <?php echo CHtml::textField('delivery_time',$now_time,
	          array('class'=>"timepick grey-fields",'placeholder'=>Yii::t("default","Timing")))?>	       
	          <span class="delivery-asap">
	          <?php #echo CHtml::checkBox('delivery_asap',false,array('class'=>"icheck"))?>
	          <span class="text-muted"><?php #echo Yii::t("default","Delivery ASAP?")?></span>	          
	         </span>       	         	        
           </div> 


         <!-- delivery_asap_wrap-->
           
           <?php if ( $checkout['code']==1):?>
              <a href="javascript:;" class="orange-button medium checkout"><?php echo $checkout['button']?></a>
           <?php else :?>
              <?php if ( $checkout['holiday']==1):?>
                 <?php echo CHtml::hiddenField('is_holiday',$checkout['msg'],array('class'=>'is_holiday'));?>
                 <p class="text-danger"><?php echo $checkout['msg']?></p>
              <?php else :?>
                 <p class="text-danger"><?php echo $checkout['msg']?></p>
                 <p class="small">
                 <?php echo Yii::app()->functions->translateDate(date('F d l')."@".timeFormat(date('Y-m-d h:i:s'),true));?></p>
              <?php endif;?>
           <?php endif;?>
                                                                
        </div>

 <!--inner-->

        <!--END DELIVERY OPTIONS-->
        
      </div> <!-- box-grey-->
      </div> <!--end theiaStickySidebar-->
     
     </div> <!--menu-right-content--> 
     <?php endif;?>
  
  </div> <!--row-->
</div> <!--container-->
</div> <!--section-menu-->
