

<?php
$kr_search_dj = FunctionsV3::getSessionAddress();

#$home_search_text=Yii::app()->functions->getOptionAdmin('home_search_text');

$home_search_text=Yii::t("default","DEALS");

#$home_search_subtext=Yii::app()->functions->getOptionAdmin('home_search_subtext');
if (empty($home_search_subtext)){
	$home_search_subtext=Yii::t("default","Check the best deals in your city");
}

$home_search_mode=Yii::app()->functions->getOptionAdmin('home_search_mode');
$placholder_search=Yii::t("default","Search Restraunt in your city..");

$placholder_search=Yii::t("default",$placholder_search);
?>

<div id="parallax-wrap" class="parallax-container parallax-home" 
data-parallax="scroll" data-position="top" data-bleed="10" 
data-image-src="<?php echo assetsURL()."/images/banner.jpg"?>">

<?php 
if ( $home_search_mode=="address" || $home_search_mode=="") { 
	if ( $enabled_advance_search=="yes"){
		$this->renderPartial('/front/advance_search',array(
		  'home_search_text'=>$home_search_text,
		  'kr_search_adrress'=>$kr_search_adrress,
		  'placholder_search'=>$placholder_search,
		  'home_search_subtext'=>$home_search_subtext,
		  'theme_search_merchant_name'=>getOptionA('theme_search_merchant_name'),
		  'theme_search_street_name'=>getOptionA('theme_search_street_name'),
		  'theme_search_cuisine'=>getOptionA('theme_search_cuisine'),
		  'theme_search_foodname'=>getOptionA('theme_search_foodname'),
		  'theme_search_merchant_address'=>getOptionA('theme_search_merchant_address'),
		));
	} else $this->renderPartial('/front/single_search',array(
	      'home_search_text'=>$home_search_text,
		  'kr_search_adrress'=>$kr_search_adrress,
		  'placholder_search'=>$placholder_search,
		  'home_search_subtext'=>$home_search_subtext
	));
} else {
	$this->renderPartial('/front/search_postcode',array(
	      'home_search_text'=>$home_search_text,
		  'placholder_search'=>$placholder_search,
		  'home_search_subtext'=>t("Enter your post code")
	));
}
?>

<?php

$this->renderPartial('/front/glasses');

?>

</div> <!--parallax-container-->

<?php if (false/*$theme_hide_how_works<>2*/):?>
<!--HOW IT WORKS SECTIONS-->
<div class="sections section-how-it-works">
<div class="container">
 <h2><?php echo t("How it works")?></h2>
 <p class="center"><?php echo t("Get your favourite food in 4 simple steps")?></p>
 
 <div class="row">
   <div class="col-md-3 col-sm-3 center">
      <div class="steps step1-icon">
        <img src="<?php echo assetsURL()."/images/step1.png"?>">
      </div>
      <h3><?php echo t("Search")?></h3>
      <p><?php echo t("Find all restaurants available near you")?></p>
   </div>
   <div class="col-md-3 col-sm-3 center">
      <div class="steps step2-icon">
         <img src="<?php echo assetsURL()."/images/step2.png"?>">
      </div>
      <h3><?php echo t("Choose")?></h3>
      <p><?php echo t("Browse hundreds of menus to find the food you like")?></p>
   </div>
   <div class="col-md-3 col-sm-3  center">
      <div class="steps step2-icon">
        <img src="<?php echo assetsURL()."/images/step3.png"?>">
      </div>
      <h3><?php echo t("Pay")?></h3>
      <p><?php echo t("It's quick, secure and easy")?></p>
   </div>
   <div class="col-md-3 col-sm-3  center">
     <div class="steps step2-icon">
       <img src="<?php echo assetsURL()."/images/step4.png"?>">
     </div>
      <h3><?php echo t("Enjoy")?></h3>
      <p><?php echo t("Food is prepared & delivered to your door")?></p>
   </div>   
 </div>

 </div> <!--container-->
</div> <!--section-how-it-works-->
<?php endif;?>

					
					<!--HAPPY HOURS-->
					
<?php $cities = explode(',',$_SESSION['fq_search_address']) ?>	
<?php if($mid=FunctionsV3::getMerchantbyCity($cities[0],'','')):?>

<div class="sections section-feature-resto">
<div class="container">
  <h1 style="text-align:center; color: #00b3ff;"><?php echo "EXTENDED HAPPY HOURS" ; ?></h1>
  <br>
  <div class="row">
  <?php foreach($mid as $id): ?>
  <?php if($val=FunctionsV3::getMerchantbyHappy($id['merchant_id'])): ?>
  <?php if($happyhours=FunctionsV3::getNoofHappy($id['merchant_id'])): ?>
  <?php //$_GET['deals']=1;  ?>
  <?php $ratings=Yii::app()->functions->getRatings($val[0]['merchant_id']);
		$name=FunctionsV3::getMerchantbyID($id['merchant_id']);?>   
  <div class="col-sm-4 col-xs-12">
		<div class="thumbnail">
		<a href="<?php echo Yii::app()->createUrl('/store/MerchantHappyHours/merchant/'. trim($name['restaurant_slug']))?>"><img src="<?php echo FunctionsV3::getMerchantBanner($val[0]['merchant_id']);?>" class="img-responsive"></a>
		<div class="caption">
			<h3 style="text-align:center;"><a href="<?php echo Yii::app()->createUrl('/store/MerchantHappyHours/merchant/'.trim($name['restaurant_slug']) )?>"><?php echo clearString($name['restaurant_name'])?></a></h3>
			<p style="text-align:center;"><?php echo $happyhours." Happy Hours Available"?></p>  
			<br>
			<p><a id="preorder" class="btn btn-info btn-sm col-sm-6 col-xs-6 col-xs-offset-3 text-center" href="<?php echo Yii::app()->createUrl('/store/MerchantHappyHours/merchant/'. trim($val[0]['restaurant_slug']) )?>" role="button">Book a Table</a></p>
			<br>
		</div><!--caption-->
	</div>
  </div>
  <?php endif; ?>
  <?php endif; ?>
  <?php endforeach; ?> 
  </div> <!--row-->
 </div> <!--container-->
</div>
<?php endif;?>
	