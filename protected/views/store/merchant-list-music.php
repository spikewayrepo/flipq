<?php
$this->renderPartial('/front/default-header',array(
   'h1'=>t("DJs by Music Type"),
   'sub_text'=>t("choose from your mood")
));
?>

<div class="sections section-grey2 section-browse" id="section-browse">

 <div class="container">
   <?php 
   if (is_array($list['list']) && count($list['list'])>=1){
   	  $this->renderPartial('/front/browse-list1',array(
		   'list'=>$list,
		   'music_page'=>2,
		   'category'=>$category
	  ));
   } else echo '<p class="text-danger">'.t("No DJs found").'</p>';
   ?>
 </div> <!--container--> 

</div><!-- sections-->