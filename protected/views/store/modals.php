<!-- -Login Modal -->
<?php
echo CHtml::hiddenField('mobile_country_code',Yii::app()->functions->getAdminCountrySet(true));
?>
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content login-modal">
        <div class="modal-header login-modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-center" id="loginModalLabel">LOGIN / REGISTER</h4>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <div role="tabpanel" class="login-tab">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a id="signin-taba" href="#home" aria-controls="home" role="tab" data-toggle="tab">Sign In</a></li>
              <li role="presentation"><a id="signup-taba" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Sign Up</a></li>
              <li role="presentation"><a id="forgetpass-taba" href="#forget_password" aria-controls="forget_password" role="tab" data-toggle="tab">Forget Password</a></li>
            </ul>

            <!-- Tab panes -->
          <div class="tab-content">
              <div role="tabpanel" class="tab-pane active text-center" id="home">
                &nbsp;&nbsp;
                <span id="login_fail" class="response_error" style="display: none;">Loggin failed, please try again.</span>
                <div class="clearfix"></div>






		  <form id="forms" class="forms" method="POST">

  <?php echo CHtml::hiddenField('action','clientLogin')?>
         <?php echo CHtml::hiddenField('currentController','store')?>  
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-user"></i></div>


  <?php echo CHtml::textField('username','',
                array('class'=>'form-control','id'=>'username',
                'placeholder'=>t("Email/Mobile"),
               'required'=>true
               ))?>
                    </div>
                    <span class="help-block has-error" id="email-error"></span>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>

 <?php echo CHtml::passwordField('password','',
                array('class'=>'form-control','id'=>'password',
                'placeholder'=>t("Password"),
               'required'=>true
               ))?>
                    </div>
                    <span class="help-block has-error" id="password-error"></span>
                  </div>
		<input type="submit" class="green-button medium full-width" value="Login">

                  <div class="clearfix"></div>
                  <div class="login-modal-footer">
                    <div class="row">
                    
                  </div>
                  </div>
              </form>
              </div>
              <div role="tabpanel" class="tab-pane" id="profile">
                  &nbsp;&nbsp;
                  <span id="registration_fail" class="response_error" style="display: none;">Registration failed, please try again.</span>
                <div class="clearfix"></div>
	    <form id="form-signup" class="form-signup uk-panel uk-panel-box uk-form" method="POST">

   	 <?php echo CHtml::hiddenField('action','clientRegistrationModal')?>
         <?php echo CHtml::hiddenField('currentController','store')?>
         <?php echo CHtml::hiddenField('single_page',2)?>    
         <?php 
             $verification=Yii::app()->functions->getOptionAdmin("website_enabled_mobile_verification");	    
	     if ( $verification=="yes"){
	        echo CHtml::hiddenField('verification',$verification);
	     }
	     if (getOptionA('theme_enabled_email_verification')==2){
	     	echo CHtml::hiddenField('theme_enabled_email_verification',2);
	     }
	     ?>
        


                <div class="row">
                  <div class="col-sm-6 col-xs-6" style="padding-right:5px;">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-user"></i></div>

  <?php echo CHtml::textField('first_name','',
                array('class'=>'form-control',
                'placeholder'=>t("First Name"),
               'required'=>true               
               ))?>
                    </div>
                    <span class="help-block has-error" data-error='0' id="username-error"></span>
                  </div>
                </div>
                <div class="col-sm-6 col-xs-6" style="padding-left:5px;">
                  <div class="form-group">
                      <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-user"></i></div>


  <?php echo CHtml::textField('last_name','',
                array('class'=>'form-control',
                'placeholder'=>t("Last Name"),
               'required'=>true
               ))?>
                      </div>
                      <span class="help-block has-error" data-error='0' id="username-error"></span>
                    </div>
                    </div>
                  </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>


  <?php echo CHtml::textField('contact_phone','',
                array('class'=>'form-control','onchange'=>'sendOTPAction()',
                'placeholder'=>t("Mobile"),
               'required'=>true,
               'maxlength'=>'13',
			   'onkeypress'=>'return isNumber(event);',
               )) ; ?>
                        </div>
                        <span class="help-block has-error" data-error='0' id="username-error"></span>
                      </div>
                  <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-at"></i></div>

  <?php echo CHtml::textField('email_address','',
                array('class'=>'form-control',
                'placeholder'=>t("Email address"),
               'required'=>true
               ))?>


                    </div>
                    <span class="help-block has-error" data-error='0' id="remail-error"></span>
                  </div>



    <div class="form-group"   id="OTPfields" style="display:none">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-at"></i></div>

               <?php echo CHtml::textField('otpfeild','',
                array('class'=>'form-control',
                'placeholder'=>t("OTP"),
               'required'=>true
               ))?>


                    </div>
                    <span class="help-block has-error" data-error='0' id="remail-error"></span>
                  </div>




                  <div class="row">
                    <div class="col-sm-6 col-xs-6"  style="padding-right:5px;">
                  <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-unlock-alt" aria-hidden="true"></i></div>



  <?php echo CHtml::passwordField('password','',
                array('class'=>'form-control',
                'placeholder'=>t("Password"),
               'required'=>true
               ))?>
                    </div>
                    <span class="help-block has-error" data-error='0' id="remail-error"></span>
                  </div>
                </div>
                <div class="col-sm-6 col-xs-6" style="padding-left:5px;">
                  <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-unlock-alt" aria-hidden="true"></i></div>


  <?php echo CHtml::passwordField('cpassword','',
                array('class'=>'form-control',
                'placeholder'=>t("Confirm Password"),
               'required'=>true
               ))?>
                    </div>
                    <span class="help-block has-error" data-error='0' id="remail-error"></span>
                  </div>
                </div>
              </div>
                  <button type="submit" id="register_btn" class="btn btn-block bt-login" data-loading-text="Registering....">Register</button>
                <div class="clearfix"></div>
                <div class="login-modal-footer">
                    <div class="row">
                  
                  </div>
                  </div>
              </form>
              </div>
              <div role="tabpanel" class="tab-pane text-center" id="forget_password">
                &nbsp;&nbsp;
                  <span id="reset_fail" class="response_error" style="display: none;"></span>
                  <div class="clearfix"></div>
<form id="frm-modal-forgotpass" class="frm-modal-forgotpass" method="POST" onsubmit="return false;" >
		<?php echo CHtml::hiddenField('action','forgotPassword')?>
		<?php echo CHtml::hiddenField('do-action', isset($_GET['do-action'])?$_GET['do-action']:'' )?>  

                  <div class="form-group">
                      <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-user"></i></div>


<?php echo CHtml::textField('username-email','',
	                array('class'=>'form-control',
	                'placeholder'=>t("Email address"),
	               'required'=>true
	               ))?>
                      </div>
                      <span class="help-block has-error" data-error='0' id="femail-error"></span>
                    </div>

                    <button type="submit" id="reset_btn" class="btn btn-block bt-login" data-loading-text="Please wait....">Forget Password</button>
                  <div class="clearfix"></div>
                  <div class="login-modal-footer">
                      <div class="row">
                    

                     
                    </div>
                    </div>
                </form>
                </div>
              </div>
          </div>

            </div>
          </div>

      </div>
   </div>
</div>
<!-- - Login Modal Ends Here -->


<!-- - Location Modal Starts Here -->
<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-content login-modal">
          <div class="modal-header login-modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="loginModalLabel">LOCATION</h4>
          </div>


          <div class="modal-body">
            <div class="text-center row">
              <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                  &nbsp;&nbsp;
                  <form>
                  <div class="form-group">
                      <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-user"></i></div>
                          <input type="text" class="form-control" placeholder="Enter Location">
                      </div>
                      <span class="help-block has-error" id="email-error"></span>
                    </div>
                    <button type="button" class="btn btn-block bt-login">Select City</button>
                    <div class="clearfix"></div>
                </form>
              </div>
            </div>
            </div>
        </div>
     </div>
  </div>
  <!-- - Location Modal Ends Here -->
  <div class="blur"></div>

  <div class="locationpopup">
    <i class="fa fa-times close-location"></i>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
            <h2>ENTER YOUR LOCATION</h2>

<form id="frm-modal-enter-address" class="frm-modal-enter-address location-form" method="POST" onsubmit="return false;" >


<?php echo CHtml::hiddenField('action','setAddress');?> 
<?php echo CHtml::hiddenField('web_session_id',
isset($this->data['web_session_id'])?$this->data['web_session_id']:''
);?>


<div class="row">
  <div class="col-md-12 ">
    <?php
		$fq_search_adrress=Cookie::getCookie('fq_search_address');

?>
		<div id="exstinglocation">
		    <p  class="font24 text-black margin-bottom20">Are you from <strong><span id="ipCity"><?php print $fq_search_adrress ?></span>?</strong></p>
		<p>
				    <a class="btn btn-xs btn-orange margin-right10" href="javascript:void(0);" id="btnYes">Yes</a>
				    <a class="btn btn-xs btn-white margin-left10" href="javascript:void(0);" id="btnNo">No</a>
				</p>
		</div>

		<input type="text" id="client_address" name="client_address" value="<?php isset($_SESSION['fq_search_address'])?$_SESSION['fq_search_address']:'' ?>" data-validation="required" class="grey-inputs valid" placeholder="Enter a location" autocomplete="on">

    
  </div> 
</div> <!--row-->





<div class="row food-item-actions top10">
  <!--<div class="col-md-6 "></div>-->
  <div class="col-md-6 ">
     <input type="submit" class="green-button inline" id="client_address_submit" value="<?php echo t("Submit")?>">
  </div>
  <div class="col-md-6 ">
     <input type="button" class="orange-button inline" id="client_address_not_shown" value="<?php echo t("Not Show Again")?>">
  </div>
</div>


            </form>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
          <h3>TOP LOCATIONS</h3>
          <ul>
            <li class="wrapper"><a class="first after">Hauz Khas</a></li>
            <li class="wrapper"><a class="first after">Connaught Place</a></li>
            <li class="wrapper"><a class="first after">Cyber Hub</a></li>
            <li class="wrapper"><a class="first after">Punjabi Bagh</a></li>
            <li class="wrapper"><a class="first after">Greater Kailash-1</a></li>
            <li class="wrapper"><a class="first after">Golf Course</a></li>
          </ul>

        </div>
      </div>
    </div>
  </div>
