<?php 
$kr_search_adrress = FunctionsV3::getSessionAddress();

$home_search_text=Yii::app()->functions->getOptionAdmin('home_search_text');
if (empty($home_search_text)){
	$home_search_text=Yii::t("default","Find Restaurants in Your City");
}
$enabled_advance_search="yes";
?>


<section class="banner">

	<?php 

	if ( $home_search_mode=="address" || $home_search_mode=="") 
	{ 
		if ( $enabled_advance_search=="yes")
		{
			$this->renderPartial('/front/advance_search',array(
				'home_search_text'=>$home_search_text,
				'kr_search_adrress'=>$kr_search_adrress,
				'placholder_search'=>$placholder_search,
				'home_search_subtext'=>$home_search_subtext,
				'theme_search_merchant_name'=>getOptionA('theme_search_merchant_name'),
				'theme_search_street_name'=>getOptionA('theme_search_street_name'),
				'theme_search_cuisine'=>getOptionA('theme_search_cuisine'),
				'theme_search_foodname'=>getOptionA('theme_search_foodname'),
				'theme_search_merchant_address'=>getOptionA('theme_search_merchant_address'),
			));
		} else
			$this->renderPartial('/front/single_search',array(
				'home_search_text'=>$home_search_text,
				'kr_search_adrress'=>$kr_search_adrress,
				'placholder_search'=>$placholder_search,
				'home_search_subtext'=>$home_search_subtext
			));
	} else {
		$this->renderPartial('/front/search_postcode',array(
			'home_search_text'=>$home_search_text,
			'placholder_search'=>$placholder_search,
			'home_search_subtext'=>t("Enter your post code")
		));
	} ?>
	
</section>



<?php 
$this->renderPartial('/front/new_menu');
?>


				  <!--FEATURED RESTAURANT SECIONS-->

<?php if ($disabled_featured_merchant==""):?>
<?php if ( getOptionA('disabled_featured_merchant')!="yes"):?>
<?php if ($res=FunctionsV3::getMerchantbyCityBook()):?>


<div class="sections section-feature-resto">
<div class="container">


  <h1 style="color:#00b3ff; text-align:center;"><?php echo t("Featured Restaurants")?></h1>

  <div class="row">
  <?php foreach ($res as $val): //dump($val);?>
  <?php $address=$val['city'];

        $ratings=Yii::app()->functions->getRatings($val['merchant_id']);
  ?>

   <div class="col-sm-4 col-xs-12">
          <div class="thumbnail">
		  <img src="<?php echo FunctionsV3::getMerchantBanner($val['merchant_id']);?>" class="img-responsive restra" style="height=436px; width:750px">
            <div class="row">
              <div class="col-sm-6 hidden-xs" style="float:right;">
		          <div class="rating-stars" id="mugs" data-score="<?php echo $ratings['ratings']?>"></div>   
	          </div>
	          <!--<div class="col-sm-2 merchantopentag">
	          <?php //echo FunctionsV3::merchantOpenTag($val['merchant_id'])?>   
	          </div>-->
          </div>
              <div class="caption">
                <span class="headding"><a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['restaurant_slug']) )?>">
			    <?php echo clearString($val['restaurant_name'])?></a></h4>
                <p><span class="glyphicon glyphicon-map-marker"></span> <?php echo $address?></p>
				<a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['restaurant_slug']).'?map=true' )?>"><p><span class="fa fa-location-arrow"></span> Get Directions</p></a>
				<br>
                <p class=""><a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'.trim($val['restaurant_slug']).'?preorder=true' )?>" id="preorder" class="btn btn-info btn-sm  col-sm-5 col-xs-5"role="button">Pre-Order</a>
					<span class="col-sm-2 col-xs-2"></span>
					<a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'.trim($val['restaurant_slug']).'?booktable=true' )?>" id="book_table" class="btn btn-danger  col-sm-5 col-xs-5"role="button">Book a Table</a></p>
            </div>
          </div>
        </div>

  <?php endforeach;?>
  </div> <!--end row-->


</div> <!--container-->
</div>
<?php endif;?>
<?php endif;?>
<?php endif; ?>
                      <!--END FEATURED RESTAURANT SECIONS-->



<?php if ($theme_show_app==2):?>
<!--MOBILE APP SECTION-->
<div id="mobile-app-sections" class="container">
<div class="container-medium">
  <div class="row">
     <div class="col-xs-5 into-row border app-image-wrap">
       <img class="app-phone" src="<?php echo assetsURL()."/images/getapp-2.jpg"?>">
     </div> <!--col-->
     <div class="col-xs-7 into-row border">
       <h2><?php echo getOptionA('website_title')." ".t("in your mobile")?>! </h2>
       <h3 class="green-text"><?php echo t("Get our app, it's the fastest way to order food on the go")?>.</h3>

       <div class="row border" id="getapp-wrap">
         <div class="col-xs-4 border">
           <a href="<?php echo $theme_app_ios?>" target="_blank">
           <img class="get-app" src="<?php echo assetsURL()."/images/get-app-store.png"?>">
           </a>
         </div>
         <div class="col-xs-4 border">
           <a href="<?php echo $theme_app_android?>" target="_blank">
             <img class="get-app" src="<?php echo assetsURL()."/images/get-google-play.png"?>">
           </a>
         </div>
       </div> <!--row-->

     </div> <!--col-->
  </div> <!--row-->
  </div> <!--container-medium-->

  <div class="mytable border" id="getapp-wrap2">
     <div class="mycol border">
           <a href="<?php echo $theme_app_ios?>" target="_blank">
           <img class="get-app" src="<?php echo assetsURL()."/images/get-app-store.png"?>">
           </a>
     </div> <!--col-->
     <div class="mycol border">
          <a href="<?php echo $theme_app_android?>" target="_blank">
             <img class="get-app" src="<?php echo assetsURL()."/images/get-google-play.png"?>">
           </a>
     </div> <!--col-->
  </div> <!--mytable-->


</div> <!--container-->
<!--END MOBILE APP SECTION-->
<?php endif; ?>



