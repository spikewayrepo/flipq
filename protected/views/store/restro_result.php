<div aria-hidden="true" aria-labelledby="etaModalLabel" role="dialog" tabindex="-1" id="etaModal" class="modal fade" style="display: none;">
  <div class="modal-dialog modal-sm">
      <div class="modal-content login-modal">
          <div class="modal-header login-modal-header">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="loginModalLabel" class="modal-title text-center">Expected Time of Arrival (ETA)</h4>
          </div>
          <div class="modal-body">
            <div class="text-center row">
              <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                  &nbsp;&nbsp;
<form action="" method="post" id="merchant_url">

                  <div class="form-group">
                      <div style="margin-bottom:15px;" class="input-group">
                          <div class="input-group-addon"><i aria-hidden="true" class="fa fa-calendar"></i></div>
                          <input type="text" placeholder="ETA Date" onclick="this.select()" readonly class="form-control text_col" name="delivery_date1" id="etaBookingDate">

                      </div>

                      <div class="input-group">
                          <div class="input-group-addon"><i aria-hidden="true" class="fa fa-clock-o"></i></div>
                          <!-- <input type="text" placeholder="ETA Time" class="form-control text_col" name="delivery_time" id="etaBookingTime"> -->
                          <select type="text" id="etaBookingTime" class="form-control text_col" name="delivery_time" placeholder="ETA">
                                   <option value="00:00">12:00 AM</option>
                                   <option value="00:30">12:30 AM</option>
                                   <option value="01:00">01:00 AM</option>
                                   <option value="01:30">01:30 AM</option>
                                   <option value="02:00">02:00 AM</option>
                                   <option value="02:30">02:30 AM</option>
                                   <option value="03:00">03:00 AM</option>
                                   <option value="03:30">03:30 AM</option>
                                   <option value="04:00">04:00 AM</option>
                                   <option value="04:30">04:30 AM</option>
                                   <option value="05:00">05:00 AM</option>
                                   <option value="05:30">05:30 AM</option>
                                   <option value="06:00">06:00 AM</option>
                                   <option value="06:30">06:30 AM</option>
                                   <option value="07:00">07:00 AM</option>
                                   <option value="07:30">07:30 AM</option>
                                   <option value="08:00">08:00 AM</option>
                                   <option value="08:30">08:30 AM</option>
                                   <option value="09:00">09:00 AM</option>
                                   <option value="09:30">09:30 AM</option>
                                   <option value="10:00">10:00 AM</option>
                                   <option value="10:30">10:30 AM</option>
                                   <option value="11:00">11:00 AM</option>
                                   <option value="11:30">11:30 AM</option>
                                   <option value="12:00">12:00 PM</option>
                                   <option value="12:30">12:30 PM</option>
                                   <option value="13:00">01:00 PM</option>
                                   <option value="13:30">01:30 PM</option>
                                   <option value="14:00">02:00 PM</option>
                                   <option value="14:30">02:30 PM</option>
                                   <option value="15:00">03:00 PM</option>
                                   <option value="15:30">03:30 PM</option>
                                   <option value="16:00">04:00 PM</option>
                                   <option value="16:30">04:30 PM</option>
                                   <option value="17:00">05:00 PM</option>
                                   <option value="17:30">05:30 PM</option>
                                   <option value="18:00">06:00 PM</option>
                                   <option value="18:30">06:30 PM</option>
                                   <option value="19:00">07:00 PM</option>
                                   <option value="19:30">07:30 PM</option>
                                   <option value="20:00">08:00 PM</option>
                                   <option value="20:30">08:30 PM</option>
                                   <option value="21:00">09:00 PM</option>
                                   <option value="21:30">09:30 PM</option>
                                   <option value="22:00">10:00 PM</option>
                                   <option value="22:30">10:30 PM</option>
                                   <option value="23:00">11:00 PM</option>
                                   <option value="23:30">11:30 PM</option>
          </select>
                      </div>

                      <span id="email-error" class="help-block has-error"></span>
                    </div>
                    <button class="btn-hover" type="submit" id="eta-submit">Submit</button>
                    <div class="clearfix"></div>
                </form>
              </div>
            </div>
            </div>
        </div>
     </div>
  </div>







<section class="product-listing" >
	
	<div class="container">
    
		<div class="row">
			
			<div class="col-sm-12 col-xs-12">
				<p class="gray search-heading"><span>Best Restaurant In Delhi</span></p>
        <p class="gray no-margin"><?php echo "(".$data['total']." Restaurants Found)"; ?></p>
				<p class=""><a id="filters_link1"><i class="fa fa-filter" aria-hidden="true"></i>Filters</a></p>
			</div>
    
		</div>



<div id="productlist">

<?php
	if ($data){
                $count = 0;
 		echo '<div class="row pad-xs-40" id="restralisting">';
		foreach ($data['list'] as $val)
		{
		        $merchant_id = $val['merchant_id']; 
			$ratings=$val['ratings'];//Yii::app()->functions->getRatings($merchant_id);

						?>

        <?php if($count%4==0 && $count!=0 )
	          echo '</div><div class="row pad-xs-40" id="restralisting">';


        ?>
			
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        

							<div class="list-card">
								
								<?php
									$logoo = FunctionsV3::getMerchantLogo($merchant_id);


								?>
<?php $urlm = Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug']);?>
<div class="list-card">
          <div class="image-div1" onclick="window.location='<?php print $urlm; ?>'" style="background: rgba(0,0,0,0) url('<?php print $logoo ?>') no-repeat scroll center center / cover;">
            <?php echo FunctionsV3::merchantOpenTag($merchant_id)?>
          </div>
          <div class="list-card-body">
            <p><a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'].'?booktable=true')?>"><?php echo $val['restaurant_name']; ?></a></p>
<p>
<?php
$displaycuision = FunctionsV3::displayCuisine($val['cuisine']);
$cui_arr = explode("/",$displaycuision );

	if(count($cui_arr) > 5)
		echo "Multicuisine";
	else
		echo "".($val['cuisine']!='null')?$displaycuision:'No Cuisine'."";


									$deals=FunctionsV3::getNoofdeals($merchant_id);
?>

</p>

            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?php echo ($val['location'])?$val['location']:'Loading...' ?></p>
            <a  onClick=getDealsinfo(this) rel="<?php print $merchant_id ?>" ><p class="deals"><svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" id="Layer_1"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z" class="cls-1"/><circle transform="translate(-0.02 0.06) rotate(-0.08)" r="3.83" cy="12.47" cx="37.99" class="cls-2"/></svg><?php echo ($deals)?$deals." Deals":'No Deals' ?></p></a>
            <?php if((int)$val['cost_for_two']){ ?><p><span style="font-family:quick-bold;">₹</span>&nbsp;&nbsp;<?php print (int)$val['cost_for_two'] ?> Cost For 2</p><?php }else{ ?><p> Not Available </p> <?php } ?>




            <div class="rating">

<?php  FunctionsV3::displayrate($ratings['ratings']) ?>


            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <a  OnClick="showpopPre('<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>')" href="javascript:void(0)"><button>Pre-Order</button></a>
              </div>
              <div class="col-sm-6 col-xs-6">
                <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>"><button>Book-Table</button></a>
              </div>
            </div>
          </div>
        </div>

			
        
							</div>
					
					
			
		
		

	</div>
	<?php
$count++;
				} 
			 ?>


</div>
<?php


 } ?>	
	
</div>	
</div>
	
</section>
<?php
  if($mid=FunctionsV3::getMerchantIDbyDeals('FLD'))
  {
    $data['result']= $mid;
    $this->renderPartial('/front/flash_deals', $data);
  }


  ?>


<div id="mobile-search-filter" class="filters">
<?php  $filterinfo['filterleft'] = $data['filterleft']; 
 $this->renderPartial('/front/filtersjs', $filterinfo);
?>
</div>




