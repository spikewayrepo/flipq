<section class="des-body">
  <div class="res-description">
  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <h1><?php echo $data['restaurant_name'];?></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <ul class="details">
        <li><?php echo $data['location'];?></li>
        <li>45 Votes</li>
        <li><div class="rating">

<?php $ratings=Yii::app()->functions->getRatings($merchant_id);?>

<?php  FunctionsV3::displayrate($ratings['ratings']) ?>
         </div></li>

      </ul>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <ul class="details1">
        <li><i class="fa fa-phone" aria-hidden="true"></i><?php print $data['contact_phone'] ?></li>
        <li><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<a class="opening-hours" style="border:0 px, color:red," data-toggle="modal" data-target="#myModal">See Opening Hours</a></li>
        <li class="rating">&#x20b9;&nbsp;&nbsp;1200 for 2</li>
      </ul>
    </div>
  </div>
</div>
</section>