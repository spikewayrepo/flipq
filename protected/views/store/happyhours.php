<?php
// print_r($_REQUEST);
// exit();
	$eta = $_REQUEST['eta'];
	$booking_date = $_REQUEST['date_booking'];
	$city = $_REQUEST['city'];
	$kr_search_dj = FunctionsV3::getSessionAddress();

	#$home_search_text=Yii::app()->functions->getOptionAdmin('home_search_text');

	$home_search_text=Yii::t("default","Extended Happy Hours");

	#$home_search_subtext=Yii::app()->functions->getOptionAdmin('home_search_subtext');
	if (empty($home_search_subtext))
	{
		$home_search_subtext=Yii::t("default","Check the best deals in your city");
	}

	$home_search_mode=Yii::app()->functions->getOptionAdmin('home_search_mode');
	$placholder_search=Yii::t("default","Search Restraunt in your city..");
	$placholder_search=Yii::t("default",$placholder_search);
?>




<section class="banner">

	<?php 

	if ( $home_search_mode=="address" || $home_search_mode=="") 
	{ 
		if ( $enabled_advance_search=="yes")
		{
			$this->renderPartial('/front/advance_search',array(
				'home_search_text'=>$home_search_text,
				'kr_search_adrress'=>$kr_search_adrress,
				'placholder_search'=>$placholder_search,
				'home_search_subtext'=>$home_search_subtext,
				'theme_search_merchant_name'=>getOptionA('theme_search_merchant_name'),
				'theme_search_street_name'=>getOptionA('theme_search_street_name'),
				'theme_search_cuisine'=>getOptionA('theme_search_cuisine'),
				'theme_search_foodname'=>getOptionA('theme_search_foodname'),
				'theme_search_merchant_address'=>getOptionA('theme_search_merchant_address'),
			));
		} else
			$this->renderPartial('/front/single_search',array(
				'home_search_text'=>$home_search_text,
				'kr_search_adrress'=>$kr_search_adrress,
				'placholder_search'=>$placholder_search,
				'home_search_subtext'=>$home_search_subtext
			));
	} else {
		$this->renderPartial('/front/search_postcode',array(
			'home_search_text'=>$home_search_text,
			'placholder_search'=>$placholder_search,
			'home_search_subtext'=>t("Enter your post code")
		));
	} ?>
	
</section>

<?php
	$this->renderPartial('/front/new_menu');
?>



						
						<!--HAPPY HOURS-->

	<?php
		$cities = explode(',',$_SESSION['fq_search_address']);


	?>
	<div class="sections section-feature-resto">
		<div class="container">
			<h1 style="text-align:center; color: #4F8BD1;"><?php echo "EXTENDED HAPPY HOURS" ; ?></h1>
			<div class="row">

			<?php

				if($_REQUEST['date_booking'] && $_REQUEST['eta'] && $_REQUEST['city']){

                                    if($vals=FunctionsV3::getMerchantbyHappy()){
?>


<section class="product-listing">
	
	<div class="container">
    
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div id="restralisting" class="row pad-xs-40">
						<?php
								
							foreach ($vals as $val){

								$merchant_id = $val['merchant_id']; 
								$ratings=$val['ratings'];

						?>
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<div class="list-card">
								<?php
									$logoo = FunctionsV3::getMerchantLogo($merchant_id);
								?>
								<?php $urlm = Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug']);?>

								<div class="list-card">
									<div class="image-div1" onclick="window.location='<?php print $urlm; ?>'" style="background: rgba(0,0,0,0) url('<?php print $logoo ?>') no-repeat scroll center center / cover;">
            							<?php echo FunctionsV3::merchantOpenTag($merchant_id)?>
      								</div>
      								<div class="list-card-body">
										<p>
										<a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'].'?booktable=true')?>"><?php echo $val['restaurant_name']; ?></a>
										</p>
											<p>
											<?php
											$displaycuision = FunctionsV3::displayCuisine($val['cuisine']);
											$cui_arr = explode("/",$displaycuision );

												if(count($cui_arr) > 5)
													echo "Multicuisine";
												else
													echo "".($val['cuisine']!='null')?$displaycuision:'No Cuisine'."";


																				$deals=FunctionsV3::getNoofdeals($merchant_id);
											?>

											</p>
										

 										<p>
 										<i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?php echo $val['location'];?>
 										</p>

							            <a  onClick=getDealsinfo(this) rel="<?php print $merchant_id ?>" >
							            <p class="deals"><svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" id="Layer_1"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z" class="cls-1"/><circle transform="translate(-0.02 0.06) rotate(-0.08)" r="3.83" cy="12.47" cx="37.99" class="cls-2"/></svg><?php echo ($deals)?$deals." Deals":'0 Deals' ?></p>
							            </a>
							            <?php 
							            if((int)$val['cost_for_two']){ ?>
							            		<p><span style="font-family:quick-bold;">₹</span>&nbsp;&nbsp;
							            		<?php print (int)$val['cost_for_two'] ?> Cost For 2</p>
							            <?php }else{ ?>
							            		<p> Not Available </p> 
							            <?php } ?>

										<div class="rating">

											<?php  FunctionsV3::displayrate($ratings['ratings']) ?>

            									<!-- <div class=" rating-stars" data-score="<?php echo $ratings['ratings']?>"></div> -->
            							</div>
									</div>
									<div class="list-card-footer text-center">
							            <div class="row">
							              <div class="col-sm-6 col-xs-6">
							                <a  OnClick="showpopPre('<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>')" href="javascript:void(0)"><button>Pre-Order</button></a>
							              </div>
							              <div class="col-sm-6 col-xs-6">
							                <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>"><button>Book-Table</button></a>
							              </div>
							            </div>
							          </div>
								</div>
							</div>
						</div>
						<?php } ?>
                    </div>
                </div>
            </div>
        </div>
</section>
<?php

                                              // $happyhours=FunctionsV3::getNoofHappy($id['merchant_id']);
                                    }


                                }
?>
		

