<?php
$kr_search_dj = FunctionsV3::getSessionDJ();

#$home_search_text=Yii::app()->functions->getOptionAdmin('home_search_text');

$home_search_text=Yii::t("default","Search Dj");

// #$home_search_subtext=Yii::app()->functions->getOptionAdmin('home_search_subtext');
// if (empty($home_search_subtext)){
// 	$home_search_subtext=Yii::t("default","Check what's going in your city");
// }

$home_search_mode=Yii::app()->functions->getOptionAdmin('home_search_mode');
$placholder_search=Yii::t("default","Search By City, Genre, Mood, Pub....");

$placholder_search=Yii::t("default",$placholder_search);
?>

<div id="" class="banner1" style="url('/images/dj-banner.jpg')" >

<?php if ( $home_search_mode=="address" || $home_search_mode=="") { 
	if ( $enabled_advance_search=="yes"){
		$this->renderPartial('/front/advance_search_dj',array(
		  'home_search_text'=>$home_search_text,
		  'kr_search_adrress'=>$kr_search_adrress,
		  'placholder_search'=>$placholder_search,
		  'home_search_subtext'=>$home_search_subtext,
		  'theme_search_merchant_name'=>getOptionA('theme_search_merchant_name'),
		  'theme_search_street_name'=>getOptionA('theme_search_street_name'),
		  'theme_search_cuisine'=>getOptionA('theme_search_cuisine'),
		  'theme_search_foodname'=>getOptionA('theme_search_foodname'),
		  'theme_search_merchant_address'=>getOptionA('theme_search_merchant_address'),
		));
	} else $this->renderPartial('/front/single_search',array(
	      'home_search_text'=>$home_search_text,
		  'kr_search_adrress'=>$kr_search_adrress,
		  'placholder_search'=>$placholder_search,
		  'home_search_subtext'=>$home_search_subtext
	));
} else {
	$this->renderPartial('/front/search_postcode',array(
	      'home_search_text'=>$home_search_text,
		  'placholder_search'=>$placholder_search,
		  'home_search_subtext'=>t("Enter your post code")
	));
}
?>
</div> <!--parallax-container-->
<!-- <?php
//$this->renderPartial('/front/new_menu');
?> -->


<?php
$this->renderPartial('/front/icons');
      $happy_hours = FunctionsV3::getNoofHappy();
      $deals = FunctionsV3::getNoofDeals();
      $fdeals = FunctionsV3::getNoofDeals( '' , 'FLD');
      $restraunts = FunctionsV3::getNoofRestra();
if($_SESSION['fq_search_address'])
{
  $client_lat_long = Yii::app()->functions->geodecodeAddress($_SESSION['fq_search_address']);
} else {
  $place= "Nehru Place, New Delhi, Delhi, India";
  $client_lat_long = Yii::app()->functions->geodecodeAddress($place);
}
$mtype['data']= $musictype;

$this->renderPartial('/front/image_grid_dj',$mtype);


?>

<!---------------- COUNTERS START HERE ------------------>
                  
<!-- <section class="counters">
  
 <!-- Container --> 
  


 <!-- Counters -->

<?php if ($theme_show_app==2):?>
<!--MOBILE APP SECTION-->
<div id="mobile-app-sections" class="container">
<div class="container-medium">
  <div class="row">
     <div class="col-xs-5 into-row border app-image-wrap">
       <img class="app-phone" src="<?php echo assetsURL()."/images/getapp-2.jpg"?>">
     </div> <!--col-->
     <div class="col-xs-7 into-row border">
       <h2><?php echo getOptionA('website_title')." ".t("in your mobile")?>! </h2>
       <h3 class="green-text"><?php echo t("Get our app, it's the fastest way to order food on the go")?>.</h3>
       
       <div class="row border" id="getapp-wrap">
         <div class="col-xs-4 border">
           <a href="<?php echo $theme_app_ios?>" target="_blank">
           <img class="get-app" src="<?php echo assetsURL()."/images/get-app-store.png"?>">
           </a>
         </div>
         <div class="col-xs-4 border">
           <a href="<?php echo $theme_app_android?>" target="_blank">
             <img class="get-app" src="<?php echo assetsURL()."/images/get-google-play.png"?>">
           </a>
         </div>
       </div> <!--row-->
       
     </div> <!--col-->
  </div> <!--row-->
  </div> <!--container-medium-->
  
  <div class="mytable border" id="getapp-wrap2">
     <div class="mycol border">
           <a href="<?php echo $theme_app_ios?>" target="_blank">
           <img class="get-app" src="<?php echo assetsURL()."/images/get-app-store.png"?>">
           </a>
     </div> <!--col-->
     <div class="mycol border">
          <a href="<?php echo $theme_app_android?>" target="_blank">
             <img class="get-app" src="<?php echo assetsURL()."/images/get-google-play.png"?>">
           </a>
     </div> <!--col-->
  </div> <!--mytable-->
  
  
</div> <!--container-->
<!--END MOBILE APP SECTION-->
<?php endif; ?>


 
