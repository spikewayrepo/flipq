

<?php
$kr_search_dj = FunctionsV3::getSessionAddress();

#$home_search_text=Yii::app()->functions->getOptionAdmin('home_search_text');

$home_search_text=Yii::t("default","DEALS");

#$home_search_subtext=Yii::app()->functions->getOptionAdmin('home_search_subtext');
if (empty($home_search_subtext)){
	$home_search_subtext=Yii::t("default","Check the best deals in your city");
}

$home_search_mode=Yii::app()->functions->getOptionAdmin('home_search_mode');
$placholder_search=Yii::t("default","Search Restraunt in your city..");

$placholder_search=Yii::t("default",$placholder_search);

?>
<section class="banner">

	<?php 

	if ( $home_search_mode=="address" || $home_search_mode=="") 
	{ 
		if ( $enabled_advance_search=="yes")
		{
			$this->renderPartial('/front/advance_search',array(
				'home_search_text'=>$home_search_text,
				'kr_search_adrress'=>$kr_search_adrress,
				'placholder_search'=>$placholder_search,
				'home_search_subtext'=>$home_search_subtext,
				'theme_search_merchant_name'=>getOptionA('theme_search_merchant_name'),
				'theme_search_street_name'=>getOptionA('theme_search_street_name'),
				'theme_search_cuisine'=>getOptionA('theme_search_cuisine'),
				'theme_search_foodname'=>getOptionA('theme_search_foodname'),
				'theme_search_merchant_address'=>getOptionA('theme_search_merchant_address'),
			));
		} else
			$this->renderPartial('/front/single_search',array(
				'home_search_text'=>$home_search_text,
				'kr_search_adrress'=>$kr_search_adrress,
				'placholder_search'=>$placholder_search,
				'home_search_subtext'=>$home_search_subtext
			));
	} else {
		$this->renderPartial('/front/search_postcode',array(
			'home_search_text'=>$home_search_text,
			'placholder_search'=>$placholder_search,
			'home_search_subtext'=>t("Enter your post code")
		));
	} ?>
	
</section>



<?php
$this->renderPartial('/front/new_menu');
?>


					
					<!--HOT DEALS-->
					
<?php $cities = explode(',',$_SESSION['fq_search_address']) ?>	
<?php if($mid=FunctionsV3::getMerchantIDbyDeals()): ?>
<div class="sections section-feature-resto">
<div class="container">
  <h1 style="text-align:center; color: #4F8BD1;"><?php echo "HOT DEALS" ; ?></h1>
  <br>
  <div class="row">
  <?php foreach($mid as $id): ?>
  <?php if($val=FunctionsV3::getMerchantbyDeals($id['merchant_id'],$cities[0])): 
			if($deals=FunctionsV3::getNoofdeals($id['merchant_id'])):
				$_GET['deals']=1; 
				$ratings=Yii::app()->functions->getRatings($id['merchant_id']);
				$address=$cities[0]; ?>   
			<div class="col-sm-4 col-xs-12">
				<div class="thumbnail">
					<a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val[0]['restaurant_slug']).'?deals=true')?>"><img src="<?php echo FunctionsV3::getMerchantBanner($val[0]['merchant_id']);?>" class="img-responsive"></a>
					<div class="row">
						<div class="col-sm-12 hidden-xs" style="float:right;">
							<div class="rating-stars" id="mugs" data-score="<?php echo $ratings['ratings']?>"></div>   
							<div class="col-sm-12" style="float:left; padding-left: 4px">
								<span class="deals"><strong><?php echo $deals." Deals Available"?></strong></span>  
							</div>
							<!--<div class="col-sm-2 merchantopentag">
							<?php //echo FunctionsV3::merchantOpenTag($val['merchant_id'])?>   
							</div>-->
						</div>
					
						<div class="caption">
							<h3 style="text-align:center;"><a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'.trim($val[0]['restaurant_slug']).'?deals=true' )?>"><?php echo clearString($val[0]['restaurant_name'])?></a></h3>
							<p class="text-center"><span class="glyphicon glyphicon-map-marker"></span> <?php echo $val[0]['city'];?></p>
						</div><!--caption-->
					</div>
				</div>
	<?php 	endif;
		endif; ?>
			</div>
  <?php endforeach; ?> 
  </div> <!--row-->
 </div> <!--container-->
</div>
<?php endif;?>
	
