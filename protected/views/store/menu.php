<div class="modal fade" id="fullImageModal" tabindex="-1" role="dialog" aria-labelledby="etaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="loginModalLabel">Lord Of The Drinks</h4>
          </div>
          <div class="modal-body">
            <a class="left-btn" onclick="prev_img()" ><i class="fa fa-chevron-left"></i></a>
            <img src="">
            <a class="right-btn" onclick="next_img()"><i class="fa fa-chevron-right"></i></a>
          </div>
      </div>
   </div>
</div>


<?php
$merchant_photo_bg=getOption($merchant_id,'merchant_photo_bg');
if ( !file_exists(FunctionsV3::uploadPath()."/$merchant_photo_bg")){
  $merchant_photo_bg='';
}

$menu=Yii::app()->functions->getMerchantMenu($merchant_id); 
?>

<?php
echo CHtml::hiddenField('is_merchant_open',isset($checkout['code'])?$checkout['code']:'' );

/*hidden TEXT*/
echo CHtml::hiddenField('restaurant_slug',$data['restaurant_slug']);
echo CHtml::hiddenField('merchant_id',$merchant_id);
echo CHtml::hiddenField('is_client_login',Yii::app()->functions->isClientLogin());

echo CHtml::hiddenField('website_disbaled_auto_cart',
Yii::app()->functions->getOptionAdmin('website_disbaled_auto_cart'));

$hide_foodprice=Yii::app()->functions->getOptionAdmin('website_hide_foodprice');
echo CHtml::hiddenField('hide_foodprice',$hide_foodprice);

echo CHtml::hiddenField('accept_booking_sameday',getOption($merchant_id
,'accept_booking_sameday'));

echo CHtml::hiddenField('customer_ask_address',getOptionA('customer_ask_address'));

echo CHtml::hiddenField('merchant_required_delivery_time',
  Yii::app()->functions->getOption("merchant_required_delivery_time",$merchant_id));   
  
/** add minimum order for pickup status*/
$merchant_minimum_order_pickup=Yii::app()->functions->getOption('merchant_minimum_order_pickup',$merchant_id);
if (!empty($merchant_minimum_order_pickup)){
	  echo CHtml::hiddenField('merchant_minimum_order_pickup',$merchant_minimum_order_pickup);
	  
	  echo CHtml::hiddenField('merchant_minimum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_minimum_order_pickup)));
}
 
$merchant_maximum_order_pickup=Yii::app()->functions->getOption('merchant_maximum_order_pickup',$merchant_id);
if (!empty($merchant_maximum_order_pickup)){
	  echo CHtml::hiddenField('merchant_maximum_order_pickup',$merchant_maximum_order_pickup);
	  
	  echo CHtml::hiddenField('merchant_maximum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_maximum_order_pickup)));
}  

/*add minimum and max for delivery*/
$minimum_order=Yii::app()->functions->getOption('merchant_minimum_order',$merchant_id);
if (!empty($minimum_order)){
	echo CHtml::hiddenField('minimum_order',unPrettyPrice($minimum_order));
	echo CHtml::hiddenField('minimum_order_pretty',
	 displayPrice(baseCurrency(),prettyFormat($minimum_order))
	);
}
$merchant_maximum_order=Yii::app()->functions->getOption("merchant_maximum_order",$merchant_id);
 if (is_numeric($merchant_maximum_order)){
 	echo CHtml::hiddenField('merchant_maximum_order',unPrettyPrice($merchant_maximum_order));
    echo CHtml::hiddenField('merchant_maximum_order_pretty',baseCurrency().prettyFormat($merchant_maximum_order));
 }

$is_ok_delivered=1;
if (is_numeric($merchant_delivery_distance)){
	if ( $distance>$merchant_delivery_distance){
		$is_ok_delivered=2;
		/*check if distance type is feet and meters*/
		if($distance_type=="ft" || $distance_type=="mm" || $distance_type=="mt"){
			$is_ok_delivered=1;
		}
	}
} 

echo CHtml::hiddenField('is_ok_delivered',$is_ok_delivered);
echo CHtml::hiddenField('merchant_delivery_miles',$merchant_delivery_distance);
echo CHtml::hiddenField('unit_distance',$distance_type);
echo CHtml::hiddenField('from_address', FunctionsV3::getSessionAddress() );

echo CHtml::hiddenField('merchant_close_store',getOption($merchant_id,'merchant_close_store'));
/*$close_msg=getOption($merchant_id,'merchant_close_msg');
if(empty($close_msg)){
	$close_msg=t("This restaurant is closed now. Please check the opening times.");
}*/
echo CHtml::hiddenField('merchant_close_msg',
isset($checkout['msg'])?$checkout['msg']:t("Sorry merchant is closed."));

echo CHtml::hiddenField('disabled_website_ordering',getOptionA('disabled_website_ordering'));
echo CHtml::hiddenField('web_session_id',session_id());

echo CHtml::hiddenField('merchant_map_latitude',$data['latitude']);
echo CHtml::hiddenField('merchant_map_longtitude',$data['lontitude']);
echo CHtml::hiddenField('restaurant_name',$data['restaurant_name']);




?>

<section class="slider">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <!--<ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>-->

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
    <?php
$logoo = FunctionsV3::getMerchantBanner($merchant_id);

//echo $logoo;
?>
      <div class="item item1 active" style="background: rgba(0,0,0,0) url(<?php echo $logoo; ?>) no-repeat scroll center / cover;">
   
    </div>

     
    </div>
  
  </div>
</section>



<section class="des-body">
  <div class="res-description">
  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <h1><?php echo $data['restaurant_name'];?></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <ul class="details">
        <?php if($data['location'] != 0){ ?><li><?php echo $data['location']; ?></li><?php } ?>
        <li>45 Votes</li>
        <li><div class="rating" id="dashboard">

<?php $ratings=Yii::app()->functions->getRatings($merchant_id);?>

<?php  FunctionsV3::displayrate($ratings['ratings']) ?>
         </div></li>

      </ul>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <ul class="details1">
        <li><i class="fa fa-phone" aria-hidden="true"></i><?php print $data['contact_phone'] ?></li>
        <li><a class="opening-hours" style="border:0 px, color:red," data-toggle="modal" data-target="#myModal"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;See Opening Hours</a></li>
        <li class="rating"><i class="fa fa-rupee"></i>&nbsp;<?php print $data['cost_for_two'] ?></li>
      </ul>
    </div>
  </div>
</div>

  
<?php
$ss=5;
 if ($_REQUEST['prebook']!=1) {?>

  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-xs-12">
        <!--  <div class="des-icons text-center">
            <div class="row">
              <div class="col-sm-3 col-lg-3 col-md-3 col-xs-3">
                <img src="images/bookmark.png">
                <p>Bookmark</p>
              </div>
              <div class="col-sm-3 col-lg-3 col-md-3 col-xs-3">
                <img src="images/star.png">
                <p>Add Review</p>
              </div>
              <div class="col-sm-3 col-lg-3 col-md-3 col-xs-3">
                <img src="images/comments.png">
                <p>Comments</p>
              </div>
              <div class="col-sm-3 col-lg-3 col-md-3 col-xs-3">
                <img src="images/compare.png">
                <p>Compare</p>
              </div>
            </div>
          </div>-->

        <div class="des-tabs">
          <div class="col-sm-12 col-xs-12 no-padding">
            <div class="panel panel-primary">
                <div class="panel-heading" id="menu-tabs">
                    <!-- Tabs -->
                    <ul class="menu-tabs">
                        <li class="active"><a href="#dashboard">Overview</a></li>
                        <!--  <li><a href="#menu">Order Online</a></li> -->
                        <li><a href="#menus">Menu</a></li>
                        <li><a href="#review">Reviews</a></li>
                        <li><a href="#images">Photos</a></li>
                        <!--<li><a data-target="#etaModal" data-toggle="modal">Pre-Order</a></li>-->
                    </ul>
                </div>
                <!-- ETA POPUP -->


<?php
                            $this->renderPartial('/front/etapopup',array(
                            'merchant_id'=>$merchant_id
                            )); ?>

              <!--  <div class="panel-body">
                    <div class="tab-content">-->
                    <div class="panel-body">
                        <div class="tab-pane active" id="overview">
                          <h4>Description</h4>
                          <p class="para1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero eos et accusam et justo duo dolores et ea rebum.</p>

                          <div class="row mar-top1">
                            <div class="col-sm-3 col-xs-3">
                              <p>Specifications</p>
                            </div>
                            <div class="col-sm-9 col-xs-9">
                              <ul>
                                <li><i class="fa fa-beer" aria-hidden="true"></i>Bar</li>
                                <li><i class="fa fa-wifi" aria-hidden="true"></i>WiFi</li>
                                <li><i class="fa fa-window-maximize" aria-hidden="true"></i>Live Screening</li>
                                <li><i class="fa fa-music" aria-hidden="true"></i>Live Music</li>
                              </ul>
                            </div>
                          </div>
                          <hr />

                          <div class="row">
                            <div class="col-sm-3 col-xs-3">
                              <p>Overview</p>
                            </div>
                            <div class="col-sm-4 col-xs-4">
                              <!--<p><span>Phone:</span> <?php print $data['contact_phone'] ?></p>
                              <span class="small">Table Booking Recommended</span>-->
                              <p class="mar-top5"><span>Cuisines :</span><?php echo FunctionsV3::displayCuisine($data['cuisine']);?></p>
                              <p class="mar-top5"><span>Cost :</span><?php //print_r($data);
                  echo "Rs. ".$data['cost_for_two'].' (approx.)'?></p>
                            </div>
                            <div class="col-sm-5 col-xs-5" id="menus">
                              <p><span>Opening Hours:</span>
                                    <a class="opening-hours" style="border:0 px, color:red," data-toggle="modal" data-target="#myModal">See Opening Hours</a></p>
                      <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" style="color: #fff;" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Opening Hours</h4>
                            </div>
                            <div class="modal-body">
                            <?php
                            $this->renderPartial('/front/merchant-hours',array(
                            'merchant_id'=>$merchant_id
                            )); ?>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>




                              <p class="small"><span>Happy Hours:</span>6PM - 8PM</p>
                              <p class="mar-top5"><span>Address:</span><?php echo $data['location'];?></p>
                              <p class="mar-top5"><span>Services :</span><?php echo FunctionsV3::displayHighlights($data['highlights']);?></p>
                              <p class="mar-top5"><span>Pint of Beer Cost :</span><?php //print_r($data);
                  echo "Rs. ".$data['pint_of_beer']; ?></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel-body">
                        <div class="tab-pane" id="menu">
                          <div class="row">
                            <div class="col-sm-12 col-xs-12" >
                              <h3>Menu</h3>
                            </div>
                            <div class="gallery">
                              <div class="col-sm-3 col-xs-6">
                                <img data-target="#fullImageModal" data-toggle="modal" src="/assets/images/images.jpeg">
                              </div>
                              <div class="col-sm-3 col-xs-6">
                                <img data-target="#fullImageModal" data-toggle="modal"  src="/assets/images/images1.jpeg">
                              </div>
                              <div class="col-sm-3 col-xs-6" id="images">
                                <img data-target="#fullImageModal" data-toggle="modal"  src="/assets/images/images2.jpeg">
                              </div>
                              <div class="col-sm-3 col-xs-6">
                                <img data-target="#fullImageModal" data-toggle="modal"  src="/assets/images/images3.png">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="panel-body">
                        <div class="tab-pane" id="pics">
                          <div class="row"><h3>Photo Gallery</h3></div>
                          <div class="gallery">
                            <div class="count"></div>
                            <div class="row">

                              <?php if ($photo_enabled):
              
                          $gallery=Yii::app()->functions->getOption("merchant_gallery",$merchant_id);
                          $gallery=!empty($gallery)?json_decode($gallery):false;
                          $this->renderPartial('/front/merchant-photos',array(
                            'merchant_id'=>$merchant_id,
                            'gallery'=>$gallery
                          )); 
                          //foreach ($gallery as $val):?>

                           <!--  <div class="col-sm-3 col-xs-3">
                                <img src="<?php echo uploadURL()."/".$val?>">
                              </div> -->
<?php
                          //endforeach;
                          // $this->renderPartial('/front/merchant-photos',array(
                          //   'merchant_id'=>$merchant_id,
                          //   'gallery'=>$gallery
                          // ));       
                          endif;?>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="panel-body">
                        <div class="tab-pane" id="reviews">
                          <div class="reviews">
                            <h3>Reviews & Ratings</h3>
                            
                            <?php

                            $client_id=Yii::app()->functions->getClientId(); 
                            // print_r($client_id);
                            // echo "null";
                            // print_r($merchant_id); 
                            $reviews=Yii::app()->functions->getReviewsList($merchant_id);                  
                            $reviewnum=Yii::app()->functions->getReviewsNum($merchant_id); 
                           // print_r($reviewnum[TOTAL]);  
                            // foreach ($reviewnum as $keynum) {
                            //   print_r($keynum[TOTAL]);
                            // }
                            ?>






<!--MERCHANT REVIEW-->
                <?php //if ($theme_reviews_tab==""):?>
                <div class="review-tab-content">                   
                <?php $this->renderPartial('/front/merchant-review',array(
                  'merchant_id'=>$merchant_id
                )); ?>          
                </div>
                <?php //endif;?>
                <!--END MERCHANT REVIEW-->



                            <div class="rating-num">
                            <?php $ratings=Yii::app()->functions->getRatings($merchant_id);?>


                              <p><?php echo $ratings['ratings']?></p>
                            </div>
                            <div class="rating">
                            <?php  FunctionsV3::displayrate($ratings['ratings']) ?>
                            
                            </div>
                            <div class="votes">
                            <?php foreach ($reviewnum as $keynum) {?>
                              
                             
                              <p><?php  print($keynum['TOTAL']); ?> Reviews</p>
                              <?php } ?>
                              <p><?php print $ratings['votes'] ?> Votes</p>
                            </div>


                           <?php foreach ($reviews as $key) { 
                            $client_info=Yii::app()->functions->getClientInfo($key['client_id']);

                            $date=Yii::app()->functions->time_elapsed_string($key['date_created']);

                          ?>
                            <div class="row">
                              <div class="media">
                                <div class="col-sm-2 col-xs-3">
                                  <div class="media-left center-block">
                                    <?php
 print ($client_info['avatar'])?"<img src='/upload/".$client_info["avatar"]."' class='media-object center-block'>": "<img src='/assets/images/user.png' class='media-object center-block'>"; ?>

                                    <p><?php print($client_info['first_name']." ".$client_info['last_name']);?></p>
                                    <p><?php echo $date; ?></p>
                                  </div>

                                </div>
                                <div class="col-sm-9 col-xs-9">
                                  <div class="media-body">
                                    <h4 class="media-heading"><?php print($key['review']); ?></h4>

                                  </div>
   

                                </div>
                                <div class="col-sm-1">
                                  <span class="rate"><?php print($key['rating']); ?></span>
                                </div>
                              </div>
                            </div>
                            <?php } ?>
                            
                            
                            
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        </div>








 <?php if ($booking_enabled):?>
	  
	    <?php $this->renderPartial('/front/merchant-book-table',array(
	      'merchant_id'=>$merchant_id
	    )); ?>        
	  
	    <?php endif;?>




    </div>
  </div>
</section>

<?php  
}
else{
?>

  <section class="booking-cat">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-xs-12 sidebar-left">
          <div id="sidebar-left">
            <?php 
            $this->renderPartial('/front/menu-category',array(
            'merchant_id'=>$merchant_id,
            'menu'=>$menu			  
            ));
            ?>
          </div>
        </div>


        <div class="col-sm-6 col-xs-12">
          <div class="combos">
            <?php
            $this->renderPartial('/front/menu-merchant-1',array(
            'merchant_id'=>$merchant_id,
            'menu'=>$menu,
            'request'=>$_REQUEST,
            'disabled_addcart'=>$disabled_addcart
            ));
            ?>
          </div>
        </div>

        <div class="col-sm-3 hidden-xs sidebar-right">
          <div id="sidebar-right">
            <div class="delivery-time text-center">
              <p>Estimated Time of Arrival</p>
              <p>
                <input type="text" value="<?php echo ($_POST[delivery_date1])?$_POST[delivery_date1]:'' ?>" placeholder="ETA Date" name="delivery_date1" class="form-control" id="delivery_date">
              <!-- <input type="text" placeholder="ETA Time" value="<?php echo ($_POST[delivery_time])?$_POST[delivery_time]:'' ?>"  name="delivery_time"  class="form-control" id="delivery_time"> -->

                <select type="text" id="delivery_time" class="form-control" name="delivery_time" placeholder="ETA" style="width: 50%;position: relative;top: 10px;">
                  <option <?php echo ($_POST[delivery_time]=='00:00')?'selected':'' ?> value="00:00">12:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='00:30')?'selected':'' ?> value="00:30">12:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='01:00')?'selected':'' ?> value="01:00">01:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='01:30')?'selected':'' ?> value="01:30">01:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='02:00')?'selected':'' ?> value="02:00">02:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='02:30')?'selected':'' ?> value="02:30">02:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='03:00')?'selected':'' ?> value="03:00">03:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='03:30')?'selected':'' ?> value="03:30">03:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='04:00')?'selected':'' ?> value="04:00">04:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='04:30')?'selected':'' ?> value="04:30">04:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='05:00')?'selected':'' ?> value="05:00">05:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='05:30')?'selected':'' ?> value="05:30">05:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='06:00')?'selected':'' ?> value="06:00">06:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='06:30')?'selected':'' ?> value="06:30">06:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='07:00')?'selected':'' ?> value="07:00">07:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='07:30')?'selected':'' ?> value="07:30">07:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='08:00')?'selected':'' ?> value="08:00">08:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='08:30')?'selected':'' ?> value="08:30">08:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='09:00')?'selected':'' ?> value="09:00">09:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='09:30')?'selected':'' ?> value="09:30">09:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='10:00')?'selected':'' ?> value="10:00">10:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='10:30')?'selected':'' ?> value="10:30">10:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='11:00')?'selected':'' ?> value="11:00">11:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='11:30')?'selected':'' ?> value="11:30">11:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='12:00')?'selected':'' ?> value="12:00">12:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='12:30')?'selected':'' ?> value="12:30">12:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='13:00')?'selected':'' ?> value="13:00">01:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='13:30')?'selected':'' ?> value="13:30">01:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='14:00')?'selected':'' ?> value="14:00">02:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='14:30')?'selected':'' ?> value="14:30">02:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='15:00')?'selected':'' ?> value="15:00">03:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='15:30')?'selected':'' ?> value="15:30">03:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='16:00')?'selected':'' ?> value="16:00">04:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='16:30')?'selected':'' ?> value="16:30">04:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='17:00')?'selected':'' ?> value="17:00">05:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='17:30')?'selected':'' ?> value="17:30">05:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='18:00')?'selected':'' ?> value="18:00">06:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='18:30')?'selected':'' ?> value="18:30">06:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='19:00')?'selected':'' ?> value="19:00">07:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='19:30')?'selected':'' ?> value="19:30">07:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='20:00')?'selected':'' ?> value="20:00">08:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='20:30')?'selected':'' ?> value="20:30">08:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='21:00')?'selected':'' ?> value="21:00">09:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='21:30')?'selected':'' ?> value="21:30">09:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='22:00')?'selected':'' ?> value="22:00">10:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='22:30')?'selected':'' ?> value="22:30">10:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='23:00')?'selected':'' ?> value="23:00">11:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='23:30')?'selected':'' ?> value="23:30">11:30 PM</option>
                </select>
              </p>
              <p class="text-center"><button class="btn btn-block btn-hover" type="button">Submit</button></p>
            </div>

            <div class="cart">
              <div class="row">
                <div class="col-sm-12 col-xs-12">
                  <h4>Your Cart</h4>
                </div>
                <div class="item-order-wrap"></div>
                <a href="javascript:;" class="btn btn-success form-control checkout-btn checkout">Checkout<?php //echo $checkout['button']?></a>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>


  <div class="fixed-book visible-xs">
    <ul>
      <li id="eta-link"><a>ETA</a></li>
      <li id="cart-link"><a>Your Cart</a></li>
    </ul>
  </div>


  <div class="eta-tab visible-xs">
      <div class="row">
        <div class="col-xs-12 text-center book-header">
          <h4>Estimated Time of Arrival</h4>
          <a id="eta-close"><i class="fa fa-close"></i></a>
        </div>

         <div class="col-xs-10 col-xs-offset-1">

           <div class="delivery-time text-center">
              <p>
                <input type="text" value="<?php echo ($_POST[delivery_date1])?$_POST[delivery_date1]:'' ?>" placeholder="ETA Date" name="delivery_date1" class="form-control" id="delivery_date1">
              <!-- <input type="text" placeholder="ETA Time" value="<?php echo ($_POST[delivery_time])?$_POST[delivery_time]:'' ?>"  name="delivery_time"  class="form-control" id="delivery_time"> -->
                </p>

                <p>
                <select type="text" id="delivery_time" class="form-control" name="delivery_time" placeholder="ETA" style="width: 50%;position: relative;top: 10px;">
                  <option <?php echo ($_POST[delivery_time]=='00:00')?'selected':'' ?> value="00:00">12:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='00:30')?'selected':'' ?> value="00:30">12:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='01:00')?'selected':'' ?> value="01:00">01:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='01:30')?'selected':'' ?> value="01:30">01:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='02:00')?'selected':'' ?> value="02:00">02:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='02:30')?'selected':'' ?> value="02:30">02:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='03:00')?'selected':'' ?> value="03:00">03:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='03:30')?'selected':'' ?> value="03:30">03:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='04:00')?'selected':'' ?> value="04:00">04:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='04:30')?'selected':'' ?> value="04:30">04:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='05:00')?'selected':'' ?> value="05:00">05:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='05:30')?'selected':'' ?> value="05:30">05:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='06:00')?'selected':'' ?> value="06:00">06:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='06:30')?'selected':'' ?> value="06:30">06:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='07:00')?'selected':'' ?> value="07:00">07:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='07:30')?'selected':'' ?> value="07:30">07:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='08:00')?'selected':'' ?> value="08:00">08:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='08:30')?'selected':'' ?> value="08:30">08:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='09:00')?'selected':'' ?> value="09:00">09:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='09:30')?'selected':'' ?> value="09:30">09:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='10:00')?'selected':'' ?> value="10:00">10:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='10:30')?'selected':'' ?> value="10:30">10:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='11:00')?'selected':'' ?> value="11:00">11:00 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='11:30')?'selected':'' ?> value="11:30">11:30 AM</option>
                  <option <?php echo ($_POST[delivery_time]=='12:00')?'selected':'' ?> value="12:00">12:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='12:30')?'selected':'' ?> value="12:30">12:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='13:00')?'selected':'' ?> value="13:00">01:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='13:30')?'selected':'' ?> value="13:30">01:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='14:00')?'selected':'' ?> value="14:00">02:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='14:30')?'selected':'' ?> value="14:30">02:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='15:00')?'selected':'' ?> value="15:00">03:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='15:30')?'selected':'' ?> value="15:30">03:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='16:00')?'selected':'' ?> value="16:00">04:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='16:30')?'selected':'' ?> value="16:30">04:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='17:00')?'selected':'' ?> value="17:00">05:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='17:30')?'selected':'' ?> value="17:30">05:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='18:00')?'selected':'' ?> value="18:00">06:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='18:30')?'selected':'' ?> value="18:30">06:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='19:00')?'selected':'' ?> value="19:00">07:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='19:30')?'selected':'' ?> value="19:30">07:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='20:00')?'selected':'' ?> value="20:00">08:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='20:30')?'selected':'' ?> value="20:30">08:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='21:00')?'selected':'' ?> value="21:00">09:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='21:30')?'selected':'' ?> value="21:30">09:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='22:00')?'selected':'' ?> value="22:00">10:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='22:30')?'selected':'' ?> value="22:30">10:30 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='23:00')?'selected':'' ?> value="23:00">11:00 PM</option>
                  <option <?php echo ($_POST[delivery_time]=='23:30')?'selected':'' ?> value="23:30">11:30 PM</option>
                </select>
              </p>
              <p class="text-center"><button class="btn btn-block btn-hover" type="button">Submit</button></p>
            </div>
          </div>
      </div>
  </div>


  <div class="cart-tab visible-xs">
      <div class="row">
        <div class="col-xs-12 text-center book-header">
          <h4>Your Cart</h4>
          <a id="cart-close"><i class="fa fa-close"></i></a>
        </div>

         <div class="col-xs-10 col-xs-offset-1">
            <div class="cart">
              <div class="row">
                <div class="fixed">

                  <div class="col-sm-12 col-xs-12">
                    <h4>Your Cart</h4>
                  </div>
                  <div class="item-order-wrap"></div>

                </div>
                <a href="javascript:;" class="btn btn-success form-control checkout-btn checkout">Checkout<?php //echo $checkout['button']?></a>
              </div>
            </div>
          </div>
      </div>
  </div>





<?php } ?>
<?php
  if($mid=FunctionsV3::getMerchantIDbyDeals('FLD'))
  {
    $data['result']= $mid;
    $this->renderPartial('/front/flash_deals', $data);
  }
  ?>
<!--
  <script>

  $(document).ready(function(){
    $(".booking-sidebar").stick_in_parent();
    $("#menu-tabs").stick_in_parent();
  })

  </script>
-->

<script>

  $(document).ready(function() {

    if($(window).width() > 1300){
      $.stickysidebarscroll("#menu-tabs",{offset: {top: 56, bottom: 400}});
      $.stickysidebarscroll("#booking_sidebar",{offset: {top: 29, bottom: 400}});
      $.stickysidebarscroll("#sidebar-right",{offset: {top: 50, bottom: 400}});
      $.stickysidebarscroll("#sidebar-left",{offset: {top: 50, bottom: 400}});
    }

    else if($(window).width() > 1020){
      $.stickysidebarscroll("#menu-tabs",{offset: {top: 50, bottom: 400}});
      $.stickysidebarscroll("#booking_sidebar",{offset: {top: 25, bottom: 400}});
      $.stickysidebarscroll("#sidebar-right",{offset: {top: 50, bottom: 400}});
      $.stickysidebarscroll("#sidebar-left",{offset: {top: 50, bottom: 400}});
    }

    else if($(window).width() >= 768 ){
      $.stickysidebarscroll("#booking_sidebar",{offset: {top: 25, bottom: 400}});
      $.stickysidebarscroll("#menu-tabs",{offset: {top: 39, bottom: 400}});
      $.stickysidebarscroll("#sidebar-right",{offset: {top: 50, bottom: 400}});
      //$.stickysidebarscroll("#calendar",{offset: {top: 180, bottom: 370}});
      $.stickysidebarscroll("#sidebar-left",{offset: {top: 50, bottom: 400}});
    }

     else if($(window).width() > 500 && $(window).width() < 767 ){
      $.stickysidebarscroll("#menu-tabs",{offset: {top: 55, bottom: 400}});
      //$.stickysidebarscroll("#calendar",{offset: {top: 180, bottom: 370}});
    }

    else if($(window).width() < 500 ){
      $.stickysidebarscroll("#menu-tabs",{offset: {top: 45, bottom: 400}});
    }
  });


  $(window).load(function(){
      $('body').addClass('description-page');

    if($( "#pics img" ).size() != 0) {
      $('#pics img:last-child').attr('id', 'review');
    }
    else{
      $('#pics').parent().attr('id' , 'review' );
    }
  });

</script>

