<?php
#print_r($data);

?>

<section class="pre-book-review">
  <div class="container">
    <div class="row">

      <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 review-block">
        <h3>Hey! You are Almost Done.</h3>
        <p>Please make sure your information is correct before submitting the request.</p>
        <h4>You Just Selected</h4>
        <div class="width33">
          <p class="mar-top5">Restaurant Name</p>
          <h4><?php echo $_POST['merchant-name'];?></h4>
          <p><?php echo $_POST['location'];?></p>
        </div>
        <div class="width33">
          <p>Arrival Details</p>
          <h4><?php echo $_POST['date_booking1']. " | ". $_POST['booking_time'];?></h4>
        </div>
        <div class="width33">
          <p>Guests</p>
          <h4><?php echo "M ".$_POST['n_males']." | F ".$_POST['n_females'];?></h4>
        </div>

        <p class="mar-top2">Offer</p>
        <h4>50% Off On Johnny Walker</h4>
        <hr />

        <h4>Terms & Conditions</h4>
        <ul>
          <li>Lorem Ipsome Is simply dummy Text.</li>
          <li>Lorem Ipsome Is simply dummy Text.</li>
          <li>Lorem Ipsome Is simply dummy Text.Lorem Ipsome Is simply dummy Text.</li>
          <li>Lorem Ipsome Is simply dummy Text.</li>
        </ul>
        <hr />
        <p><a href="pre-book-thankyou.php"><button class="btn-hover">Confirm Booking</button></a></p>
      </div>

    </div>
  </div>
</section>
