<?php
$this->renderPartial('/front/banner-receipt',array(
   'h1'=>'Privacy Policy'
));
?>

<div class="sections section-grey2 section-custom-page" id="section-custom-page">
	<h2 class="text-center" style="color: #fff;font-size: 20px;margin-bottom: 20px;">Privacy Policy and Use of Personal Information/Profile data</h2> 
	<div class="container">
		<ol class="tclist">
			<li>
				FlipQ App is designed to help you find the best of restaurants, nightlife, events food and beverages around you and your city. In order to do this, FlipQ App requires the use of such information including, but not limited to, your email, location, food preferences and or other information that was used while registering with the FlipQ App.
			</li><br>
			
			<li>
				Whenever you open and interact with FlipQ App, we may use the location information from your mobile device or browser (latitude and longitude) to customize the experience around your location (i.e., showing you recommended places to eat, club etc. Your location information is never shared with others. FlipQ App may allow you to select a locality of your choice or let the app determine your current location. It may allow us to tell you where the recommendations are based on the location that is determined by the device or your selection.
			</li><br>
			
			<li>
				We may use your personal information to provide the services you request. However, to the extent we use your personal information to market to you, we will also provide you the ability to opt-out of such uses. The personal information shall be used by us to resolve disputes; troubleshoot problems; help/promote a safe service; measure consumer interest in our products and services, inform you about online and offline offers, products, services, and updates; customize your experience; detect and protect us against error, fraud and other criminal activity; enforce our terms and conditions; and as otherwise described to you at the time of collection. In our efforts to continually improve our products and services, we collect and analyze demographic and profile data about our users' activity on our electronic application. We may occasionally ask you to engage in factual or interest based surveys. These surveys may ask you for more information around your interest and demographic information (like preferred cuisines etc.). The data so collected through surveys is solely to enhance your experience in the usage of our  applications, providing you with content that we think you might be interested in, and to display content according to your preferences. It is however clarified that, filling in of such surveys is optional.
			</li><br>
			
			<li>
				Only authorised employees within the Company will have access to User’s details and information & are prohibited from using this information for any personal or commercial use. User records are regarded as confidential and will not be divulged to any third party, other than for the purposes detailed above. We will not sell or rent your personal information to any third party.
			</li><br>
			
			<li>
				The Company reserves the right to disclose any information in response to / that it is required to be shared, disclosed or make made available to any governmental, administrative, regulatory or judicial authority under any law or regulation applicable to the Company. Further, the Company can and you authorize the Company to disclose your name, street address, city, state, zip code, country, phone number, email, and company name to Intellectual Property right's owners, as we in our sole discretion believe necessary or appropriate in connection with an investigation of fraud, intellectual property infringement, piracy, or other unlawful activity.
			</li><br>
			
			<li>
				FlipQ App uses Facebook, Google+ platforms and direct registration as medium for registrations and signups.  Kindly note that the manner in which Facebook and Google+ uses, stores and discloses your information is governed solely by its policies, and the Company bears no liabilities/responsibility for its privacy practices and/or other actions of any third party site or service that may be enabled within the FlipQ App.
			</li><br>
			
			<li>
				In the event, the Company goes through a business transition or transfer, such as merger or acquisition by another company or sale of portion of its assets or in the event that the Company goes out of business, the Users acknowledge the fact that their personal information may form part of the assets transferred and that any acquirer of the Company or its assets may continue to use such personal information as set forth in this privacy policy.
			</li><br>

		</ol>
	</div>
</div>	