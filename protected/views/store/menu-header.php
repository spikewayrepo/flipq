<!--<div class="mobile-banner-wrap relative">
 <div class="layer"></div>
 <img class="mobile-banner" src="<?php echo empty($background)?assetsURL()."/images/b-2-mobile.jpg":uploadURL()."/$background"; ?>">
</div>-->
<div id="parallax-wrap" class="parallax-search parallax-menu" 
data-parallax="scroll" data-position="top" data-bleed="10" 
data-image-src="<?php echo empty($background)?assetsURL()."/images/b-2.jpg":uploadURL()."/$background"; ?>">

<div class="search-wraps menu-header">
		<?php
		 //print_r($data); ?>
	<div class="row" style="width: 99.7vw; background: rgba(0, 0, 0, 0.8);">	 
		<div class="col-sm-8 col-md-8 col-xs-12">
			<h1 style="display: inline;"><?php echo clearString($restaurant_name)?></h1>
			
			<?php if(FunctionsV3::checkDJ($_SESSION['kr_merchant_id'])){ ?>
					<span>
						<?php if(!Yii::app()->functions->getFollowInfo()) { ?>
							<a href="javascript:;" id="follow" data-id="<?php print $_SESSION['kr_client']['client_id'] ?>" class="label label-success follow">
							<?php echo t("Follow")?>
							</a>
					<?php } else { ?>
							<a href="javascript:;" id="follow" data-id="<?php print $_SESSION['kr_client']['client_id'] ?>" class=" label label-success follow">
								<?php echo t("Unfollow")?>
							</a>
					<?php } ?>
					</span>
			<?php } else { ?>
					<span>
					<?php echo FunctionsV3::merchantOpenTag($merchant_id)?>             
					</span>
			<?php } ?>
			
			
			
			<?php if(FunctionsV3::checkDJ($_SESSION['kr_merchant_id'])){ ?>
				
				  <p class="small hidden-xs" style="font-weight: bold; margin-left:1%"><?php echo FunctionsV3::displayMusicType($music_name);?></p>
				  <!--<img class="logo-medium bottom15" src="<?php //echo $merchant_logo;?>">-->
				
		 
			<?php } else { ?>

					
					  <p><i class="fa fa-map-marker"></i> <?php echo $merchant_address?></p>
					
			
			<?php } ?>
		</div>
		
		
			<div class="mycol col-sm-4 col-md-4 col-xs-8 text-right" style="margin-top: 2%;">
				<div class="rating-stars" data-score="<?php echo $ratings['ratings']?>"></div>
				<p class="small">User Ratings</p>
				<p class="small">
				<?php	if($ratings['votes'])
				{ ?>
					<a href="javascript:;"class="goto-reviews-tab">
					<?php echo $ratings['votes']." ".t("Reviews")?>
					</a>
		  <?php } ?>
				</p>
				
			</div>	
		
		<?php if(FunctionsV3::checkDJ($_SESSION['kr_merchant_id'])){} 
		else { ?>		
			<div class="col-sm-8 col-md-8 hidden-xs text-left">
			  <p class="small"><?php echo FunctionsV3::displayCuisine($cuisine);?></p>
			  <!--<img class="logo-medium bottom15" src="<?php //echo $merchant_logo;?>">-->
			</div>
		<?php } ?>
		
	     <!--<div class="mycol">
	        <div class="rating-stars" data-score="<?php echo $ratings['ratings']?>"></div>   
	     </div>
	     <div class="mycol">
	        <p class="small">
	        <a href="javascript:;"class="goto-reviews-tab">
	        <?php echo $ratings['votes']." ".t("Reviews")?>
	        </a>
	        </p>
	     </div>-->
			
		<!--<div class="mytable">-->
			 <!--<div class="mycol">
				<?php echo FunctionsV3::merchantOpenTag($merchant_id)?>             
			 </div>-->
			 <!--<div class="mycol">
				<p class="small"><?php //echo t("Minimum Order").": ".FunctionsV3::prettyPrice($minimum_order)?></p>
			 </div>-->
		<!--</div> <!--mytable-->

	<!--<h1><?php echo clearString($restaurant_name)?></h1>-->
	<!--<p><?php echo FunctionsV3::getFreeDeliveryTag($merchant_id)?></p>-->
	
		<?php if ( getOption($merchant_id,'merchant_show_time')=="yes"):?>
		<p class="small">
		<?php echo t("Merchant Current Date/Time").": ".
		Yii::app()->functions->translateDate(date('F d l')."@".timeFormat(date('Y-m-d h:i:s'),true));?>
		</p>
		<?php endif;?>
		
		<?php if (!empty($merchant_website)):?>
		<p class="small">
		<?php echo t("Website").": "?>
		<a target="_blank" href="<?php echo FunctionsV3::fixedLink($merchant_website)?>">
		  <?php echo $merchant_website;?>
		</a>
		</p>
		<?php endif;?>
	</div>		
</div> <!--search-wraps-->

</div> <!--parallax-container-->
