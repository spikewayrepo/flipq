<?php
$this->renderPartial('/front/banner-receipt',array(
   'h1'=>t("SignUp"),
   'sub_text'=>t("step 2 of 4")
));

/*PROGRESS ORDER BAR*/
#$this->renderPartial('/front/progress-merchantsignup',array(
#   'step'=>2,
#   'show_bar'=>true
#));

?>


<?php 

if($_REQUEST['btype']=='dj')
  $btype = "DJ";
else
  $btype = "Restaurant";
?>

<div class="sections section-grey2">

  <div class="container">
  
  <div class="row">  
  <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 ">
    <div class="box-grey round top-line-green whit-text">
                  
       <!-- <form class="forms" id="forms" onsubmit="return false;"> -->
       <div class="row">
      	<div class="col-sm-12 col-md-12">
      		<h3>General Information</h3>
      	</div>
      </div>

      <hr class="thick">  

       <form class="forms" id="formsreg">
	  <?php echo CHtml::hiddenField('action','merchantSignUp2')?>
	  <?php echo CHtml::hiddenField('currentController','store')?>	 
      


      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo $btype . t(" name")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
             <?php echo CHtml::textField('restaurant_name',
			  isset($data['restaurant_name'])?$data['restaurant_name']:""
			  ,array(
			  'class'=>'grey-fields full-width',
			  'placeholder'=>'Restaurant Name',
			  'data-validation'=>"required"
			  ))?>
        </div>
      </div>
      <?php if($btype=='Restaurant'){ ?>
		<div class="row top10">
			<div class="col-md-3 col-sm-4 col-xs-12 top10 "><?php echo t("Restaurant Type")?></div>
			<div class="col-md-9 col-sm-8 col-xs-12">
				<select name="restaurant_type" placeholder="Restaurant Type" class="grey-fields full-width" aria-controls="table_list">   
				  <option value="pub/bar">Pub/Bar</option>
				  <option value="cafe">Cafe</option>
				  <option value="lounge">Lounge</option>
				  <option value="sports_bar">Sports Bar</option>
				  <option value="casual_dining">Casual Dining</option>
				  <option value="fine_dining">Fine Dining</option>
				  <option value="microbrewery_pub">Microbrewery Pub</option>
				</select>
			</div>
		</div>

      <?php }
		else if($btype='dj')
		{ ?>
		<div class="row top10">
		  <div class="col-md-3 col-sm-4 col-xs-12 top10 "><?php echo t("Music Type")?></div>
		  <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php $list=Yii::app()->functions->MusicType(true); ?>
            <select name="restaurant_type" class="grey-fields full-width" aria-controls="table_list">
			<option value="">Select</option>
			 <?php foreach($list as $res){?>
			 <?php if($data['rtype']==$res){ ?>
			 <option  selected value="<?php echo $res;?>"><?php echo $res;?></option>
			 <?php }else{?>
			 <option value="<?php echo $res;?>"><?php echo $res;?></option>
			 <?php } ?>
			 <?php } ?>
            </select>
          </div>
		</div>	
	<?php } ?>


    <?php if ( getOptionA('merchant_reg_abn')=="yes"):?>
     <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("ABN")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
              <?php echo CHtml::textField('abn',
			  isset($data['restaurant_name'])?$data['abn']:""
			  ,array(
			  'class'=>'grey-fields full-width',
			  'placeholder'=>'ABN',
			  'data-validation'=>"required"
			  ))?>
        </div>
      </div>
     <?php endif;?>      
      
    <input type="hidden" name="btype" value="<?php print $btype  ?>" />


     <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo $btype . t(" phone")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
         <?php echo CHtml::textField('restaurant_phone',
		  isset($data['restaurant_phone'])?$data['restaurant_phone']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Restaurant Phone'
		  ))?>    
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Contact Name")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::textField('contact_name',
		  isset($data['contact_name'])?$data['contact_name']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Contact Name',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10" style="display:none;">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Contact Phone")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::textField('contact_phone',
		  isset($data['contact_phone'])?$data['contact_phone']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Contact Phone',
		  'data-validation'=>""
		  ))?>           
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Contact Email")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::textField('contact_email',
		  isset($data['contact_email'])?$data['contact_email']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Contact Email',
		  'data-validation'=>"email"
		  ))?>           
        </div>
      </div> 
      
      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Street Address")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::textField('street',
		  isset($data['street'])?$data['street']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Street Address',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("City")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::textField('city',
		  isset($data['city'])?$data['city']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'City',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Post code/Zip code")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::textField('post_code',
		  isset($data['post_code'])?$data['post_code']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Post code/Zip code',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10" style="display:none">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Country")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::dropDownList('country_code',
		  getOptionA('merchant_default_country'),
		  (array)Yii::app()->functions->CountryListMerchant(),          
		  array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Country',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("State/Region")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::textField('state',
		  isset($data['state'])?$data['state']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'State/Region',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10" style="display:none">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Cuisine")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::dropDownList('cuisine[]',
		  isset($data['cuisine'])?(array)json_decode($data['cuisine']):"",
		  (array)Yii::app()->functions->Cuisine(true),          
		  array(
		  'class'=>'full-width chosen',
		  'placeholder'=>'Cousine',
		  'multiple'=>true,
		  'data-validation'=>""  
		  ))?>           
        </div>
      </div>
      
      <div class="row top10" style="display:none">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Services Pick Up or Delivery?")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::dropDownList('service',
		  isset($data['service'])?$data['service']:"",
		  (array)Yii::app()->functions->Services(),          
		  array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Services Pick Up or Delivery?',
		  'data-validation'=>""
		  ))?>           
        </div>
      </div>
      
      <div class="row top15">
      	<div class="col-sm-12 col-md-12">
      		<h3>Login Information</h3>
      	</div>
      </div>

      <hr />
      
      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Username")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		<?php echo CHtml::textField('username',
		  ''
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Username',
		  'data-validation'=>"required"
		  ))?>
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Password")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::passwordField('password',
		  ''
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Password',
		  'data-validation'=>"required"
		  )) ?>           
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"><?php echo t("Confirm Password")?></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
		  <?php echo CHtml::passwordField('cpassword',
		  ''
		  ,array(
		  'class'=>'grey-fields full-width',
		  'placeholder'=>'Confirm Password',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <?php if ( $terms_merchant=="yes"):?>
      <?php $terms_link=Yii::app()->functions->prettyLink($terms_merchant_url);?>
      <div class="row top10">
        <div class="col-md-3 col-sm-4 col-xs-12 top10"></div>
        <div class="col-md-9 col-sm-8 col-xs-12">
          <?php 
		  echo CHtml::checkBox('terms_n_condition',false,array(
		   'value'=>2,
		   'class'=>"",
		   'data-validation'=>"required"
		  ));
		  echo " ". t("I Agree To")." <a href=\"$terms_link\" target=\"_blank\">".t("The Terms & Conditions")."</a>";
		  ?>  
        </div>
      </div>
      <?php endif;?>
      
     
      <?php if ($kapcha_enabled==2):?>      
      <div class="top10 capcha-wrapper">        
        <div id="kapcha-1"></div>
      </div>
      <?php endif;?>
      
      <div class="row top10">
        <div class="col-md-12 col-xs-12 text-center">
          <input type="submit" value="<?php echo t("Submit")?>" class="btn-hover inline medium">
        </div>
      </div>
      
      </form>
                   
    </div> <!--box-grey-->
    
   </div> <!--col-->
    
   </div> <!--row--> 
  </div> <!--container-->  
</div> <!--sections-->
