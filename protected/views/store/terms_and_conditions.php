<?php
$this->renderPartial('/front/banner-receipt',array(
   'h1'=>'Terms & Conditions'
));
?>

<div class="sections section-grey2 section-custom-page" id="section-custom-page">
	<h1 class="text-center" style="color: #fff">USER AGREEMENT</h1> 
	<div class="container">
		<p style="color: #fff;">These Terms and Conditions (hereinafter referred to as the “Terms”) constitute a legally binding agreement between you and FlipQ Digital Private Limited (hereinafter referred to as the “FDPL” or the “Company”) regarding your use of its website <a href="http://www.flipq.in" style="color: blue;">http://www.flipq.in</a> mobile or electronic applications, call centers or any other service or platform offered by the Company via any website, any mobile or internet connected device or otherwise (hereinafter both collectively and individually referred to as the “FlipQ App”). By accessing the FlipQ App, you agree to have accepted all of the terms, conditions and notices contained in these Terms/User Agreement.</p> 
		<br>
		
		<strong style="color: #fff; font-size: 18px;">USER AGREEMENT </strong>
		<br>
		
		<p style="color: #fff">“User”, “You”, “Your” and “Yours” are references to the person who access the FlipQ App or avails the service offered by the Company for any purpose including but not limited to:</p>
		
		<ol class="tclist">
			<li>
			Carrying out table reservations, group bookings, availing deals, discounts, offers and any other service at a Fine dining Restaurant, Casual dining Restaurant, Pub, Café, Lounge, Bar or any other third party merchant establishment by whatever name called, which is listed or being hosted on the FlipQ App (hereinafter referred to as the <strong>“Participating Restaurant”</strong>).
			</li>
			<br>
			<li>
				Ordering or Pre ordering food, beverages or any other service at a Participating Restaurant.
			</li>
			<br>
			<li>
				Viewing, hosting, publishing, sharing, transacting, displaying or uploading information, views, videos, pictures, links, creative profiles, schedules.
			</li>
			<br>
			<li>
				It is understood that a performing artist like Disc Jockey, Singer, Musician, Stand up comedian etc., accessing the FlipQ App for any purpose would also constitute a user for the purposes of these terms.
			</li>

		</ol>
		
		<ol class="decimallist">
			<li><strong><u>Acceptance of Terms</u></strong><br>
				<ol>
					<li>
						By accepting the Terms, you warrant that you have fully read and understood the terms and conditions without any impairment in judgment resulting from (but not limited to) mental illness, mental handicap, intoxication, medication, or any other health or other problem that could impair judgment. These Terms govern your usage of FlipQ App. If you disagree with any part of the Terms please do not use FlipQ App.
					</li><br>
					
					<li>
						By visiting, accessing or registering on FlipQ App, you unconditionally accept to be bound by the Terms. The Terms apply to and govern your access to and use of the FlipQ App owned by the Company. Access of the FlipQ App or the services offered by the Company in any manner, whether automated or otherwise, constitutes use of the FlipQ App and services of the Company and your unconditional consent to be bound by the Terms. You agree and confirm that the Terms may be amended from time to time without notice. Please ensure that you periodically update yourself of the current version of the Terms. 
					</li><br>
					
					<li>
						You agree, understand and acknowledge that FlipQ App is an online platform that enables the User to make reservations, avail deals, discount and offers, and order/pre-order food, beverages and other services at any Participating Restaurant listed or being hosted on the FlipQ App. You further agree and acknowledge that the Company is only a facilitator and shall not be liable in any manner for your dining or drinking experience regarding quality of service, food or beverages, as it would be provided by the Participating Restaurant and not by the Company. Accordingly, the transaction on FlipQ App to such extent shall be a strictly bipartite contract between you and the Participating Restaurant.

					</li><br>
					
					<li>
						The Company  reserves the right to suspend / cancel, or discontinue any or all platforms, products or service at any time without notice, make modifications and alterations in any or all of the content, products and services contained on the FlipQ App without prior notice.
					</li><br>
				
				</ol>
			</li>
			
			<li><strong><u>Intended Users</u></strong><br>
				<ol>
				
					<li>
						FlipQ App is intended for use by individuals 18 years of age or older. FlipQ App is not directed for use by individuals under the age of 18. Users under the age of 18 should get the assistance of a parent or guardian to use FlipQ App.
					</li><br>
					
					<li>
						With regards to any bookings made by the User with the Participating Restaurant involving the consumption of alcohol, the User must warrant that they are of legal drinking age in strict adherence to specific State laws and or the laws made by the Government of India.
					</li><br>
					
					<li>
						Users who can form legally binding contracts under the Indian Contract Act, 1872 and are willing to agree and abide by the Terms are entitled to use FlipQ App. Persons who are "incompetent to contract" as per Indian Laws, more particularly the Indian Contract Act, 1872 and not willing to agree or abide by the Terms are not eligible to use FlipQ App.
					</li><br>
				
				</ol>

			</li>
			
			<li><strong><u>Registration</u></strong><br>
			
				<ol>
				
					<li>
						Accessing certain areas of the FlipQ App and using certain functions or features of the FlipQ App may require the User to register. This registration may be for free or for a charge, depending upon the functions or features accessed on the FlipQ App. Company reserves the right to charge subscription and / or membership fees from a User, by giving reasonable prior notice, in respect of any product, service or any other aspect of the FlipQ App.	
					</li><br>
					
					<li>
						When you register, you may be required to provide details including but not limited to name, age, residential address, contact number etc. You must choose a password and provide a unique, valid, current and verifiable e-mail address and phone number. Duplicate e-mail addresses are not allowed, so if the name or address you enter is already in use, you will be prompted to choose another one. We will send you a confirmation e-mail with your registered information. In the event that delivery of such information fails for any reason, your access or use of areas, functions or features requiring such registration may be refused or terminated. FlipQ may also offer registration or signup process through other platforms such as Facebook and Google+. You will promptly update your registration to keep it accurate and current. You are solely responsible for maintaining the confidentiality of your password. You are also solely responsible for restricting access to your computer(s) or mobile devices. You agree to accept responsibility for all activities occurring under your account that are due to your conduct, inaction, or negligence.	
					</li><br>

					<li>
						If you become aware of any suspicious or unauthorized conduct concerning your account, you agree to contact us immediately by e-mail. We may, at our own discretion, bar registration from any specific e-mail service or Internet Service Provider.
					</li><br>
				
				</ol>
			
			</li>
			
			<li><strong><u>Reservations & Ordering</u></strong><br>
			
				<ol>
				
					<li>
						You agree to take particular care when providing us with your details and warrant that these details are accurate and complete at the time of availing deals, offers, making reservations or pre-ordering.
					</li><br>
					
					<li>
						Please note that some of the food and beverages may be suitable for certain age ranges only. You should check that the food and beverages that you are ordering is suitable for the intended consumer.
					</li><br>
					
					<li>
						We will take all reasonable care, in so far as it is in our power to do so, to keep the details of your order and payment secure, but in the absence of negligence on our part we cannot be held liable for any loss you may suffer if a third party procures unauthorized access to any data you provide when accessing or ordering via the FlipQ App.
					</li><br>
					
					<li>
						Any reservation, order or service that you book via the FlipQ App is subject to availability, acceptance and confirmation by the Participating Restaurant.						
					</li><br>
				
				</ol>
			
			</li>
			
			<li><strong><u>Prices and Payment</u></strong><br>
			
				<ol>
				
					<li>
						You warrant that the credit, debit card or other payment details that you provide are your own and that you have sufficient funds to make the payment.
					</li><br>
					
					<li>
						All prices listed on the FlipQ App for reservations, food, beverages other services are correct at the time of listing and have been input as received by the Participating Restaurant. In case the price listed is not current and the Participating Restaurant informs us immediately after placing the order, we will put our best effort to contact you to inform you about the price difference and you can choose to opt-out of the pre order at that time.
					</li><br>
					
					<li>
						All Statutory taxes, duties and fees, and any service charge, cover charge or any other similar charge by whatever name called, levied by the restaurant (hereinafter referred to as “Levies”) would be extra unless otherwise specifically included, and would be chargeable and collected directly by the participating restaurant.
					</li><br>
					
					<li>
						While we give great care to keep them up to date, the final price charged to you by the Participating Restaurant can change at the time of actual serving based on the latest menu and prices of the Participating Restaurant. Further, the final price to be charged by the Participating Restaurant may also change in case of request for any deviation from the booked orders made by the User at the Participating Restaurant including but not limited to added menu items, request for larger portions, special add on etc. The differential price shall be paid directly at the Participating Restaurant or via the FlipQ App where such facility is being provided.
					</li><br>
					
					<li>
						We reserve the right to alter the food, beverages or services available for pre order via the FlipQ App and to stop listing any restaurants or any services, at our discretion.
					</li><br>
					
					<li>
						The total price excluding Levies for the food, beverages or other services ordered/pre ordered via the FlipQ App will be displayed on the FlipQ App when you place your order. Full or partial payment, as may be provided, must be made for all food, beverages or services pre-ordered at the time of the booking. Payment has to be made via any mode provided on the FlipQ App. In case of partial payment, the balance differential payment and the applicable Levies will have to be made by the User at the time of consumption of order directly at the Participating Restaurant or via the FlipQ App. In case of full payment, the user would only be required to pay only the applicable Levies at the participating restaurant. The Company via the FlipQ App may also provide for direct full payment at the restaurant facility. Taxes, duties and fees, and any service charge, cover charge or any other similar charge levied by the restaurant by whatever name called would be charged and collected by the participating restaurant directly.
					</li><br>
					
					<li>
						To ensure that online commerce is secure, your debit/credit card/net banking/e-wallet or other online payment details will be encrypted to prevent the possibility of someone being able to read them as they are sent over the internet. Your credit card/ debit/online payment company or bank may also conduct security checks to confirm it is you making the payment.
					</li><br>
					
					<li>
						The prices reflected on the FlipQ App are determined solely by the Participating Restaurant and informed to Company at the time of listing or afterwards. Any change in the prices of menu at the time of placing order is at the sole discretion of the Participating Restaurant.
					</li><br>
					
					<li>
						All applicable Levies, the rates thereof and the manner of applicability of such Levies are being charged and determined by the Participating Restaurant and the Company is merely collecting the same on behalf of such Participating Restaurant.
					</li><br>
					
					<li>
						The entire amount of applicable Levies collected by the Company is directly remitted to the Participating Restaurants and the Company does not retain any amounts thereof.
					</li><br>
					
					<li>
						The Company is not responsible for validating the legal sanctity of the applicable Levies and the manner of its applicability on behalf of the Participating Restaurant. The Company holds no responsibility for the legal correctness/validity of the levy of such Levies. The sole responsibility for any legal issue arising on such Levies shall reside with the Participating Restaurant.
					</li><br>
					
					<li>
						The transaction of sale or service of food, beverages or any other service at the Participating Restaurant booked via the FlipQ App is between the Participating Restaurant and the User, and accordingly, the Company is not liable to charge or deposit any taxes on such transaction. However, if applicable in future, the Company reserves the right to charge such taxes from the User at the time of booking, pre-order and payment.
					</li><br>
					
					<li>
						The final tax invoice will be issued by the Participating Restaurant and delivered to the User at its establishment or electronically via the FlipQ App.
					</li><br>
				
				</ol>
			
			</li>
			
			<li><strong><u>Cancelations, Rescheduling and Refunds</u></strong><br>
			
				<ol>
				
					<li>
						It is the Participating Restaurants sole responsibility to supply food, beverages and services pre-ordered or booked via the FlipQ App at its establishment at the designated time of arrival i.e. the time which has been confirmed at the time of the booking. The Company shall not be liable for any delays caused by the Participating Restaurant in supply of such food, beverages or services.	
					</li><br>
					
					<li>
						In case we are unable to confirm any appointment, bookings or order made by the User for any reason, the advance amount paid by the User will be refunded back in the same mode of payment within 14 working days subject to the refund policies of the third party payment gateways where the same have been used.
					</li><br>
					
					<li>
						User may cancel the booking via the FlipQ App anytime at least 30 minutes in advance of the designated time of arrival for the booking and be typically entitled for a full refund of any advance payment made. However, in certain cases, User may be required to cancel the reservation in accordance with the Participating Restaurant’s cancellation policy, which will be disclosed at the time of booking and also shared with user in the confirmation email / SMS / in app message / call / booking status details in Users account on FlipQ App platforms. The Company shall have no liability for any charges made to the User’s account or their debit or credit card or internet banking or mobile wallet or any other mode of transaction, for any deductions made from the advance payment for any failure to cancel their booking in accordance with a Participating Restaurant’s cancellation policy.
					</li><br>
					
					<li>
						User may reschedule any bookings or reservations at the Participating Restaurant   anytime atleast 30 minutes prior to the original booking. This however would be subject to availability, confirmation and final discretion of the Participating Restaurant.
					</li><br>
					
					<li>
						No-Show Policy: If the User is unable to honor the booking and fails to cancel the same at least 30 minutes in advance of designated time of arrival, the Company will send an email / SMS, in App message or call the User, communicating to the User that our records indicate that the User has been a no-show. Having used FlipQ App for bookings, User agrees to receive a no-show notifications by email / SMS, in App message or call after a report that the reservation or booking was not honored, whether or not that was in fact the case. The above will also reflect in User’s account history and can be viewed by the User at any time. User’s account may be terminated if the User is a no-show for 4 bookings within a 12-month period. User agrees that all final no-show determinations will be made by the Company in its sole discretion.
					</li><br>
					
					<li>
						In case of no-show the User needs to claim refund from the Company within 2 days. The refund will be dependent on the discretion & policies of the Participating Restaurant. The Company will help in all commercially feasible ways to get the User their refund unless certain food & beverage, cocktails /mocktails, setting aside of materials or any other preparations were in the process or already made for the User to provide for a distinguished service level at the designated time of arrival of the User.
					</li><br>
					
					<li>
						In the unlikely event that the Participating Restaurant supplies a wrong item i.e. food, beverage or services other than the one you pre-ordered, you have the right to reject the same and you shall be entitled to full refund of any amounts paid by you at the time of the booking for  that item. If the Participating Restaurant can only do a partial supply (few items might not be available), its staff should inform you or propose a replacement for missing items. You have the right to refuse a partial order and get a refund. We are not responsible for wrong or partial supply. The issue has to be settled directly with the Participating Restaurant.
					</li><br>
					
					<li>
						All admissible refunds will be made in the same mode in which the payment was made within 14 working days subject to the refund policies of the third party payment gateways where the same have been used.
					</li><br>
					
				</ol>
			
			</li>
			
			<li><strong><u>Intellectual Property Rights</u></strong><br>
			
				<ol>
					
					<li>
						Unless otherwise stated, copyright and all intellectual property rights in all material presented on the FlipQ App (including but not limited to text, audio, video or graphical images), trademarks and logos appearing on the FlipQ App are the property of the Company, its parent, affiliates and associates and are protected under applicable Indian and International laws.
					</li><br>
					
					<li>
						Unless otherwise stated, copyright and all intellectual property rights in all material presented on the FlipQ App (including but not limited to text, audio, video or graphical images), trademarks and logos appearing on the FlipQ App are the property of the Company, its parent, affiliates and associates and are protected under applicable Indian and International laws.
					</li><br>
					
					<li>
						Any infringement shall be vigorously defended and pursued to the fullest extent permitted by law.
					</li><br>
					
				</ol>
			
			</li>
			
			<li><strong><u>Privacy Policy and Use of Personal Information/Profile data</u></strong><br>
			
				<ol>
					
					<li>
						FlipQ App is designed to help you find the best of restaurants, nightlife, events food and beverages around you and your city. In order to do this, FlipQ App requires the use of such information including, but not limited to, your email, location, food preferences and or other information that was used while registering with the FlipQ App. 
					</li><br>
					
					<li>
						Whenever you open and interact with FlipQ App, we may use the location information from your mobile device or browser (latitude and longitude) to customize the experience around your location (i.e., showing you recommended places to eat, club etc. Your location information is never shared with others. FlipQ App may allow you to select a locality of your choice or let the app determine your current location. It may allow us to tell you where the recommendations are based on the location that is determined by the device or your selection.
					</li><br>
					
					<li>
						We may use your personal information to provide the services you request. However, to the extent we use your personal information to market to you, we will also provide you the ability to opt-out of such uses. The personal information shall be used by us to resolve disputes; troubleshoot problems; help/promote a safe service; measure consumer interest in our products and services, inform you about online and offline offers, products, services, and updates; customize your experience; detect and protect us against error, fraud and other criminal activity; enforce our terms and conditions; and as otherwise described to you at the time of collection. In our efforts to continually improve our products and services, we collect and analyze demographic and profile data about our users' activity on our electronic application. We may occasionally ask you to engage in factual or interest based surveys. These surveys may ask you for more information around your interest and demographic information (like preferred cuisines etc.). The data so collected through surveys is solely to enhance your experience in the usage of our  applications, providing you with content that we think you might be interested in, and to display content according to your preferences. It is however clarified that, filling in of such surveys is optional.
					</li><br>
					
					<li>
						Only authorised employees within the Company will have access to User’s details and information & are prohibited from using this information for any personal or commercial use. User records are regarded as confidential and will not be divulged to any third party, other than for the purposes detailed above. We will not sell or rent your personal information to any third party.
					</li><br>
					
					<li>
						The Company reserves the right to disclose any information in response to / that it is required to be shared, disclosed or make made available to any governmental, administrative, regulatory or judicial authority under any law or regulation applicable to the Company. Further, the Company can and you authorize the Company to disclose your name, street address, city, state, zip code, country, phone number, email, and company name to Intellectual Property right's owners, as we in our sole discretion believe necessary or appropriate in connection with an investigation of fraud, intellectual property infringement, piracy, or other unlawful activity.
					</li><br>		
					
					<li>
						FlipQ App uses Facebook, Google+ platforms and direct registration as medium for registrations and signups.  Kindly note that the manner in which Facebook and Google+ uses, stores and discloses your information is governed solely by its policies, and the Company bears no liabilities/responsibility for its privacy practices and/or other actions of any third party site or service that may be enabled within the FlipQ App. 
					</li><br>		
				
					<li>
						In the event, the Company goes through a business transition or transfer, such as merger or acquisition by another company or sale of portion of its assets or in the event that the Company goes out of business, the Users acknowledge the fact that their personal information may form part of the assets transferred and that any acquirer of the Company or its assets may continue to use such personal information as set forth in this privacy policy. 
					</li><br>		
				
				</ol>
			
			</li>
			
			<li><strong><u>Restricted Use and Prohibitions</u></strong><br>
			
				<ol>
					
					<li><strong><u>Limited Permission to Copy</u></strong>
						
						<ol>
						
							<li>
								The Company grants you permission to only access and make personal use of the FlipQ App and you agree not to, directly or indirectly download or modify / alter / change / amend / vary / transform / revise / translate / copy / publish / distribute or otherwise disseminate any content on FlipQ App, or any portion of it; or delete or fail to display any promotional taglines included in the FlipQ App either directly or indirectly, except with the express consent of FDPL. However, you may print or download extracts from these pages for your personal / individual, non-commercial use only. You must not retain any copies of these pages saved to disk or to any other storage medium except for the purposes of using the same for subsequent viewing purposes or to print extracts for personal / individual use.
							</li><br>
							
							<li>
								FDPL forbids you from any attempts to resell or put to commercial use any part of the FlipQ App; any collection and use of any product listings, descriptions, or prices; any derivative use of the FlipQ App or its contents; any downloading or copying of account information for the benefit of any other merchant; any renting, leasing, or otherwise transferring rights to the FlipQ App; displaying the name, logo, trademark or other identifier of another person in such a manner as to give the viewer the impression that such other person is a publisher or distributor of the FlipQ, or any data gathering or extraction tools; or any use of meta tags. 
							</li><br>
							
							<li>
								You may not (whether directly or through the use of any software program) create a database in electronic or structured manual form by regularly or systematically downloading and storing all or any part of the pages from this site. No part of the Site may be reproduced or transmitted to or stored in any other web site, nor may any of its pages or part thereof be disseminated in any electronic or non-electronic form, nor included in any public or private electronic retrieval system or service without prior written permission.
							</li><br>
					
						</ol>
					
					</li>
					
					<li><strong><u>Material Posted/transmitted at FlipQ App</u></strong><br>
					
						<ol>
						
							<li>
								FlipQ App may also provide a web platform for various community interactions for persons to interact and exchange views with each other. The content posted on such applications will be by general public therefore the accuracy, integrity or quality of such content cannot be guaranteed. You understand that by using such services, you may be exposed to objectionable matter.
							</li><br>
							
							<li>
								All information, data, text, software, music, sound, photographs, graphics, video, messages or other materials ("Content"), whether publicly or privately transmitted /posted, is the sole responsibility of the person from where such content is originated (“the Originator”). By Posting any material which contain images, photographs, pictures or that are otherwise graphical in whole or in part ("Images"), you warrant and represent that -
									
								<ul style="margin-left: 5%; margin-top: 1%;">
								
									<li>
										you are the copyright owner of such Images, or that the copyright owner of such Images has granted you permission to use such Images or any content and/or images contained in such Images consistent with the manner and purpose of your use and as otherwise permitted by these Terms,
									</li><br>
									
									<li>
										you have the rights necessary to grant the licenses and sublicenses described in these Terms,		
									</li><br>
										
									<li>
										that each person depicted in such Images, if any, has provided consent to the use of the Images as set forth in these Terms of Use, including, by way of limitation, the distribution, public display and reproduction of such Images.	
									</li><br>
								
								</ul>
							
							</li><br>
							
							<li>
								You represent that you have valid rights and title in any and all Content/Images that you submit on the FlipQ App, that you have not infringed on any IPR belonging to any party and further that you will indemnify FDPL or its affiliates for all claims arising out of any content that you post on the FlipQ App.
								FDPL accepts no responsibility for the said Content / Images. However, you understand that all Content / Images posted by you becomes the property of FDPL and you agree to grant/assign to FDPL, a non-exclusive, royalty free, perpetual, irrevocable and sub-licenseable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, perform and display such Content / Images (in whole or part) worldwide and/or to incorporate it in other works in any form, media, or technology now known or later developed throughout the world.
							</li><br>
						
						</ol>
					
					</li>
					
					<li><strong><u>Prohibited Activities</u></strong><br>
					
						<ol>
						
							<li>
								As a condition of your use of the FlipQ App, you will not use the services of the Company for any purpose that is unlawful or prohibited by these terms, conditions, and notices.
							</li><br>
				
							<li>
								You may not use the FlipQ App in any manner that could damage, disable, overburden, or impair any of our servers, or the network(s) connected to any “FDPL” server, or interfere with any other party's use and enjoyment of any of our services.
							</li><br>
							
							<li>
								You may not attempt to gain unauthorized access to FlipQ App, other accounts, computer systems or to any of our services, through hacking, password mining or any other means. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available through the FlipQ App.
							</li><br>
								
							<li>
								You agree that you shall not host, display, upload, modify, publish, transmit, update or share any information on the FlipQ App, that -
								
								<ul style="margin-left: 5%; margin-top: 1%">
								
									<li>
										belongs to another person and to which you do not have any right to;
									</li><br>
									
									<li>
										is grossly harmful, harassing, blasphemous defamatory, obscene, pornographic, paedophilic, libellous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever;
									</li><br>
									
									<li>
										harm minors in any way;
									</li><br>
									
									<li>
										infringes any patent, trademark, copyright or other proprietary rights;
									</li><br>
									
									<li>
										violates any law for the time being in force;
									</li><br>
									
									<li>
										deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;
									</li><br>
									
									<li>
										impersonate another person
									</li><br>
									
									<li>
										contains software viruses or any other computer code, files or programs designed to interrupt, destroy, and negatively affect in any manner whatsoever, any electronic equipment in connection with the use of the FlipQ App or other user's ability to engage in real time exchanges
									</li><br>
									
									<li>
										threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognisable offence or prevents investigation of any offence or is insulting any other nation.
									</li><br>
								
								</ul>
						
							</li><br>
						
							<li>
								Any Content and or comment uploaded by you, shall be subject to relevant Indian laws and may be disabled, or and may be subject to investigation under appropriate laws. Furthermore, if you are found to be in non-compliance with the laws and regulations, these Terms, the Company shall have the right to immediately terminate/block your access and usage of the FlipQ App and the Company shall have the right to immediately remove any non-compliant Content and or comment, uploaded by you and shall further have the right to take recourse to such remedies as would be available to the Company under the applicable laws.
							</li><br>
						
						</ol>
						
					</li>
				
				</ol>
			
			</li>
			
			<li><strong><u>Limitation of Liability</u></strong><br>
			
				<ol>
					
					<li>
						Any transactions entered by the User with a Participating Restaurant via the FlipQ App including but not limited to reservations, deals, discounts, offers, ordering or pre ordering and supply of food, beverages or any other service at a Participating Restaurant are to be settled inter-se between the User and the Participating Restaurant. The Company shall not be liable in any manner whatsoever for any loss, injury or harm that you may suffer, while making reservations or bookings of the third party/Participating Restaurant or availing any of their services including consumption of food and beverages. All warranties express or implied of any kind, regarding any matter pertaining thereto, including without limitation the implied warranties of merchantability, quality, fitness for a particular purpose, and non-infringement are disclaimed by the Company.
					</li><br>
					
					<li>
						Under no circumstances, including, but not limited to, negligence, will the Company or its subsidiaries, parent companies or affiliates be liable for any direct, indirect, incidental, special or consequential damages that result from the use of, or the inability to use, the FlipQ App, including its materials, products, or services, or third-party materials, products, or services made available through the FlipQ App. You specifically agree that Company is not responsible for any content sent using and/or included in FlipQ’s App by any third party. The Users specifically acknowledge and agree that the Company is not liable for any defamatory, offensive or illegal conduct of any User. If you are dissatisfied with the FlipQ App, or any materials, products, or services on the FlipQ App, or with any of the Terms, your sole and exclusive remedy is to discontinue using the FlipQ App.>
					</li><br>
					
					<li>
						We cannot warrant that use of the FlipQ App will be error free or fit for purpose, timely, that defects will be corrected, or that the FlipQ App or the server that makes it available are free of viruses or bugs or represents the full functionality, accuracy, reliability of the FlipQ App and we do not make any warranty whatsoever, whether express or implied, relating to fitness for purpose, or accuracy.
					</li><br>
					
				</ol>
			
			</li>
			
			<li><strong><u>No Solicitation or Advertisement of Liquor</u></strong>
			
				<ol>
					
					<li>
						The Users are warned that consumption of alcoholic beverages or liquor is injurious to health. Users are advised to refrain from its consumption. The Company via the FlipQ App in no manner whatsoever (i) offers any alcoholic beverages or liquor for sale (ii) promotes alcohol or liquor, and (iii) advertises or solicits the use or consumption of liquor or alcohol.
					</li><br>
					
					<li>
						FlipQ App merely connects the User and the Participating Restaurant. All bookings or reservation relating to sale or consumption of liquor is made by the User directly with the Participating Restaurant.
					</li><br>
					
					<li>
						The sale and consumption of liquor being a licensed activity, the Users and Participating Restaurant alone will be required to abide by the relevant rules and regulations.  The Company disclaims all or any liability in this regard.
					</li><br>
				
				</ol>
			
			</li>
			
			<li><strong><u>Indemnity</u></strong><br>
			
				The User shall indemnify and hold harmless the Company and its parent, subsidiaries, affiliates, third-parties and their respective officers, directors, agents, and employees, from any claim or demand, or actions including attorneys' fees/legal expenses, made by any third party or penalty imposed due to or arising out of your breach of the Terms, or your violation of any law, rules or regulations or the rights of a third party. We reserve the right to take over the exclusive defense of any claim for which we are entitled to indemnification under this section. In such event, you agree to provide us with such cooperation as is reasonably requested by us. This clause shall survive the expiry or termination of this user agreement.
			
			</li>
			
			<li><strong><u>Termination of Access</u></strong><br>
				The Company reserves its right to (a) refuse any of its service, (b) restrict, suspend, or terminate your account; (c) terminate this Agreement; (d) terminate or suspend your access to the FlipQ App; (e) refuse, move or remove for any reason any Content / Image that you submit on or through the FlipQ App; (f) refuse, move, or remove any Content / Image that is available on or through the FlipQ App; (g) establish general practices and limits concerning use of the FlipQ App at any time and, (h) remove or edit contents or cancel orders (entered by you) in its sole discretion with or without cause, and with or without any prior notice for any violation of the Terms	
			</li>
			
			<li><strong><u>User Conduct and Obligations</u></strong><br>
			
				<ol>
					
					<li>
						You hereby agree and assure the Company that the FlipQ App shall be used for lawful purposes only and that you will not violate laws, regulations, ordinances or other such requirements of any applicable Central, State or local government or any other international laws. You further concur that you will not, through the FlipQ App:
						
						<ul style="margin-left: 5%; margin-top 1%;">
							
							<li>
								post, distribute, or otherwise make available or transmit any software or other computer files that contain a virus trojan horses, time bombs, bots, botnets, malicious content, content theft, data manipulation, threats or any other harmful programs or elements or component;
							</li><br>
							
							<li>
								delete from the FlipQ App any legal notices, disclaimers, or proprietary notices such as copyright or trademark symbols, or modify any logos that you do not own or have express permission to modify;
							</li><br>
							
							<li>
								not use the FlipQ App in any manner that could damage, disable, overburden, or impair and not to undertake any action which is harmful or potentially harmful to any Company server, or the network(s), computer systems / resource connected to any Company server, or interfere with any other party's use and enjoyment of the FlipQ App;
							</li><br>
							
							<li>
								obtain or attempt to obtain any materials or information through any means not intentionally made available through the FlipQ App;
							</li><br>
							
							<li>
								engage in any activity that causes / may harm minors; or
							</li><br>
							
							<li>
								perform any activity which is likely to cause such harm;
							</li><br>
							
							<li>
								impersonate any person or entity, including, but not limited to, Company’s official, forum leader, guide or host, or falsely state or otherwise misrepresent your affiliation with a person or entity;
							</li><br>
							
							<li>
								take any action which encourages or consists of any threat of harm of any kind to any person or property;
							</li><br>
							
							<li>
								carry out any "denial of service" (DoS, DDoS) or any other harmful attacks on application or internet service or;
							</li><br>
							
							<li>
								use the FlipQ App for illegal purposes;
							</li><br>
							
							<li>
								disrupt, place unreasonable burdens or excessive loads on, interfere with or attempt to make or attempt any unauthorized access to any Company website or mobile apps.
							</li><br>
							
							<li>
								transmit through the FlipQ App, any unlawful, harassing, libelous, abusive, threatening, harmful, vulgar, obscene, hateful, or racially, ethnically or otherwise objectionable material of any kind or nature. This includes text, graphics, video, programs or audio, etc.;
							</li><br>
							
							<li>
								collect or attempt to collect personally identifiable information of any person or entity without their express written consent and you shall maintain records of any such written consent throughout the terms of this agreement and for a period of 2 years thereafter;
							</li><br>
							
							<li>
								engage in antisocial, disruptive, or destructive acts, including "flaming," "spamming," "flooding," "trolling," and "griefing" as those terms are commonly understood and used on the Internet
							</li><br>
							
							<li>
								forge headers or otherwise manipulate identifiers in order to disguise the origin of any content transmitted through the FlipQ App.	
							</li><br>
							
							<li>
								upload, post, email, transmit or otherwise make available any unsolicited or unauthorised advertising, promotional materials, "junk mail", "spam", "chain letters", "pyramid schemes", duplicative messages or any other form of solicitation.
							</li><br>
							
							<li>
								encumber or suffer to exist any lien or security interest on the subject matter of this Agreement; or	
							</li><br>
							
							<li>
								make any representation or warranty on behalf of  the Company
							</li><br>
						
						</ul>

					</li><br>
					
					<li>
						You agree not to post, distribute, transmit or otherwise make available any data, text, message, computer file, or other material that infringes and/or violates any right of a third party or any domestic or international law, rule, or regulation, including but not limited to:
					
						<ul  style="margin-left: 5%; margin-top: 1%">
						
							<li>
								infringement of any copyright, trademark, patent, trade secret, or other proprietary rights of any third party, including, but not limited to, the unauthorized copying of copyrighted material, the digitization and distribution of photographs from magazines, books, or other copyrighted sources, and the unauthorized transmittal of copyrighted software;
							</li><br>
							
							<li>
								right of privacy (specifically, you must not distribute another person's personal information of any kind without their express permission) or publicity;
							</li><br>
							
							<li>
								any confidentiality obligation. Unless otherwise permitted, you will: (i) display the FlipQ App in the exact form received by you, and not modify or edit any of the foregoing without the Company’s prior written consent; (ii) ensure that the fundamental meaning of the FlipQ App is not changed or distorted; (iii) comply with all applicable laws and all limitations and restrictions (if any) placed by the Company on the use, display or distribution FlipQ App in any manner.	
							</li><br>
						
						</ul>
					
					</li><br>
					
					<li>
						Spam Policy or Unsolicited E-mails-
						
						<ul style="margin-left: 5%; margin-top: 1%">
						
							<li>
								You will not use any communication tool or other means available on the FlipQ App to transmit, directly or indirectly, any unsolicited bulk communications (including emails and instant messages).
							</li><br>
						
							<li>
								You may not harvest information about users of the Company for the purpose of sending or to facilitate the sending of unsolicited bulk communications. We may terminate your access or use of the FlipQ App immediately, with or without any notice, and take any other legal action if you, or anyone using your access details to the FlipQ App, violates these terms. We may adopt any technical remedy (including any filtering technology or other measures) to prevent unsolicited bulk communications from entering, utilizing or remaining within our computer or communication networks. Such filtering technology or other measures may block, either temporarily or permanently, some e-mail sent to you through the FlipQ App.
							</li><br>
						
						</ul><br>
						
					</li><br>
				
				</ol>
			
			</li>
			
			<li><strong><u>Disclaimer of Warranties and Liability</u></strong><br>
			
				<ol>
					
					<li>
						The Company disclaims all warranties, express or implied, statutory or otherwise, as to the services provided, including without limitation, the implied warranties of merchantability, fitness for a particular purpose, workmanlike effort, title and non-infringement are disclaimed and excluded.
					</li><br>
					
					<li>
						The Company  and its parent, affiliates and associates shall not be liable, at any time for any, direct, indirect, punitive, incidental, special, consequential, damages including without limitation, damages for loss of business projects, damage to your computer system or damages for loss of profits, damages for loss of use, data or profits, arising out of or in any way connected with the use or performance of the FlipQ App, with the delay or inability to use the FlipQ App or related services, the provision of or failure to provide services, or for any information, software, products, services and related graphics obtained through the FlipQ App, or otherwise arising out of the use of the FlipQ App  arising in contract, tort or otherwise from the use of or inability to use the FlipQ App, or any of its contents, or from any act or omissions a result of using the FlipQ App or any such contents or for any failure of performance, error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus, communications line failure, theft or destruction or unauthorized access to, alteration of, or use of information contained on the FlipQ App.
					</li><br>
					
					<li>
						No representations, warranties or guarantees whatsoever are made by the Company as to the (accuracy, adequacy, reliability, completeness, suitability or applicability of the information to a particular situation; (b) that the service will be uninterrupted, timely, secure, or error-free; (c) the quality of any products, services, content, information, or other material purchased or obtained from the FlipQ App will meet your expectations or requirements; or (d) any errors in the FlipQ App will be corrected.
					</li><br>
					
				</ol>
			
			</li>
			
			<li><strong><u>Advertisements and Third Party Links</u></strong><br>
			
				<ol>
					
					<li>
						All the contents of the FlipQ App are only for general information or use. They do not constitute advice and should not be relied upon in making (or refraining from making) any decision. Any specific advice or replies to queries in any part of the FlipQ App is/are the personal opinion of such experts/consultants/persons and are not subscribed to by the Company. The information from or through the FlipQ App is provided on "AS IS" basis, and all warranties and conditions, expressed or implied of any kind, regarding any matter pertaining to any goods, service or channel, including without certain links on the FlipQ App lead to resources located on servers maintained by third parties, these sites of third party(s) may contain the Company's-logo, please understand that it is independent from the Company, over whom the Company has no control or connection, business or otherwise as these sites are external to FlipQ App. You agree and understand that by visiting such sites you are beyond the FlipQ's App. The Company, therefore neither endorses nor offers any judgement or warranty and accepts no responsibility or liability for the authenticity, availability, suitability, reliability, accuracy of the information, software, products, services and related graphics contained, of any of the goods/services/or for any damage, loss or harm, direct or consequential or any violation of local or international laws that may be incurred by your visit and/or interactions/s on these site(s), as the same is provided on "as is" without warranty of any kind.
					</li><br>
					
					<li>
						The Company gives no warranty and makes no representation whether expressed or implied, that the information contained in this site is error free. The Company shall not be responsible nor liable for any consequential damages arising on account of your relying on the contents of such information. Before relying on the material, users should independently verify its relevance for their purposes, and should obtain any appropriate professional advice.
					</li><br>
					
					<li>
						Part of the FlipQ App contains advertising information or promotion material or other material submitted to the Company by third parties. Responsibility for ensuring that material submitted for inclusion on FlipQ App complies with applicable International and National law is exclusively on the party providing the information/material. Your correspondence or business dealings with, or participation in promotions of, advertisers other than the Company found on or through the FlipQ App, including payment and delivery of related goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such advertiser.
					</li><br>
					
					<li>
						The Company will not be responsible or liable for any claim, error, omission, inaccuracy in advertising material or any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of such non-Company advertisers on the FlipQ App. Company reserves the right to omit, suspend or change the position of any advertising material submitted for insertion. Acceptance of advertisements on the FlipQ App will be subject to these terms and conditions.
					</li><br>
				
				</ol>
			
			</li>
			
			<li><strong><u>Relationship</u></strong><br>
				None of the provisions of these Terms shall be deemed to constitute a partnership or agency between you and the Company. You shall have no authority to bind the Company in any manner, whatsoever.
			</li>
			
			<li><strong><u>Applicable Law</u></strong><br>
				This User Agreement shall be governed by the laws of India. The courts of law at Delhi/New Delhi shall have exclusive jurisdiction over any disputes arising under this agreement.
			</li>
			
			<li><strong><u>Entire Agreement & Survival</u></strong><br>

				<ol>
					
					<li>
						If any part of this User Agreement/Terms is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed to be superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the User Agreement/Terms shall continue in effect.	
					</li><br>
					
					<li>
						Unless otherwise specified herein, this User Agreement constitutes the entire agreement between you and us with respect to the FlipQ App and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between you and us with respect to the FlipQ App and or any of our services.
					</li><br>
					
					<li>
						Our failure to act with respect to a breach by you or others does not waive its rights to act with respect to subsequent or similar breaches.
					</li><br>
					
					<li>
						Rights and obligations under this User Agreement/Terms which by their nature should survive will remain in full effect after termination or expiration of the User Agreement/Terms.
					</li><br>
									
				</ol>
			
			</li>
			
			<li><strong><u>Dispute Resolution</u></strong><br>
				
				<ol>
					
					<li>
						If any dispute arises between you and the Company during your use of the FlipQ App or thereafter, in connection with the validity, interpretation, implementation or alleged breach of any provision of the User Agreement or the documents incorporated by reference, the dispute shall be referred to a sole Arbitrator who shall be an independent and neutral third party identified by the Company. The place of arbitration shall be New Delhi. The Arbitration & Conciliation Act, 1996, shall govern the arbitration proceedings. The arbitration proceedings shall be in the English language.
					</li><br>
					
					<li>
						You and the Company agree that any cause of action arising out of or related to the FlipQ’s App, must commence within one (1) year after the cause of action accrues otherwise, such cause of action will be permanently barred.
					</li><br>
									
				</ol>
			
			</li>
			
			<li><strong><u>Grievance Redressal Mechanism</u></strong><br>
				
				<ol>
					
					<li>
						Any complaints, abuse or concerns with regards to content and or comment or breach of these Terms shall be immediately informed to the designated Grievance Officer as mentioned below in writing or through email signed with the electronic signature to <a href="grievance@FlipQ.in" style="color: blue;">grievance@FlipQ.in</a>	
					</li><br>
					
					<li>
						<strong>Grievance Redressal Officer</strong>
						<p style="color:#fff"><strong>Ms. Preeti Budhiraja</strong></p>
						<p style="color:#fff">FlipQ Digital Private Limited</p>
						<p style="color:#fff">16/223, Malviya Nagar,</p>
						<p style="color:#fff">New Delhi - 110019</p>
						<p style="color:#fff">India</p>
					</li><br>
					
				</ol>
			
			</li>
			
		</ol>
	</div> <!--container-->
</div> <!--sections-->