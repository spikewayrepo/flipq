<?php
/*POINTS PROGRAM*/
if (FunctionsV3::hasModuleAddon("pointsprogram")){
	unset($_SESSION['pts_redeem_amt']);
	unset($_SESSION['pts_redeem_points']);
}

$merchant_photo_bg=getOption($merchant_id,'merchant_photo_bg');
if ( !file_exists(FunctionsV3::uploadPath()."/$merchant_photo_bg")){
	$merchant_photo_bg='';
} 

/*RENDER MENU HEADER FILE*/
$ratings=Yii::app()->functions->getRatings($merchant_id);   
$merchant_info=array(   
  'merchant_id'=>$merchant_id ,
  'music_name'=>$data['music_name'],
  'ratings'=>$ratings,
  'merchant_address'=>$data['merchant_address'],
  'restaurant_name'=>$data['restaurant_name'],
  'background'=>$merchant_photo_bg,
  'merchant_logo'=>FunctionsV3::getMerchantLogo($merchant_id)
);
//print_r($merchant_info);
//$this->renderPartial('/front/menu-header',$merchant_info);

/*ADD MERCHANT INFO AS JSON */
$cs = Yii::app()->getClientScript();
$cs->registerScript(
  'merchant_information',
  "var merchant_information =".json_encode($merchant_info)."",
  CClientScript::POS_HEAD
);		

/*PROGRESS ORDER BAR*/
#$this->renderPartial('/front/order-progress-bar',array(
#   'step'=>3,
#   'show_bar'=>true
#));

$now=date('Y-m-d');
$now_time='';

$checkout=FunctionsV3::isMerchantcanCheckout($merchant_id); 
$menu=Yii::app()->functions->getMerchantMenu($merchant_id); 

echo CHtml::hiddenField('is_merchant_open',isset($checkout['code'])?$checkout['code']:'' );

/*hidden TEXT*/
echo CHtml::hiddenField('restaurant_slug',$data['restaurant_slug']);
echo CHtml::hiddenField('merchant_id',$merchant_id);
echo CHtml::hiddenField('is_client_login',Yii::app()->functions->isClientLogin());

echo CHtml::hiddenField('website_disbaled_auto_cart',
Yii::app()->functions->getOptionAdmin('website_disbaled_auto_cart'));

$hide_foodprice=Yii::app()->functions->getOptionAdmin('website_hide_foodprice');
echo CHtml::hiddenField('hide_foodprice',$hide_foodprice);

echo CHtml::hiddenField('accept_booking_sameday',getOption($merchant_id
,'accept_booking_sameday'));

echo CHtml::hiddenField('customer_ask_address',getOptionA('customer_ask_address'));

echo CHtml::hiddenField('merchant_required_delivery_time',
  Yii::app()->functions->getOption("merchant_required_delivery_time",$merchant_id));   
  
/** add minimum order for pickup status*/
$merchant_minimum_order_pickup=Yii::app()->functions->getOption('merchant_minimum_order_pickup',$merchant_id);
if (!empty($merchant_minimum_order_pickup)){
	  echo CHtml::hiddenField('merchant_minimum_order_pickup',$merchant_minimum_order_pickup);
	  
	  echo CHtml::hiddenField('merchant_minimum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_minimum_order_pickup)));
}
 
$merchant_maximum_order_pickup=Yii::app()->functions->getOption('merchant_maximum_order_pickup',$merchant_id);
if (!empty($merchant_maximum_order_pickup)){
	  echo CHtml::hiddenField('merchant_maximum_order_pickup',$merchant_maximum_order_pickup);
	  
	  echo CHtml::hiddenField('merchant_maximum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_maximum_order_pickup)));
}  


$is_ok_delivered=1;
if (is_numeric($merchant_delivery_distance)){
	if ( $distance>$merchant_delivery_distance){
		$is_ok_delivered=2;
		/*check if distance type is feet and meters*/
		if($distance_type=="ft" || $distance_type=="mm" || $distance_type=="mt"){
			$is_ok_delivered=1;
		}
	}
} 

echo CHtml::hiddenField('is_ok_delivered',$is_ok_delivered);
echo CHtml::hiddenField('merchant_delivery_miles',$merchant_delivery_distance);
echo CHtml::hiddenField('unit_distance',$distance_type);
echo CHtml::hiddenField('from_address', FunctionsV3::getSessionAddress() );

echo CHtml::hiddenField('merchant_close_store',getOption($merchant_id,'merchant_close_store'));
/*$close_msg=getOption($merchant_id,'merchant_close_msg');
if(empty($close_msg)){
	$close_msg=t("This restaurant is closed now. Please check the opening times.");
}*/
echo CHtml::hiddenField('merchant_close_msg',
isset($checkout['msg'])?$checkout['msg']:t("Sorry merchant is closed."));

echo CHtml::hiddenField('disabled_website_ordering',getOptionA('disabled_website_ordering'));
echo CHtml::hiddenField('web_session_id',session_id());

echo CHtml::hiddenField('merchant_map_latitude',$data['latitude']);
echo CHtml::hiddenField('merchant_map_longtitude',$data['lontitude']);
echo CHtml::hiddenField('restaurant_name',$data['restaurant_name']);


/*add meta tag for image*/
Yii::app()->clientScript->registerMetaTag(
Yii::app()->getBaseUrl(true).FunctionsV3::getMerchantLogo($merchant_id)
,'og:image');
//print_r($_SESSION);
?>


<section class="slider">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <!--<ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>-->

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
    <?php
$logoo = FunctionsV3::getMerchantBanner($merchant_id);

//echo $logoo;
?>
      <div class="item item1 active" style="background: rgba(0,0,0,0) url(<?php echo $logoo; ?>) no-repeat scroll center / cover;">
   
    </div>

     
    </div>
  
  </div>
</section>

<section class="des-body">
  <div class="res-description">
  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <h1><?php echo $data['restaurant_name'];?></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <ul class="details">
        <li><?php echo $data['location'];?></li>
        <li>45 Votes</li>
        <li><div class="rating">

<?php $ratings=Yii::app()->functions->getRatings($merchant_id);?>

<?php  FunctionsV3::displayrate($ratings['ratings']) ?>
         </div></li>

      </ul>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <ul class="details1">
        <li><i class="fa fa-phone" aria-hidden="true"></i><?php print $data['contact_phone'] ?></li>
        <li><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<a class="opening-hours" style="border:0 px, color:red," data-toggle="modal" data-target="#myModal">See Opening Hours</a></li>
        <li class="rating">&#x20b9;&nbsp;&nbsp;<?php print $data['cost_for_two'] ?></li>
      </ul>
    </div>
  </div>
</div>
</section>


<section class="des-body">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-xs-12">
       

        <div class="des-tabs">
          <div class="col-sm-12 col-xs-12 no-padding">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <!-- Tabs -->
                    <ul class="nav panel-tabs">
                        <li class="active"><a href="#tab1">Type of Music</a></li>
                      <!--  <li><a href="#tab2">Order Online</a></li> -->
                        <li><a href="#tab3">Type of Music</a></li>
                        <li><a href="#tab4">Reviews</a></li>
                        <li><a href="#tab5">Photos</a></li>
                        <li><a href="#tab6">Pre-Book</a></li>
                    </ul>


		
                </div>
                <!-- ETA POPUP -->


<?php
                            $this->renderPartial('/front/etapopup',array(
                            'merchant_id'=>$merchant_id
                            )); ?>

              <!--  <div class="panel-body">
                    <div class="tab-content">-->
                    <div class="panel-body">
                        <div class="tab-pane active" id="tab1">
                          <h4>Description</h4>
                          <p class="para1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero eos et accusam et justo duo dolores et ea rebum.</p>

                          <div class="row mar-top1">
                            <div class="col-sm-3 col-xs-3">
                              <p>Specifications</p>
                            </div>
                            <div class="col-sm-9 col-xs-9">
                              <ul>
                                <li><i class="fa fa-beer" aria-hidden="true"></i>Bar</li>
                                <li><i class="fa fa-wifi" aria-hidden="true"></i>WiFi</li>
                                <li><i class="fa fa-window-maximize" aria-hidden="true"></i>Live Screening</li>
                                <li><i class="fa fa-music" aria-hidden="true"></i>Live Music</li>
                              </ul>
                            </div>
                          </div>
                          <hr />

                          <div class="row">
                            <div class="col-sm-3 col-xs-3">
                              <p>Overview</p>
                            </div>
                            <div class="col-sm-4 col-xs-4">
                              <!--<p><span>Phone:</span> <?php print $data['contact_phone'] ?></p>
                              <span class="small">Table Booking Recommended</span>-->
                              <p class="mar-top5"><span>Cuisines :</span><?php echo FunctionsV3::displayCuisine($data['cuisine']);?></p>
                              <p class="mar-top5"><span>Cost :</span><?php //print_r($data);
                  echo "Rs. ".$data['cost_for_two'].' (approx.)'?></p>
                            </div>
                            <div class="col-sm-5 col-xs-5">
                              <p><span>Opening Hours:</span>
                                    <a class="opening-hours" style="border:0 px, color:red," data-toggle="modal" data-target="#myModal">See Opening Hours</a></p>
                      <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-sm">
                          <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" style="color: #fff;" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Opening Hours</h4>
                            </div>
                            <div class="modal-body">
                            <?php
                            $this->renderPartial('/front/merchant-hours',array(
                            'merchant_id'=>$merchant_id
                            )); ?>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>




                              <p class="small"><span>Happy Hours:</span>6PM - 8PM</p>
                              <p class="mar-top5"><span>Address:</span><?php echo $data['location'];?></p>
                              <p class="mar-top5"><span>Services :</span><?php echo FunctionsV3::displayHighlights($data['highlights']);?></p>
                              <p class="mar-top5"><span>Pint of Beer Cost :</span><?php //print_r($data);
                  echo "Rs. ".$data['pint_of_beer']; ?></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel-body">
                        <div class="tab-pane" id="tab3">
                          <div class="row">
                          <div class="col-sm-12 col-xs-12">
                            <h3>Menu</h3>
                          </div>
                          <div class="col-sm-3 col-xs-6">
                            <img src="/assets/images/images.jpeg">
                          </div>
                          <div class="col-sm-3 col-xs-6">
                            <img src="/assets/images/images1.jpeg">
                          </div>
                          <div class="col-sm-3 col-xs-6">
                            <img src="/assets/images/images2.jpeg">
                          </div>
                          <div class="col-sm-3 col-xs-6">
                            <img src="/assets/images/images3.png">
                          </div>
                        </div>
                        </div>
                      </div>

                      <div class="panel-body">
                        <div class="tab-pane" id="tab5">
                          <div class="row"><h3>Photo Gallery</h3></div>
                          <div class="gallery">
                            <div class="count"></div>
                            <div class="row">

                              <?php if ($photo_enabled):
              
                          $gallery=Yii::app()->functions->getOption("merchant_gallery",$merchant_id);
                          $gallery=!empty($gallery)?json_decode($gallery):false;
                          $this->renderPartial('/front/merchant-photos',array(
          'merchant_id'=>$merchant_id,
          'gallery'=>$gallery
        )); 
                          //foreach ($gallery as $val):?>

                           <!--  <div class="col-sm-3 col-xs-3">
                                <img src="<?php echo uploadURL()."/".$val?>">
                              </div> -->
<?php
                          //endforeach;
                          // $this->renderPartial('/front/merchant-photos',array(
                          //   'merchant_id'=>$merchant_id,
                          //   'gallery'=>$gallery
                          // ));       
                          endif;?>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="panel-body">
                        <div class="tab-pane" id="tab4">
                          <div class="reviews">
                            <h3>Reviews & Ratings</h3>
                            
                            <?php

                            $client_id=Yii::app()->functions->getClientId(); 
                            // print_r($client_id);
                            // echo "null";
                            // print_r($merchant_id); 
                            $reviews=Yii::app()->functions->getReviewsList($merchant_id);                  
                            $reviewnum=Yii::app()->functions->getReviewsNum($merchant_id); 
                           // print_r($reviewnum[TOTAL]);  
                            // foreach ($reviewnum as $keynum) {
                            //   print_r($keynum[TOTAL]);
                            // }
                            ?>






<!--MERCHANT REVIEW-->
                <?php //if ($theme_reviews_tab==""):?>
                <div class="review-tab-content">                   
                <?php $this->renderPartial('/front/merchant-review',array(
                  'merchant_id'=>$merchant_id
                )); ?>          
                </div>
                <?php //endif;?>
                <!--END MERCHANT REVIEW-->



                            <div class="rating-num">
                            <?php $ratings=Yii::app()->functions->getRatings($merchant_id);?>


                              <p><?php echo $ratings['ratings']?></p>
                            </div>
                            <div class="rating">
                            <?php  FunctionsV3::displayrate($ratings['ratings']) ?>
                            
                            </div>
                            <div class="votes">
                            <?php foreach ($reviewnum as $keynum) {?>
                              
                             
                              <p><?php  print($keynum['TOTAL']); ?> Reviews</p>
                              <?php } ?>
                              <p><?php print $ratings['votes'] ?> Votes</p>
                            </div>


                           <?php foreach ($reviews as $key) { 
                            $client_info=Yii::app()->functions->getClientInfo($key['client_id']);

                            $date=Yii::app()->functions->time_elapsed_string($key['date_created']);

                          ?>
                            <div class="row">
                              <div class="media">
                                <div class="col-sm-2 col-xs-3">
                                  <div class="media-left center-block">
                                    <?php
 print ($client_info['avatar'])?"<img src='/upload/".$client_info["avatar"]."' class='media-object center-block'>": "<img src='/assets/images/user.png' class='media-object center-block'>"; ?>

                                    <p><?php print($client_info['first_name']." ".$client_info['last_name']);?></p>
                                    <p><?php echo $date; ?></p>
                                  </div>

                                </div>
                                <div class="col-sm-9 col-xs-9">
                                  <div class="media-body">
                                    <h4 class="media-heading"><?php print($key['review']); ?></h4>

                                  </div>
   

                                </div>
                                <div class="col-sm-1">
                                  <span class="rate"><?php print($key['rating']); ?></span>
                                </div>
                              </div>
                            </div>
                            <?php } ?>
                            
                            
                            
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        </div>








 <?php if ($booking_enabled):?>
	  
	    <?php $this->renderPartial('/front/merchant-book-table',array(
	      'merchant_id'=>$merchant_id
	    )); ?>        
	  
	    <?php endif;?>




    </div>
  </div>
</section>

















<div class="sections section-menu section-grey2">
<div class="container">
  <div class="row">

     <div class="col-md-offset-1 col-md-10 border menu-left-content">
         
        <div class="tabs-wrapper" id="menu-tab-wrapper">
	    <ul id="tabs">
		    <li class="active">
		       <span><?php echo t("Type of Music")?></span>
		       <i class="ion-music-note"></i>
		    </li>
		    
		    <?php if ($theme_hours_tab==""):?>
		    <li>
		       <span><?php echo t("Play Music")?></span>
		       <i class="ion-play"></i>
		    </li>
		    <?php endif;?>
		    
		    <?php if ($theme_reviews_tab==""):?>
		    <li class="view-reviews">
		       <span><?php echo t("Reviews")?></span>
		       <i class="ion-ios-star-half"></i>
		    </li>
		    <?php endif;?>
		    
		    <?php if ($booking_enabled):?>
		      <li>
		      <span><?php echo t("Performance Schedule")?></span>
		      <i class="ion-calendar"></i>



		      </li>
		    <?php endif;?>
		    
		    <?php if ($photo_enabled):?>
		      <li class="view-merchant-photos">
		       <span><?php echo t("About")?></span>
		       <i class="ion-images"></i>
		      </li>
		    <?php endif;?>
		    
		    <?php if ( $promo['enabled']==2 && $theme_promo_tab==""):?>
		      <li>
		       <span><?php echo t("Promos")?></span>
		       <i class="ion-pizza"></i>
		      </li>
		    <?php endif;?>
		</ul>
		
		<ul id="tab">
		
		<!-- MUSIC TYPE -->
	    <li class="active">
	        <div class="mbot">
                <div class="res-info-group clearfix">
				<h2 class="mt0 mb5" tabindex="0">Music Type</h2>
					<div class="res-info-cuisines clearfix">
						<?php echo FunctionsV3::displayMusicType($data['music_name']);?>
					</div>    
                </div>
            </div>
	    </li>
	    <!--END MUSIC TYPE-->
	    
	    
	    <!-- PLAY MUSIC -->
	    <?php if ($theme_hours_tab==""):?>
	    <li>	       	     
			<div class="box-grey rounded " style="margin-top:0;">
				<?php if(getOption($merchant_id,'merchant_information')){
						echo getOption($merchant_id,'merchant_information');
				} else {
					echo "No Information Available";
				}?>
	        </div>          
	    </li>
	    <?php endif;?>
	    <!--END PLAY MUSIC-->
	    
	    <!--MERCHANT REVIEW-->
	    <?php if ($theme_reviews_tab==""):?>
	    <li class="review-tab-content">	       	     
	    <?php $this->renderPartial('/front/merchant-review',array(
	      'merchant_id'=>$merchant_id
	    )); ?>           
	    </li>
	    <?php endif;?>
	    <!--END MERCHANT REVIEW-->
		
		
	    <!--PERFORMANCE SCHEDULE-->
	    <li>
		
		<div class="box-grey rounded merchant-promo" style="margin-top:0;">
			<?php
			if($res = Yii::app()->functions->getMerchantevent($merchant_id)){ ?>
				<div class="section-label">
					<a class="section-label-a">
					  <span class="bold" style="background:#000;">
					  <?php echo t("Upcoming Events")?></span>
					  <b></b>
					</a>     
				</div> 
					 <?php foreach($res as $val): ?>
					 <?php //print_r($val); ?>
							<p class="spacer40" style="color: #fff;">
								<i class="green-color ion-ios-plus-empty"></i> 
									<strong><?php echo $val['event_name'] ?> starts from <?php $start_date = explode(" ", $val['start_date']); echo $start_date[0]; ?> to <?php $end_date = explode(" ", $val['end_date']); echo $end_date[0]; ?> at <?php echo $val['venue'] ?></strong>
									<p style="margin-left: 2.5%; margin-top: -2%"><strong> Desc.: </strong><?php echo $val['event_description']; ?> </p>
							</p>
					  <?php endforeach; ?>
			<?php } else {
					echo "No Performance Schedule.";
			} ?>	    
		</li>
	    
		<?php  ?>
		
	    <!--END PERFORMANCE SCHEDULE-->
	    
	    <!--ABOUT-->
	    <?php if ($theme_info_tab==""):?>
	    <li>
	        <div class="box-grey rounded " style="margin-top:0;">
	          <?php echo getOption($merchant_id,'merchant_information')?>
	        </div>
	    </li>
	    <?php endif;?>
	    <!--END ABOUT-->	    
	    
	   </ul>
	   </div>
     
     </div> <!-- menu-left-content-->
     
  </div> <!--row-->
</div> <!--container-->
</div> <!--section-menu-->
