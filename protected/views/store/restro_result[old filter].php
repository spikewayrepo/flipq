
<div class="modal fade" id="dealsModal" tabindex="-1" role="dialog" aria-labelledby="dealsModalLabel" aria-hidden="true">
    <div class="modal-dialog">
           

        <div class="modal-content login-modal">

             <div class="modal-header login-modal-header">
              <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
              <h4 id="loginmerchantLabel" class="modal-title text-center"></h4>
            </div>

		<div class="modal-body">
			   <div class="row" id="deals_model">


			   </div>
                </div>
		<div  id="footermsg" class="modal-footer">
                    <div class="col-sm-12 col-xs-12 text-center">
                    <p>Note:You can only avail one deal in a day.</p>
                  </div>
                </div>
           
          </div>
       </div>
  </div>

<section class="product-listing">
	
	<div class="container">
    
		<div class="row">
			
			<div class="col-sm-12 col-xs-12">
				<p class="gray search-heading"><span>Best Restaurant In Delhi</span><?php echo "(".$data['total']." Restaurants Found)"; ?></p>
				<p class="hidden-xs"><a id="filters_link1"><i class="fa fa-filter" aria-hidden="true"> Filters</i></a></p>
			</div>
    
		</div>
<div class="row">
<div id="mobile-search-filter" class="col-md-3 border search-left-content">
<?php  $filterinfo['filterleft'] = $filterleft['list']; 
 $this->renderPartial('/front/filtersjs', $filterinfo);
?>
</div>

<div class="col-md-9 border search-right-content"> 

	<div class="row pad-xs-40" id="restralisting">
			<?php
			if ($data){
				foreach ($data['list'] as $val)
					{
						$merchant_id = $val['merchant_id']; 
						$ratings=Yii::app()->functions->getRatings($merchant_id);
						?>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        
							<div class="list-card">
								
								<?php
									$logoo = FunctionsV3::getMerchantLogo($merchant_id);


								?>

<div class="list-card">
          <div class="image-div1" onclick="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>" style="background: rgba(0,0,0,0) url('<?php print $logoo ?>') no-repeat scroll center center / cover;">
            <?php echo FunctionsV3::merchantOpenTag($merchant_id)?>
          </div>
          <div class="list-card-body">
            <p><a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'].'?booktable=true')?>"><?php echo $val['restaurant_name']; ?></a></p>
<p>
<?php
$cui_arr = explode("/", FunctionsV3::displayCuisine($val['cuisine']));

	if(count($cui_arr) > 5)
		echo "Multicuisine";
	else
		echo "".($val['cuisine']!='null')?FunctionsV3::displayCuisine($val['cuisine']):'No Cuisine'."";


									$deals=FunctionsV3::getNoofdeals($merchant_id);
?>

</p>

            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?php echo ($val['location'])?$val['location']:'Loading...' ?></p>
            <a  onClick=getDealsinfo(this) rel="<?php print $merchant_id ?>" ><p class="deals"><svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" id="Layer_1"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z" class="cls-1"/><circle transform="translate(-0.02 0.06) rotate(-0.08)" r="3.83" cy="12.47" cx="37.99" class="cls-2"/></svg><?php echo ($deals)?$deals." Deals":'No Deals' ?></p></a>
            <?php if((int)$val['cost_for_two']){ ?><p><span style="font-family:quick-bold;">₹</span>&nbsp;&nbsp;<?php print (int)$val['cost_for_two'] ?> Cost For 2</p><?php }else{ ?><p> Not Available </p> <?php } ?>




            <div class="rating">


<?php  FunctionsV3::displayrate($ratings['ratings']) ?>

              



            	<!-- <div class=" rating-stars" data-score="<?php echo $ratings['ratings']?>"></div> -->
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>"><button data-toggle="modal" data-target="#etaModal">Pre-Order</button></a>
              </div>
              <div class="col-sm-6 col-xs-6">
                <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>"><button>Book-Table</button></a>
              </div>
            </div>
          </div>
        </div>

			
        
							</div>
					
					</div>
				<?php
				} 
			} ?>
		
		</div>
</div>
  </div>		
		
	</div>
	
</section>
<?php
  if($mid=FunctionsV3::getMerchantIDbyDeals('FLD'))
  {
    $data['result']= $mid;
    $this->renderPartial('/front/flash_deals', $data);
  }
  ?>
<!-- 
<?php
  // if($mid=FunctionsV3::getMerchantIDbyDeals('FLD'))
  // {
  //   $data['result']= $mid;
  //   $this->renderPartial('/front/flash_deals', $data);
  // }
  // $filterinfo['filter'] = $filterleft['list']; 
  // $this->renderPartial('/front/filters', $filterinfo);
  ?> -->













<?php /*
$search_address=isset($_GET['s'])?$_GET['s']:'';
$rtype=isset($_GET['rtype'])?$_GET['rtype']:'';

if (isset($_GET['s']))
{
	$search_address=$_GET['s'];
}
$this->renderPartial('/front/search-header',array(
   'search_address'=>$search_address,
   //'total'=>$data['total']
));

//print_r($data);
?>

<?php 
$this->renderPartial('/front/order-progress-bar',array(
   'step'=>2,
   'show_bar'=>true
));

echo CHtml::hiddenField('clien_lat',$data['client']['lat']);
echo CHtml::hiddenField('clien_long',$data['client']['long']);
?>

<div class="search-map-results">  
</div> <!--search-map-results-->
<div class="sections section-search-results">

  <div class="container">

   <div class="row">
   
     <div class="col-md-3 border search-left-content" id="mobile-search-filter">
       
        <?php if ( $enabled_search_map=="yes"):?>
        <a href="javascript:;" class="search-view-map green-button block center upper rounded">
        <?php echo t("View by map")?>
        </a>
        <?php endif;?>
        
        <div class="filter-wrap rounded2 <?php echo $enabled_search_map==""?"no-marin-top":""; ?>" style="background-color: #000;">
                
          <button type="button" class="close modal-close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>  
        
           <p class="bold"><?php echo t("Filters")?></p>
           
           
		   <div class="filter-box">
	           <a href="javascript:;">	             
	             <span style="background-color: #000; color: #fff;">
					<i class="<?php echo $fc==2?"ion-ios-arrow-thin-down":'ion-ios-arrow-thin-right'?>"></i>
					<?php echo t("Search NearBy")?>
	             </span>   
	             <b></b>
	           </a>
			   <form action="<?php echo $_SERVER['REQUEST_URI']?>">
			   <div class="row distance_row" style="margin-left: 7.5%" >
				<div id="slider-range"></div>
					<div class="row">
						<!--<label for="amount">Distance range:</label>-->
						<input type="text" id="amount_from" name="range_from" style="margin-left: 20%; width: 30%" readonly  >
						<input type="text" id="amount_to" name="range_to" style="width:25%" readonly >
					</div>
			   </div>
			   
			   <!--<div class="range range-warning">
				<section class="range-slider">
				 <span class="rangeValues" style="color: #fff; font-weight: bold">0-15 </span><span style="color:#fff; font-weight: bold;"> Km</span>
				  <input name="range_from" id="range_from" value="1" min="1" max="15" step="0.5" type="range">
				  <input name="range_to" id="range_to" value="15" min="1" max="15" step="0.5" type="range">
				</section>
				<br>
			   </div>-->
				<!--<a href="<?php// echo $_SERVER['REQUEST_URI']."&range_to="..""; ?>">-->
					<button type="submit" style="background-color: #000; border-color : #fff; color: #fff;">
						<span style="color: #fff">Search</span>
					</button>
				<!--</a>-->
				</form>
		   </div>
		   
		   
           <!--FILTER MERCHANT NAME-->       
           <?php if (!empty($restaurant_name)):?>                      
           <a href="<?php echo FunctionsV3::clearSearchParams('restaurant_name')?>">[<?php echo t("Clear")?>]</a>
           <?php endif;?>    
           <div class="filter-box">
	           <a href="javascript:;">	             
	             <span style="background-color: #000; color: #fff;">
	             <i class="<?php echo $fc==2?"ion-ios-arrow-thin-down":'ion-ios-arrow-thin-right'?>"></i>
	             <?php echo t("Search by name")?>
	             </span>   
	             <b></b>
	           </a>
	           <ul class="<?php echo $fc==2?"hide":''?>">
	              <li>
	              <form method="get" onsubmit="/store/searchresult">
		              <div class="search-input-wraps rounded30">
		              <div class="row">
				        <div class="col-md-10 col-xs-10">
				        <?php echo CHtml::textField('restro',$restaurant_name,array(
				          'required'=>true,
				          'placeholder'=>t("enter restaurant name"),
						  'style'=>'color: #000; border: 0px;'
				        ))?>
				        </div>        
				        <div class="col-md-2 relative col-xs-2 ">
				          <button type="submit"><i class="fa fa-search" style="color: #000;"></i></button>         
				        </div>
				     </div>
			     </div>
			     </form>
	              </li>
	           </ul>
           </div> <!--filter-box-->
           <!--END FILTER MERCHANT NAME-->           
           
           
           <?php  
             function cuitext($cui)
            {
                $DbExt=new DbExt();
                if($cui)
                {
		        $sql = "SELECT * FROM {{cuisine}} where cuisine_name='".$cui."' ";
                //print_r($sql);       
		        if ($res=$DbExt->rst($sql))
                        {
							//print_r($res);

		           return $res[0]['cuisine_name'];
		        }
		}			
            } 
            $filterableAttributes = array('rtype'=>"Restaurant Type",'cuisine'=>"Cuisine",'city'=>"City",'distance'=>"Distance");

            foreach($filterableAttributes as $attribute=>$value2)
	    {
	        $attcode    = $attribute;
                if($attcode != 'distance')
		{
			$attributeCountArray = array();
			$attributeValues = array();


                foreach($filterleft['list'] as $key=>$product) 
				{

                              if($product[$attribute] != '' && $attribute !='cuisine') 
			      {

					if($attributeCountArray[$product[$attribute]])
					{
						$attributeCountArray[$product[$attribute]] = $attributeCountArray[$product[$attribute]] + 1;
						$attributeValues[$product[$attribute]] = $product[$attribute];
					}
					else 
					{
						$attributeCountArray[$product[$attribute]] = 1;
						$attributeValues[$product[$attribute]] = $product[$attribute];
					}
                              }
                              else
                              {
                                        $cuisine = json_decode($product[$attribute]);
                                        if(is_array($cuisine))
                                        {

                                        	foreach( $cuisine as  $cfilter)
                                                {

							if($attributeCountArray[$cfilter])
                                                        {
								$attributeCountArray[$cfilter] = $attributeCountArray[$cfilter] + 1;
								$attributeValues[$cfilter] = cuitext($cfilter);
							}
							else
							{
								$attributeCountArray[$cfilter] = 1;
								$attributeValues[$cfilter] = cuitext($cfilter);
							}
						}
                                        }
                              }

					}
                }
                else
                {

                }
				
                if(count($attributeCountArray)) 
				{
					$html .= "<div id='filterlabel".$value2."' class='filter-box'>
					<a href='javascript:;'>	<span style='background-color: #000; color: #fff;'><i class='ion-ios-arrow-thin-right'></i> ".$value2."</span><b></b></a><ul class='$attcode'>";



					foreach ($attributeCountArray as $key=>$attributePair)
					{
           
						if($attcode=='cuisine')
						{
							$cuiarra= explode(',',$_GET['cuisine']);

							if(in_array($key,$cuiarra))
							{
								$html .= "<li id='filter_".$attributeValues[$key]."' class='filters_a'>
								<input type='checkbox' class='filters_checkbox check-filter' name='$attcode' checked='checked' value='$key'><a class='check-filter-anch' href='#' >".strtoupper($attributeValues[$key])."(".$attributeCountArray[$key].")</a></li>";
							} else 
							{
								$html .= "<li id='filter_".$attributeValues[$key]."' class='filters_a'>
								<input type='checkbox' class='filters_checkbox check-filter' name='$attcode' value='$key'><a class='check-filter-anch' href='#' >".strtoupper($attributeValues[$key])."(".$attributeCountArray[$key].")</a></li>";
							}

						} else
						{
							$otherkey= explode(',',$_GET[$attcode]);
							if(in_array($key,$otherkey))
							{
								$html .= "<li id='filter_".$attributeValues[$key]."' class='filters_a'>
								<input type='checkbox' class='filters_checkbox check-filter' name='$attcode' checked='checked' value='$attributeValues[$key]'><a class='check-filter-anch' href='#' >".strtoupper($attributeValues[$key])."(".$attributeCountArray[$key].")</a></li>";
							} else
							{
								$html .= "<li id='filter_".$attributeValues[$key]."' class='filters_a'>
								<input type='checkbox' class='filters_checkbox check-filter' name='$attcode'  value='$attributeValues[$key]'><a class='check-filter-anch' href='#' >".strtoupper($attributeValues[$key])."(".$attributeCountArray[$key].")</a></li>";
							}
						}
               

					}
					$html .= "</ul></div>";
				}


      #print_r($attributeCountArray);
  #      echo "----------";
      #print_r($attributeValues);

           }

      
           ?>
<form id="list_filter_form">
                    <?php echo $html; ?>
           <!--FILTER DELIVERY FEE-->           
 </form>          
           
         
           
           
           
           <!--MINIUM DELIVERY FEE-->           
           <?php if (false/*!empty($filter_minimum)*//*):?>                      
           <a href="<?php echo FunctionsV3::clearSearchParams('filter_minimum')?>">[<?php echo t("Clear")?>]</a>
           <?php endif;?>
           <?php if ( false/*$minimum_list=FunctionsV3::minimumDeliveryFee()*//*):?>
           <div class="filter-box">
	           <a href="javascript:;">	             
	             <span>
	             <i class="<?php echo $fc==2?"ion-ios-arrow-thin-down":'ion-ios-arrow-thin-right'?>"></i>
	             <?php echo t("Minimum Delivery")?>
	             </span>   
	             <b></b>
	           </a>
	            <ul class="<?php echo $fc==2?"hide":''?>">
	             <?php foreach ($minimum_list as $key=>$val):?>
	              <li>
		           <?php 
		          echo CHtml::radioButton('filter_minimum[]',
		          $filter_minimum==$key?true:false
		          ,array(
		          'value'=>$key,
		          'class'=>"filter_by_radio filter_minimum icheck"
		          ));
		          ?>
	              <?php echo $val;?>
	              </li>
	             <?php endforeach;?> 
	           </ul>
           </div> <!--filter-box-->
           <?php endif;?>
           <!--END MINIUM DELIVERY FEE-->
           
        </div> <!--filter-wrap-->
        
     </div> <!--col search-left-content-->
     
     <div class="col-md-9 border search-right-content">
          
     <?php echo CHtml::hiddenField('sort_filter',$sort_filter)?>
     <?php echo CHtml::hiddenField('display_type',$display_type)?>     
     
         <div class="sort-wrap">
           <div class="row">           
              <div class="col-md-12 col-xs-12 border ">	           
	           <?php 
	           $filter_list=array(
	             'restaurant_name'=>t("Name"),
	             'ratings'=>t("Rating"),
	             'minimum_order'=>t("Minimum"),
	             //'distance'=>t("Distance")
	           );
	           if (isset($_GET['s'])){
	           	   unset($filter_list['distance']);
	           }
	           echo CHtml::dropDownList('sort-results',$sort_filter,$filter_list,array(
	             'class'=>"sort-results selectpicker",
	             'title'=>t("Sort By")
	           ));
	           ?>
              </div> <!--col-->
              <!--<div class="col-md-6 col-xs-6 border">                
               
                          
                <a href="<?php echo FunctionsV3::clearSearchParamscustom('','display_type=listview')?>" 
	           class="display-type orange-button block center rounded 
	           <?php echo $display_type=="gridview"?'inactive':''?>" 
		          data-type="listview">
                <i class="fa fa-th-list"></i>
                </a>
                
                <a href="<?php echo FunctionsV3::clearSearchParamscustom('','display_type=gridview')?>" 
		          class="display-type orange-button block center rounded mr10px 
	             <?php echo $display_type=="listview"?'inactive':''?>" 
		          data-type="gridview">
                <i class="fa fa-th-large"></i>
                </a>           
                
                <a href="javascript:;" id="mobile-filter-handle" class="orange-button block center rounded mr10px">
                  <i class="fa fa-filter"></i>
                </a>    
                
                <?php if ( $enabled_search_map=="yes"):?>
                <a href="javascript:;" id="mobile-viewmap-handle" class="orange-button block center rounded mr10px">
                  <i class="ion-ios-location"></i>
                </a>    
                <?php endif;?>
                
                <div class="clear"></div>
                
              </div>-->
           </div> <!--row-->
         </div>  <!--sort-wrap-->  
         
         
         <!--MERCHANT LIST -->
                  
         <div class="result-merchant">
             <div class="row infinite-container" style="margin-right: 2%; margin-left: 1%">
             
             <?php if ($data):
					foreach ($data['list'] as $val):  
						 $merchant_id=$val['merchant_id'];             
						 $ratings=Yii::app()->functions->getRatings($merchant_id);   
						 
						 /*get the distance from client address to merchant Address*/             
						 /*$distance_type=FunctionsV3::getMerchantDistanceType($merchant_id); 
						 $distance_type_orig=$distance_type;
						 
						 /*dump("c lat=>".$data['client']['lat']);         
						 dump("c lng=>".$data['client']['long']);	             
						 dump("m lat=>".$val['latitude']);
						 dump("c lng=>".$val['lontitude']);*/
						 
						   
						 /*$distance=FunctionsV3::getDistanceBetweenPlot(
							$data['client']['lat'],$data['client']['long'],
							$val['latitude'],$val['lontitude'],$distance_type
						 );      
												 
						 $distance_type_raw = $distance_type=="M"?"miles":"kilometers";
						 $distance_type = $distance_type=="M"?t("miles"):t("kilometers");
						 $distance_type_orig = $distance_type_orig=="M"?t("miles"):t("kilometers");
						 
						 if(!empty(FunctionsV3::$distance_type_result)){
							$distance_type_raw=FunctionsV3::$distance_type_result;
							$distance_type=t(FunctionsV3::$distance_type_result);
						 }
						 
						 $merchant_delivery_distance=getOption($merchant_id,'merchant_delivery_miles');             
						 
						 $delivery_fee=FunctionsV3::getMerchantDeliveryFee(
									  $merchant_id,
									  $val['delivery_charges'],
									  $distance,
									  $distance_type_raw);
						 ?>
						 
						 <?php 	             
						if ( $display_type=="listview")
						{
							if( $_GET['range_to'] && $_GET['range_from'] )
							{
								$merchant_address = $val['restaurant_name'].", ".$val['street'].", ".$val['city'].", ".$val['state']." ".$val['post_code'];
								if($_SESSION['fq_search_address'])
								{  
									$var1 = Yii::app()->functions->getDistance($_SESSION['fq_search_address'], $merchant_address);	
								} else {
									$place = "Nehru Place, New Delhi, Delhi, India";
									$var1 = Yii::app()->functions->getDistance($place, $merchant_address);
								}
								 $var2 = round( ($var1->rows[0]->elements[0]->distance->value) / 1000 );
								 //echo rtrim($_GET['range_from'] , "Km -");	
								if( $var2 >= rtrim( $_GET['range_from'] , " Km -" ) && $var2 <= rtrim( $_GET['range_to'] , " Km" ))
								{
								 $this->renderPartial('/front/search-distance-list-2',array(
								   'data'=>$data,
								   'val'=>$val,
								   'merchant_id'=>$merchant_id,
								   'ratings'=>$ratings,
								   'distance_type'=>$distance_type,
								   'distance_type_orig'=>$distance_type_orig,
								   //'distance'=>$var2,
								   //'merchant_delivery_distance'=>$merchant_delivery_distance,
								   //'delivery_fee'=>$delivery_fee
								 ));
								}
							} else
							{
								$this->renderPartial('/front/search-list-2',array(
							   'data'=>$data,
							   'val'=>$val,
							   'merchant_id'=>$merchant_id,
							   'ratings'=>$ratings,
							   'distance_type'=>$distance_type,
							   'distance_type_orig'=>$distance_type_orig,
							   //'distance'=>$var2,
							   //'merchant_delivery_distance'=>$merchant_delivery_distance,
							   //'delivery_fee'=>$delivery_fee
							 ));
							} 
						} else
							{
								if( $_GET['range_to'] && $_GET['range_from'] )
								{
									$merchant_address = $val['restaurant_name'].", ".$val['street'].", ".$val['city'].", ".$val['state']." ".$val['post_code'];
									if($_SESSION['fq_search_address'])
									{  
										$var1 = Yii::app()->functions->getDistance($_SESSION['fq_search_address'], $merchant_address);	
									} else {
										$place = "Nehru Place, New Delhi, Delhi, India";
										$var1 = Yii::app()->functions->getDistance($place, $merchant_address);
									}
									 $var2 = round( ($var1->rows[0]->elements[0]->distance->value) / 1000 );
									 //echo rtrim($_GET['range_from'] , "Km -");	
									if( $var2 >= rtrim( $_GET['range_from'] , " Km -" ) && $var2 <= rtrim( $_GET['range_to'] , " Km" ))
									{
									 $this->renderPartial('/front/search-distance-list-1',array(
									   'data'=>$data,
									   'val'=>$val,
									   'merchant_id'=>$merchant_id,
									   'ratings'=>$ratings,
									   'distance_type'=>$distance_type,
									   'distance_type_orig'=>$distance_type_orig,
									   //'distance'=>$var2,
									   //'merchant_delivery_distance'=>$merchant_delivery_distance,
									   //'delivery_fee'=>$delivery_fee
									 ));
									}
								} else
								{
								 $this->renderPartial('/front/search-list-1',array(
								   'data'=>$data,
								   'val'=>$val,
								   'merchant_id'=>$merchant_id,
								   'ratings'=>$ratings,
								   'distance_type'=>$distance_type,
								   'distance_type_orig'=>$distance_type_orig,
								   //'distance'=>$var2,
								   //'merchant_delivery_distance'=>$merchant_delivery_distance,
								   //'delivery_fee'=>$delivery_fee
								 ));	
								}
							}
						/*else 
						{
							 $merchant_id=$val['merchant_id'];             
							 $ratings=Yii::app()->functions->getRatings($merchant_id);   
							 
							 /*get the distance from client address to merchant Address             
							 $distance_type=FunctionsV3::getMerchantDistanceType($merchant_id); 
							 $distance_type_orig=$distance_type;
							 
							 /*dump("c lat=>".$data['client']['lat']);         
							 dump("c lng=>".$data['client']['long']);	             
							 dump("m lat=>".$val['latitude']);
							 dump("c lng=>".$val['lontitude']);
							 
							   
							 $distance=FunctionsV3::getDistanceBetweenPlot(
								$data['client']['lat'],$data['client']['long'],
								$val['latitude'],$val['lontitude'],$distance_type
							 );      
													 
							 $distance_type_raw = $distance_type=="M"?"miles":"kilometers";
							 $distance_type = $distance_type=="M"?t("miles"):t("kilometers");
							 $distance_type_orig = $distance_type_orig=="M"?t("miles"):t("kilometers");
							 
							 if(!empty(FunctionsV3::$distance_type_result)){
								$distance_type_raw=FunctionsV3::$distance_type_result;
								$distance_type=t(FunctionsV3::$distance_type_result);
							 }
							 
							 $merchant_delivery_distance=getOption($merchant_id,'merchant_delivery_miles');             
							 
							 $delivery_fee=FunctionsV3::getMerchantDeliveryFee(
										  $merchant_id,
										  $val['delivery_charges'],
										  $distance,
										  $distance_type_raw);
							 ?>
							 
							 <?php 	             
							 if ( $display_type=="listview")
							 {
								 $this->renderPartial('/front/search-list-2',array(
								   'data'=>$data,
								   'val'=>$val,
								   'merchant_id'=>$merchant_id,
								   'ratings'=>$ratings,
								   'distance_type'=>$distance_type,
								   'distance_type_orig'=>$distance_type_orig,
								   //'distance'=>$var2,
								   //'merchant_delivery_distance'=>$merchant_delivery_distance,
								   //'delivery_fee'=>$delivery_fee
								 ));
							 } else {
								 $this->renderPartial('/front/search-list-1',array(
								   'data'=>$data,
								   'val'=>$val,
								   'merchant_id'=>$merchant_id,
								   'ratings'=>$ratings,
								   'distance_type'=>$distance_type,
								   'distance_type_orig'=>$distance_type_orig,
								   //'distance'=>$var2,
								   //'merchant_delivery_distance'=>$merchant_delivery_distance,
								   //'delivery_fee'=>$delivery_fee
								 ));
							}
						} *//* ?>
				              
	              <?php endforeach;?>     
              <?php else :?>     
              <p class="center top25 text-danger"><?php echo t("No results with your selected filters")?></p>
              <?php endif;?>
                                                   
             </div> <!--row-->                
             
             <div class="search-result-loader">
                <i></i>
                <p><?php echo t("Loading more restaurant...")?></p>
             </div> <!--search-result-loader-->
             
             <?php                         
             if (!isset($current_page_url)){
             	$current_page_url='';
             }
             if (!isset($current_page_link)){
             	$current_page_link='';
             }
             echo CHtml::hiddenField('current_page_url',$current_page_url);
             require_once('pagination.class.php'); 
             $attributes                 =   array();
			 $attributes['wrapper']      =   array('id'=>'pagination','class'=>'pagination');			 
			 $options                    =   array();
			 $options['attributes']      =   $attributes;
			 $options['items_per_page']  =   FunctionsV3::getPerPage();
			 $options['maxpages']        =   1;
			 $options['jumpers']=false;
			 $options['link_url']=$current_page_link.'&page=##ID##';			
			 $pagination =   new pagination( $data['total'] ,((isset($_GET['page'])) ? $_GET['page']:1),$options);		
			 $data   =   $pagination->render();
             ?>             
                    
         </div> <!--result-merchant-->
     
     </div> <!--col search-right-content-->
     
   </div> <!--row-->
  
  </div> <!--container-->
</div> <!--section-search-results-->

<style>
.filters_a{display:inline;}
.filters_checkbox {
    display: inline;
    float: left;
    width: 45px;
}
.check-filter-anch {
    display: block;
    float: left;
    margin-top: -6px;
    padding: 0;
    width: 80%;
}
.filter-box{float:left;width:100%;}
.filter-wrap{float:left;}
</style>

<script>

</script>
*/
?>
