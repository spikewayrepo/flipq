<section class="product-listing">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-xs-12">
        <p class="gray search-heading"><span>Best Restaurant In Delhi</span>(1280 Restaurants Found)</p>
        <p class="hidden-xs"><a id="filters_link1"><i class="fa fa-filter" aria-hidden="true"> Filters</i></a></p>
      </div>
    </div>
    <div class="row pad-xs-40">
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div1">
            <a class="status-open">Open</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a data-target="#dealsModal" data-toggle="modal"><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-o"></i>
              <i class="fa fa-star-o"></i>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button data-target="#etaModal" data-toggle="modal">Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <a href="description.php"><button>Book-Table</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div2">
            <a class="status-close">Close</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating-num">
              <a>3.5 / 5</a>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div3">
            <a class="status-open">Open</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating-img text-right">
              <img src="images/star-on.png">
              <img src="images/star-on.png">
              <img src="images/star-on.png">
              <img src="images/star-half.png">
              <img src="images/star-off.png">
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div4">
            <a class="status-close">Close</a>
            <div class="rating-other">
              <a>4.2</a>
            </div>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row pad-xs-40">
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div1">
            <a class="status-open">Open</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-o"></i>
              <i class="fa fa-star-o"></i>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div2">
            <a class="status-close">Close</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-o"></i>
              <i class="fa fa-star-o"></i>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div3">
            <a class="status-open">Open</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-o"></i>
              <i class="fa fa-star-o"></i>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div4">
            <a class="status-open">Open</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-o"></i>
              <i class="fa fa-star-o"></i>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row  pad-xs-40">
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div1">
            <a class="status-open">Open</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-o"></i>
              <i class="fa fa-star-o"></i>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div2">
            <a class="status-open">Open</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-o"></i>
              <i class="fa fa-star-o"></i>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div3">
            <a class="status-open">Open</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-o"></i>
              <i class="fa fa-star-o"></i>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="list-card">
          <div class="image-div4">
            <a class="status-open">Open</a>
          </div>
          <div class="list-card-body">
            <p>Restaurant Name</p>
            <p>Chinese / Indian / Thai / Italian / Japanese </p>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;xyz , Nehru Place , South Delhi , Delhi</p>
            <a><p class="deals"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>20 Deals</p></a>
            <p><span style="font-family:quick-bold;">&#x20b9;</span>&nbsp;&nbsp;2300 Cost For 2</p>
            <div class="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-o"></i>
              <i class="fa fa-star-o"></i>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button>Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <button>Book-Table</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>




<div class="flash-deals-link">
    <img class="img-responsive" src="images/fd.png">
</div>
<div class="flash-deals">
  <i class="fa fa-times close-flash"></i>
  <h3 class="text-center white">Flash Deals</h3>
  <p class="gray text-center">300 Deals Currently Running</p>
  <div class="row">
    <div class="col-sm-12">
      <div class="media">
        <div class="row">
          <div class="media-left col-sm-4 col-xs-4">
            <img src="images/img1.jpg" class="media-object">
          </div>
          <div class="media-body">
            <h5 class="media-heading">50% Off On All Drinks </h5>
            <p>In The Punjab</p>
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>
            <span>18 Deals</span>
            <p>Ending in :&nbsp;<span class="timer" data-seconds-left=6599 style="color:red;"></span></p>
            <button>Get a Code</button>
          </div>
        </div>
      </div>

      <div class="success-msg text-center">
        <h5>Your Deal Code is sent to 9890987678</h5>
        <p>You can avail your deal by showing your code to the restaurant.</p>
      </div>

    </div>

    <div class="col-sm-12">
      <div class="media">
        <div class="row">
          <div class="media-left col-sm-4 col-xs-4">
            <img src="images/img1.jpg" class="media-object">
          </div>
          <div class="media-body">
            <h5 class="media-heading">50% Off On All Drinks </h5>
            <p>In The Punjab</p>
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>
            <span>18 Deals</span>
            <p>Ending in :&nbsp;<span class="timer" data-seconds-left=6500 style="color:red;"></span></p>
            <button>Get a Code</button>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12">
      <div class="media">
        <div class="row">
          <div class="media-left col-sm-4 col-xs-4">
            <img src="images/img1.jpg" class="media-object">
          </div>
          <div class="media-body">
            <h5 class="media-heading">50% Off On All Drinks </h5>
            <p>In The Punjab</p>
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>
            <span>18 Deals</span>
            <p>Ending in :&nbsp;<span class="timer" data-seconds-left=6500 style="color:red;"></span></p>
            <button>Get a Code</button>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12">
      <div class="media">
        <div class="row">
          <div class="media-left col-sm-4 col-xs-4">
            <img src="images/img1.jpg" class="media-object">
          </div>
          <div class="media-body">
            <h5 class="media-heading">50% Off On All Drinks </h5>
            <p>In The Punjab</p>
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>
            <span>18 Deals</span>
            <p>Ending in :&nbsp;<span class="timer" data-seconds-left=6500 style="color:red;"></span></p>
            <button>Get a Code</button>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12">
      <div class="media">
        <div class="row">
          <div class="media-left col-sm-4 col-xs-4">
            <img src="images/img1.jpg" class="media-object">
          </div>
          <div class="media-body">
            <h5 class="media-heading">50% Off On All Drinks </h5>
            <p>In The Punjab</p>
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>
            <span>18 Deals</span>
            <p>Ending in :&nbsp;<span class="timer" data-seconds-left=6500 style="color:red;"></span></p>
            <button>Get a Code</button>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12">
      <div class="media">
        <div class="row">
          <div class="media-left col-sm-4 col-xs-4">
            <img src="images/img1.jpg" class="media-object">
          </div>
          <div class="media-body">
            <h5 class="media-heading">50% Off On All Drinks </h5>
            <p>In The Punjab</p>
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>
            <span>18 Deals</span>
            <p>Ending in :&nbsp;<span class="timer" data-seconds-left=6500 style="color:red;"></span></p>
            <button>Get a Code</button>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12">
      <div class="media">
        <div class="row">
          <div class="media-left col-sm-4 col-xs-4">
            <img src="images/img1.jpg" class="media-object">
          </div>
          <div class="media-body">
            <h5 class="media-heading">50% Off On All Drinks </h5>
            <p>In The Punjab</p>
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>
            <span>18 Deals</span>
            <p>Ending in :&nbsp;<span class="timer" data-seconds-left=6500 style="color:red;"></span></p>
            <button>Get a Code</button>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xs-12 text-center">
      <button class="btn-hover-orange">More</button>
    </div>
  </div>



  <!--<div class="container deal-count">
    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-6">
        <img src="images/img1.jpg">
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6">
        <img src="images/img1.jpg">
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6">
        <img src="images/img1.jpg">
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6">
        <img src="images/img1.jpg">
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6">
        <img src="images/img1.jpg">
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6">
        <img src="images/img1.jpg">
      </div>
    </div>
  </div>-->
</div>

<section class="filters">
    <div class="head">
      <div class="col-sm-4 col-xs-4 text-center">
        <a class="reset"><i class="fa fa-undo" aria-hidden="true"></i>Reset</a>
      </div>
      <div class="col-sm-5 col-xs-5 text-center">
        Filters
      </div>
      <div class="col-sm-3 col-xs-3 text-right">
        <a id="close_filters"><i class="fa fa-times"></i></a>
      </div>
    </div>
    <div class="filter">
      <form>
      <p class="filterlb"><a>Quick Filters</a></p>
      <div class="content" style="display:block;">
        <p><label for="rest">Restaurants</label><input type="checkbox" id="rest"></p>
        <p><label for="pub">Pub / Bar</label><input type="checkbox" id="pub"></p>
        <p><label for="mpub">Microbrewery Pub</label><input type="checkbox" id="mpub"></p>
        <p><label for="sbar">Sports Bar</label><input type="checkbox" id="sbar"></p>
      </div>

      <p class="filterlb"><a>Quick Filters</a></p>
      <div class="content">
        <p><label for="rest">Restaurants</label><input type="checkbox" id="rest"></p>
        <p><label for="pub">Pub / Bar</label><input type="checkbox" id="pub"></p>
        <p><label for="mpub">Microbrewery Pub</label><input type="checkbox" id="mpub"></p>
        <p><label for="sbar">Sports Bar</label><input type="checkbox" id="sbar"></p>
      </div>

      <p class="filterlb"><a>Quick Filters</a></p>
      <div class="content">
        <p><label for="rest">Restaurants</label><input type="checkbox" id="rest"></p>
        <p><label for="pub">Pub / Bar</label><input type="checkbox" id="pub"></p>
        <p><label for="mpub">Microbrewery Pub</label><input type="checkbox" id="mpub"></p>
        <p><label for="sbar">Sports Bar</label><input type="checkbox" id="sbar"></p>
      </div>

      <p class="filterlb"><a>Quick Filters</a></p>
      <div class="content">
        <p><label for="rest">Restaurants</label><input type="checkbox" id="rest"></p>
        <p><label for="pub">Pub / Bar</label><input type="checkbox" id="pub"></p>
        <p><label for="mpub">Microbrewery Pub</label><input type="checkbox" id="mpub"></p>
        <p><label for="sbar">Sports Bar</label><input type="checkbox" id="sbar"></p>
      </div>

      <p class="filterlb"><a>Quick Filters</a></p>
      <div class="content">
        <p><label for="rest">Restaurants</label><input type="checkbox" id="rest"></p>
        <p><label for="pub">Pub / Bar</label><input type="checkbox" id="pub"></p>
        <p><label for="mpub">Microbrewery Pub</label><input type="checkbox" id="mpub"></p>
        <p><label for="sbar">Sports Bar</label><input type="checkbox" id="sbar"></p>
      </div>

      <p class="filterlb"><a>Quick Filters</a></p>
      <div class="content">
        <p><label for="rest">Restaurants</label><input type="checkbox" id="rest"></p>
        <p><label for="pub">Pub / Bar</label><input type="checkbox" id="pub"></p>
        <p><label for="mpub">Microbrewery Pub</label><input type="checkbox" id="mpub"></p>
        <p><label for="sbar">Sports Bar</label><input type="checkbox" id="sbar"></p>
      </div>

      <p class="filterlb"><a>Quick Filters</a></p>
      <div class="content">
        <p><label for="rest">Restaurants</label><input type="checkbox" id="rest"></p>
        <p><label for="pub">Pub / Bar</label><input type="checkbox" id="pub"></p>
        <p><label for="mpub">Microbrewery Pub</label><input type="checkbox" id="mpub"></p>
        <p><label for="sbar">Sports Bar</label><input type="checkbox" id="sbar"></p>
      </div>

      <p class="filterlb"><a>Quick Filters</a></p>
      <div class="content">
        <p><input type="checkbox" id="rest"><label for="rest">Restaurants</label></p>
        <p><label for="pub">Pub / Bar</label><input type="checkbox" id="pub"></p>
        <p><label for="mpub">Microbrewery Pub</label><input type="checkbox" id="mpub"></p>
        <p><label for="sbar">Sports Bar</label><input type="checkbox" id="sbar"></p>
      </div>
      </form>
    </div>
</section>












<?php

 /*
$search_address=isset($_GET['s'])?$_GET['s']:'1';
$restaurant_type=isset($_GET['restaurant_type'])?$_GET['restaurant_type']:'';

if (isset($_GET['s'])){
	$search_address=$_GET['s'];
}
$this->renderPartial('/front/search-header',array(
   'search_address'=>$search_address,
   'total'=>$data['total']
));
print_r($data);
exit;
?>

<?php 
$this->renderPartial('/front/order-progress-bar',array(
   'step'=>2,
   'show_bar'=>true
));

echo CHtml::hiddenField('clien_lat',$data['client']['lat']);
echo CHtml::hiddenField('clien_long',$data['client']['long']);
?>

<div class="search-map-results">  
</div> <!--search-map-results-->

<div class="sections section-search-results">

  <div class="container">

   <div class="row">
   
     <div class="col-md-3 border search-left-content" id="mobile-search-filter">
       
        <?php if ( $enabled_search_map=="yes"):?>
        <a href="javascript:;" class="search-view-map green-button block center upper rounded">
        <?php echo t("View by map")?>
        </a>
        <?php endif;?>
        
        <div class="filter-wrap rounded2 <?php echo $enabled_search_map==""?"no-marin-top":""; ?>">
                
          <button type="button" class="close modal-close-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>  
        
           <p class="bold"><?php echo t("Filters")?></p>
           
           
           <!--FILTER MERCHANT NAME-->       
           <?php if (!empty($restaurant_name)):?>                      
           <a href="<?php echo FunctionsV3::clearSearchParams('restaurant_name')?>">[<?php echo t("Clear")?>]</a>
           <?php endif;?>    
           <div class="filter-box">
	           <a href="javascript:;">	             
	             <span>
	             <i class="<?php echo $fc==2?"ion-ios-arrow-thin-down":'ion-ios-arrow-thin-right'?>"></i>
	             <?php echo t("Search by name")?>
	             </span>   
	             <b></b>
	           </a>
	           <ul class="<?php echo $fc==2?"hide":''?>">
				  <li>
	              <form method="POST" onsubmit="return research_merchant();">
		              <div class="search-input-wraps rounded30">
		              <div class="row">
				        <div class="col-md-10 col-xs-10">
				        <?php echo CHtml::textField('restaurant_name',$restaurant_name,array(
				          'required'=>true,
				          'placeholder'=>t("enter restaurant name")
				        ))?>
				        </div>        
				        <div class="col-md-2 relative col-xs-2 ">
				          <button type="submit"><i class="fa fa-search"></i></button>         
				        </div>
				     </div>
			     </div>
			     </form>
	              </li>
	           </ul>
           </div> <!--filter-box-->
           <!--END FILTER MERCHANT NAME-->           
           
           <!--FILTER DELIVERY FEE-->           
           <div class="filter-box">
	           <a href="javascript:;">	             
	             <span>
	             <i class="<?php echo $fc==2?"ion-ios-arrow-thin-down":'ion-ios-arrow-thin-right'?>"></i>
	             <?php echo t("Restaurant Type")?>
	             </span>   
	             <b></b>
	           </a>
	            <ul class="<?php echo $fc==2?"hide":''?>">
	            
	              <?php 
		          #echo CHtml::checkBox('filter_by[]',false,array(
		          #'value'=>'free-delivery',
		          #'class'=>"filter_promo icheck"
		          #));
		          ?>

  <li><div class="icheckbox_minimal" style="position: relative;"><input type="checkbox" id="filter_restaurant_type" name="filter_restaurant_type[]" class="filter_by icheck filter_filter_restaurant_type" value="pub/bar" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>Pub/Bar </li>

  <li><div class="icheckbox_minimal" style="position: relative;"><input type="checkbox" id="filter_restaurant_type" name="filter_restaurant_type[]" class="filter_by icheck filter_filter_restaurant_type" value="lounge" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>Lounge </li>

  <li><div class="icheckbox_minimal" style="position: relative;"><input type="checkbox" id="filter_restaurant_type" name="filter_restaurant_type[]" class="filter_by icheck filter_filter_restaurant_type" value="sports_bar" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>Sports Bar</li>

  <li><div class="icheckbox_minimal" style="position: relative;"><input type="checkbox" id="filter_restaurant_type" name="filter_restaurant_type[]" class="filter_by icheck filter_filter_restaurant_type" value="Cafe" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>Cafe</li>

  <li><div class="icheckbox_minimal" style="position: relative;"><input type="checkbox" id="filter_restaurant_type" name="filter_restaurant_type[]" class="filter_by icheck filter_filter_restaurant_type" value="casual_dining" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>Casual Dining</li>

  <li><div class="icheckbox_minimal" style="position: relative;"><input type="checkbox" id="filter_restaurant_type" name="filter_restaurant_type[]" class="filter_by icheck filter_filter_restaurant_type" value="fine_dining" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>Fine Dining</li>

  <li><div class="icheckbox_minimal" style="position: relative;"><input type="checkbox" id="filter_restaurant_type" name="filter_restaurant_type[]" class="filter_by icheck filter_filter_restaurant_type" value="microbrewery_pub" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>Microbrewery Pub</li>



	              <?php #echo t("Free Delivery")?>
	             
	           </ul>
           </div> <!--filter-box-->
           <!--END FILTER DELIVERY FEE-->
           
           <!--FILTER DELIVERY -->
           <?php if (false /*!empty($filter_delivery_type)*//*):?>                      
           <a href="<?php echo FunctionsV3::clearSearchParams('filter_delivery_type')?>">[<?php echo t("Clear")?>]</a>
           <?php endif;?>
           <?php if (false/* $services=Yii::app()->functions->Services() *//*):?>
           <div class="filter-box">
	           <a href="javascript:;">	             
	             <span>
	             <i class="<?php echo $fc==2?"ion-ios-arrow-thin-down":'ion-ios-arrow-thin-right'?>"></i>
	             <?php echo t("By Delivery")?>
	             </span>   
	             <b></b>
	           </a>
	           <ul class="<?php echo $fc==2?"hide":''?>">
	             <?php foreach ($services as $key=> $val):?>
	              <li>	           	              
	              <?php 
		           echo CHtml::radioButton('filter_delivery_type',
		           $filter_delivery_type==$key?true:false
		           ,array(
		          'value'=>$key,
		          'class'=>"filter_by filter_delivery_type icheck"
		          ));
		          ?>
		          <?php echo $val;?>   
	              </li>
	             <?php endforeach;?> 
	           </ul>
           </div> <!--filter-box-->
           <?php endif;?>
           <!--END FILTER DELIVERY -->
           
           <!--FILTER CUISINE-->
           <?php if (!empty($filter_cuisine)):?>                      
           <a href="<?php echo FunctionsV3::clearSearchParams('filter_cuisine')?>">[<?php echo t("Clear")?>]</a>
           <?php endif;?>
           <?php if ( $cuisine=Yii::app()->functions->Cuisine(false)):?>
           <div class="filter-box">
	           <a href="javascript:;">	             
	             <span>
	             <i class="<?php echo $fc==2?"ion-ios-arrow-thin-down":'ion-ios-arrow-thin-right'?>"></i>
	             <?php echo t("By Cuisines")?>
	             </span>   
	             <b></b>
	           </a>
	            <ul class="<?php echo $fc==2?"hide":''?>">
	             <?php foreach ($cuisine as $val): ?>
	              <li>
		           <?php 
		           $cuisine_json['cuisine_name_trans']=!empty($val['cuisine_name_trans'])?
	    		   json_decode($val['cuisine_name_trans'],true):'';
	    		   
		           echo CHtml::checkBox('filter_cuisine[]',
		           in_array($val['cuisine_id'],(array)$filter_cuisine)?true:false
		           ,array(
		           'value'=>$val['cuisine_id'],
		           'class'=>"filter_by icheck filter_cuisine"
		           ));
		          ?>
	              <?php echo qTranslate($val['cuisine_name'],'cuisine_name',$cuisine_json)?>
	              </li>
	             <?php endforeach;?> 
	           </ul>
           </div> <!--filter-box-->
           <?php endif;?>
           <!--END FILTER CUISINE-->
           
           
           <!--MINIUM DELIVERY FEE-->           
           <?php if (false/*!empty($filter_minimum)*//*):?>                      
           <a href="<?php echo FunctionsV3::clearSearchParams('filter_minimum')?>">[<?php echo t("Clear")?>]</a>
           <?php endif;?>
           <?php if ( false/*$minimum_list=FunctionsV3::minimumDeliveryFee()*//*):?>
           <div class="filter-box">
	           <a href="javascript:;">	             
	             <span>
	             <i class="<?php echo $fc==2?"ion-ios-arrow-thin-down":'ion-ios-arrow-thin-right'?>"></i>
	             <?php echo t("Minimum Delivery")?>
	             </span>   
	             <b></b>
	           </a>
	            <ul class="<?php echo $fc==2?"hide":''?>">
	             <?php foreach ($minimum_list as $key=>$val):?>
	              <li>
		           <?php 
		          echo CHtml::radioButton('filter_minimum[]',
		          $filter_minimum==$key?true:false
		          ,array(
		          'value'=>$key,
		          'class'=>"filter_by_radio filter_minimum icheck"
		          ));
		          ?>
	              <?php echo $val;?>
	              </li>
	             <?php endforeach;?> 
	           </ul>
           </div> <!--filter-box-->
           <?php endif;?>
           <!--END MINIUM DELIVERY FEE-->
           
        </div> <!--filter-wrap-->
        
     </div> <!--col search-left-content-->
     
     <div class="col-md-9 border search-right-content">
          
     <?php echo CHtml::hiddenField('sort_filter',$sort_filter)?>
     <?php echo CHtml::hiddenField('display_type',$display_type)?>     
     
         <div class="sort-wrap">
           <div class="row">           
              <div class="col-md-6 col-xs-6 border ">	           
	           <?php 
	           $filter_list=array(
	             'restaurant_name'=>t("Name"),
	             'ratings'=>t("Rating"),
	             'minimum_order'=>t("Minimum"),
	             'distance'=>t("Distance")
	           );
	           if (isset($_GET['st'])){
	           	   unset($filter_list['distance']);
	           }
	           echo CHtml::dropDownList('sort-results',$sort_filter,$filter_list,array(
	             'class'=>"sort-results selectpicker",
	             'title'=>t("Sort By")
	           ));
	           ?>
              </div> <!--col-->
              <div class="col-md-6 col-xs-6 border">                
               
                          
                <a href="<?php echo FunctionsV3::clearSearchParams('','display_type=listview')?>" 
	           class="display-type orange-button block center rounded 
	           <?php echo $display_type=="gridview"?'inactive':''?>" 
		          data-type="listview">
                <i class="fa fa-th-list"></i>
                </a>
                
                <a href="<?php echo FunctionsV3::clearSearchParams('','display_type=gridview')?>" 
		          class="display-type orange-button block center rounded mr10px 
	             <?php echo $display_type=="listview"?'inactive':''?>" 
		          data-type="gridview">
                <i class="fa fa-th-large"></i>
                </a>           
                
                <a href="javascript:;" id="mobile-filter-handle" class="orange-button block center rounded mr10px">
                  <i class="fa fa-filter"></i>
                </a>    
                
                <?php if ( $enabled_search_map=="yes"):?>
                <a href="javascript:;" id="mobile-viewmap-handle" class="orange-button block center rounded mr10px">
                  <i class="ion-ios-location"></i>
                </a>    
                <?php endif;?>
                
                <div class="clear"></div>
                
              </div>
           </div> <!--row-->
         </div>  <!--sort-wrap-->  
         
         
         <!--MERCHANT LIST -->
                  
         <div class="result-merchant">
             <div class="row infinite-container" style="margin-right: 0.1%">
             
             <?php 
					//print_r($data);
					if ($data):?>
	             <?php foreach ($data['list'] as $val):?>
	             <?php
					
					$merchant_address = $val['restaurant_name'].$val['street'].", ".$val['city'].", ".$val['state']." ".$val['post_code'];
					if($_SESSION['fq_search_address']){
						$getdistance = Yii::app()->functions->getDistance($_SESSION['fq_search_address'], $merchant_address);
					} else {
						$place = "Nehru Place, New Delhi, Delhi, India";
						$getdistance = Yii::app()->functions->getDistance($place, $merchant_address);
					}
					$how_far = round($getdistance->rows[0]->elements[0]->distance->value/1000);
					if($how_far <= $_GET['range']){
					 $merchant_id=$val['merchant_id'];             
					 $ratings=Yii::app()->functions->getRatings($merchant_id);   
					 
					 /*get the distance from client address to merchant Address*//*             
					 $distance_type=FunctionsV3::getMerchantDistanceType($merchant_id); 
					 $distance_type_orig=$distance_type;
					 
					 /*dump("c lat=>".$data['client']['lat']);         
					 dump("c lng=>".$data['client']['long']);	             
					 dump("m lat=>".$val['latitude']);
					 dump("c lng=>".$val['lontitude']);*/
					 /*
					   
					 $distance=FunctionsV3::getDistanceBetweenPlot(
						$data['client']['lat'],$data['client']['long'],
						$val['latitude'],$val['lontitude'],$distance_type
					 );      
											 
					 $distance_type_raw = $distance_type=="M"?"miles":"kilometers";
					 $distance_type = $distance_type=="M"?t("miles"):t("kilometers");
					 $distance_type_orig = $distance_type_orig=="M"?t("miles"):t("kilometers");
					 
					 if(!empty(FunctionsV3::$distance_type_result)){
						$distance_type_raw=FunctionsV3::$distance_type_result;
						$distance_type=t(FunctionsV3::$distance_type_result);
					 }
					 
					 $merchant_delivery_distance=getOption($merchant_id,'merchant_delivery_miles');             
					 
					 $delivery_fee=FunctionsV3::getMerchantDeliveryFee(
								  $merchant_id,
								  $val['delivery_charges'],
								  $distance,
								  $distance_type_raw);
					 ?>
					 
					 <?php 	             
					 if ( $display_type=="listview"){
						 $this->renderPartial('/front/search-list-2',array(
						   'data'=>$data,
						   'val'=>$val,
						   'merchant_id'=>$merchant_id,
						   'ratings'=>$ratings,
						   //'distance_type'=>$distance_type,
						   //'distance_type_orig'=>$distance_type_orig,
						   //'distance'=>$distance,
						   //'merchant_delivery_distance'=>$merchant_delivery_distance,
						   //'delivery_fee'=>$delivery_fee
						 ));
					 } else {
						 $this->renderPartial('/front/search-list-1',array(
						   'data'=>$data,
						   'val'=>$val,
						   'merchant_id'=>$merchant_id,
						   'ratings'=>$ratings,
						   //'distance_type'=>$distance_type,
						   //'distance_type_orig'=>$distance_type_orig,
						   //'distance'=>$distance,
						   //'merchant_delivery_distance'=>$merchant_delivery_distance,
						   //'delivery_fee'=>$delivery_fee
						 ));
					 }
					}
					?>
								  
					  <?php endforeach;?>     
				  <?php else :?>     
				  <p class="center top25 text-danger"><?php echo t("No results with your selected filters")?></p>
				  <?php endif;?>
													   
				 </div> <!--row-->                
             
             <div class="search-result-loader">
                <i></i>
                <p><?php echo t("Loading more restaurant...")?></p>
             </div> <!--search-result-loader-->
             
             <?php                         
             if (!isset($current_page_url)){
             	$current_page_url='';
             }
             if (!isset($current_page_link)){
             	$current_page_link='';
             }
             echo CHtml::hiddenField('current_page_url',$current_page_url);
             require_once('pagination.class.php'); 
             $attributes                 =   array();
			 $attributes['wrapper']      =   array('id'=>'pagination','class'=>'pagination');			 
			 $options                    =   array();
			 $options['attributes']      =   $attributes;
			 $options['items_per_page']  =   FunctionsV3::getPerPage();
			 $options['maxpages']        =   1;
			 $options['jumpers']=false;
			 $options['link_url']=$current_page_link.'&page=##ID##';			
			 $pagination =   new pagination( $data['total'] ,((isset($_GET['page'])) ? $_GET['page']:1),$options);		
			 $data   =   $pagination->render();
             ?>             
                    
         </div> <!--result-merchant-->
     
     </div> <!--col search-right-content-->
     
   </div> <!--row-->
  
  </div> <!--container-->
</div> <!--section-search-results-->
 */?>