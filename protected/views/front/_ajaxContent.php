
<div class="container">

<?php
	if ($data){

                $count = 0;
 		echo '<div class="row pad-xs-40" id="restralisting">';
                foreach ($data['list'] as $val)
                {
			$merchant_id = $val['merchant_id']; 
			$ratings=$val['ratings'];
			if($count%4==0 && $count!=0 )
	                echo '</div><div class="row pad-xs-40" id="restralisting">';

?>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<div class="list-card">
<?php
				$logoo = FunctionsV3::getMerchantLogo($merchant_id);
?>
<?php $urlm = Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug']);?>

					<div class="list-card">

						<div class="image-div1" onclick="window.location='<?php print $urlm; ?>'" style="background: rgba(0,0,0,0) url('<?php print $logoo ?>') no-repeat scroll center center / cover;">
						<?php echo FunctionsV3::merchantOpenTag($merchant_id)?>
						</div>
          					<div class="list-card-body">
   							<p><a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'].'?booktable=true')?>"><?php echo $val['restaurant_name']; ?></a></p>

<p>
<?php
$displaycuision = FunctionsV3::displayCuisine($val['cuisine']);
$cui_arr = explode("/",$displaycuision );

	if(count($cui_arr) > 5)
		echo "Multicuisine";
	else
		echo "".($val['cuisine']!='null')?$displaycuision:'No Cuisine'."";


									$deals=FunctionsV3::getNoofdeals($merchant_id);
?>

</p>

<p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?php echo ($val['location'])?$val['location']:'Loading...' ?></p>
            <a  onClick=getDealsinfo(this) rel="<?php print $merchant_id ?>" ><p class="deals"><svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" id="Layer_1"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z" class="cls-1"/><circle transform="translate(-0.02 0.06) rotate(-0.08)" r="3.83" cy="12.47" cx="37.99" class="cls-2"/></svg><?php echo ($deals)?$deals." Deals":'No Deals' ?></p></a>
            <?php if((int)$val['cost_for_two']){ ?><p><span style="font-family:quick-bold;">₹</span>&nbsp;&nbsp;<?php print (int)$val['cost_for_two'] ?> Cost For 2</p><?php }else{ ?><p> Not Available </p> <?php } ?>


   <div class="rating">

<?php  FunctionsV3::displayrate($ratings['ratings']) ?>


            </div>




                                                </div>


 <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>"><button data-toggle="modal" data-target="#etaModal">Pre-Order</button></a>
              </div>
              <div class="col-sm-6 col-xs-6">
                <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>"><button>Book-Table</button></a>
              </div>
            </div>
          </div>

                                        </div>



                                </div>


                        </div>

<?php
$count++;
                }

echo "</div>";
       }
?>

</div>
