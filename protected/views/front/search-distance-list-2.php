<div id="search-listgrid" class="row infinite-container infinite-item <?php echo $delivery_fee!=true?'free-wrap':'non-free'; ?>" style="margin-bottom: 3%; margin-left: 0%; margin-right:0%;">
    <div class="inner list-view">
    
    <?php 
			//print_r($val);
			if ( $val['is_sponsored']==2):?>
    <div class="ribbon"><span><?php echo t("Sponsored")?></span></div>
    <?php endif;?>
    
    <?php if ($offer=FunctionsV3::getOffersByMerchant($merchant_id)):?>
    <div class="ribbon-offer"><span><?php echo $offer;?></span></div>
    <?php endif;?>
    
    <div class="row">
	    <div class="col-md-2 border ">
	     <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>">
	      <img class="logo-small"src="<?php echo FunctionsV3::getMerchantLogo($merchant_id);?>">
	     </a>
	     <?php /* echo FunctionsV3::displayServicesList($val['service'])?>    
	     <?php FunctionsV3::displayCashAvailable($merchant_id) */ ?>      
	    </div> <!--col-->
	    
	    <div class="col-md-10 border">
	     
			<div class="col-sm-7 col-md-7 col-xs-12">
				<h2><?php echo clearString($val['restaurant_name'])?></h2>
			</div>
			
			<div class="col-sm-5 col-md-5 col-xs-12 octag">
	            <?php echo FunctionsV3::merchantOpenTag($merchant_id)?>                
	        </div>
			
			<div class="col-sm-8 col-md-8 col-xs-12">
				<p><span class="fa fa-map-marker" style="color: #fff;"></span><?php echo "  ".$val['merchant_address'];?></p>
				<p class="cuisine">
					<?php echo FunctionsV3::displayCuisine($val['cuisine']);?>
				</p>
				<p><?php $deals=FunctionsV3::getNoofdeals($merchant_id);
						if($deals)
						{
							echo  "No. of Deals: ".$deals;
						}else {
							echo "No. of Deals: 0";
						} 
						
						?>
				</p>
				<p> <?php echo "Cost for Two: Rs. ".$val['cost_for_two']."(approx.)"; ?>
				</p>
			</div>
			
			<div class="col-sm-4 col-md-4 col-xs-12 text-right">
				<p><a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'].'?preorder=true')?>" 
					class="orange-button rounded3">
					<?php echo t("Pre-Order");?> 
				</a></p>

				<p><a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'].'?booktable=true')?>" 
					class="orange-button rounded3">
					<?php echo t("Book Table");?> 
				</a></p>	
				
				<p>
				    <div class="rating-stars" style="margin-top: 2%" data-score="<?php echo $ratings['ratings']?>"></div>
				</p>
				
			</div>
			<!--<div class="mytable menu">
	         <div class="mycol">
	            <div class="rating-stars" data-score="<?php echo $ratings['ratings']?>"></div>   
	         </div>
	         <div class="mycol">
	            <p><?php echo $ratings['votes']." ".t("Reviews")?></p>
	         </div>
	         
	         
	         <div class="mycol">
	          <p><?php echo t("Minimum Order").": ".FunctionsV3::prettyPrice($val['minimum_order'])?></p>
	         </div>
	         
	       </div> <!--mytable-->
	       
	       <!--<h2><?php echo clearString($val['restaurant_name'])?></h2>-->
	       <!--<p class="merchant-address concat-text"><?php echo $val['merchant_address']?></p>-->   
	       	       	       
	       <!--<p class="cuisine">
           <?php echo FunctionsV3::displayCuisine($val['cuisine']);?>
           </p>-->                
                                                       
           <!--<p>
	        <?php /*
	        if ($distance){
	        	echo t("Distance").": ".number_format($distance,1)." $distance_type";
	        } else echo  t("Distance").": ".t("not available");
	        ?>
	        </p>
	        
	        <?php /*if($val['service']!=3):?>
	        <p><?php echo t("Delivery Est")?>: <?php echo FunctionsV3::getDeliveryEstimation($merchant_id)?></p>
	        <?php endif;?>
	        
	        <p>
	        <?php 
	        if($val['service']!=3){
		        if (!empty($merchant_delivery_distance)){
		        	echo t("Delivery Distance").": ".$merchant_delivery_distance." $distance_type_orig";
		        } else echo  t("Delivery Distance").": ".t("not available");
	        }
	        ?>
	        </p>
	                                
	        <p>
	        <?php 
	        if($val['service']!=3){
		        if ($delivery_fee){
		             echo t("Delivery Fee").": ".FunctionsV3::prettyPrice($delivery_fee);
		        } else echo  t("Delivery Fee").": ".t("Free Delivery");
	        }
	        ?>
	        </p>
	        
	        <p class="top15"><?php echo FunctionsV3::getFreeDeliveryTag($merchant_id)*/ ?></p>-->
	    
	    </div>


		<!--col-->
	</div>
    </div> <!--row-->
    
    </div> <!--inner-->

   