
<div class="modal fade" id="etaModal" tabindex="-1" role="dialog" aria-labelledby="etaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-content login-modal">
          <div class="modal-header login-modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="loginModalLabel">Expected Time of Arrival (ETA)</h4>
          </div>
          <div class="modal-body">
            <div class="text-center row">
              <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                  &nbsp;&nbsp;
                  <form method="post" action="?prebook=1">
                  <div class="form-group">
                      <div class="input-group" style="margin-bottom:15px;">
                          <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                          <input id="etaBookingDate" onclick="this.select()" readonly type="text" name="bookdate" class="form-control" placeholder="ETA Date">
                      </div>

                      <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
                          <!-- <input id="etaBookingTime" type="text" name="booktime" class="form-control" placeholder="ETA Time"> -->
                         
                          <select type="text" id="eta" class="form-control" name="eta" placeholder="ETA">
           <option value="00:00">12:00 AM</option>
                                   <option value="00:30">12:30 AM</option>
                                   <option value="01:00">01:00 AM</option>
                                   <option value="01:30">01:30 AM</option>
                                   <option value="02:00">02:00 AM</option>
                                   <option value="02:30">02:30 AM</option>
                                   <option value="03:00">03:00 AM</option>
                                   <option value="03:30">03:30 AM</option>
                                   <option value="04:00">04:00 AM</option>
                                   <option value="04:30">04:30 AM</option>
                                   <option value="05:00">05:00 AM</option>
                                   <option value="05:30">05:30 AM</option>
                                   <option value="06:00">06:00 AM</option>
                                   <option value="06:30">06:30 AM</option>
                                   <option value="07:00">07:00 AM</option>
                                   <option value="07:30">07:30 AM</option>
                                   <option value="08:00">08:00 AM</option>
                                   <option value="08:30">08:30 AM</option>
                                   <option value="09:00">09:00 AM</option>
                                   <option value="09:30">09:30 AM</option>
                                   <option value="10:00">10:00 AM</option>
                                   <option value="10:30">10:30 AM</option>
                                   <option value="11:00">11:00 AM</option>
                                   <option value="11:30">11:30 AM</option>
                                   <option value="12:00">12:00 PM</option>
                                   <option value="12:30">12:30 PM</option>
                                   <option value="13:00">01:00 PM</option>
                                   <option value="13:30">01:30 PM</option>
                                   <option value="14:00">02:00 PM</option>
                                   <option value="14:30">02:30 PM</option>
                                   <option value="15:00">03:00 PM</option>
                                   <option value="15:30">03:30 PM</option>
                                   <option value="16:00">04:00 PM</option>
                                   <option value="16:30">04:30 PM</option>
                                   <option value="17:00">05:00 PM</option>
                                   <option value="17:30">05:30 PM</option>
                                   <option value="18:00">06:00 PM</option>
                                   <option value="18:30">06:30 PM</option>
                                   <option value="19:00">07:00 PM</option>
                                   <option value="19:30">07:30 PM</option>
                                   <option value="20:00">08:00 PM</option>
                                   <option value="20:30">08:30 PM</option>
                                   <option value="21:00">09:00 PM</option>
                                   <option value="21:30">09:30 PM</option>
                                   <option value="22:00">10:00 PM</option>
                                   <option value="22:30">10:30 PM</option>
                                   <option value="23:00">11:00 PM</option>
                                   <option value="23:30">11:30 PM</option>
          </select>
                      </div>

                      <span class="help-block has-error" id="email-error"></span>
                    </div>
                    <button type="submit" class="btn btn-block bt-login">Submit</button>
                    <div class="clearfix"></div>
                </form>
              </div>
            </div>
            </div>
        </div>
     </div>
  </div>

