
<div class="box-grey rounded merchant-opening-hours" style="margin-top:0px;">


<?php if ( $res=FunctionsV3::getMerchantOpeningHours($merchant_id)):?>
<?php foreach ($res as $val):?>
   <div class="row">
      <div class="col-md-3 col-xs-3 capitalize">
      <?php echo t($val['day'])?></div>
      <?php if($val['open_text'] != ""){ ?>
      <div class="col-md-6 col-xs-6 capitalize"><?php echo $val['hours']?></div>
      <div class="col-md-3 col-xs-3 capitalize"><?php echo $val['open_text']?></div>
      <?php } else{ ?>
      <div class="col-md-9 col-xs-9 capitalize"><?php echo $val['hours']?></div>
      <?php } ?>
   </div>
<?php endforeach;?>
<?php else :?>
<p class="text-danger"><?php echo t("Sorry no data available.")?></p>
<?php endif;?>

</div> <!--box-grey-->