<!--<script src="Scripts/bootstrap-slider.js" language="javascript" type="text/javascript"></script>-->
<style>
	.easy-autocomplete{
		width: 100% !important;
	}
</style>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!--<link rel="stylesheet" href="/resources/demos/style.css">-->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="container search-wraps advance-search">

 <input type="hidden" name="find_restaurant_by_address" 
id="find_restaurant_by_address" value="<?php echo $home_search_text?>" >

  <ul class="search-menu hide">
  
   <?php if ($theme_search_merchant_address!=2):?> 
   <li><a href="javascript:;" class="selected byaddress" data-tab="tab-byaddress"><i></i></a></li>
   <?php endif;?>
   
   <?php if ($theme_search_merchant_name!=2):?>
   <li><a href="javascript:;" class="byname" data-tab="tab-byname"><i></i></a></li>
   <?php endif;?>
   
   <?php if ($theme_search_street_name!=2):?>
   <li><a href="javascript:;" class="bystreet" data-tab="tab-bystreet"><i></i></a></li>
   <?php endif;?>
   
   <?php if ($theme_search_cuisine!=2):?>
   <li><a href="javascript:;" class="bycuisine" data-tab="tab-bycuisine"><i></i></a></li>
   <?php endif;?>
   
   <?php if ($theme_search_foodname!=2):?>
   <li><a href="javascript:;" class="byfood" data-tab="tab-byfood"><i></i></a></li>   
   <?php endif;?>
  </ul>
  
  <!--<div class="border mobile-search-menu mytable">    
    
     <?php if ($theme_search_merchant_address!=2):?> 
     <a href="javascript:;" class="mycol selected byaddress" data-tab="tab-byaddress" ><i class="ion-record"></i></a>
     <?php endif;?>
     
     <?php if ($theme_search_merchant_name!=2):?>
     <a href="javascript:;" class="mycol byname" data-tab="tab-byname" ><i class="ion-record"></i></a>
     <?php endif;?>
     
     <?php if ($theme_search_street_name!=2):?>
     <a href="javascript:;" class="mycol bystreet" data-tab="tab-bystreet" ><i class="ion-record"></i></a>
     <?php endif;?>
     
     <?php if ($theme_search_cuisine!=2):?>
     <a href="javascript:;" class="mycol bycuisine" data-tab="tab-bycuisine"><i class="ion-record"></i></a>
     <?php endif;?>
     
     <?php if ($theme_search_foodname!=2):?>
     <a href="javascript:;" class="mycol byfood" data-tab="tab-byfood"><i class="ion-record"></i></a>
     <?php endif;?>
  </div> <!--end row-->
  
  <h1 class="col-sm-12 home-search-text text-center" style="margin-top:10%;margin-bottom:3%;"><?php echo $home_search_text;?></h1>
  <form method="get" class="tab-byaddress forms-search" id="forms-search" action="<?php echo Yii::app()->createUrl('store/searchresult')?>">
     <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 inputgrp" style="padding-bottom: 1%;">
		<div class="input-group" style="box-shadow: 0px 2px 6px;">
			<div class="input-group-btn search-panel">
				<button data-toggle="modal" data-target="#myModal5" class="btn btn-default dropdown-toggle hidden-xs srchpnl" style="background-color: #fff; color: #000;   border-color: #fff; " type="button" id="search-btn" onclick="geolocate();">
					<span id="search_concept"><span class="fa fa-location-arrow" id="mapmaker"></span>
						Search <span class="hidden-xs">City<br></span> 
					</span>
				</button>
			</div>
		  
			<input placeholder="Search Pubs/Bars, DJ Restaurant & more..." style="border-radius:0px;" class="form-control form search search-btn search_resto_name" value="" id="restro" type="text" name="restro" required>
			
			<span class="input-group-btn">
				<button type="submit" class="btn btn-default" id="search-logo"><span class="glyphicon glyphicon-search glysrch"></span></button>
			</span>
		</div> 
		<br> 
		<br> 
	</form>	
	<form action="/store/home">		
		<div class="col-sm-6 col-sm-offset-2 col-lg-6 col-lg-offset-2 col-md-6 col-md-offset-2 col-xs-10">
			
			<div class="row">
				<!--<label for="amount">Distance range:</label>-->
				<input type="text" id="amount_from" name="range_from" readonly>
				<input type="text" id="amount_to" name="range_to" readonly>
			</div>
			<div id="slider-range"></div>
			<!--<div class="col-sm-6 col-xs-12 range_slider">
				<!--<section class="range-slider">
				 <span class="rangeValues" style="color: #fff; font-weight: bold">0-15 </span><span style="color:#fff; font-weight: bold;"> Km</span>
				  <input name="range_from" id="range_from" value="1" min="1" max="15" step="0.5" type="range">
				  <input name="range_to" id="range_to" value="15" min="1" max="15" step="0.5" type="range">
				</section>
			</div>-->
		</div>
		<div class="col-sm-2 col-lg-2 col-md-2 col-xs-2">
			<button type="submit" class="btn-circle btn-default center-block"><i class="fa fa-search"></i></button>
		</div>
		
  </div>
  </form>
  
  
  
  <?php $_SESSION['profile_page']="Restaurant"; ?>
 
 <!--<div class="search-input-wraps">
     <div class="row">
        <div class="col-sm-11 col-xs-10">
        <?php /* echo CHtml::textField('restro',$kr_search_restro,array(
         'placeholder'=>"Search Restro near by You",
         'class'=>"search_resto_name",
         'required'=>true
        ))*/ ?>       
        </div>        
        <div class="col-sm-1 col-xs-2 relative">
          <button type="submit"><i class="ion-ios-search"></i></button>         
        </div>
     </div>
  </div> <!--search-input-wrap-->
  
	
      
  <?php /*<form method="GET" class="tab-byname forms-search" id="forms-search" action="<?php echo Yii::app()->createUrl('store/searchresult')?>">
  <?php echo CHtml::hiddenField('st',$kr_search_adrress,array('class'=>"st"));	?>
  <div class="search-input-wraps rounded30">
     <div class="row">
        <div class="col-sm-11 col-xs-10">
        <?php echo CHtml::textField('restaurant-name','',array(
         'placeholder'=>t("Restaurant name"),
         'required'=>true,
         'class'=>"search-field search_resto_name"
        ))?>        
        </div>        
        <div class="col-sm-1 col-xs-2 relative">
          <button type="submit"><i class="ion-ios-search"></i></button>         
        </div>
     </div>
  </div> <!--search-input-wrap-->
  </form>  
  
  <form method="GET" class="tab-bystreet forms-search" id="forms-search" action="<?php echo Yii::app()->createUrl('store/searchresult')?>">
  <?php echo CHtml::hiddenField('st',$kr_search_adrress,array('class'=>"st"));	?>
  <div class="search-input-wraps rounded30">
     <div class="row">
        <div class="col-sm-11 col-xs-10">
        <?php echo CHtml::textField('street-name','',array(
         'placeholder'=>t("Street name"),
         'required'=>true,
         'class'=>"search-field street_name"
        ))?>        
        </div>        
        <div class="col-sm-1 col-xs-2 relative">
          <button type="submit"><i class="ion-ios-search"></i></button>         
        </div>
     </div>
  </div> <!--search-input-wrap-->
  </form>    
  
  <form method="GET" class="tab-bycuisine forms-search" id="forms-search" action="<?php echo Yii::app()->createUrl('store/searchresult')?>">
  <?php echo CHtml::hiddenField('st',$kr_search_adrress,array('class'=>"st"));	?>
  <div class="search-input-wraps rounded30">
     <div class="row">
        <div class="col-sm-11 col-xs-10">
        <?php echo CHtml::textField('category','',array(
         'placeholder'=>t("Enter Cuisine"),
         'required'=>true,
         'class'=>"search-field cuisine"
        ))?>        
        </div>        
        <div class="col-sm-1 col-xs-2 relative">
          <button type="submit"><i class="ion-ios-search"></i></button>         
        </div>
     </div>
  </div> <!--search-input-wrap-->
  </form>      
  
  <form method="GET" class="tab-byfood forms-search" id="forms-search" action="<?php echo Yii::app()->createUrl('store/searchresult')?>">
  <?php echo CHtml::hiddenField('st',$kr_search_adrress,array('class'=>"st"));	?>
  <div class="search-input-wraps rounded30">
     <div class="row">
        <div class="col-sm-11 col-xs-10">
        <?php echo CHtml::textField('foodname','',array(
         'placeholder'=>t("Enter Food Name"),
         'required'=>true,
         'class'=>"search-field foodname"
        ))?>        
        </div>        
        <div class="col-sm-1 col-xs-2 relative">
          <button type="submit"><i class="ion-ios-search"></i></button>         
        </div>
     </div>
  </div> <!--search-input-wrap-->
  </form> */ ?>        
  
</div> <!--search-wrapper-->
