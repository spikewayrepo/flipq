<div class="container-fluid" style="background-color: #000; border-bottom: 1px solid #fff;border-top: 1px solid #fff;padding-bottom: 2%;padding-top: 2%;">

	<?php
	if( Yii::app()->controller->action->id=='home' )
	{ ?>

		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder">
			<div class="icondiv">
			<a href="/store/home"><img src="<?php echo assetsURL()?>/images/pubnew.png" class="newicons"><br>
			<span class="hidden-xs" style="background-color : #000;" id="home_cat">Pub/Bar</span></a>
			</div>
		</div>
	
	<?php } else { ?>
	
		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder">
		<div class="icondiv">
			<a href="/store/home"><img src="<?php echo assetsURL()?>/images/pubnew.png" class="newicons"><br>
			<span class="hidden-xs" id="home_cat">Pub/Bar</span></a>
		</div>
		</div>
	
	<?php } ?>
	
	<?php if( Yii::app()->controller->action->id=='comparison' )
	{ ?>

		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder" style="display:none;">
		<div class="icondiv">
			<a href="javascript:;"><img src="<?php echo assetsURL()?>/images/compare.png" class="newicons"></i><br>
			<span id="home_cat" style="background-color:#000;" class="hidden-xs">Comparison</span></a>
		</div>
		</div>

	<?php } else { ?>

		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder" style="display:none;">
		<div class="icondiv">
			<a href="javascript:;"><img src="<?php echo assetsURL()?>/images/compare.png" class="newicons"></i><br>
			<span id="home_cat" class="hidden-xs">Comparison</span></a>
		</div>
		</div>
	
	<?php }?>
	
	
	<?php if( Yii::app()->controller->action->id=='deals' )
	{ ?>
	
		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder">
		<div class="icondiv">
			<a href="/store/deals"><img src="<?php echo assetsURL()?>/images/deals.png" class="newicons"><br>
			<span id="home_cat" style="background-color:#000;" class="hidden-xs">Deals</span></a>
		</div>
		</div>

	<?php } else { ?>
	
		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder">
		<div class="icondiv">
			<a href="/store/deals"><img src="<?php echo assetsURL()?>/images/deals.png" class="newicons"><br>
			<span id="home_cat" class="hidden-xs">Deals</span></a>
		</div>
		</div>
	
	<?php } ?>	
	
	
	<?php if( Yii::app()->controller->action->id=='happyhours' )
	{ ?>
	
		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder">
		<div class="icondiv">
			<a href="/store/happyhours"><<img src="<?php echo assetsURL()?>/images/happyhours.png" class="newicons"><br>
			<span id="home_cat_happy" style="background-color:#000;" class="hidden-xs">Extended Happy Hours</span></a>
		</div>
		</div>
	
	<?php } else { ?>
	
		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder">
		<div class="icondiv">
			<a href="/store/happyhours"><img src="<?php echo assetsURL()?>/images/happyhours.png" class="newicons"><br>
			<span id="home_cat_happy" class="hidden-xs">Extended Happy Hours</span></a>
		</div>
		</div>
	
	<?php } ?>
	
	<?php if( Yii::app()->controller->action->id=='dj' )
	{ ?>
	
		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder">
		<div class="icondiv">
			<a href="/store/dj"><img src="<?php echo assetsURL()?>/images/dj.png" class="newicons"><br>
			<span class="hidden-xs" style="background-color:#000;" id="home_cat">DJ</span></a>
		</div>
		</div>

	<?php } else { ?>	
		
		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder">
		<div class="icondiv">
			<a href="/store/dj"><img src="<?php echo assetsURL()?>/images/dj.png" class="newicons"></i><br>
			<span class="hidden-xs" id="home_cat">DJ</span></a>
		</div>
		</div>
		
	<?php } ?>

	<?php if( Yii::app()->controller->action->id=='BookTable' )
	{ ?>
	
		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder">
		<div class="icondiv">
			<a href="/store/BookTable"><img src="<?php echo assetsURL()?>/images/booking-table.png" class="newicons"></i><br>
			<span id="home_cat" style="background-color:#000;" class="hidden-xs">Book-Table</span></a>
		</div>
		</div>
	<?php } else { ?>
	
		<div class="col-sm-2 col-md-2 col-xs-4 text-center newiconholder">
		<div class="icondiv">
			<a href="/store/BookTable"><img src="<?php echo assetsURL()?>/images/booking-table.png" class="newicons"><br>
			<span id="home_cat" class="hidden-xs">Book-Table</span></a>
		</div>
		</div>
	
	<?php } ?>

</div>