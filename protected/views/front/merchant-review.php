<div class="">
<a href="javascript:;" onClick="$('#reviewform').toggle('slow')" class="write-review-new btn-hover inline bottom10 upper">
<?php echo t("write a review")?>
</a>
</div>

<div class="bottom10 row" style="display:none" id="reviewform">
<form class="forms" id="rforms" onsubmit="return false;">
<?php echo CHtml::hiddenField('action','addReviews')?>
<?php echo CHtml::hiddenField('currentController','store')?>
<?php echo CHtml::hiddenField('merchant-id',$merchant_id)?>
<?php echo CHtml::hiddenField('initial_review_rating','')?>	        
   <div class="col-md-12 border">
     <div>
     <div class="raty-stars" data-score="5"></div>   
     </div>
     <div class="review-input-wrap2">
     <?php echo CHtml::textArea('review_content','',array(
        'class'=>"grey-inputs"
     ))?>
     </div>
     <div class="top10">
        <button class="orange-button inline" type="submit"><?php echo t("PUBLISH REVIEW")?></button>
     </div>
   </div> <!--col-->	        
</form>
</div> <!--review-input-wrap-->


