<?php if(is_array($menu) && count($menu)>=1):?>

<?php 
$merc=Yii::app()->functions->getMerchantUserInfo($merchant_id);
$ddl=Yii::app()->functions->drydaylist($merc,$request,$merchant_id);

//print_r($merc);

foreach ($menu as $val):
//print_r($ddl);
?>


<div class="<?php echo $val['category_name']?>" id="cat-<?php echo $val['category_id']?>" style="margin-top:0;">

  <div class="menu-cat" >

         <div class="panel panel-default">
                      <div class="panel-heading"><h3><?php echo qTranslate($val['category_name'],'category_name',$val) ?></h3></div>

                      <div class="panel-body">

    
     <?php $x=0?>
     

     
     <?php if (!empty($val['category_description'])):?>
     <!--<p class="small">
       <?php echo qTranslate($val['category_description'],'category_description',$val)?>
     </p>-->
     <?php endif;?>
     <?php echo Widgets::displaySpicyIconNew($val['dish'],"dish-category")?>
     
     <?php if (is_array($val['item']) && count($val['item'])>=1):?>
     <?php foreach ($val['item'] as $val_item):?>
     <?php //print_r($val_item); ?>
     <?php 
	    $atts='';
	    if ( $val_item['single_item']){
	  	  $atts.='data-price="'.$val_item['single_details']['price'].'"';
	  	  $atts.=" ";
	  	  $atts.='data-size="'.$val_item['single_details']['size'].'"';
	    }
	  ?>       
     

        <div class="width70">
          <p id="combo_name"><?php echo qTranslate($val_item['item_name'],'item_name',$val_item)?></p>
          <p class="combo-details">Lorem Ipsome is Simply Dummy Text.Lorem Ipsome is Simply Dummy Text.</p>
        </div>
  <?php 
  if($ddl){

   if($val['category_name']!="Cocktail"&&$val['category_name']!="Alcohol") { ?>      
        <div class="width30">

          <?php if ( $disabled_addcart==""):?>
          
          <p>
          <a  href="javascript:;" class="dsktop menu-item <?php echo $val_item['not_available']==2?"item_not_available":''?>" 
            rel="<?php echo $val_item['item_id']?>"
            data-single="<?php echo $val_item['single_item']?>" 
            <?php echo $atts;?>
           >
           <i class="ion-ios-plus-outline green-color bold"></i>
          </a>
         </p>
        
          
          <?php endif;?>


           <p class="combo-price"><?php echo FunctionsV3::getItemFirstPrice($val_item['prices'],$val_item['discount']) ?></p>
        </div>
    <?php 
    } 
    }
    else{ ?>
      <div class="width30">

          <?php if ( $disabled_addcart==""):?>
          
          <p>
          <a  href="javascript:;" class="dsktop menu-item <?php echo $val_item['not_available']==2?"item_not_available":''?>" 
            rel="<?php echo $val_item['item_id']?>"
            data-single="<?php echo $val_item['single_item']?>" 
            <?php echo $atts;?>
           >
           <i class="ion-ios-plus-outline green-color bold"></i>
          </a>
         </p>
        
          
          <?php endif;?>


           <p class="combo-price"><?php echo FunctionsV3::getItemFirstPrice($val_item['prices'],$val_item['discount']) ?></p>
        </div>
     <?php }?>

		
     <!--row-->
     <?php $x++?>
     <?php endforeach;?>
    <?php else :?> 
      <p class="small text-danger"><?php echo t("no item found on this category")?></p>
     <?php endif;?>


</div>


   </div> 
  </div> <!--menu-cat-->

</div> <!--menu-1-->
<?php endforeach;?>

<?php else :?>
<p class="text-danger"><?php echo t("This restaurant has not published their menu yet.")?></p>
<?php endif;?>
