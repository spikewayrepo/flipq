<?php 
	

	$pubs=FunctionsV3::getMerchantbyCity($city,'pub/bar');
	$lounges=FunctionsV3::getMerchantbyCity($city,'lounge');
	$cafes=FunctionsV3::getMerchantbyCity($city,'cafe');
	$sbars=FunctionsV3::getMerchantbyCity($city,'sports_bar');
	$mpubs=FunctionsV3::getMerchantbyCity($city,'microbrewery_pub');
	$fdinings=FunctionsV3::getMerchantbyCity($city,'fine_dining');
	$cdinings=FunctionsV3::getMerchantbyCity($city,'casual_dining');
	$pubcounter_hh = 0;
	$pubcounter_d = 0;
	$loungecounter_hh = 0;
	$loungecounter_d = 0;
	$cafecounter_hh = 0;
	$cafecounter_d = 0;
	$mpubcounter_hh = 0;
	$mpubcounter_d = 0;
	$sbarcounter_d = 0;
	$sbarcounter_d = 0;
	$fdcounter_hh = 0;
	$fdcounter_d = 0;
	$cdcounter_hh = 0;
	$cdcounter_d = 0;
	
	foreach($pubs as $pubs1)
	{ 
		if( ($pubs2 = FunctionsV3::getMerchantbyHappy($pubs1['merchant_id'])) || ($pubs3 = FunctionsV3::getMerchantbyDeals($pubs1['merchant_id'])) )
		{
			$happyhourspub = FunctionsV3::getNoofHappy($pubs1['merchant_id']);
			$dealspub = FunctionsV3::getNoofDeals($pubs1['merchant_id']);
			$pubcounter_hh = $pubcounter_hh + $happyhourspub;  //  Total No. of Happy Hours in Pub/bar
			$pubcounter_d = $pubcounter_d + $dealspub;     // Total No. of Deals in Pub/bar
		}
	}
	
	foreach($lounges as $lounges1)
	{
	if( ($lounges2 = FunctionsV3::getMerchantbyHappy($lounges1['merchant_id'])) || ($lounges3 = FunctionsV3::getMerchantbyDeals($lounges1['merchant_id'])) )
		{
			$happyhourslounges = FunctionsV3::getNoofHappy($lounges1['merchant_id']);
			$dealslounges = FunctionsV3::getNoofDeals($lounges1['merchant_id']);
			$loungecounter_hh = $loungecounter_hh + $happyhourslounges;  // ----------- Total No. of Happy Hours in Lounge ------------------- 
			$loungecounter_d = $loungecounter_d + $dealslounges;     // ----------- Total No. of Deals in Lounge ------------------- 
		}
	}
	
	foreach($cafes as $cafes1)
	{
	if( ($cafes2 = FunctionsV3::getMerchantbyHappy($cafes1['merchant_id'])) || ($cafes3 = FunctionsV3::getMerchantbyDeals($cafes1['merchant_id'])) )
		{
			$happyhourscafes = FunctionsV3::getNoofHappy($cafes1['merchant_id']);
			$dealscafes = FunctionsV3::getNoofDeals($cafes1['merchant_id']);
			$cafecounter_hh = $cafecounter_hh + $happyhourscafes;  // ----------- Total No. of Happy Hours in Cafe ------------------- 
			$cafecounter_d = $cafecounter_d + $dealscafes;     // ----------- Total No. of Deals in Cafe -------------------
		}
	}
	
	foreach($sbars as $sbars1)
	{
	if( ($sbars2 = FunctionsV3::getMerchantbyHappy($sbars1['merchant_id'])) || ($sbars3 = FunctionsV3::getMerchantbyDeals($sbars1['merchant_id'])) )
		{
			$happyhourssbars = FunctionsV3::getNoofHappy($sbars1['merchant_id']);
			$dealssbars = FunctionsV3::getNoofDeals($sbars1['merchant_id']);
			$sbarcounter_hh = $sbarcounter_hh + $happyhourssbars;  // ----------- Total No. of Happy Hours in Sports Bar ------------------- 
			$sbarcounter_d = $sbarcounter_d + $dealssbars;   // ----------- Total No. of Deals in Sports Bar -------------------
		}
	}
	
	foreach($mpubs as $mpubs1)
	{
	if( ($mpubs2 = FunctionsV3::getMerchantbyHappy($mpubs1['merchant_id'])) || ($mpubs3 = FunctionsV3::getMerchantbyDeals($mpubs1['merchant_id'])) )
		{
			$happyhoursmpubs = FunctionsV3::getNoofHappy($mpubs1['merchant_id']);
			$dealsmpubs = FunctionsV3::getNoofDeals($mpubs1['merchant_id']);
			$mpubcounter_hh = $mpubcounter_hh + $happyhoursmpubs;  // ----------- Total No. of Happy Hours in Microbrewery Pub ------------------- 
			$mpubcounter_d = $mpubcounter_d + $dealsmpubs;    // ----------- Total No. of Deals in Microbrewery Pub ------------------- 
		}
	}
	
	foreach($fdinings as $fdinings1)
	{
	if( ($fdinings2 = FunctionsV3::getMerchantbyHappy($fdinings1['merchant_id'])) || ($fdinings3 = FunctionsV3::getMerchantbyDeals($fdinings1['merchant_id'])) )
		{
			$happyhoursfdinings = FunctionsV3::getNoofHappy($fdinings1['merchant_id']);
			$dealsfdinings = FunctionsV3::getNoofDeals($fdinings1['merchant_id']);
			$fdcounter_hh = $fdcounter_hh + $happyhoursfdinings;  // ----------- Total No. of Happy Hours in Fine Dining ------------------- 
			$fdcounter_d = $fdcounter_d + $dealsfdinings;  // ----------- Total No. of Deals in Fine Dining -------------------
		}
	}
	
	foreach($cdinings as $cdinings1)
	{
	if( ($cdinings2 = FunctionsV3::getMerchantbyHappy($cdinings1['merchant_id'])) || ($cdinings3 = FunctionsV3::getMerchantbyDeals($cdinings1['merchant_id'])) )
		{
			$happyhourscdinings = FunctionsV3::getNoofHappy($cdinings1['merchant_id']);
			$dealscdinings = FunctionsV3::getNoofDeals($cdinings1['merchant_id']);
			$cdcounter_hh = $cdcounter_hh + $happyhourscdinings;  // ----------- Total No. of Happy Hours in Casual Dining ------------------- 
			$cdcounter_d = $cdcounter_d + $dealscdinings;    // ----------- Total No. of Deals in Casual Dining -------------------
		}
	}



?>
<section class="grid1 hidden-xs">
  <div class="container">

<?php
 if($data)
 {
        $i = 0;
        $count = count($data);
        if($count%7==0)
                  
         

 	foreach($data as $rtype)
        {

        $img=Yii::app()->request->baseUrl."/upload/$rtype[banner]";
        $name = $rtype['rtype_name'];
?>

<div class="hovereffect1" style="background:url('<?php print $img ?>') no-repeat scroll center center / cover ">
          <div class="overlay">
            <a class="name1" href="/store/searchresult?restro=<?php print $name ?>"><?php print $name ?></a>
            <a class="ehh" href="/store/happyhours?restrac=<?php print $name ?>">Extended happy Hours 4</a>
            <a class="deal1" href="/store/deals?restrac=<?php print $name ?>">2 Deals</a>
          </div>
        </div>



<?php
        }
 }

?>  
 
</div>
  </div>
</section>



<section class="grid visible-xs">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 col-xs-12 hovereffect">
        <a href="/store/searchresult?restro=pub/bar"><img src="/assets/images/img1.jpg"></a>
        <a href="/store/searchresult?restro=pub/bar"><p class="name">Pub / Bar</p></a>
        <div class="overlay">
          <a href="/store/happyhours?restrac=pub/bar"><p class="eta">Extended Happy <br/>Hours  <?php echo $pubcounter_hh; ?></p></a>
          <a href="/store/deals?restrac=pub/bar"><span class="deal">Deals<br/> <?php echo $pubcounter_d; ?></span></a>
        </div>
      </div>
      <div class="col-sm-4 col-xs-12 hovereffect">
        <a href="/store/searchresult?restro=lounge"><img src="/assets/images/img2.jpg"></a>
        <a href="/store/searchresult?restro=pub/bar"><p class="name">Lounge</p></a>
        <div class="overlay">
          <a href="/store/happyhours?restrac=lounge"><p class="eta">Extended Happy <br/>Hours  <?php echo $loungecounter_hh; ?></p></a>
          <a href="/store/deals?restrac=lounge"><span class="deal">Deals<br/> <?php echo $loungecounter_d; ?></span></a>
        </div>
      </div>
      <div class="col-sm-4 col-xs-12 hovereffect">
        <a href="/store/searchresult?restro=sports_bar"><img src="/assets/images/img3.jpg"></a>
        <a href="/store/searchresult?restro=sports_bar"><p class="name">Sports Bar</p></a>
        <div class="overlay">
          <a href="/store/happyhours?restrac=sports_bar"><p class="eta">Extended Happy <br/>Hours  <?php echo $sbarcounter_hh; ?></p></a>
          <a href="/store/deals?restrac=sports_bar"><span class="deal">Deals<br/> <?php echo $sbarcounter_d; ?></span></a>
        </div>
      </div>
      <div class="col-sm-4 col-xs-12 hovereffect">
        <a href="/store/searchresult?restro=fine_dining"><img src="/assets/images/img4.jpg"></a>
        <a href="/store/searchresult?restro=fine_dining"><p class="name">Fine Dining</p></a>
        <div class="overlay">
          <a href="/store/happyhours?restrac=fine_dining"><p class="eta">Extended Happy <br/>Hours  <?php echo $fdcounter_hh; ?></p></a>
          <a href="/store/deals?restrac=fine_dining"><span class="deal">Deals<br/> <?php echo $fdcounter_d; ?></span></a>
        </div>
      </div>
      <div class="col-sm-4 col-xs-12 hovereffect">
        <a href="/store/searchresult?restro=casual_dining"><img src="/assets/images/img5.jpg"></a>
        <a href="/store/searchresult?restro=casual_dining"><p class="name">Casual Dining</p></a>
        <div class="overlay">
          <a href="/store/happyhours?restrac=casual_dining"><p class="eta">Extended Happy <br />Hours  <?php echo $cdcounter_hh; ?></p></a>
          <a href="/store/deals?restrac=casual_dining"><span class="deal">Deals<br/> <?php echo $cdcounter_d; ?></span></a>
        </div>
      </div>
      <div class="col-sm-4 col-xs-12 hovereffect">
        <a href="/store/searchresult?restro=microbrewery_pub"><img src="/assets/images/img6.jpg"></a>
        <a href="/store/searchresult?restro=microbrewery_pub"><p class="name">Microbrewery Pub</p></a>
        <div class="overlay">
          <a href="/store/happyhours?restrac=microbrewery_pub"><p class="eta">Extended Happy <br/>Hours <?php echo $mpubcounter_hh; ?></p></a>
          <a href="/store/deals?restrac=microbrewery_pub"><span class="deal">Deals<br/> <?php echo $mpubcounter_d; ?></span></a>
        </div>
      </div>
    </div>
  </div>
</section>
