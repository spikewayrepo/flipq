
<div class="search-wraps single-search">

  <h1><?php echo $home_search_text;?></h1>
  <form method="GET" class="forms-search" id="forms-search" action="<?php echo Yii::app()->createUrl('store/searchresult')?>">
  <div class="l-pre-1" role="link" aria-label="Please type a location..." aria-describedby="location_input_sp" tabindex="0" aria-flowto="explore-location-suggest" style="display: block;">
                        <span class="location_placeholder ml5">
                            <i class="location arrow icon tiny pr2"></i>
                        </span>
                        <span id="location_input_sp" class="location_input_sp mr5">Delhi NCR</span>
                        <span class="right location-dropdown"><i class="ui icon black tiny caret down left"></i></span>
                    </div>
  <div class="search-input-wraps">
     <div class="row">
        <div class=" border col-sm-11 col-xs-10">
        <?php echo CHtml::textField('happy',$kr_search_adrress,array(
         'placeholder'=>$placholder_search,
         'required'=>true
        ))?>        
        </div>        
        <div class=" relative border col-sm-1 col-xs-2">
          <button type="submit"><i class="ion-ios-search"></i></button>         
        </div>
     </div>
  </div> <!--search-input-wrap-->
  </form>
  
</div> <!--search-wrapper-->
