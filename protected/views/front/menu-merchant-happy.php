
<?php //print_r($menu);
if(is_array($menu) && count($menu)>=1):?>



<div class="menu-1 box-grey rounded" style="margin-top:0;">
<?php foreach ($menu as $val): 
//print_r($val); ?>
  <div class="menu-cat cat-<?php echo $val['category_id']?> ">
     
          
     <?php $x=0?>
     
     <div class="items-row <?php echo $tc==2?"hide":''?>">
     
     
     <?php #echo Widgets::displaySpicyIconNew($val['dish'],"dish-category")?>
     
     <?php #if (is_array($val['item']) && count($val['item'])>=1):	?>
     
     <?php 
	    $atts='';
	    if ( $val['single_item']==2){
	  	  $atts.='data-price="'.$val['single_details']['price'].'"';
	  	  $atts.=" ";
	  	  $atts.='data-size="'.$val['single_details']['size'].'"';
	    }
	  ?>       
     
     <div class="row <?php echo $x%2?'odd':'even'?>">
        <div class="col-md-5 col-xs-4 border">
          <?php echo qTranslate($val['item_name'],'item_name',$val)?>
        </div>
        <div class="col-md-3 col-xs-2 food-price-wrap border">
           <?php echo FunctionsV3::getItemFirstPrice($val['prices'],$val['discount']) ?>
        </div>
        <div class="col-md-4 col-xs-6 relative food-price-wrap border text-right">
     <?php	if ( $disabled_addcart==""):?>
				<input type='button' value='-' class="qty_minus" />
				<input type='text' name='quantity' value='0' class='qty' id="item_qty" readonly />
				<input type='button' value='+' class='qty_plus' />
				<input type='button' value='Add' class="dsktop menu-item <?php echo $val['not_available']==2?"item_not_available":''?>" rel="<?php echo $val['item_id']?>" data-single="<?php echo $val['single_item']?>" <?php echo $atts; ?> style="color: #000;" />
      <?php endif;?>
        </div>
     </div> <!--row-->
     <?php $x++;?>
     <?php #endforeach;?>
   
     <?php #endif;?>
    </div> 
    
  </div> <!--menu-cat-->


<?php endforeach;?>
</div> <!--menu-1-->
<?php else :?> 
      <p class="small text-danger"><?php echo t("no item found on this category")?></p>
<?php endif;?>
