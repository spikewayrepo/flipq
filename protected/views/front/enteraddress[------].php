
<!-- - Location Modal Starts Here -->
<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-content login-modal">
          <div class="modal-header login-modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="loginModalLabel">LOCATION</h4>
          </div>
          <div class="modal-body">
            <div class="text-center row">
              <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                  &nbsp;&nbsp;
                  <form>
                  <div class="form-group">
                      <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-user"></i></div>
                          <input type="text" class="form-control" placeholder="Enter Location">
                      </div>
                      <span class="help-block has-error" id="email-error"></span>
                    </div>
                    <button type="button" class="btn btn-block bt-login">Select City</button>
                    <div class="clearfix"></div>
                </form>
              </div>
            </div>
            </div>
        </div>
     </div>
  </div>
  <!-- - Location Modal Ends Here -->
 <!-- - Location Modal Ends Here -->
  <div class="blur"></div>

  <div class="locationpopup">
    <i class="fa fa-times close-location"></i>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
            <h2>ENTER YOUR LOCATION</h2>
            <form class="location-form">
                <input type="text" class="form-control location-feild" placeholder="Enter Location">

<form id="frm-modal-enter-address" class="frm-modal-enter-address" method="POST" onsubmit="return false;" >
<?php echo CHtml::hiddenField('action','setAddress');?> 
<?php echo CHtml::hiddenField('web_session_id',
isset($this->data['web_session_id'])?$this->data['web_session_id']:''
);?>

 <?php
		$fq_search_adrress=Cookie::getCookie('fq_search_address');

?>
<input type="text" id="client_address" name="client_address" value="<?php isset($_SESSION['fq_search_address'])?$_SESSION['fq_search_address']:'' ?>" data-validation="required" class="grey-inputs valid" placeholder="Enter a location" autocomplete="on">


<div class="col-md-6 ">
     <input type="submit" class="green-button inline" id="client_address_submit" value="<?php echo t("Submit")?>">
  </div>



            </form>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
          <h3>TOP CITIES</h3>
          <ul>
            <li class="wrapper"><a class="first after">Hauz Khas</a></li>
            <li class="wrapper"><a class="first after">Connaught Place</a></li>
            <li class="wrapper"><a class="first after">Cyber Hub</a></li>
            <li class="wrapper"><a class="first after">Punjabi Bagh</a></li>
            <li class="wrapper"><a class="first after">Greater Kailash-1</a></li>
            <li class="wrapper"><a class="first after">Golf Course</a></li>
          </ul>

        </div>
      </div>
    </div>
  </div>









 <!--container-->

<script type="text/javascript">
$.validate({ 	
	language : jsLanguageValidator,
	language : jsLanguageValidator,
    form : '#frm-modal-enter-address',    
    onError : function() {      
    },
    onSuccess : function() {     
      form_submit('frm-modal-enter-address');
	  close_fb();
      return false;
    }  
})

jQuery(document).ready(function() {
	var google_auto_address= $("#google_auto_address").val();	
	if ( google_auto_address =="yes") {		
	} else {
		$("#client_address").geocomplete({
		    country: $("#admin_country_set").val()
		});	
	}


$('#btnYes').click(function(){
	     $('#client_address').val($.cookie("fq_search_address"));
          close_fb();
		 $('#client_address_submit').click();
		 //close_fb();

	});
	
$('#client_address_not_shown').click(function(){
	//alert("Hi");
	var date= new Date();
	//date.setHours(23,59,59,0);
	$.cookie("Notshowdialog", true, {expires: 1});
	close_fb();
	//location.reload();
	//alert($.cookie("Notshowdialog"));
	});	

$('#btnNo').click(function(){
    $('#client_address').show();
    $('#exstinglocation').hide();
	});
});
</script>
<?php

die();
