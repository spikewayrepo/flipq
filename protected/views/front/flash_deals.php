<div class="flash-deals-link">
    <img class="img-responsive" src="/assets/images/fd.png">
</div>
<div class="flash-deals">
  <i class="fa fa-times close-flash" style="position:absolute;top:10px;right:10px;height:30px;width:30px;font-size:22px;color:#777;"></i>
  <h3 class="text-center white">Flash Deals</h3>
  <div class="row">
	<?php
	$func = "getCode()";
	foreach($result as $id)
	{
		if($val=FunctionsV3::getMerchantbyDeals($id['merchant_id']))
		{
			if($deals=FunctionsV3::getNoofdeals($id['merchant_id']))
			{ 
	?>
				<div class="col-sm-12">
					<div class="media">
						<div class="row">
							<div class="media-left col-sm-4 col-xs-4">
								<a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val[0]['restaurant_slug']).'?deals=true')?>"><img src="<?php echo FunctionsV3::getMerchantBanner($val[0]['merchant_id']);?>" class="media-object"></a>
							</div>
							<div class="media-body">
								<h5 class="media-heading"><?php print $id['deal_desc']?></h5>
								<a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'.trim($val[0]['restaurant_slug']).'?deals=true' )?>"><p><?php echo clearString($val[0]['restaurant_name'])?></p></a>
								<a onclick="getDealsinfo(this)" rel="<?php print $val[0]['merchant_id'] ?>"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path class="cls-1" d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z"/><circle class="cls-2" cx="37.99" cy="12.47" r="3.83" transform="translate(-0.02 0.06) rotate(-0.08)"/></svg>
								<span><?php echo $deals." Deals"; ?></span></a>

								<?php
									$datem =  $id['valid_to'];
									$timem =  $id['end_time'];
									$combinedDT = strtotime("$datem $timem")-time();
								?>

								<p>Ending in :&nbsp;<span class="timer" data-minutes-left=<?php print ($combinedDT/60) ?> style="color:red;"></span></p>
								<button id="get_a_code" type="button" class="get_a_code"  onClick="<?php echo $func; ?>">Get a Code</button>
								<!--<a><button>Get a code</button></a>-->						
							</div>
						</div>
					</div>
				</div>	
			<?php
			}
		}
	} ?>	
	

  </div>
</div>
 <script>
function getCode()
{
  var mid=$(".selectedbox #mmid").val();
  var cid=$(".selectedbox #ccid").val();
  var did=$(".selectedbox #ddid").val();
var params="action=getCouponCode&currentController=store&mid="+mid+"&cid="+cid+"&did="+did;

   $.ajax({    
      type: "POST",
      url: ajax_url,
      data: params,
      dataType: "json",       
      success: function(data){ 
                  $("#deals_model").html(data.msg);
      }, 
      error: function(){                  
      }   
      });
}
</script>
