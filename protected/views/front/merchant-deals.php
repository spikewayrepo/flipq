

<div class="box-grey rounded merchant-promo" style="margin-top:0;">

<?php if (!empty($promo['offer'])):?>
<div class="section-label">
    <a class="section-label-a">
      <span class="bold" style="background:#000;">
      <?php echo t("Deals")?></span>
      <b></b>
    </a>     
</div> 

<?php 


if($_SESSION['kr_client']['client_id']){
$cid  = $_SESSION['kr_client']['client_id'];
 ?>

<?php foreach($promo['offer'] as $dealsoffer){ ?>
<p class="spacer40" style="color: #fff;"><i class="green-color ion-ios-plus-empty"></i> <?php echo (int)$dealsoffer['offer_percentage'] ?>% Off today on orders over <?php echo FunctionsV3::prettyPrice($dealsoffer['offer_price']) ?> <button onclick="getCode('<?php print $_SESSION[kr_merchant_id] ?>','<?php print $cid ;?>','<?php print $dealsoffer['deals_id'] ?>')" class="rounded book-table-button orange-button inline"> Get Code</button><span id="offer_<?php print $dealsoffer['deals_id'] ?>"></span></p>
<br>
<?php } ?>

<?php }else{ ?>
<?php foreach($promo['offer'] as $dealsoffer){ ?>
<p class="spacer40" style="color: #fff;"><i class="green-color ion-ios-plus-empty"></i><?php echo (int)$dealsoffer['offer_percentage'] ?>% Off today on orders over <?php echo FunctionsV3::prettyPrice($dealsoffer['offer_price']) ?> <a href="<?php echo baseUrl()."/store/signup"; ?>"><button class="rounded book-table-button orange-button inline"> Login to Get Code</button></a></p>  
<br>
<?php } ?>

<?php } ?>

<?php endif;?>


<?php if (isset($promo['voucher'])):?>
<?php if (is_array($promo['voucher']) && count($promo['voucher'])>=1):?>
<div class="section-label top15">
    <a class="section-label-a">
      <span class="bold" style="background:#fff;">
      <?php echo t("Vouchers")?></span>
      <b></b>
    </a>     
</div>  
<?php foreach ($promo['voucher'] as $val): 
      if ( $val['voucher_type']=="fixed amount"){
      	  $amount=FunctionsV3::prettyPrice($val['amount']);
      } else $amount=number_format( ($val['amount']/100)*100 )." %";
?>
   <p><i class="green-color ion-ios-plus-empty"></i> <?php echo $val['voucher_name']." - ".$amount." ".t("Discount")?></p>
<?php endforeach;?>
<?php endif;?>
<?php endif;?>


<?php if (!empty($promo['free_delivery'])):?>
<div class="section-label">
    <a class="section-label-a">
      <span class="bold" style="background:#fff;">
      <?php echo t("Delivery")?></span>
      <b></b>
    </a>     
</div>  
<p><i class="green-color ion-ios-plus-empty"></i> <?php 
echo t("Free Delivery On Orders Over")." ". FunctionsV3::prettyPrice($promo['free_delivery'])?></p>
<?php endif;?>

</div>
