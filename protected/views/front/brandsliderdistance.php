<div class="sections section-feature-resto">
<div class="container">
<div class="row">
		 <div class="col-sm-12 col-md-12 col-lg-12">
			    <h1 style="color:#f47e31; font-size: 30px; font-weight: bold;"><?php echo $namebrand ; ?></h1>
		  </div>


		 <div class="col-sm-12 col-md-12 col-lg-12">
            <ul id="<?php print $ids ?>" class="brandslider bxslider" style="display:none;">
                <?php foreach ($result as $val): ?>
                <li class="slide">
				<?php 
					$address= $val['city'];
					$ratings=Yii::app()->functions->getRatings($val['merchant_id']); 
					$merchant_address = $val['restaurant_name'].", ".$val['street'].", ".$val['city'].", ".$val['state']." ".$val['post_code'];
					$merchant_lat_long = FunctionsV3::GetMerchantcoordinates($val['merchant_id']);
					//print_r($merchant_lat_long); 
					$getdistance = Yii::app()->functions->get_distance_btw_two($merchant_lat_long['lat'], $merchant_lat_long['long'], $client_lat, $client_long,  'Km' );
					$range_to = rtrim($_GET['range_to'], " Km");
					$range_from = rtrim($_GET['range_from'], " Km - ");
					//echo $range_to;
					//echo $range_from;
					if( ($distance <= $range_to) && ($distance >= $range_from) )
					{
						$count++; ?>
						<div class="info-web">

							<div class="col-sm-12 col-md-12 col-xs-12 thumbnail">
								<div class="hovereffect" style="width:100%">
									<a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['restaurant_slug']) )?>"><img src="<?php echo FunctionsV3::getMerchantBanner($val['merchant_id']);?>" class="img-responsive restra" ></a>
									<div class="overlay ">
										<?php $open=array(); 
											$open=FunctionsV3::isMerchantcanCheckout($val['merchant_id']);
											if($open['code']==1 && $open['holiday']==2)
											{
												$open1="Open"; ?>
												<div id="message" class="heading">
													<span class="label label-success label-lg text-center"><?php echo $open1; ?></span>
												</div>
											<?php 
											} else 
											{
												$open1="Closed"; ?>
												<div id="message" class="heading">
													<span class="label label-danger label-lg text-center"><?php echo $open1; ?></span>
												</div>	
												<?php
											}
										?>
										<div class="clearfix"></div>
												<!--<p> 
												<?php echo $getdistance; ?>&nbsp; km away
												</p> -->
												<div id="milestone">
													<img src="<?php echo assetsURL()."/images/mile-stone1.png"; ?>">
													<span class="mile_stone">
													<?php echo $getdistance; ?></span>
												</div>
									</div>
									<div class="row">
									  <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12" style="text-align:right">
										  <div class="rating-stars" id="mugs" data-score="<?php echo $ratings['ratings']?>"></div>   
									  </div>
									  <!--<div class="col-sm-2 merchantopentag">
										 
									  </div>-->
									</div>					
								</div>
								<div class="caption">
								
									<div class="headding col-xs-12 col-sm-8 col-md-8 col-lg-8">
										<a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['restaurant_slug']) )?>">
										<?php echo clearString($val['restaurant_name'])?>
										</a>
									</div>
									<?php $count = FunctionsV3::NoofCuisine($val['cuisine']);
										  if( $count<=2 ){
											$count = FunctionsV3::displayCuisine($val['cuisine']);  
										  } else {
											  $count= "Multicuisine";
										  }?>
									<div class="col-sm-4 col-md-4 col-xs-8 text-left" style="margin-top: 3.5%">
										<p class="small"><?php echo $count;?></p>
									</div>
									<div class="col-xs-4 col-sm-6 col-md-6 col-lg-6 text-left" style="color: #fff; padding-bottom: 4%; padding-left: 0;"><span class="glyphicon glyphicon-map-marker"></span> <?php echo $address?></div>
									<div class="col-xs-8 col-sm-6 col-md-6 col-lg-6 text-right" style="color:#fff; padding-right : 2%; padding-bottom: 4%;"><a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['restaurant_slug']).'?map=true' )?>"><span class="fa fa-location-arrow"></span> Get Directions</a></div>
									
									<div class="clear"></div>
									<p class=""><a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['restaurant_slug']).'?preorder=true' )?>" id="preorder" class="btn btn-danger col-sm-5 col-xs-12"role="button">Pre-Order</a>
										<span class="col-sm-2 col-xs-2 col-md-2 col-lg-2"></span>
										<a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['restaurant_slug']).'?booktable=true' )?>" id="book_table" class="btn btn-danger col-sm-5 col-xs-12"role="button">Book a Table</a></p>
								</div>
								
								
								<?php if($deals=FunctionsV3::getNoofdeals($val['merchant_id']))
								{ ?>
									<div class="ribbon sale">
										<div class="theribbon"><?php echo $deals; ?> Deals</div>
										<div class="ribbon-background"></div>
									</div>
						<?php   } ?>
							</div>
						</div>
             <?php  } ?>
				</li>

	              <?php endforeach; ?>
                     <li class="slide"><a href="/store/searchresult?restro=<?php echo $search ; ?>"><img class="thumbnail1" src="<?php echo assetsURL()."/images/viewall.png"; ?>"></a>  </li>
                    </ul>
</div>
</div>
</div>
</div>



<script>


$(window).load(function(){
 var width = document.body.parentNode.clientWidth;
            if(width>768)
            {
              var mins = 3;
              var maxs = 3;
              var swidth = 360;
            }
            if(width>555&&width<=768)
            {
	      var mins = 2;
              var maxs = 2;
              var swidth = 350;
            }
            if(width<=555)
            {
 	      var mins = 1;
              var maxs = 1;
              var swidth = 311;
            }

$('#<?php print $ids ?>').show().bxSlider({
  infiniteLoop: false,
  minSlides: mins,
  maxSlides: maxs,
  slideWidth: swidth,
  slideMargin: 10
});

});
</script>

