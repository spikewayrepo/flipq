<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<?php
$list=array();
$orders =  "['Date', 'Combo', 'Deals', 'Preorder'],";
if($_GET['m']){
	$month=$_GET['m'];
}
else{
$month = date('m');
}
$year = date('Y');
$res=json_decode($_SESSION['kr_merchant_user']);
$mid=$res[0]->merchant_id;
for($d=1; $d<=31; $d++)
{
	if($d<=9){
		$date="0".$d;
	} else{
		$date=$d;
	}

	$time1= $year."-".$month."-".$date;
	$time=mktime(12, 0, 0, $month, $d, $year);	
	$rand = FunctionsV3::getMerchantOrders($mid, $time1);
	$rand2 = FunctionsV3::getMerchantDeals($mid, $time1);
	$rand3 = FunctionsV3::getMerchantPreOrders($mid, $time1);
    if (date('m', $time)==$month) {      
        #$list[]=date('Y-m-d-D', $time);
        $orders .="['".date('d-M', $time)."',".$rand.",".$rand2.",".$rand3."],"; 
    }
}
#echo "<pre>";
#print_r($list);
#echo "</pre>";

?>


    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([<?php print $orders ?>]);

     
var options ={ hAxis: {title: "DATE" , direction:-1, slantedText:true, slantedTextAngle:180 }
				
			}





        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>

    <div id="columnchart_material" style="width: 1000px; height: 500px;"></div>
  </body>
</html>
