<div class="container-fluid" style="background-color: #EDEDED; padding-bottom: 1%">

	<?php if( Yii::app()->controller->action->id=='home' )
	{ ?>

		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="/store/home"><i class="fa fa-glass fafaficon" style="color:#000;" aria-hidden="true"></i><br>
			<span class="hidden-xs" style="background-color : #000;" id="home_cat">Pub/Bar</span></a>
		</div>
	
	<?php } else { ?>
	
		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="/store/home"><i class="fa fa-glass fafaficon" aria-hidden="true"></i><br>
			<span class="hidden-xs" id="home_cat">Pub/Bar</span></a>
		</div>
	
	<?php } ?>
	
	<?php if( Yii::app()->controller->action->id=='comparison' )
	{ ?>

		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="javascript:;"><i class="fa fa-hourglass-half fafaficon" style="color:#000;" aria-hidden="true"></i><br>
			<span id="home_cat" style="background-color:#000;" class="hidden-xs">Comparison</span></a>
		</div>

	<?php } else { ?>

		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="javascript:;"><i class="fa fa-hourglass-half fafaficon" aria-hidden="true"></i><br>
			<span id="home_cat" class="hidden-xs">Comparison</span></a>
		</div>
	
	<?php }?>
	
	
	<?php if( Yii::app()->controller->action->id=='deals' )
	{ ?>
	
		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="/store/deals"><i class="fa fa-ticket fafaficon" style="color:#000;" aria-hidden="true"></i><br>
			<span id="home_cat" style="background-color:#000;" class="hidden-xs">Deals</span></a>
		</div>

	<?php } else { ?>
	
		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="/store/deals"><i class="fa fa-ticket fafaficon" aria-hidden="true"></i><br>
			<span id="home_cat" class="hidden-xs">Deals</span></a>
		</div>
	
	<?php } ?>	
	
	
	<?php if( Yii::app()->controller->action->id=='happyhours' )
	{ ?>
	
		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="/store/happyhours"><i class="fa fa-clock-o fafaficon" style="color:#000;" aria-hidden="true"></i><br>
			<span id="home_cat_happy" style="background-color:#000;" class="hidden-xs">Extended Happy Hours</span></a>
		</div>
	
	<?php } else { ?>
	
		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="/store/happyhours"><i class="fa fa-clock-o fafaficon" aria-hidden="true"></i><br>
			<span id="home_cat_happy" class="hidden-xs">Extended Happy Hours</span></a>
		</div>
	
	<?php } ?>
	
	<?php if( Yii::app()->controller->action->id=='dj' )
	{ ?>
	
		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="/store/dj"><i class="fa fa-music fafaficon" style="color:#000;" aria-hidden="true"></i><br>
			<span class="hidden-xs" style="background-color:#000;" id="home_cat">DJ</span></a>
		</div>

	<?php } else { ?>	
		
		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="/store/dj"><i class="fa fa-music fafaficon" aria-hidden="true"></i><br>
			<span class="hidden-xs" id="home_cat">DJ</span></a>
		</div>
		
	<?php } ?>

	<?php if( Yii::app()->controller->action->id=='BookTable' )
	{ ?>
	
		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="/store/BookTable"><i class="fa fa-credit-card fafaficon" style="color:#000;" aria-hidden="true"></i><br>
			<span id="home_cat" style="background-color:#000;" class="hidden-xs">Book-Table</span></a>
		</div>

	<?php } else { ?>
	
		<div class="col-sm-2 col-md-2 col-xs-2 text-center">
			<a href="/store/BookTable"><i class="fa fa-credit-card fafaficon" aria-hidden="true"></i><br>
			<span id="home_cat" class="hidden-xs">Book-Table</span></a>
		</div>
	
	<?php } ?>

</div>