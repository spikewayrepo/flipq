<div class="container">
    <div class="row text-center">
        <p id="pub-finder-text happy-hrs-text" class="translucent">Pre-Order & Enjoy Extended Happy Hours</p>
		<div class="toggle-btns translucent">
			<a id="happy-hrs-link" class="toggle-link color-orange">Extended Happy Hours</a>
			<a id="pub-finder-link" class="toggle-link">Pub Finder</a>
		</div>
        
		
        
		<!-- <p id="happy-hrs-text" class="translucent">Pre-Book & Enjoy Extended Happy Hours</p> -->
        
		<div class="col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1"></div>
    </div>

    <div class="row form-row">
        
		<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
			
			<form id="happy-hrs" action="happyhours" class="happy-hrs-form text-center">
				
				<div class="col-sm-3 col-xs-6">
				<div style="
    position: absolute;
    right: 6%;
    top: 20px;
">
  <i class="fa fa-map-marker  fa-6 foc" aria-hidden="true"></i>

</div>
					<input type="text"  class="form-control text_col" id="city_location" name="city" placeholder="Location">
				</div>

				<div class="col-sm-3 col-xs-6 date" data-provide="datepicker">
				<div style="
    position: absolute;
    right: 6%;
    top: 20px;
">
  <i class="fa fa-calendar fa-6 foc" aria-hidden="true"></i>

</div>
					<input type="text" readonly onClick="this.select()" class="form-control text_col" id="daateofbooking" name="date_booking" placeholder="Date Of Booking">
				</div>
            
				<div class="col-sm-3 col-xs-6" style="padding-top:10px;">
					<select type="text" id="eta" class="form-control text_col" name="eta" placeholder="ETA">
					 <option value="00:00">12:00 AM</option>
                                   <option value="00:30">12:30 AM</option>
                                   <option value="01:00">01:00 AM</option>
                                   <option value="01:30">01:30 AM</option>
                                   <option value="02:00">02:00 AM</option>
                                   <option value="02:30">02:30 AM</option>
                                   <option value="03:00">03:00 AM</option>
                                   <option value="03:30">03:30 AM</option>
                                   <option value="04:00">04:00 AM</option>
                                   <option value="04:30">04:30 AM</option>
                                   <option value="05:00">05:00 AM</option>
                                   <option value="05:30">05:30 AM</option>
                                   <option value="06:00">06:00 AM</option>
                                   <option value="06:30">06:30 AM</option>
                                   <option value="07:00">07:00 AM</option>
                                   <option value="07:30">07:30 AM</option>
                                   <option value="08:00">08:00 AM</option>
                                   <option value="08:30">08:30 AM</option>
                                   <option value="09:00">09:00 AM</option>
                                   <option value="09:30">09:30 AM</option>
                                   <option value="10:00">10:00 AM</option>
                                   <option value="10:30">10:30 AM</option>
                                   <option value="11:00">11:00 AM</option>
                                   <option value="11:30">11:30 AM</option>
                                   <option value="12:00">12:00 PM</option>
                                   <option value="12:30">12:30 PM</option>
                                   <option value="13:00">01:00 PM</option>
                                   <option value="13:30">01:30 PM</option>
                                   <option value="14:00">02:00 PM</option>
                                   <option value="14:30">02:30 PM</option>
                                   <option value="15:00">03:00 PM</option>
                                   <option value="15:30">03:30 PM</option>
                                   <option value="16:00">04:00 PM</option>
                                   <option value="16:30">04:30 PM</option>
                                   <option value="17:00">05:00 PM</option>
                                   <option value="17:30">05:30 PM</option>
                                   <option value="18:00">06:00 PM</option>
                                   <option value="18:30">06:30 PM</option>
                                   <option value="19:00">07:00 PM</option>
                                   <option value="19:30">07:30 PM</option>
                                   <option value="20:00">08:00 PM</option>
                                   <option value="20:30">08:30 PM</option>
                                   <option value="21:00">09:00 PM</option>
                                   <option value="21:30">09:30 PM</option>
                                   <option value="22:00">10:00 PM</option>
                                   <option value="22:30">10:30 PM</option>
                                   <option value="23:00">11:00 PM</option>
                                   <option value="23:30">11:30 PM</option>
					</select>
				</div>
            
				
            
				<div class="col-sm-3 col-xs-6">
					<button class="form-control" type="submit"><span class="hidden-xs">Search</span><img src="/assets/images/search.png"></button>
				</div>
          
			</form>

			<form id="pub-finder" class="pub-finder-form text-center" action="/store/searchresult">
            
				<div class="col-sm-9 col-xs-8">
					<input type="text" class="form-control form search search-btn search_resto_name" placeholder="Search Pubs/Bars, DJ Restaurant & more..." id="restro" name="restro">
				</div>
            
				<div class="col-sm-3 col-xs-4">
					<button class="form-control" type="submit"><span class="hidden-xs">Search</span><img src="/assets/images/search.png"></button>
            	</div>
          
			</form>

        </div>
    </div>
</div>
