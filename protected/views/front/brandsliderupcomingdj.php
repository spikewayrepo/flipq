<div class="sections section-feature-resto">
	<div class="container">
		<div class="row">
		
			<div class="col-sm-12 col-md-12 col-lg-12">
				<h1 style="color:#00b3ff; font-size: 30px; font-weight: bold;"><?php echo $namebrand ; ?></h1>
			</div>
	 
			<div class="col-sm-12 col-md-12 col-lg-12">
				<ul id="<?php print $ids ?>" class="brandslider bxslider" style="display:none;">
			  <?php foreach ($result as $val1): ?>
						<li class="slide"> 
					<?php	if($val = Yii::app()->functions->getMerchant($val1['merchant_id'])):
								$address=$val['city'];
								$ratings=Yii::app()->functions->getRatings($val['merchant_id']);
								$music=FunctionsV3::displayMusicType($val['music_name']); 
								$music_name = explode(",", $music);
								//print_r($music_name);
								?>	
								<div class="info-web">
									<div class="col-sm-12 col-md-12 col-xs-12 thumbnail">
										<div class="hovereffect" style="width:100%">
											<a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['restaurant_slug']) )?>"><img src="<?php echo FunctionsV3::getMerchantBanner($val['merchant_id']);?>" class="img-responsive">
											</a>
										</div>	
										<div class="caption">
											<div class="headding col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['restaurant_slug']) )?>">
												<?php echo clearString($val['restaurant_name'])?>
												</a>
												<div class="col-sm-12 col-xs-12 col-md-12 col-lg-12" style="text-align:right">
													<div class="rating-stars" id="mugs" style="margin-top: -10%; position: absolute; right: -5%;" data-score="<?php echo $ratings['ratings']?>"></div>   
												</div>
											</div>
										
											<p><?php 
												if(count($music_name)>2){
													echo $music_name[0].", ".$music_name[1].",...." ;	
												} else{
													 echo "ROCK";
												}  ?>
											</p>
											<br>
											<p>
												<a href="javascript:;" id="preorder" class="btn btn-danger col-sm-5 col-xs-12"role="button"><i class="fa fa-play"></i> Play Music</a>
												<span class="col-sm-2 col-xs-2 col-md-2 col-lg-2"></span>
												<a id="book_table" class="btn btn-danger col-sm-5 col-xs-12" href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['restaurant_slug']) )?>" role="button">Follow</a>
											</p>
											<br>
										</div>
									</div>
								</div>
					<?php   endif; ?>	
						</li>	
			<?php   endforeach; ?>
				</ul>
			</div> <!--col-->
		</div> <!--row-->		
	</div> <!--container-->
</div>



<script>


$(window).load(function(){
 var width = document.body.parentNode.clientWidth;
            if(width>768)
            {
              var mins = 3;
              var maxs = 3;
              var swidth = 360;
            }
            if(width>555&&width<=768)
            {
	      var mins = 2;
              var maxs = 2;
              var swidth = 350;
            }
            if(width<=555)
            {
 	      var mins = 1;
              var maxs = 1;
              var swidth = 311;
            }

$('#<?php print $ids ?>').show().bxSlider({
	infiniteLoop: false,
	minSlides: mins,
	maxSlides: maxs,
	slideWidth: swidth,
	slideMargin: 10
});

});
</script>

