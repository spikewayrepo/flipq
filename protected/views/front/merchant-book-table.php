
      <div class="col-sm-4 hidden-xs booking-sidebar ">
      <div id="booking_sidebar">
         <div id="pre_order" class="pre-order">
          <div class="row">
            <div class="col-sm-12 col-xs-12 text-center book-header">
              <h4>Pre-Order</h4>
            </div>

             <div class="col-md-6 col-sm-12 col-xs-6">

              <svg id="Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.88 62.94"><g id="calander"><polyline points="11.63 6.25 0.97 6.25 0.97 61.97 61.91 61.97 61.91 6.25 51.24 6.25" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></polyline><rect x="11.63" y="0.97" width="7.92" height="10.68" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></rect><rect x="43.32" y="0.97" width="7.92" height="10.68" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></rect><line x1="19.55" y1="6.26" x2="43.32" y2="6.26" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></line><line x1="0.97" y1="19.57" x2="61.91" y2="19.57" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></line></g></svg>

              <p><input id="etaBookingDate2" type="text" name="date_booking" class="form-control text_col" onclick="this.select()" readonly placeholder="Enter Date"></p>

            </div>
            
            <div class="col-md-6 col-sm-12 col-xs-6">

              <svg id="Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 63 63"><g id="time"><path d="M62,32A30,30,0,1,1,32,2,30,30,0,0,1,62,32Z" transform="translate(-0.5 -0.5)" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px"></path><polyline points="30.2 17.1 30.2 31.5 45.9 45.9" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px"></polyline></g></svg>

              <p><!-- <input id="etaBookingTime2" type="text" name="booking_time" class="form-control" placeholder="Enter Time"> -->
                
                <select type="text" id="etaBookingTime2" class="form-control" name="booking_time" placeholder="Enter time" style="height: 30px ;position: relative;top: 10px;padding-left: 25px;">
               <option <?php echo ($_POST['delivery_time']=='00:00')?'selected':'' ?> value="00:00">12:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='00:30')?'selected':'' ?> value="00:30">12:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='01:00')?'selected':'' ?> value="01:00">01:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='01:30')?'selected':'' ?> value="01:30">01:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='02:00')?'selected':'' ?> value="02:00">02:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='02:30')?'selected':'' ?> value="02:30">02:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='03:00')?'selected':'' ?> value="03:00">03:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='03:30')?'selected':'' ?> value="03:30">03:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='04:00')?'selected':'' ?> value="04:00">04:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='04:30')?'selected':'' ?> value="04:30">04:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='05:00')?'selected':'' ?> value="05:00">05:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='05:30')?'selected':'' ?> value="05:30">05:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='06:00')?'selected':'' ?> value="06:00">06:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='06:30')?'selected':'' ?> value="06:30">06:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='07:00')?'selected':'' ?> value="07:00">07:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='07:30')?'selected':'' ?> value="07:30">07:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='08:00')?'selected':'' ?> value="08:00">08:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='08:30')?'selected':'' ?> value="08:30">08:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='09:00')?'selected':'' ?> value="09:00">09:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='09:30')?'selected':'' ?> value="09:30">09:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='10:00')?'selected':'' ?> value="10:00">10:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='10:30')?'selected':'' ?> value="10:30">10:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='11:00')?'selected':'' ?> value="11:00">11:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='11:30')?'selected':'' ?> value="11:30">11:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='12:00')?'selected':'' ?> value="12:00">12:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='12:30')?'selected':'' ?> value="12:30">12:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='13:00')?'selected':'' ?> value="13:00">01:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='13:30')?'selected':'' ?> value="13:30">01:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='14:00')?'selected':'' ?> value="14:00">02:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='14:30')?'selected':'' ?> value="14:30">02:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='15:00')?'selected':'' ?> value="15:00">03:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='15:30')?'selected':'' ?> value="15:30">03:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='16:00')?'selected':'' ?> value="16:00">04:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='16:30')?'selected':'' ?> value="16:30">04:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='17:00')?'selected':'' ?> value="17:00">05:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='17:30')?'selected':'' ?> value="17:30">05:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='18:00')?'selected':'' ?> value="18:00">06:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='18:30')?'selected':'' ?> value="18:30">06:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='19:00')?'selected':'' ?> value="19:00">07:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='19:30')?'selected':'' ?> value="19:30">07:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='20:00')?'selected':'' ?> value="20:00">08:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='20:30')?'selected':'' ?> value="20:30">08:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='21:00')?'selected':'' ?> value="21:00">09:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='21:30')?'selected':'' ?> value="21:30">09:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='22:00')?'selected':'' ?> value="22:00">10:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='22:30')?'selected':'' ?> value="22:30">10:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='23:00')?'selected':'' ?> value="23:00">11:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='23:30')?'selected':'' ?> value="23:30">11:30 PM</option>
            </select>

              </p>

            </div>

            <div class="col-sm-12 col-xs-12 text-center">
              <button class="but" type="submit">Pre-Order</button>
            </div>

          </div>
        </div>

        <div id="calendar" class="calendar">

          <div class="row">
            <div class="col-sm-12 col-xs-12 text-center book-header">
              <h4>Book a Table</h4>
            </div>
          </div>  

 
    <form id="frm-book" method="post" onsubmit="return false;" action="<?php echo $baseUrl."/store/BookingConfirmation" ?>"> 

<?php echo CHtml::hiddenField('action','bookATable')?>
<?php echo CHtml::hiddenField('currentController','store')?>
<?php echo CHtml::hiddenField('merchant-id',$merchant_id)?>
<?php echo CHtml::hiddenField('proceedbooking',1)?>
<?php echo CHtml::hiddenField('bookingredirect','bookingsuccess')?>



 

      <?php echo CHtml::hiddenField('merchant-name',$data['restaurant_name']);?>
      <?php echo CHtml::hiddenField('email',$_SESSION['kr_client']['email_address']);?>
      <?php echo CHtml::hiddenField('location',$data['location']);?>
      <?php echo CHtml::hiddenField('mobile',$_SESSION['kr_client']['contact_phone']);?>
      <?php $cust_name = $_SESSION['kr_client']['first_name']." ".$_SESSION['kr_client']['last_name']; 
      echo CHtml::hiddenField('booking_name',$cust_name);?>
      
          <div class="row">
            <div class="col-sm-12 col-xs-12">
              <h6 class="bold">No of People</h6>
              </div>
              <div class="col-sm-6 col-xs-6 males text-center">
              <p>Males</p>
              <button id="remove_item" type="button" class="btn btn-default"  onclick="changeQty(0); return false;">-</button>
              <input type="text" value="0" class="cart_item" name="n_males" id="n_males" readonly>
              <button id="add_item" type="button" class="btn btn-default" onclick="changeQty(1); return false;">+</button>
              
            </div>
            <br>
            <div class="col-sm-6 col-xs-6 females text-center">
              <p>Females</p>
              <button id="remove_item" type="button" class="btn btn-default" onclick="changeQty2(0); return false;">-</button>
              <input type="text" value="0" class="cart_item" name="n_females" id="n_females" readonly>
              <button id="add_item" type="button" class="btn btn-default" onclick="changeQty2(1); return false;">+</button>
              
            </div>
            
            <div class="col-md-6 col-sm-12 col-xs-6">

              <svg id="Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.88 62.94"><g id="calander"><polyline points="11.63 6.25 0.97 6.25 0.97 61.97 61.91 61.97 61.91 6.25 51.24 6.25" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></polyline><rect x="11.63" y="0.97" width="7.92" height="10.68" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></rect><rect x="43.32" y="0.97" width="7.92" height="10.68" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></rect><line x1="19.55" y1="6.26" x2="43.32" y2="6.26" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></line><line x1="0.97" y1="19.57" x2="61.91" y2="19.57" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></line></g></svg>

              <p><input id="etaBookingDate3" type="text" onclick="this.select()" readonly name="date_booking" class="form-control text_col" placeholder="Enter Date"></p>

            </div>
            <div class="col-md-6 col-sm-12 col-xs-6">

              <svg id="Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 63 63"><g id="time"><path d="M62,32A30,30,0,1,1,32,2,30,30,0,0,1,62,32Z" transform="translate(-0.5 -0.5)" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px"></path><polyline points="30.2 17.1 30.2 31.5 45.9 45.9" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px"></polyline></g></svg>

              <p><!-- <input id="etaBookingTime2" type="text" name="booking_time" class="form-control" placeholder="Enter Time"> -->
                
                <select type="text" id="etaBookingTime" class="form-control" name="booking_time" placeholder="Enter Time" style="height: 30px ;position: relative;top: 10px;">
               <option <?php echo ($_POST['delivery_time']=='00:00')?'selected':'' ?> value="00:00">12:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='00:30')?'selected':'' ?> value="00:30">12:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='01:00')?'selected':'' ?> value="01:00">01:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='01:30')?'selected':'' ?> value="01:30">01:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='02:00')?'selected':'' ?> value="02:00">02:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='02:30')?'selected':'' ?> value="02:30">02:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='03:00')?'selected':'' ?> value="03:00">03:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='03:30')?'selected':'' ?> value="03:30">03:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='04:00')?'selected':'' ?> value="04:00">04:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='04:30')?'selected':'' ?> value="04:30">04:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='05:00')?'selected':'' ?> value="05:00">05:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='05:30')?'selected':'' ?> value="05:30">05:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='06:00')?'selected':'' ?> value="06:00">06:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='06:30')?'selected':'' ?> value="06:30">06:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='07:00')?'selected':'' ?> value="07:00">07:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='07:30')?'selected':'' ?> value="07:30">07:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='08:00')?'selected':'' ?> value="08:00">08:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='08:30')?'selected':'' ?> value="08:30">08:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='09:00')?'selected':'' ?> value="09:00">09:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='09:30')?'selected':'' ?> value="09:30">09:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='10:00')?'selected':'' ?> value="10:00">10:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='10:30')?'selected':'' ?> value="10:30">10:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='11:00')?'selected':'' ?> value="11:00">11:00 AM</option>
               <option <?php echo ($_POST['delivery_time']=='11:30')?'selected':'' ?> value="11:30">11:30 AM</option>
               <option <?php echo ($_POST['delivery_time']=='12:00')?'selected':'' ?> value="12:00">12:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='12:30')?'selected':'' ?> value="12:30">12:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='13:00')?'selected':'' ?> value="13:00">01:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='13:30')?'selected':'' ?> value="13:30">01:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='14:00')?'selected':'' ?> value="14:00">02:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='14:30')?'selected':'' ?> value="14:30">02:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='15:00')?'selected':'' ?> value="15:00">03:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='15:30')?'selected':'' ?> value="15:30">03:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='16:00')?'selected':'' ?> value="16:00">04:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='16:30')?'selected':'' ?> value="16:30">04:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='17:00')?'selected':'' ?> value="17:00">05:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='17:30')?'selected':'' ?> value="17:30">05:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='18:00')?'selected':'' ?> value="18:00">06:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='18:30')?'selected':'' ?> value="18:30">06:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='19:00')?'selected':'' ?> value="19:00">07:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='19:30')?'selected':'' ?> value="19:30">07:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='20:00')?'selected':'' ?> value="20:00">08:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='20:30')?'selected':'' ?> value="20:30">08:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='21:00')?'selected':'' ?> value="21:00">09:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='21:30')?'selected':'' ?> value="21:30">09:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='22:00')?'selected':'' ?> value="22:00">10:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='22:30')?'selected':'' ?> value="22:30">10:30 PM</option>
               <option <?php echo ($_POST['delivery_time']=='23:00')?'selected':'' ?> value="23:00">11:00 PM</option>
               <option <?php echo ($_POST['delivery_time']=='23:30')?'selected':'' ?> value="23:30">11:30 PM</option>
            </select>

              </p>

            </div>

           

            

            <!--<div class="col-sm-12 col-xs-12 tnc">
              <a>Terms & Conditions*</a>
            </div>-->

          </div>
          <div class="row">
            <div class="col-sm-12 col-xs-12 all-deals">
              <h6 class="bold">Choose a Deal</h6>


              <div class="deal-list">
                <div class="panel panel-default">
                  <div class="panel-body">
                      <p class="deal-heading">25% off on Johnny Walker</p>                    
                  </div>
                </div>
              </div>
              <div class="deal-list">
                <div class="panel panel-default">
                  <div class="panel-body">
                      <p class="deal-heading">25% off on Johnny Walker</p>
                  </div>
                </div>
              </div>

              <!-- <a data-target="" data-toggle="">More Deals >></a> -->
<!--

              <?php foreach($promo['offer'] as $dealsoffer){ ?>
              <div class="deal-list">
                <div class="panel panel-default">
                  <div class="panel-body deal-heading">
          <a href="getCode('<?php print $_SESSION['kr_merchant_id'] ?>','<?php print $cid ;?>','<?php print $dealsoffer['deals_id'] ?>')">
            <p class="spacer40" style="color: #555;"><?php echo $dealsoffer['deal_desc']; ?>
              <span id="offer_<?php print $dealsoffer['deals_id'] ?>"></span></p></a>

                     
                  </div>
                </div>
              </div>
              <?php } ?>
-->
              
            </div>

          </div>
          <div class="row">
            <div class="col-sm-12 col-xs-12 text-center">
              <button class="but" type="submit">Reserve Table</button>
            </div>
          </div>
      </form>
        </div>
    

</div>
      </div>



<div class="fixed-book visible-xs">
  <ul>
    <li id="pre-order-link"><a>Pre-Order</a></li>
    <li id="book-table-link"><a>Book Table</a></li>
  </ul>
</div>



<div class="pre-order-tab visible-xs">
  <div>
    <div class="row">
      <div class="col-xs-12 text-center book-header">
        <h4>Pre-Order</h4>
        <a id="pre-order-close"><i class="fa fa-close"></i></a>
      </div>

       <div class="col-xs-10 col-xs-offset-1">

        <svg id="Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.88 62.94"><g id="calander"><polyline points="11.63 6.25 0.97 6.25 0.97 61.97 61.91 61.97 61.91 6.25 51.24 6.25" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></polyline><rect x="11.63" y="0.97" width="7.92" height="10.68" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></rect><rect x="43.32" y="0.97" width="7.92" height="10.68" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></rect><line x1="19.55" y1="6.26" x2="43.32" y2="6.26" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></line><line x1="0.97" y1="19.57" x2="61.91" y2="19.57" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></line></g></svg>

        <p><input id="etaBookingDate5" type="text" name="date_booking" class="form-control text_col" placeholder="Enter Date" onclick="this.select()" readonly></p>

      </div>
      
      <div class="col-xs-10 col-xs-offset-1">

        <svg id="Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 63 63"><g id="time"><path d="M62,32A30,30,0,1,1,32,2,30,30,0,0,1,62,32Z" transform="translate(-0.5 -0.5)" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px"></path><polyline points="30.2 17.1 30.2 31.5 45.9 45.9" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px"></polyline></g></svg>

        <p><!-- <input id="etaBookingTime2" type="text" name="booking_time" class="form-control" placeholder="Enter Time"> -->
          
          <select type="text" id="etaBookingTime2" class="form-control" name="booking_time" placeholder="Enter time" >
             <option <?php echo ($_POST['delivery_time']=='00:00')?'selected':'' ?> value="00:00">12:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='00:30')?'selected':'' ?> value="00:30">12:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='01:00')?'selected':'' ?> value="01:00">01:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='01:30')?'selected':'' ?> value="01:30">01:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='02:00')?'selected':'' ?> value="02:00">02:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='02:30')?'selected':'' ?> value="02:30">02:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='03:00')?'selected':'' ?> value="03:00">03:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='03:30')?'selected':'' ?> value="03:30">03:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='04:00')?'selected':'' ?> value="04:00">04:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='04:30')?'selected':'' ?> value="04:30">04:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='05:00')?'selected':'' ?> value="05:00">05:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='05:30')?'selected':'' ?> value="05:30">05:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='06:00')?'selected':'' ?> value="06:00">06:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='06:30')?'selected':'' ?> value="06:30">06:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='07:00')?'selected':'' ?> value="07:00">07:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='07:30')?'selected':'' ?> value="07:30">07:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='08:00')?'selected':'' ?> value="08:00">08:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='08:30')?'selected':'' ?> value="08:30">08:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='09:00')?'selected':'' ?> value="09:00">09:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='09:30')?'selected':'' ?> value="09:30">09:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='10:00')?'selected':'' ?> value="10:00">10:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='10:30')?'selected':'' ?> value="10:30">10:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='11:00')?'selected':'' ?> value="11:00">11:00 AM</option>
             <option <?php echo ($_POST['delivery_time']=='11:30')?'selected':'' ?> value="11:30">11:30 AM</option>
             <option <?php echo ($_POST['delivery_time']=='12:00')?'selected':'' ?> value="12:00">12:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='12:30')?'selected':'' ?> value="12:30">12:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='13:00')?'selected':'' ?> value="13:00">01:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='13:30')?'selected':'' ?> value="13:30">01:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='14:00')?'selected':'' ?> value="14:00">02:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='14:30')?'selected':'' ?> value="14:30">02:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='15:00')?'selected':'' ?> value="15:00">03:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='15:30')?'selected':'' ?> value="15:30">03:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='16:00')?'selected':'' ?> value="16:00">04:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='16:30')?'selected':'' ?> value="16:30">04:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='17:00')?'selected':'' ?> value="17:00">05:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='17:30')?'selected':'' ?> value="17:30">05:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='18:00')?'selected':'' ?> value="18:00">06:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='18:30')?'selected':'' ?> value="18:30">06:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='19:00')?'selected':'' ?> value="19:00">07:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='19:30')?'selected':'' ?> value="19:30">07:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='20:00')?'selected':'' ?> value="20:00">08:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='20:30')?'selected':'' ?> value="20:30">08:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='21:00')?'selected':'' ?> value="21:00">09:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='21:30')?'selected':'' ?> value="21:30">09:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='22:00')?'selected':'' ?> value="22:00">10:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='22:30')?'selected':'' ?> value="22:30">10:30 PM</option>
             <option <?php echo ($_POST['delivery_time']=='23:00')?'selected':'' ?> value="23:00">11:00 PM</option>
             <option <?php echo ($_POST['delivery_time']=='23:30')?'selected':'' ?> value="23:30">11:30 PM</option>
          </select>

        </p>

      </div>

      <div class="col-xs-10 col-xs-offset-1 text-center">
        <button class="form-control but" type="submit">Pre-Order</button>
      </div>

    </div>
  </div>
</div>



<div class="book-table-tab">
  <div>

    <div class="row">
      <div class="col-xs-12 text-center book-header">
        <h4>Book a Table</h4>
        <a id="book-table-close"><i class="fa fa-close"></i></a>
      </div>
    </div>  

    <form id="frm-book" method="post" onsubmit="return false;" action="<?php echo $baseUrl."/store/BookingConfirmation" ?>"> 

      <?php echo CHtml::hiddenField('action','bookATable')?>
      <?php echo CHtml::hiddenField('currentController','store')?>
      <?php echo CHtml::hiddenField('merchant-id',$merchant_id)?>
      <?php echo CHtml::hiddenField('proceedbooking',1)?>
      <?php echo CHtml::hiddenField('bookingredirect','bookingsuccess')?>
      <?php echo CHtml::hiddenField('merchant-name',$data['restaurant_name']);?>
      <?php echo CHtml::hiddenField('email',$_SESSION['kr_client']['email_address']);?>
      <?php echo CHtml::hiddenField('location',$data['location']);?>
      <?php echo CHtml::hiddenField('mobile',$_SESSION['kr_client']['contact_phone']);?>
      <?php $cust_name = $_SESSION['kr_client']['first_name']." ".$_SESSION['kr_client']['last_name']; 
      echo CHtml::hiddenField('booking_name',$cust_name);?>

      <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
          <h6 class="bold">No of People</h6>
        </div>

        <div class="col-xs-10 col-xs-offset-1">
          <div class="row">
            <div class="col-sm-6 col-xs-6 males text-center">

              <svg id="Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60.56 63.02"><g id="pax"><ellipse cx="15.59" cy="10.26" rx="9.28" ry="9.25" style="fill:none;stroke:#fa4616;stroke-linejoin:round;stroke-width:2.018522388147056px"></ellipse><path d="M32,25.39H2.73c0,10.78,4.39,17.08,9.28,19.83V62.5H22.72V45.22C27.61,42.47,32,36.17,32,25.39Z" transform="translate(-1.72 -0.49)" style="fill:none;stroke:#fa4616;stroke-linejoin:round;stroke-width:2.018522388147056px"></path><ellipse cx="44.87" cy="10.26" rx="9.28" ry="9.25" style="fill:none;stroke:#fa4616;stroke-linejoin:round;stroke-width:2.018522388147056px"></ellipse><path d="M46.59,24.07C38.53,24.07,32,36,32,49.28h9.28V62.5H52V49.28h9.28C61.17,36,54.64,24.07,46.59,24.07Z" transform="translate(-1.72 -0.49)" style="fill:none;stroke:#fa4616;stroke-linejoin:round;stroke-width:2.018522388147056px"></path></g></svg>

              <select name="n_males" id="n_males" class="form-control" placeholder="Males">
                <option>Males</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>9</option>
              </select>
              <!--<p>Males</p>
              <button id="remove_item" type="button" class="btn btn-default"  onclick="changeQty(0); return false;">-</button>
              <input type="text" value="0" class="cart_item" name="n_males" id="n_males" readonly>
              <button id="add_item" type="button" class="btn btn-default" onclick="changeQty(1); return false;">+</button>-->

            </div>
            <div class="col-sm-6 col-xs-6 females text-center">

              <svg id="Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60.56 63.02"><g id="pax"><ellipse cx="15.59" cy="10.26" rx="9.28" ry="9.25" style="fill:none;stroke:#fa4616;stroke-linejoin:round;stroke-width:2.018522388147056px"></ellipse><path d="M32,25.39H2.73c0,10.78,4.39,17.08,9.28,19.83V62.5H22.72V45.22C27.61,42.47,32,36.17,32,25.39Z" transform="translate(-1.72 -0.49)" style="fill:none;stroke:#fa4616;stroke-linejoin:round;stroke-width:2.018522388147056px"></path><ellipse cx="44.87" cy="10.26" rx="9.28" ry="9.25" style="fill:none;stroke:#fa4616;stroke-linejoin:round;stroke-width:2.018522388147056px"></ellipse><path d="M46.59,24.07C38.53,24.07,32,36,32,49.28h9.28V62.5H52V49.28h9.28C61.17,36,54.64,24.07,46.59,24.07Z" transform="translate(-1.72 -0.49)" style="fill:none;stroke:#fa4616;stroke-linejoin:round;stroke-width:2.018522388147056px"></path></g></svg>

              <select name="n_females" id="n_females" class="form-control" placeholder="Females">
                <option>Females</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>9</option>
              </select>
              <!--<p>Females</p>
              <button id="remove_item" type="button" class="btn btn-default" onclick="changeQty2(0); return false;">-</button>
              <input type="text" value="0" class="cart_item" name="n_females" id="n_females" readonly>
              <button id="add_item" type="button" class="btn btn-default" onclick="changeQty2(1); return false;">+</button>-->

            </div>
          </div>
        </div>

        <div class="col-xs-10 col-xs-offset-1">

          <svg id="Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.88 62.94"><g id="calander"><polyline points="11.63 6.25 0.97 6.25 0.97 61.97 61.91 61.97 61.91 6.25 51.24 6.25" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></polyline><rect x="11.63" y="0.97" width="7.92" height="10.68" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></rect><rect x="43.32" y="0.97" width="7.92" height="10.68" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></rect><line x1="19.55" y1="6.26" x2="43.32" y2="6.26" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></line><line x1="0.97" y1="19.57" x2="61.91" y2="19.57" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.9370360182267976px"></line></g></svg>

          <input id="etaBookingDate4" type="text" name="date_booking" class="form-control text_col" placeholder="Enter Date" onclick="this.select()" readonly>

        </div>
        <div class="col-xs-10 col-xs-offset-1">

          <svg id="Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 63 63"><g id="time"><path d="M62,32A30,30,0,1,1,32,2,30,30,0,0,1,62,32Z" transform="translate(-0.5 -0.5)" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px"></path><polyline points="30.2 17.1 30.2 31.5 45.9 45.9" style="fill:none;stroke:#fa4616;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px"></polyline></g></svg>

          <!-- <input id="etaBookingTime2" type="text" name="booking_time" class="form-control" placeholder="Enter Time"> -->

            <select type="text" id="etaBookingTime2" class="form-control" name="booking_time" placeholder="Enter Time">
              <option <?php echo ($_POST['delivery_time']=='00:00')?'selected':'' ?> value="00:00">12:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='00:30')?'selected':'' ?> value="00:30">12:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='01:00')?'selected':'' ?> value="01:00">01:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='01:30')?'selected':'' ?> value="01:30">01:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='02:00')?'selected':'' ?> value="02:00">02:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='02:30')?'selected':'' ?> value="02:30">02:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='03:00')?'selected':'' ?> value="03:00">03:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='03:30')?'selected':'' ?> value="03:30">03:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='04:00')?'selected':'' ?> value="04:00">04:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='04:30')?'selected':'' ?> value="04:30">04:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='05:00')?'selected':'' ?> value="05:00">05:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='05:30')?'selected':'' ?> value="05:30">05:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='06:00')?'selected':'' ?> value="06:00">06:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='06:30')?'selected':'' ?> value="06:30">06:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='07:00')?'selected':'' ?> value="07:00">07:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='07:30')?'selected':'' ?> value="07:30">07:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='08:00')?'selected':'' ?> value="08:00">08:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='08:30')?'selected':'' ?> value="08:30">08:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='09:00')?'selected':'' ?> value="09:00">09:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='09:30')?'selected':'' ?> value="09:30">09:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='10:00')?'selected':'' ?> value="10:00">10:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='10:30')?'selected':'' ?> value="10:30">10:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='11:00')?'selected':'' ?> value="11:00">11:00 AM</option>
              <option <?php echo ($_POST['delivery_time']=='11:30')?'selected':'' ?> value="11:30">11:30 AM</option>
              <option <?php echo ($_POST['delivery_time']=='12:00')?'selected':'' ?> value="12:00">12:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='12:30')?'selected':'' ?> value="12:30">12:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='13:00')?'selected':'' ?> value="13:00">01:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='13:30')?'selected':'' ?> value="13:30">01:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='14:00')?'selected':'' ?> value="14:00">02:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='14:30')?'selected':'' ?> value="14:30">02:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='15:00')?'selected':'' ?> value="15:00">03:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='15:30')?'selected':'' ?> value="15:30">03:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='16:00')?'selected':'' ?> value="16:00">04:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='16:30')?'selected':'' ?> value="16:30">04:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='17:00')?'selected':'' ?> value="17:00">05:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='17:30')?'selected':'' ?> value="17:30">05:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='18:00')?'selected':'' ?> value="18:00">06:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='18:30')?'selected':'' ?> value="18:30">06:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='19:00')?'selected':'' ?> value="19:00">07:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='19:30')?'selected':'' ?> value="19:30">07:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='20:00')?'selected':'' ?> value="20:00">08:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='20:30')?'selected':'' ?> value="20:30">08:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='21:00')?'selected':'' ?> value="21:00">09:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='21:30')?'selected':'' ?> value="21:30">09:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='22:00')?'selected':'' ?> value="22:00">10:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='22:30')?'selected':'' ?> value="22:30">10:30 PM</option>
              <option <?php echo ($_POST['delivery_time']=='23:00')?'selected':'' ?> value="23:00">11:00 PM</option>
              <option <?php echo ($_POST['delivery_time']=='23:30')?'selected':'' ?> value="23:30">11:30 PM</option>
            </select>
        </div>
        <!--<div class="col-sm-12 col-xs-12 tnc">
        <a>Terms & Conditions*</a>
        </div>-->

      </div>
      <div class="row">
        <div class="col-xs-10 col-xs-offset-1 all-deals">
        <h6 class="bold">Choose a Deal</h6>
          <div class="deal-list">
            <div class="panel panel-default">
              <div class="panel-body">
                <p class="deal-heading">25% off on Johnny Walker</p>                    
              </div>
            </div>
          </div>
          <div class="deal-list">
            <div class="panel panel-default">
              <div class="panel-body">
                <p class="deal-heading">25% off on Johnny Walker</p>
              </div>
            </div>
          </div>

        <!-- <a data-target="" data-toggle="">More Deals >></a> -->
        <!--
        <?php foreach($promo['offer'] as $dealsoffer){ ?>
        <div class="deal-list">
        <div class="panel panel-default">
        <div class="panel-body deal-heading">
        <a href="getCode('<?php print $_SESSION['kr_merchant_id'] ?>','<?php print $cid ;?>','<?php print $dealsoffer['deals_id'] ?>')">
        <p class="spacer40" style="color: #555;"><?php echo $dealsoffer['deal_desc']; ?>
        <span id="offer_<?php print $dealsoffer['deals_id'] ?>"></span></p></a>
        </div>
        </div>
        </div>
        <?php } ?>
        -->
        </div>

      </div>
      <div class="row">
        <div class="col-sm-12 col-xs-12 text-center">
          <button class="but" type="submit">Reserve Table</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script>
jQuery(window).load(function(){
jQuery('body').addClass('description-page');
})
</script>

