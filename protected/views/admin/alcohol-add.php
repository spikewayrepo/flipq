<?php ini_set('display_errors',1); 
 error_reporting(E_ALL); ?>
<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/admin/Alcohol/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/admin/Alcohol" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
</div>

<?php 
if (isset($_GET['id'])){
	if (!$data=Yii::app()->functions->GetAlcohol($_GET['id'])){
		echo "<div class=\"uk-alert uk-alert-danger\">".
		Yii::t("default","Sorry but we cannot find what you are looking for.")."</div>";
		return ;
	}
}
?>                                   

<div class="spacer"></div>

<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','addAlcohol')?>
<?php echo CHtml::hiddenField('id',isset($_GET['id'])?$_GET['id']:"");?>
<?php if (!isset($_GET['id'])):?>
<?php echo CHtml::hiddenField("redirect",Yii::app()->request->baseUrl."/admin/Alcohol")?>
<?php endif;?>


<?php if ( Yii::app()->functions->multipleField()==2):?>

<ul data-uk-tab="{connect:'#tab-content'}" class="uk-tab uk-active">
    <li class="uk-active" ><a href="#"><?php echo t("English")?></a></li>
    <?php if ( $fields=Yii::app()->functions->getLanguageField()):?>  
    <?php foreach ($fields as $f_val): ?>
    <li class="" ><a href="#"><?php echo $f_val;?></a></li>
    <?php endforeach;?>
    <?php endif;?>
</ul>

<ul class="uk-switcher" id="tab-content">

  <li class="uk-active">      
  
  <div class="uk-form-row">
   <label class="uk-form-label"><?php echo Yii::t("default","Alcohol Name")?></label>
  <?php echo CHtml::textField('alcohol_name',
  isset($data['alcohol_name'])?stripslashes($data['alcohol_name']):""
  ,array(
  'class'=>'uk-form-width-large',
  'data-validation'=>"required"
  ))?>  
    </div>    
    
    <div style="height:10px;"></div>
  
   </li>
   
   <?php 
   $alcohol_name_trans=isset($data['alcohol_name_trans'])?json_decode($data['alcohol_name_trans'],true):'';   
   ?>
   
   <?php if (is_array($fields) && count($fields)>=1):?>
   <?php foreach ($fields as $key_f => $f_val): ?>
   <li>
   
   <div class="uk-form-row">
	   <label class="uk-form-label"><?php echo Yii::t("default","Alcohol Name")?></label>
	  <?php echo CHtml::textField("alcohol_name_trans[$key_f]",
	  array_key_exists($key_f,(array)$alcohol_name_trans)?$alcohol_name_trans[$key_f]:''
	  ,array(
	  'class'=>'uk-form-width-large',
	  ))?>  
   </div>    
   
   <div style="height:10px;"></div>
   
   </li>
   <?php endforeach;?>
   <?php endif;?>
</ul>

<?php else :?>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo t("Type")?></label>
  <?php $list=Yii::app()->functions->AlcoholCategory(true);?>

  <select name="category">
  <option value="">Select</option>
  <?php foreach($list as $res){?>
  <?php if($data['category']==$res){ ?>
  <option  selected value="<?php echo $res;?>"><?php echo $res;?></option>
  <?php }else{?>
    <option value="<?php echo $res;?>"><?php echo $res;?></option>

  <?php } ?>
  <?php }?>
  </select>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo t("Manufacturing")?></label>
	<?php $list=Yii::app()->functions->AlcoholSubCategory(true);?>

  <select name="sub_category">
  <option value="">Select</option>
  <?php foreach($list as $res){?>
  <?php if($data['sub_category']==$res){ ?>
  <option  selected value="<?php echo $res;?>"><?php echo $res;?></option>
  <?php }else{?>
    <option value="<?php echo $res;?>"><?php echo $res;?></option>

  <?php } ?>
  <?php }?>
  </select>	  
		  
		  
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Brand Name")?></label>
  <?php 
  echo CHtml::textField('alcohol_name',
  isset($data['alcohol_name'])?$data['alcohol_name']:""
  ,array('class'=>"uk-form-width-large",'data-validation'=>"required"))
  ?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo t("Status")?></label>
  <?php echo CHtml::dropDownList('status',
  isset($data['status'])?$data['status']:"",
  (array)statusList(),          
  array(
  'class'=>'uk-form-width-large',
  'data-validation'=>"required"
  ))?>
</div>

<?php endif;?>

<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>