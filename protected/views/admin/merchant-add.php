<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<?php 
//print_r($this->data);
$url_login='';
if (isset($_GET['id'])){
	if (!$data=Yii::app()->functions->getMerchant($_GET['id'])){
		echo "<div class=\"uk-alert uk-alert-danger\">".
		Yii::t("default","Sorry but we cannot find what your are looking for.")."</div>";
		return ;
	} else {
		$url_login=baseUrl()."/merchant/autologin/id/".$data['merchant_id']."/token/".$data['password'];		
	}
}
?>                              

<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/admin/merchantAdd" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/admin/merchant" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>

<?php if (!empty($url_login)):?>
<a target="_blank" href="<?php echo $url_login;?>" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","AutoLogin")?></a>
<?php endif;?>

</div>

<div class="spacer"></div>

<div class="merchant-add"></div>

<div class="right" style="margin-top:-30px">
<?php if ($data['is_commission']==2):?>
<h3 style="margin:0 0 8px;"><?php echo t("Charges Type")?>: <span class="uk-text-danger"><?php echo t("Commission")?><span></h3>
<?php else :?>
<h3 style="margin:0 0 8px;"><?php echo t("Charges Type")?>: <span class="uk-text-danger"><?php echo t("Membership")?></span></h3>
<?php endif;?>
</div>
<div class="clear"></div>

<ul data-uk-tab="{connect:'#tab-content'}" class="uk-tab uk-active">
<li class="uk-active"><a href="#"><?php echo t("Restaurant Information")?></a></li>
<!--<li class=""><a href="#"><?php echo Yii::t("default","Login Information")?></a></li>-->
<li class=""><a href="#"><?php echo Yii::t("default","Membership")?></a></li>
<li class=""><a href="#"><?php echo Yii::t("default","Featured")?></a></li>
<li class=""><a href="#"><?php echo Yii::t("default","Payment History")?></a></li>
<li class=""><a href="#"><?php echo Yii::t("default","Google Map")?></a></li>
<li class=""><a href="#"><?php echo Yii::t("default","Commission Settings")?></a></li>
</ul>     

<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','addMerchant')?>
<?php echo CHtml::hiddenField('id',isset($_GET['id'])?$_GET['id']:"");?>
<?php echo CHtml::hiddenField('old_status',isset($data['status'])?$data['status']:"")?>
<?php if (!isset($_GET['id'])):?>
<?php echo CHtml::hiddenField("redirect",Yii::app()->request->baseUrl."/admin/merchantAdd")?>
<?php endif;?>

<ul class="uk-switcher uk-margin " id="tab-content">
<li class="uk-active">
    <fieldset>        
    
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Restaurant Slug")?></label>
          <?php echo CHtml::textField('restaurant_slug',
          isset($data['restaurant_slug'])?stripslashes($data['restaurant_slug']):""
          ,array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
    
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Restaurant name")?></label>
          <?php echo CHtml::textField('restaurant_name',
          isset($data['restaurant_name'])?stripslashes($data['restaurant_name']):""
          ,array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
		
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Holding Company name")?></label>
          <?php echo CHtml::textField('holdingcompany_name',
          isset($data['holdingcompany_name'])?stripslashes($data['holdingcompany_name']):""
          ,array(
		  'placeholder'=>'(Optional)',
          'class'=>'uk-form-width-large'
          ))?>
        </div>
        
         <?php if ( Yii::app()->functions->getOptionAdmin('merchant_reg_abn')=="yes"):?>
         <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","ABN")?></label>
          <?php echo CHtml::textField('abn',
          isset($data['abn'])?$data['abn']:""
          ,array(
          'class'=>'uk-form-width-large',
          //'data-validation'=>"required"
          ))?>
        </div>
        <?php endif;?>
        
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Restaurant phone")?></label>
          <?php echo CHtml::textField('restaurant_phone',
          isset($data['restaurant_phone'])?$data['restaurant_phone']:""
          ,array(
          'class'=>'uk-form-width-large'
          ))?>
        </div>
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Contact name")?></label>
          <?php echo CHtml::textField('contact_name',
          isset($data['contact_name'])?$data['contact_name']:""
          ,array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Contact phone")?></label>
          <?php echo CHtml::textField('contact_phone',
          isset($data['contact_phone'])?$data['contact_phone']:""
          ,array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Contact email")?></label>
          <?php echo CHtml::textField('contact_email',
          isset($data['contact_email'])?$data['contact_email']:""
          ,array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
        
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Promoter's phone")?></label>
          <?php echo CHtml::textField('promoter_phone',
          isset($data['promoter_phone'])?$data['promoter_phone']:""
          ,array(
          'class'=>'uk-form-width-large numeric_only',
          'data-validation'=>"required"
          ))?>
        </div>
		
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Manager's phone")?></label>
          <?php echo CHtml::textField('manager_phone',
          isset($data['manager_phone'])?$data['manager_phone']:""
          ,array(
          'class'=>'uk-form-width-large numeric_only',
          'data-validation'=>"required"
          ))?>
        </div>
		
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","General Manager's phone")?></label>
          <?php echo CHtml::textField('g_manager_phone',
          isset($data['g_manager_phone'])?$data['g_manager_phone']:""
          ,array(
          'class'=>'uk-form-width-large numeric_only',
          'data-validation'=>"required"
          ))?>
        </div>
		
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Entry Criteria")?></label>
          <?php echo CHtml::textField('entry_criteria',
          isset($data['entry_criteria'])?$data['entry_criteria']:""
          ,array(
          'class'=>'uk-form-width-large'
          ))?>
        </div>
		
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Cost for Two")?></label>
          <?php echo CHtml::textField('cost_for_two',
          isset($data['cost_for_two'])?$data['cost_for_two']:""
          ,array(
          'class'=>'uk-form-width-medium numeric_only'
          ))?>
        </div>
		
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Pint of Beer Cost")?></label>
          <?php echo CHtml::textField('pint_of_beer',
          isset($data['pint_of_beer'])?$data['pint_of_beer']:""
          ,array(
          'class'=>'uk-form-width-medium numeric_only'
          ))?>
        </div>
		
		<div class="uk-form-row">
			<label class="uk-form-label"><?php echo t("Stag Entry")?></label>    	  
			<?php echo CHtml::checkBox('stag_entry',
			$data['stag_entry']==2?true:false
			,array(
			'class'=>"icheck",
			//'onclick'=>"checkfunction();"
			//'value'=>2
			))?>	  	  	  
		</div>
		
		<div class="uk-form-row">
			<label class="uk-form-label"><?php echo t("Corporate Stag Entry")?></label>    	  
			<?php echo CHtml::checkBox('corporate_stag_entry',
			$data['corporate_stag_entry']==2?true:false
			,array(
			'class'=>"icheck",
			'value'=>2
			))?>	  	  	  
		</div>
		
		<div class="uk-form-row">
			<label class="uk-form-label"><?php echo t("Live Band")?></label>    	  
			<?php echo CHtml::checkBox('live_band',
			$data['live_band']==2?true:false
			,array(
			'class'=>"icheck",
			'value'=>2
			))?>	  	  	  
		</div>
		
		<div class="uk-form-row">
			<label class="uk-form-label"><?php echo t("Dance Floor")?></label>    	  
			<?php echo CHtml::checkBox('dance_floor',
			$data['dance_floor']==2?true:false
			,array(
			'class'=>"icheck",
			'value'=>2
			))?>	  	  	  
		</div>
		
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Previous Awards")?></label>
          <?php echo CHtml::textField('awards',
          isset($data['awards'])?$data['awards']:""
          ,array(
          'class'=>'big-textarea'
          ))?>
        </div>
		
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo t("Country")?></label>
          <?php echo CHtml::dropDownList('country_code',
          isset($data['country_code'])?$data['country_code']:"",
          (array)Yii::app()->functions->CountryList(),          
          array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
		
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Street address")?></label>
          <?php echo CHtml::textField('street',
          isset($data['street'])?$data['street']:""
          ,array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div> 
		
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Area / Location")?></label>
          <?php echo CHtml::textField('location',
          isset($data['location'])?$data['location']:""
          ,array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
                
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","City")?></label>
          <?php echo CHtml::textField('city',
          isset($data['city'])?$data['city']:""
          ,array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
        
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Post code/Zip code")?></label>
          <?php echo CHtml::textField('post_code',
          isset($data['post_code'])?$data['post_code']:""
          ,array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
		
		<div class="uk-form-row">
		<label class="uk-form-label"><?php echo Yii::t("default","State/Region")?></label>
		<?php echo CHtml::textField('state',
		isset($data['state'])?$data['state']:""
		,array(
		'class'=>'uk-form-width-large',
		'data-validation'=>"required"
		))?>
		</div>    		
		
		<?php
			
			if ($data['btype']=="DJ"){
		?>
			
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Music Type")?></label>
		  <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Music Type
					 <span class="caret"></span></button>
					<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
					<li>
					<?php 
						$musicarr = json_decode($data['music_name']);
						$list=Yii::app()->functions->MusicType(true);
						foreach($list as $res=>$value){	?>
						<div class="col-sm-6">
						<input type="checkbox" <?php if(in_array($res,$musicarr)){ echo "checked" ;} ?>  name="MusicType[]" value='<?php print $res ?>' /><?php print $value ?>					
						</div>		
						<?php }
						?>
					</li>
					</ul>
					</div>
		<?php } else 
		{?>

			<div class="uk-form-row">
			  <label class="uk-form-label"><?php echo Yii::t("default","Cuisine")?></label>
				<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Cuisines
						 <span class="caret"></span></button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
						<li>
						<?php 
							$cuisinearr = json_decode($data['cuisine']);
						?>
						<?php
							$list=Yii::app()->functions->Cuisine(true);
							foreach($list as $res=>$value){	
						?>
						<div class="col-sm-6">
						<input type="checkbox"  <?php if(in_array($res,$cuisinearr)){ echo "checked" ;} ?>  name="cuisine[]" value='<?php print $res ?>' /><?php print $value ?>					
						</div>		
						<?php }
						?>
						</li>
						</ul>
				</div>
	  
			  <?php /*
			  <?php echo CHtml::dropDownList('cuisine[]',
			  isset($data['cuisine'])?(array)json_decode($data['cuisine']):"",
			  (array)Yii::app()->functions->Cuisine(true),          
			  array(
			  'class'=>'uk-form-width-large chosen',
			  'multiple'=>true,
			  'data-validation'=>"required"
			  ))?>   */?>
			</div>
			
			<div class="uk-form-row">
				<label class="uk-form-label"><?php echo Yii::t("default","Facilities")?></label>
				<button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Highlights
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
					<li>
					<?php 
						$highlightsarr = json_decode($data['highlights']);
						$list=Yii::app()->functions->Highlights(true);
						foreach($list as $res=>$value)
						{ ?>
							<div class="col-sm-6">
							<input type="checkbox" <?php if(in_array($res,$highlightsarr)){ echo "checked" ;} ?>  name="Highlights[]" value='<?php print $res ?>' /><?php print $value ?>					
							</div>		
				  <?php } ?>
					</li>
				</ul>
			</div>
			
  <?php }?>
		
		<?php /* 
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Pick Up or Delivery?")?></label>
          <?php echo CHtml::dropDownList('service',
          isset($data['service'])?$data['service']:"",
          (array)Yii::app()->functions->Services(),          
          array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
         */
		 ?>
		 
		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Website Address")?></label>
          <?php echo CHtml::textField('website_address',
          isset($data['website_address'])?stripslashes($data['website_address']):""
          ,array(
          'class'=>'uk-form-width-large',
          ))?>
        </div> 
		 
		 
         <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Published Merchant")?></label>
          <?php 
          echo CHtml::checkBox('is_ready',
          $data['is_ready']==2?true:false
          ,array(
            'value'=>2,
            'class'=>"icheck"
          ))
          ?>
        </div>
        
        <div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Status")?></label>
          <?php echo CHtml::dropDownList('status',
          isset($data['status'])?$data['status']:"",
          (array)clientStatus(),          
          array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>

    </fieldset>
	
	<br>
	
	<div class="uk-form-row">
		<label class="uk-form-label"></label>
		<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
	</div>
		
</li>

<!--<li>
<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Username")?></label>
  <?php echo CHtml::textField('username',
  isset($data['username'])?$data['username']:""
  ,array(
  'class'=>'uk-form-width-large'
  ))?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Password")?></label>
  <?php echo CHtml::passwordField('password',
  ""
  ,array(
  'class'=>'uk-form-width-large',
  'autocomplete'=>"off"
  ))?>
</div>
</li>-->

<li>
<?php 
Yii::app()->functions->data="list";
?>
<div class="uk-form-row">
  <!--<label class="uk-form-label"><?php echo Yii::t("default","Package Name")?></label>
  <span class="uk-text-bold"><?php echo isset($data['package_name'])?ucwords($data['package_name']):"Not Available";?></span>-->
  <label class="uk-form-label"><?php echo Yii::t("default","Package Name")?></label>
  <?php 
  echo CHtml::dropDownList('package_id',
  $data['package_id']
  ,Yii::app()->functions->getPackagesList())
  ?>
  </div>
  
  <div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Package Price")?></label>
  <span class="uk-text-primary"><?php echo adminCurrencySymbol().standardPrettyFormat($data['package_price'])?></span>
  </div>
  
  <div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Membership Expired On")?></label>
  <span class="uk-text-success">
  <?php 
  echo CHtml::hiddenField('membership_expired',$data['membership_expired']);
  echo CHtml::textField('membership_expired1',FormatDateTime($data['membership_expired'],false),array(
    'class'=>"j_date",
    'data-validation'=>"requiredx",
    'data-id'=>'membership_expired'
  ))
  ?>
  </div>  

<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

  </li>

<li>
 <div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Featured")?>?</label>
  <?php 
  echo CHtml::checkBox('is_featured',
  $data['is_featured']==2?true:false
  ,array('class'=>"icheck",'value'=>2))
  ?>
  <p class="uk-text-muted"><?php echo Yii::t("default","Check this if you want this merchant featured on homepage")?></p>
  </div>
  
<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>
  
</li>

<li>
  <?php if ($payment_res=Yii::app()->functions->getMerchantPaymentTransaction($_GET['id'])):?>
	  <table id="table_list" class="uk-table uk-table-hover uk-table-striped uk-table-condensed">
	  <caption><?php echo Yii::t("default","Merchant Payment History")?></caption>
	   <thead>	
	   <th><?php echo Yii::t("default","Package Name")?></th>
	   <th><?php echo Yii::t("default","Amount")?></th>
	   <th><?php echo Yii::t("default","Expired On")?></th>
	   <th><?php echo Yii::t("default","Payment Type")?></th>
	   <th><?php echo Yii::t("default","Status")?></th>
	   <th><?php echo Yii::t("default","Transaction Date")?></th>	   
	   </thead>    
	   <tbody>
	  <?php foreach ($payment_res as $val):?>
	  <tr>
	   <td><?php echo $val['package_name']?></td>
	   <td><?php echo Yii::app()->functions->standardPrettyFormat($val['price'])?></td>
	   <td><?php echo prettyDate($val['membership_expired'])?></td>
	   <td><?php echo strtoupper($val['payment_type']);?></td>
	   <td><?php echo Yii::t("default",$val['status'])?></td>
	   <td><?php echo prettyDate($val['date_created'],true)?></td>
	  </tr>
	  <?php endforeach;?>  
	  </tbody>
	  </table>
  <?php else :?>	  
  <p class="uk-text-warning"><?php echo Yii::t("default","No Payment records")?></p>
  <?php endif;?>

<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</li>

<li>

 <?php 
 $lat=''; $lng='';
 if (!empty($merchant_id)){
    $lat=getOption($merchant_id,'merchant_latitude');
    $lng=getOption($merchant_id,'merchant_longtitude');
 }
 ?>

 <div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Latitude")?></label>
  <?php echo CHtml::textField('merchant_latitude',
  $lat
  ,array(
  'class'=>'uk-form-width-large',
  //'data-validation'=>"required"
  ))?>
</div>

 <div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Longitude")?></label>
  <?php echo CHtml::textField('merchant_longtitude',
  $lng
  ,array(
  'class'=>'uk-form-width-large',
  //'data-validation'=>"required"
  ))?>
</div>

<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</li>

</form>

<form class="uk-form uk-form-horizontal forms" id="cmsn_form" method="post">
<?php echo CHtml::hiddenField('action','addCommission')?>
<?php echo CHtml::hiddenField('id',isset($_GET['id'])?$_GET['id']:"");?>
<?php //echo CHtml::hiddenField('old_status',isset($data['status'])?$data['status']:"")?>
<?php //if (isset($_GET['id'])):?>
<?php //echo CHtml::hiddenField("redirect",Yii::app()->request->baseUrl."/admin/merchantAdd")?>
<?php //endif;?>

	<li>
		
	<div class="uk-form-row">
		<div class="row" style="padding-left:10px">
			<h2 class="text-center"><?php echo Yii::t("default","Categories")?></h2>
		</div>
	</div>	
		
	<div class="uk-form-row" id="dealscom">
		<div class="row" style="padding-left:10px">
			<label class="uk-form-label"><h3><?php echo Yii::t("default","Deals") ?></h3></label>
		</div>
		
		<?php 
			//print_r($_GET['id']);
			if( $deals_row = FunctionsV3::GetCommissionRulesbyCategory($_GET['id'], 'Deals') )
				{
					
					foreach($deals_row as $deals_row1)
					{ ?>
		
						<div class="row">
							
							<div class="col-sm-2">
								From Amount
								<input type="text" name="from_amount[]"  class="uk-form-width from_amount_deals" value="<?php echo $deals_row1['from_amount'];?>" readonly>
							</div>
							
							<div class="col-sm-2">
								To Amount
								<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_deals" value="<?php echo $deals_row1['to_amount'];?>" required>
							</div>
							
							<div class="col-sm-2">
								Type
								<select name="type[]" class="uk-form-width type_deals" required>
									<option value="">Please Select</option>
									<?php if($deals_row1['commission_type']=='Fixed'){ ?>
										<option selected value="Fixed">Fixed</option>
										<option value="Percentage">Percentage</option>
									<?php } else {?>
										<option value="Fixed">Fixed</option>
										<option selected value="Percentage">Percentage</option>
									<?php } ?>
								</select>
							</div>
							
							<div class="col-sm-2">
								Commission Value
								<input type="text" name="commission_value[]" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_deals" value="<?php echo $deals_row1['commission_value'];?>" required>
							</div>
							
							<div class="col-sm-2 hidden">
								<input type="text" name="category[]" value="Deals" >
							</div>
							
							<div class="col-sm-2 hidden">
								<input type="text" name="commission_id[]" value="<?php echo $deals_row1['id']; ?>" >
							</div>
						
						</div>  
			<?php	}
			
				}
				
				else 
				{ ?> 
					<div class="row">
						
						<div class="col-sm-2">
							From Amount
							<input type="text" name="from_amount[]"  class="uk-form-width from_amount_deals" value="0" readonly>
						</div>
						
						<div class="col-sm-2">
							To Amount
							<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_deals" value="" required>
						</div>
						
						<div class="col-sm-2">
							Type
							<select name="type[]" class="uk-form-width type_deals" required>
								<option value="">Please Select</option>
								<option value="fixed">Fixed</option>
								<option value="Percentage">Percentage</option>
							</select>
						</div>
						
						<div class="col-sm-2">
							Commission Value
							<input type="text" name="commission_value[]" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_deals" value="" required>
						</div>
						
						<div class="col-sm-2 hidden">
							<input type="text" name="category[]" value="Deals" >
						</div>
						
						<div class="col-sm-2 hidden">
							<input type="text" name="commission_id[]" value="" >
						</div>
						
					</div> 
		<?php	} ?>
		
		<div class="rowaaa" style="padding-left: 10px">
			<a class="btn btn-info add_deal_row">
			  <span class="fa fa-plus"></span>
			</a>
		</div>

	</div>
	
	<div class="uk-form-row" id="comboscom">
		<div class="row" style="padding-left:10px">
			<label class="uk-form-label"><h3><?php echo Yii::t("default","Combos") ?></h3></label>
		</div>
		<?php if( $combos_row = FunctionsV3::GetCommissionRulesbyCategory($_GET['id'], 'Combo') )
				{
					
					foreach($combos_row as $combos_row1)
					{ ?>
			
						<div class="row">

							<div class="col-sm-2">
								From Amount
								<input type="text" name="from_amount[]" class="uk-form-width from_amount_combos" value="<?php echo $combos_row1['from_amount']; ?>" readonly>
							</div>
							
							<div class="col-sm-2">
								To Amount
								<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_combos" value="<?php echo $combos_row1['to_amount']; ?>" required>
							</div>
							
							<div class="col-sm-2">
								Type
								<select name="type[]" class="uk-form-width type_combos" selected="<?php echo $combos_row1['commission_type']; ?>" required>
									<option value="">Please Select</option>
									<?php if($combos_row1['commission_type']=='Fixed'){ ?>
										<option selected value="Fixed">Fixed</option>
										<option value="Percentage">Percentage</option>
									<?php } else {?>
										<option value="Fixed">Fixed</option>
										<option selected value="Percentage">Percentage</option>
									<?php } ?>
								</select>
							</div>
							
							<div class="col-sm-2">
								Commission Value
								<input type="text" name="commission_value[]" onkeypress="return isNumberKey(event)" value="<?php echo $combos_row1['commission_value']; ?>" class="uk-form-width commission_value_combos" required>
							</div>
							
							<div class="col-sm-2 hidden">
								<input type="text" name="category[]"  class="uk-form-width" value="Combo" >
							</div>
						
							<div class="col-sm-2 hidden">
								<input type="text" name="commission_id[]" value="<?php echo $combos_row1['id']; ?>" >
							</div>
						
						</div>  
			<?php   }
				} else
				{ ?>
				
					<div class="row">

						<div class="col-sm-2">
							From Amount
							<input type="text" name="from_amount[]"  class="uk-form-width from_amount_combos" value="0" readonly>
						</div>
						
						<div class="col-sm-2">
							To Amount
							<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_combos" value="" required>
						</div>
						
						<div class="col-sm-2">
							Type
							<select name="type[]" class="uk-form-width type_combos" required>
								<option value="">Please Select</option>
								<option value="fixed">Fixed</option>
								<option value="Percentage">Percentage</option>
							</select>
						</div>
						
						<div class="col-sm-2">
							Commission Value
							<input type="text" name="commission_value[]" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_combos" required>
						</div>
						
						<div class="col-sm-2 hidden">
							<input type="text" name="category[]"  class="uk-form-width" value="Combo" >
						</div>
					
						<div class="col-sm-2 hidden">
							<input type="text" name="commission_id[]" value="" >
						</div>
					
					</div>  
		<?php	} ?>
		
		<div class="rowaaa" style="padding-left: 10px">
			<a class="btn btn-info add_combos_row">
			  <span class="fa fa-plus"></span>
			</a>
		</div>

	</div>
	
	<div class="uk-form-row" id="preorderscom">
		<div class="row" style="padding-left:10px">
			<label class="uk-form-label"><h3><?php echo Yii::t("default","Pre-Orders") ?></h3></label>
		</div>
		
	<?php if( $preorders_row = FunctionsV3::GetCommissionRulesbyCategory($_GET['id'], 'PreOrder') )
			{
				foreach($preorders_row as $preorders_row1)
				{ ?>
				
					<div class="row">

						<div class="col-sm-2">
							From Amount
							<input type="text" name="from_amount[]"  class="uk-form-width from_amount_preorders" value="<?php echo $preorders_row1['from_amount']; ?>" readonly>
						</div>
						
						<div class="col-sm-2">
							To Amount
							<input type="text" name="to_amount[]"  onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_preorders" value="<?php echo $preorders_row1['to_amount']; ?>" required>
						</div>
						
						<div class="col-sm-2">
							Type
							<select name="type[]" class="uk-form-width type_preorders" required>
								<option value="">Please Select</option>
								<?php if($preorders_row1['commission_type']=='Fixed'){ ?>
										<option selected value="Fixed">Fixed</option>
										<option value="Percentage">Percentage</option>
									<?php } else {?>
										<option value="Fixed">Fixed</option>
										<option selected value="Percentage">Percentage</option>
									<?php } ?>
							</select>
						</div>
						
						<div class="col-sm-2">
							Commission Value
							<input type="text" name="commission_value[]" value="<?php echo $preorders_row1['commission_value']; ?>" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_preorders" required>
						</div>
						
						<div class="col-sm-2 hidden">
							<input type="text" name="category[]"  class="uk-form-width" value="PreOrder" >
						</div>					
					
						<div class="col-sm-2 hidden">
							<input type="text" name="commission_id[]" value="<?php echo $preorders_row1['id']; ?>" >
						</div>
					
					</div>
		<?php 	}
			} 
			else { ?>
				
				<div class="row">
				
					<div class="col-sm-2">
						From Amount
						<input type="text" name="from_amount[]"  class="uk-form-width from_amount_preorders" value="0" readonly>
					</div>
					
					<div class="col-sm-2">
						To Amount
						<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_preorders" value="" required>
					</div>
					
					<div class="col-sm-2">
						Type
						<select name="type[]" class="uk-form-width type_preorders" required>
							<option value="">Please Select</option>
							<option value="fixed">Fixed</option>
							<option value="Percentage">Percentage</option>
						</select>
					</div>
					
					<div class="col-sm-2">
						Commission Value
						<input type="text" name="commission_value[]" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_preorders" required>
					</div>
					
					<div class="col-sm-2 hidden">
						<input type="text" name="category[]"  class="uk-form-width" value="PreOrder" >
					</div>
				
					<div class="col-sm-2 hidden">
						<input type="text" name="commission_id[]" value="" >
					</div>
					
				</div>
				
	<?php	} ?>
			
		<div class="rowaaa" style="padding-left: 10px">
			<a class="btn btn-info add_preorders_row">
			  <span class="fa fa-plus"></span>
			</a>
		</div>

	</div>
	
	<div class="uk-form-row" id="booktablecom">
		<div class="row" style="padding-left:10px">
			<label class="uk-form-label"><h3><?php echo Yii::t("default","Table Booking") ?></h3></label>
		</div>
		
	<?php if( $booktable_row = FunctionsV3::GetCommissionRulesbyCategory($_GET['id'], 'Book_Table') )
			{
			
				foreach($booktable_row as $booktable_row1)
				{ ?>	
		
					<div class="row">

						<div class="col-sm-2">
							From Amount
							<input type="text" name="from_amount[]"  class="uk-form-width from_amount_booktable" value="<?php echo $booktable_row1['from_amount']; ?>" readonly>
						</div>
						
						<div class="col-sm-2">
							To Amount
							<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_booktable" value="<?php echo $booktable_row1['to_amount']; ?>" required>
						</div>
						
						<div class="col-sm-2">
							Type
							<select name="type[]" class="uk-form-width type_booktable" selected="<?php echo $booktable_row1['commission_type']; ?>" required>
								<option value="">Please Select</option>
								<?php if($booktable_row1['commission_type']=='Fixed'){ ?>
										<option selected value="Fixed">Fixed</option>
										<option value="Percentage">Percentage</option>
									<?php } else { ?>
										<option value="Fixed">Fixed</option>
										<option selected value="Percentage">Percentage</option>
									<?php } ?>
							</select>
						</div>
						
						<div class="col-sm-2">
							Commission Value
							<input type="text" name="commission_value[]" value="<?php echo $booktable_row1['commission_value']; ?>" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_booktable" required>
						</div>
						
						<div class="col-sm-2 hidden">
							<input type="text" name="category[]"  class="uk-form-width" value="Book_Table" >
						</div>
					
						<div class="col-sm-2 hidden">
							<input type="text" name="commission_id[]" value="<?php echo $booktable_row1['id']; ?>" >
						</div>
					
					</div>  
		<?php 	}
			} else 
			
			{ ?>
				<div class="row">

					<div class="col-sm-2">
						From Amount
						<input type="text" name="from_amount[]"  class="uk-form-width from_amount_booktable" value="0" readonly>
					</div>
					
					<div class="col-sm-2">
						To Amount
						<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_booktable" value="" required>
					</div>
					
					<div class="col-sm-2">
						Type
						<select name="type[]"  class="uk-form-width type_booktable" required>
							<option value="">Please Select</option>
							<option value="fixed">Fixed</option>
							<option value="Percentage">Percentage</option>
						</select>
					</div>
					
					<div class="col-sm-2">
						Commission Value
						<input type="text" name="commission_value[]" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_booktable" required>
					</div>
					
					<div class="col-sm-2 hidden">
						<input type="text" name="category[]"  class="uk-form-width" value="Book_Table" >
					</div>
				
					<div class="col-sm-2 hidden">
						<input type="text" name="commission_id[]" value="" >
					</div>
					
				</div> 
	<?php	} ?>
	
		<div class="rowaaa" style="padding-left: 10px">
			<a class="btn btn-info add_booktable_row">
				<span class="fa fa-plus"></span>
			</a>
		</div>

	</div>
	
		<!--<div class="uk-form-row">
		  <label class="uk-form-label"><?php echo Yii::t("default","Start Date")?></label>
		  <?php echo CHtml::textField('start_date',
		  isset($_GET['start_date'])?$_GET['start_date']:""
		  ,array(
		  'class'=>'uk-form-width-large j_date',
		  'data-id'=>'start_date'
		  ))?>
		</div>
		
		<div class="uk-form-row">
		  <label class="uk-form-label"><?php echo Yii::t("default","End Date")?></label>
		  <?php echo CHtml::textField('end_date',
		  isset($_GET['end_date'])?$_GET['end_date']:""
		  ,array(
		  'class'=>'uk-form-width-large j_date',
		  'data-id'=>'end_date'
		  ))?>
		</div>-->

		<div class="uk-form-row">
          <label class="uk-form-label"><?php echo Yii::t("default","Status")?></label>
          <?php echo CHtml::dropDownList('status',
          isset($data['status'])?$data['status']:"",
          (array)Statuslist(),          
          array(
          'class'=>'uk-form-width-large',
          'data-validation'=>"required"
          ))?>
        </div>
		


<!--<div class="uk-form-row">
  <label class="uk-form-label"><?php //echo Yii::t("default","Enabled Commission")?>?</label>
  <?php   
  /*echo CHtml::checkBox('is_commission',
  $data['is_commission']==2?true:false
  ,array(
    'value'=>2,
    'class'=>"icheck"
  ))*/
  ?> 
</div>

<p class="uk-text-danger">
<?php echo t("Note: If this is ticked, the merchant will be charged commission per order and membership package will be ignored")?>
</p>


<?php $merchant_id=isset($_GET['id'])?$_GET['id']:''; ?>
<h3><?php echo t("Offline Payment Option")?></h3>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Disabled Cash On delivery")?>?</label>
  <?php   
  echo CHtml::checkBox('merchant_switch_master_cod',
  Yii::app()->functions->getOption("merchant_switch_master_cod",$merchant_id)==2?true:false
  ,array(
    'value'=>2,
    'class'=>"icheck"
  ))
  ?> 
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Disabled Offline Credit Card Payment")?>?</label>
  <?php   
  echo CHtml::checkBox('merchant_switch_master_ccr',
  Yii::app()->functions->getOption("merchant_switch_master_ccr",$merchant_id)==2?true:false
  ,array(
    'value'=>2,
    'class'=>"icheck"
  ))
  ?> 
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Disabled Pay On Delivery")?>?</label>
  <?php   
  echo CHtml::checkBox('merchant_switch_master_pyr',
  Yii::app()->functions->getOption("merchant_switch_master_pyr",$merchant_id)==2?true:false
  ,array(
    'value'=>2,
    'class'=>"icheck"
  ))
  ?> 
</div>-->

<div class="uk-form-row">
<button type="button" class="uk-button uk-form-width-medium uk-button-success" id="savebutton" onclick="return InsertCommission();">Save</Button>
</div>    

</li>
</form>
</ul>

		<!-- USED TO CLONE ROW-->

<script>

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$(document).ready(function(){

    $(".add_deal_row").click(function()
    {	
        //alert("Hello");
        if($(".add_deal_row").parent().prev().find('.to_amount_deals').val()!=''
		&& $(".add_deal_row").parent().prev().find('.type_deals').val()!=''
		&& $(".add_deal_row").parent().prev().find('.commission_value_deals').val()!='')
        {

			var next_from_amount_deals =  parseInt($(".add_deal_row").parent().prev().find('.to_amount_deals').val())+1;
			var dealhtml = '<div class="row"><div class="col-sm-2">From Amount<input type="text" name="from_amount[]" class="uk-form-width from_amount_deals" value="'+next_from_amount_deals+'" readonly></div><div class="col-sm-2">To Amount<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_deals" value="" required></div><div class="col-sm-2">Type<select name="type[]" class="uk-form-width type_deals" required><option value="">Please Select</option><option value="fixed">Fixed</option><option value="Percentage">Percentage</option></select></div><div class="col-sm-2">Commission Value<input type="text" name="commission_value[]" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_deals" required></div><div class="col-sm-2 hidden"><input type="text" name="category[]"  class="uk-form-width" value="Deals" ></div></div>';
			$("#dealscom .row:last").after(dealhtml);

        } else {
			alert("Please Fill All the Fields.");
		}
              
    }); 
	
	$(".add_combos_row").click(function()
	{	
        //alert("Hello");
        if($(".add_combos_row").parent().prev().find('.to_amount_combos').val()!=''
		&& $(".add_combos_row").parent().prev().find('.type_combos').val()!=''
		&& $(".add_combos_row").parent().prev().find('.commission_value_combos').val()!='')
        {

			var next_from_amount_combos =  parseInt($(".add_combos_row").parent().prev().find('.to_amount_combos').val())+1;
			var combohtml = '<div class="row"><div class="col-sm-2">From Amount<input type="text" name="from_amount[]" class="uk-form-width from_amount_combos" value="'+next_from_amount_combos+'" readonly></div><div class="col-sm-2">To Amount<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_combos" value="" required></div><div class="col-sm-2">Type<select name="type[]" class="uk-form-width type_combos" required><option value="">Please Select</option><option value="fixed">Fixed</option><option value="Percentage">Percentage</option></select></div><div class="col-sm-2">Commission Value<input type="text" name="commission_value[]" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_combos" required></div><div class="col-sm-2 hidden"><input type="text" name="category[]"  class="uk-form-width" value="Combo" ></div></div>';
			$("#comboscom .row:last").after(combohtml);

		} else {
			alert("Please Fill All the Fields.");
		}
              
    });

	$(".add_preorders_row").click(function()
    {	
        //alert("Hello");
        if($(".add_preorders_row").parent().prev().find('.to_amount_preorders').val()!=''
		&& $(".add_preorders_row").parent().prev().find('.type_preorders').val()!=''
		&& $(".add_preorders_row").parent().prev().find('.commission_value_preorders').val()!='')
        {

		var next_from_amount_preorders =  parseInt($(".add_preorders_row").parent().prev().find('.to_amount_preorders').val())+1;
        var preorderhtml = '<div class="row"><div class="col-sm-2">From Amount<input type="text" name="from_amount[]" class="uk-form-width from_amount_preorders" value="'+next_from_amount_preorders+'" readonly></div><div class="col-sm-2">To Amount<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_preorders" value="" required></div><div class="col-sm-2">Type<select name="type[]" class="uk-form-width type_preorders" required><option value="">Please Select</option><option value="fixed">Fixed</option><option value="Percentage">Percentage</option></select></div><div class="col-sm-2">Commission Value<input type="text" name="commission_value[]" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_preorders" required></div><div class="col-sm-2 hidden"><input type="text" name="category[]"  class="uk-form-width" value="PreOrder" ></div></div>';
        $("#preorderscom .row:last").after(preorderhtml);

        } else {
			alert("Please Fill All the Fields.");
		}
              
    });

	$(".add_booktable_row").click(function()
    {	
        //alert("Hello");
        if($(".add_booktable_row").parent().prev().find('.to_booktable_deals').val()!=''
		&& $(".add_booktable_row").parent().prev().find('.type_booktable').val()!=''
		&& $(".add_booktable_row").parent().prev().find('.commission_value_booktable').val()!='')
        {
			
			var next_from_amount_booktable =  parseInt($(".add_booktable_row").parent().prev().find('.to_amount_booktable').val())+1;
			var booktablehtml = '<div class="row"><div class="col-sm-2">From Amount<input type="text" name="from_amount[]" class="uk-form-width from_amount_booktable" value="'+next_from_amount_booktable+'" readonly></div><div class="col-sm-2">To Amount<input type="text" name="to_amount[]" onkeypress="return isNumberKey(event)" class="uk-form-width to_amount_booktable" value="" required></div><div class="col-sm-2">Type<select name="type[]" class="uk-form-width type_booktable" required><option value="">Please Select</option><option value="fixed">Fixed</option><option value="Percentage">Percentage</option></select></div><div class="col-sm-2">Commission Value<input type="text" name="commission_value[]" onkeypress="return isNumberKey(event)" class="uk-form-width commission_value_booktable" required></div><div class="col-sm-2 hidden"><input type="text" name="category[]"  class="uk-form-width" value="Book_Table" ></div></div>';
			$("#booktablecom .row:last").after(booktablehtml);

        } else {
			alert("Please Fill All the Fields.");
		}
              
    }); 
	
});
</script>
