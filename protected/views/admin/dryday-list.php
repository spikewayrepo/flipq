
<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/admin/dryday/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/admin/dryday" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
<!-- <a href="<?php echo Yii::app()->request->baseUrl; ?>/admin/dryday/Do/Sort" class="uk-button"><i class="fa fa-sort-alpha-asc"></i> <?php echo Yii::t("default","Sort")?></a> -->
</div>

<form id="frm_table_list" method="POST" >
<input type="hidden" name="action" id="action" value="drydayList">
<input type="hidden" name="tbl" id="tbl" value="dryday">
<input type="hidden" name="clear_tbl"  id="clear_tbl" value="clear_tbl">
<input type="hidden" name="whereid"  id="whereid" value="dryday_id">
<input type="hidden" name="slug" id="slug" value="dryday">
<table id="table_list" class="uk-table uk-table-hover uk-table-striped uk-table-condensed">
  <caption><?php echo Yii::t("default","Restaurant List")?></caption>
   <thead>
        <tr>
            <th width="3%"><?php echo Yii::t("default","ID")?></th>
            <th width="7%"><?php echo Yii::t("default","Rule Name")?></th>
            <th><?php echo Yii::t('default',"UID")?></th>                       
            <th><?php echo Yii::t('default',"User Type")?></th>                       
            <th><?php echo Yii::t('default',"State")?></th>                       
            <th><?php echo Yii::t('default',"Dry Date")?></th>                       
            <th><?php echo Yii::t('default',"Rating")?></th>                       
            <th><?php echo Yii::t('default',"Custom Message")?></th>                       
            <th width="5%"><?php echo Yii::t("default","Date Created")?></th>            
        </tr>
    </thead>
    <tbody>    
    </tbody>
</table>
<div class="clear"></div>
</form>
