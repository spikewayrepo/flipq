
<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/admin/dryday/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/admin/dryday" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
</div>

<?php 
if (isset($_GET['id'])){
	if (!$data=Yii::app()->functions->GetDryDay($_GET['id'])){
		echo "<div class=\"uk-alert uk-alert-danger\">".
		Yii::t("default","Sorry but we cannot find what your are looking for.")."</div>";
		return ;
	}
}
?>                                   

<div class="spacer"></div>

<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','adddryday')?>
<?php echo CHtml::hiddenField('id',isset($_GET['id'])?$_GET['id']:"");?>
<?php if (!isset($_GET['id'])):?>
<?php echo CHtml::hiddenField("redirect",Yii::app()->request->baseUrl."/admin/dryday/Do/Add")?>
<?php endif;?>



<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Rule Name")?></label>
  <?php 
  echo CHtml::textField('rule_name',
  isset($data['rule_name'])?$data['rule_name']:""
  ,array('class'=>"uk-form-width-large",'data-validation'=>"required"))
  ?>

<?php echo CHtml::hiddenField('uid',1);?>
<?php echo CHtml::hiddenField('user_type','admin');?>
</div>
<?php
$ratinglist = array('2' =>'less than 2-Star Hotels' , '3' =>'less than 3-Star Hotels' ,'4' =>'less than 4-Star Hotels' ,'5' =>'less than 5-Star Hotels' ,'99' =>'All Hotels' ,);


$state=Yii::app()->functions->Getstate();
$states = array('All States' => 'All States');
$list = array_merge($states,$state);

?>
<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","State")?></label>
  <?php 
  echo CHtml::dropDownList('state',
  isset($data['state'])?$data['state']:""
  ,(array)$list
  ,array('class'=>"uk-form-width-large",'data-validation'=>"required"))
  ?>
</div>
<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Rating")?></label>
  <?php 
  echo CHtml::dropDownList('rating',
  isset($data['rating'])?$data['rating']:""
  ,(array)$ratinglist,
  array('class'=>"uk-form-width-large",'data-validation'=>"required"))
  ?>

  
</div>



 <div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Dry Date")?></label>
  <span class="uk-text-success">
  <?php 
  echo CHtml::hiddenField('dry_date',$data['dry_date']);
  echo CHtml::textField('dry_date1',FormatDateTime($data['dry_date'],false),array(
    'class'=>"j_date",
    'data-validation'=>"requiredx",
    'data-id'=>'dry_date'
  ))
  ?>
  </div> 



<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Description")?>
  </label>
  <?php echo CHtml::textArea('message',
  isset($data['message'])?$data['message']:""
  ,array(
  'class'=>'uk-form-width-large',
  'data-validation'=>"required"
  ))?>
</div>






<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>
