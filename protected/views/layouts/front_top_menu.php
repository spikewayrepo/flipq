<?php $this->renderPartial('/front/modal');?>

<?php //$this->renderPartial('/front/modals'); ?>
<header>

<div class="loader"></div>
<section class="top-header">
    <div class="container-fluid">
		<div clas="row text-right">
			<div class="col-sm-7 col-sm-offset-5 col-md-6 col-md-offset-6 col-lg-5 col-lg-offset-7 col-xs-12 text-right">
				<ul>
					<li>support@flipq.in</li>
					<li>+91 9971234009</li>
				</ul>
			</div>
		</div>
    </div> <!-- container -->
</section> <!-- top-header -->
  
<section class="main-header">
	<div class="container leftlogo">
		<div class="row">
			<div class="col-lg-2 col-md-3 col-sm-3 col-xs-4">
				<?php
				if ( $theme_hide_logo<>2)
				{
					if ($_SESSION['profile_page']=='djhome')
					{ ?>
						<a href="<?php echo Yii::app()->createUrl('/store/home');?>" >
			<?php	} else { ?> 
						<a href="<?php echo Yii::app()->createUrl("/store/home");?>">
			<?php	} ?>
							<img src="<?php echo FunctionsV3::getDesktopLogo();?>" class="site-logo">
						</a>
		<?php	} ?>
			</div>
			
			<div class="col-lg-10 col-md-9 col-sm-9 text-right hidden-xs">
				<ul class="nav-main">
					
					<li><a href="/store/home">Home</a></li>
					<li><a href="/store/page/how-it-work">How It Works</a></li>
					<li><a id="open-location3"><span class="fa fa-map-marker position-abt text-white"></span>
                                        <?php
                                          if($_SESSION['fq_search_address'])
                                          {
                                           $address =  explode(',',$_SESSION['fq_search_address']);
                                           echo $address[0];
                                          }
                                          else
                                           echo 'Location';

                                         ?>
                                         </a>
						<!--<a id="open-location">
							<div class="global-search">
								<input type="hidden" value="-1" id="hdnGlobalCity">
								<span class="font13" id="global-place">Location </span>
								<span class="fa fa-map-marker position-abt text-white"></span>
							</div>
						</a>-->
					</li>
					<li>
				<?php	if($_SESSION['kr_client']['client_id'])
						{

							$cid  = $_SESSION['kr_client']['client_id']; ?>
							
							<a href="<?php echo Yii::app()->request->baseUrl."/store/profile"?>"><?php echo $_SESSION['kr_client']['first_name']?></a> | <a href="<?php echo Yii::app()->request->baseUrl."/store/Logout"?>">Logout</a>
				  <?php } else { ?>
							<a data-toggle="modal" data-target="#loginModal">Login | Register</a>
				  <?php } ?>
					</li>
					
					
				</ul>
			</div>
						
			<div class="col-xs-8 visible-xs text-right">
				<ul class="nav-main-xs">
					<li><a href="/store/home"><i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li><a data-toggle="modal" data-target="#loginModal"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp; |&nbsp; <i class="fa fa-user-plus" aria-hidden="true"></i></a></li>
					<li><a id="open-location2"><i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>  <!-- row -->
    </div> <!-- container -->
</section> <!-- main-header -->
		
<section class="main-header sticky">
    <div class="container leftlogo">
		<div class="row">
        
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
				<?php
				if ( $theme_hide_logo<>2)
				{
					if ($_SESSION['profile_page']=='djhome')
					{ ?>
						<a href="<?php echo Yii::app()->createUrl('/store/dj');?>" >
			<?php	} else { ?> 
						<a href="<?php echo Yii::app()->createUrl("/store");?>">
			<?php	} ?>
							<img src="<?php echo FunctionsV3::getDesktopLogo();?>" class="site-logo">
						</a>
		<?php	} ?>
			</div>
			<?php

if(Yii::app()->controller->action->id=='home' || Yii::app()->controller->action->id=='searchresult')
{
$styles = 'col-lg-5 col-md-5 col-sm-6 text-right hidden-xs';
?>
<div class="col-lg-5 col-md-5 col-sm-4 text-center hidden-xs">
          <input type="text" placeholder="Search Cusines, Restaurants, Pub ..." class="searchbar">
        </div>
<?php
}
else
{
$styles = 'col-lg-10 col-md-10 col-sm-10 text-right hidden-xs';
}

                        ?>
			<div class="<?php print $styles ?>">
				<ul class="nav-main">
				
					<li><a href="/store/home">Home</a></li>
					<li><a href="/store/page/how-it-work">How It Works</a></li>
					<li><span class="fa fa-map-marker position-abt text-white" style="color:#fff"></span>
						<a id="open-location"> <?php
                                          if($_SESSION['fq_search_address'])
                                          {
                                           $address =  explode(',',$_SESSION['fq_search_address']);
                                           echo $address[0];
                                          }
                                          else
                                           echo 'Location';

                                         ?></a>
					</li>
					<li>
				<?php	if($_SESSION['kr_client']['client_id'])
						{

							$cid  = $_SESSION['kr_client']['client_id']; ?>
							
							<a href="<?php echo Yii::app()->request->baseUrl."/store/profile"?>"><?php echo $_SESSION['kr_client']['first_name']?></a> | <a href="<?php echo Yii::app()->request->baseUrl."/store/Logout"?>">Logout</a>
				  <?php } else { ?>
							<a data-toggle="modal" data-target="#loginModal">Login | Register</a>
				  <?php } ?>
					</li>
							
					
			
				</ul>
			</div>
			
			<div class="col-xs-8 visible-xs text-right">
				
				<ul class="nav-main-xs">
				
					<li><a href="/store/home"><i class="fa fa-home" aria-hidden="true"></i></a></li>
					
					<li><a data-toggle="modal" data-target="#loginModal"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp; |&nbsp; <i class="fa fa-user-plus" aria-hidden="true"></i></a></li>
					
					<li><a id="open-location4"><i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
				
				</ul>
			</div>
		
		</div> <!-- row -->
    
	</div> <!-- container -->

</section> <!-- main-header sticky -->	
</header>
	

<?php  require_once('modals.php'); ?>
