<section class="footer">
	
	<div class="container">
	
		<div class="row">
		
			<div class="col-sm-12 col-xs-12 text-center footer-content">
				<p>Download flipQ App</p>
				<p>Best way to Pre-Order Happy Hours</p>
			</div>
		  
			<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1 text-center">
			
				<div class="col-sm-6 col-xs-6">
					<a href="https://itunes.apple.com/" target="_blank">
						<img src="http://52.10.201.183/dev/flipq/images/appstore.png">
					</a>
				</div>
			
				<div class="col-sm-6 col-xs-6">
					<a href="https://play.google.com/" target="_blank">
						<img src="http://52.10.201.183/dev/flipq/images/googleplay.png">
					</a>
				</div>
			
			</div>
		
		</div>
		
		<hr>
		
		<div class="row  footer-content1">
			
			
		
			<div class="col-sm-4 col-xs-6">
		<?php 	if ($theme_hide_footer_section1!=2)
				{ ?>
					<h4><?php echo t("Menu")?></h4>
			<?php 	if (is_array($menu) && count($menu)>=1)
					{ ?>
				<?php	foreach ($menu as $val)
						{ ?>
							<!--
							<li>
				<a href="<?php //echo FunctionsV3::customPageUrl($val)?>" <?php FunctionsV3::openAsNewTab($val)?>><?php //echo $val['page_name']?></a>
							</li>
							--!> 
				<?php 	} ?>
					
						<li>
							<a href="/store/terms">Terms & Conditions</a>
						</li>
					
						<li>
							<a href="/store/policy">Privacy Policy</a>
						</li>
						<li>
							<a href="/store/calcallationpolicy">Cancellation Policy</a>
						</li>
						

						<li>
							<a href="/store/contact">Contact Us</a>
						</li>
			<?php	} ?>
		<?php	} ?>
			</div> <!--col-->
				  
			<div class="col-sm-4 col-xs-6">
		<?php 	if ($theme_hide_footer_section2!=2)
				{ ?>
				
					<h4><?php echo t("Others")?></h4>
					
	
			   
					<li><a href="<?php echo  Yii::app()->request->baseUrl; ?>/store/merchantsignupinfo?btype=restaurant">Restaurant Registration</a></li>
				 
					<li><a href="<?php echo  Yii::app()->request->baseUrl; ?>/store/merchantsignupinfo?btype=dj">Performing Artists/DJs Sign Up</a></li>

					<li><a href="<?php echo  Yii::app()->request->baseUrl; ?>/merchant/login">Restaurant Login</a></li>
					
		<?php 	} ?>  
			</div> <!--col-->
		 
		<?php 	if ($social_flag<>1)
				{ ?>
					<div class="col-sm-4 col-xs-6">
						<h4><?php echo t("Connect with us")?></h4>
						<div class="mytable social-wrap" style="text-align: left;">
					<?php	if (!empty($google_page))
							{?>
								<div class="mycol border">
									<a style="color:#D14836" target="_blank" href="<?php echo FunctionsV3::prettyUrl($google_page)?>"><i class="fa fa-google-plus"></i></a>
							   </div> <!--col-->
					<?php	} 
							
							if (!empty($twitter_page))
							{	?>
								<div class="mycol border">
									<a style="color:#00ACEE" target="_blank" href="<?php echo FunctionsV3::prettyUrl($twitter_page)?>"><i class="fa fa-twitter"></i></a>
							   </div> <!--col-->
					<?php	} ?>
	   
					<?php 	if (!empty($fb_page))
							{ ?>
							   <div class="mycol border">
								<a style="color:#3B5999" target="_blank" href="<?php echo FunctionsV3::prettyUrl($fb_page)?>"><i class="fa fa-facebook"></i></a>
							   </div> <!--col-->
					<?php 	} ?>
					   
						   
					<?php 	if (!empty($intagram_page))
							{ ?>
								<div class="mycol border">
									<a style="color:#3E729A" target="_blank" href="<?php echo FunctionsV3::prettyUrl($intagram_page)?>">
										<i class="fa fa-instagram"></i>
									</a>
								</div> <!--col-->
					<?php 	} ?>
						   
					<?php 	if (!empty($youtube_url))
							{	?>
								<div class="mycol border">
									<a style="color:#CC332D" target="_blank" href="<?php echo FunctionsV3::prettyUrl($youtube_url)?>">
										<i class="ion-social-youtube-outline"></i>
									</a>
								</div> <!--col-->
					<?php 	} ?>
						   
						</div> <!--social wrap-->
							 
					</div> <!--col-->
		<?php	} ?>
		 
		</div> <!--row-->
</section>



