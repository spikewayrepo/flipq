<style>
.prettyline {
  height: 5px;
  border-top: 0;
  background: #c4e17f;
  border-radius: 5px;
  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
}

</style>

<!--<div class="modal fade bs-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <br>
        <div class="bs-example bs-example-tabs">
            <ul id="myTab" class="nav nav-tabs">
              <li class="active"><a href="#signin" data-toggle="tab">Sign In</a></li>
              <li class=""><a href="#signup" data-toggle="tab">Register</a></li>
              <li class=""><a href="#why" data-toggle="tab">Why?</a></li>
            </ul>
        </div>
      <div class="modal-body">
        <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade in" id="why">
        <p>We need this information so that you can receive access to the site and its content. Rest assured your information will not be sold, traded, or given to anyone.</p>
        <p></p><br> Please contact <a mailto:href="JoeSixPack@Sixpacksrus.com"></a>JoeSixPack@Sixpacksrus.com</a> for any other inquiries.</p>
        </div>
        <div class="tab-pane fade active in" id="signin">
            <form class="form-horizontal">
            <fieldset>
           
            <div class="control-group">
              <label class="control-label" for="userid">Alias:</label>
              <div class="controls">
                <input required="" id="userid" name="userid" type="text" class="form-control" placeholder="JoeSixpack" class="input-medium" required="">
              </div>
            </div>

           
            <div class="control-group">
              <label class="control-label" for="passwordinput">Password:</label>
              <div class="controls">
                <input required="" id="passwordinput" name="passwordinput" class="form-control" type="password" placeholder="********" class="input-medium">
              </div>
            </div>

         
            <div class="control-group">
              <label class="control-label" for="rememberme"></label>
              <div class="controls">
                <label class="checkbox inline" for="rememberme-0">
                  <input type="checkbox" name="rememberme" id="rememberme-0" value="Remember me">
                  Remember me
                </label>
              </div>
            </div>

           
            <div class="control-group">
              <label class="control-label" for="signin"></label>
              <div class="controls">
                <button id="signin" name="signin" class="btn btn-success">Sign In</button>
              </div>
            </div>
            </fieldset>
            </form>
        </div>
        <div class="tab-pane fade" id="signup">
            <form class="form-horizontal">
            <fieldset>
            
            <div class="control-group">
              <label class="control-label" for="Email">Email:</label>
              <div class="controls">
                <input id="Email" name="Email" class="form-control" type="text" placeholder="JoeSixpack@sixpacksrus.com" class="input-large" required="">
              </div>
            </div>
            
           
            <div class="control-group">
              <label class="control-label" for="userid">Alias:</label>
              <div class="controls">
                <input id="userid" name="userid" class="form-control" type="text" placeholder="JoeSixpack" class="input-large" required="">
              </div>
            </div>
            
          
            <div class="control-group">
              <label class="control-label" for="password">Password:</label>
              <div class="controls">
                <input id="password" name="password" class="form-control" type="password" placeholder="********" class="input-large" required="">
                <em>1-8 Characters</em>
              </div>
            </div>
            
  
            <div class="control-group">
              <label class="control-label" for="reenterpassword">Re-Enter Password:</label>
              <div class="controls">
                <input id="reenterpassword" class="form-control" name="reenterpassword" type="password" placeholder="********" class="input-large" required="">
              </div>
            </div>
            
           
            <br>
            <div class="control-group">
              <label class="control-label" for="humancheck">Humanity Check:</label>
              <div class="controls">
                <label class="radio inline" for="humancheck-0">
                  <input type="radio" name="humancheck" id="humancheck-0" value="robot" checked="checked">I'm a Robot</label>
                <label class="radio inline" for="humancheck-1">
                  <input type="radio" name="humancheck" id="humancheck-1" value="human">I'm Human</label>
              </div>
            </div>
            
           
            <div class="control-group">
              <label class="control-label" for="confirmsignup"></label>
              <div class="controls">
                <button id="confirmsignup" name="confirmsignup" class="btn btn-success">Sign Up</button>
              </div>
            </div>
            </fieldset>
            </form>
      </div>
    </div>
      </div>
      <div class="modal-footer">
      <center>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </center>
      </div>
    </div>
  </div>
</div>-->


<div class="top-menu-wrapper <?php echo "top-".$action;?>">

<div class="container border" >
  <div class="col-md-2 col-xs-3 border col-a">
    <?php if ( $theme_hide_logo<>2):?>
	<?php if ($_SESSION['profile_page']=='djhome'){ ?>
		<a href="<?php echo Yii::app()->createUrl('/store/dj');?>" >
	<?php } else { ?> 
		<a href="<?php echo Yii::app()->createUrl("/store");?>">
	<?php } ?>
     <img src="<?php echo FunctionsV3::getDesktopLogo();?>" class="logo logo-desktop">
     <img src="<?php echo FunctionsV3::getMobileLogo();?>" class="logo logo-mobile">
    </a>
    <?php endif;?>
  </div>
  
  <div class="col-xs-1 menu-nav-mobile border relative">
     <a href="#"><i class="ion-android-menu"></i></a>
  </div> <!--menu-nav-mobile-->
  
  <?php if ( Yii::app()->controller->action->id =="menu"):?>
  <div class="col-xs-1 cart-mobile-handle border relative">
     <a href="javascript:;"><i class="ion-ios-cart"></i></a>
  </div> <!--cart-mobile-handle-->
  <?php endif;?>
  
  
  <div class="col-md-10 border col-b">
    <span class="mobileno"><i class="fa fa-phone"></i>+91-9898989899</span>
	<ul id="menu">
		<li class="active">
			<a href="/store/home">Home</a>
		</li>
		
		<li>
			<a href="/store/page/how-it-work">How It Work</a>
		</li>
		
		<!--<li>
			<a href="/store/contact">Contact</a>
		</li>-->
		
		<?php $check_login=Yii::app()->functions->isMerchantLogin();
			if($check_login){
				
			} else 
			{ ?>
				<li>
				
					<?php 
						if($_SESSION['kr_client']['client_id'])
						{
							$cid  = $_SESSION['kr_client']['client_id']; ?>
							<div data-uk-dropdown="{mode:'click'}" class="uk-button-dropdown">
								<button class="uk-button btn btn-default"  id="dd1" style="background:transparent;border:none;color:white;"><i class="fa fa-user">
								</i> 
								<?php echo "My Account"; ?> <i class="uk-icon-caret-down">
								</i>
								</button>
								<!--<div class="uk-dropdown" >-->
									<ul class="uk-nav uk-nav-dropdown" id="uk-nav" style="display:none;">	   
										<?php //if (isset($merchant_info[0]->user_access)):?>
										<li style="display:block;"><a href="<?php echo websiteUrl()."/store/profile"?>"><i class="fa fa-user"></i> Profile</a></li>
										<li style="display:block;">
											<a href="<?php echo Yii::app()->request->baseUrl."/store/Logout"?>">
												<i class="fa fa-sign-out"></i> <?php echo Yii::t("default","Logout")?>
											</a>
										</li>	    
									</ul>
								<!--</div>-->
							</div>
				  <?php } else { ?>
							<!--<a class="btn btn-primary btn-lg" href="#signup" style="background:transparent; border: 0px;" data-toggle="modal" data-target=".bs-modal-sm">Login & Signup</a>-->
							<a href="/store/signup">Login & Signup</a>
				  <?php } //print_r($_SESSION); ?>
				</li>
	
				<li class="location-selection">
					<a href="javascript:;">

						<div class="global-location position-rel" id="global-location">

							<div class="global-search">
								<input type="hidden" value="-1" id="hdnGlobalCity">
								<span class="font13" id="global-place">Enter your location </span>
								<span class="fa fa-map-marker position-abt text-white"></span>
							</div>

						</div>
						</a>
				</li>
	  <?php } ?>
	</ul>
    <?php //$this->widget('zii.widgets.CMenu', FunctionsV3::getMenu() );?>

	
    <div class="clear"></div>
  </div>
  
</div> <!--container-->

</div> <!--END top-menu-->

<div class="menu-top-menu">
    <?php $this->widget('zii.widgets.CMenu', FunctionsV3::getMenu('mobile-menu') );?> 
    <div class="clear"></div>
</div> <!--menu-top-menu-->

<script>
	$(document).ready(function(){
		$(".uk-button").click(function(){
			$("#uk-nav").toggle();
		});
	});
</script>

