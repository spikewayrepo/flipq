
<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Alcohol/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Alcohol" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
</div>

<form id="frm_table_list" method="POST" >
<input type="hidden" name="action" id="action" value="AlcoholListMerchant">
<input type="hidden" name="tbl" id="tbl" value="item">
<input type="hidden" name="clear_tbl"  id="clear_tbl" value="clear_tbl">
<input type="hidden" name="whereid"  id="whereid" value="item_id">
<input type="hidden" name="slug" id="slug" value="Alcohol/Do/Add">
<table id="table_list" class="uk-table uk-table-hover uk-table-striped uk-table-condensed">
  <caption>Liquor's List</caption>
	<thead>
        <tr>
            <!--<th width="2%"><input type="checkbox" id="chk_all" class="chk_all"></th>-->
            <th width="5%"><?php echo Yii::t('default',"Name")?></th>
            <!--<th width="4%"><?php echo Yii::t('default',"Description")?></th>-->
            <th width="4%"><?php echo Yii::t('default',"Category")?></th>
			<th width="4%"><?php echo Yii::t('default',"Sub-Category")?></th>
			<th width="4%"><?php echo Yii::t('default',"Order By")?></th>
            <th width="4%"><?php echo Yii::t('default',"Price")?></th>
			<th width="4%"><?php echo Yii::t('default',"Discount")?></th>
			<th width="4%"><?php echo Yii::t('default',"Discount Type")?></th>
            <th width="4%"><?php echo Yii::t('default',"Item Not Available")?></th>
            <th width="4%"><?php echo Yii::t('default',"Date")?></th>
        </tr>
    </thead>
</table>
<div class="clear"></div>
</form>