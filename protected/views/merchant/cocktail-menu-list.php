
<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Menu" class="uk-button"><i class="fa fa-beer"></i> <?php echo Yii::t("default","Alcohol List")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Cocktail_List" class="uk-button"><i class="fa fa-glass"></i> <?php echo Yii::t("default","Cocktail List")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Starter_List" class="uk-button"><i class="fa fa-cutlery"></i> <?php echo Yii::t("default","Starter List")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Combos" class="uk-button"><i class="fa fa-cutlery"></i> <?php echo Yii::t("default","Combo List")?></a>
</div>

<form id="frm_table_list" method="POST" >
<input type="hidden" name="action" id="action" value="CocktailList">
<input type="hidden" name="tbl" id="tbl" value="item">
<input type="hidden" name="clear_tbl"  id="clear_tbl" value="clear_tbl">
<input type="hidden" name="whereid"  id="whereid" value="item_id">
<input type="hidden" name="slug" id="slug" value="Cocktail/Do/Add">
<table id="table_list" class="uk-table uk-table-hover uk-table-striped uk-table-condensed">
  <caption>Cocktail List</caption>
   <thead>
        <tr>
            <th width="5%"><?php echo Yii::t('default',"Name")?></th>
            <th width="4%"><?php echo Yii::t('default',"Description")?></th>            
            <th width="4%"><?php echo Yii::t('default',"Price")?></th>
			<th width="4%"><?php echo Yii::t('default',"Quantity")?></th>
			<th width="4%"><?php echo Yii::t('default',"Discount")?></th>
			<th width="4%"><?php echo Yii::t('default',"Ingredients")?></th>
            <th width="4%"><?php echo Yii::t('default',"Photos")?></th>
            <th width="4%"><?php echo Yii::t('default',"Item Not Available")?></th>
            <th width="4%"><?php echo Yii::t('default',"Date")?></th>
        </tr>
    </thead>
    <tbody> 
    </tbody>
</table>
<div class="clear"></div>
</form>