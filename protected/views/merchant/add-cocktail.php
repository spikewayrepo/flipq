<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Cocktail/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Cocktail" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
</div>
 
<?php 
$addon_item='';
$price='';
$category='';
$cooking_ref_selected='';
$multi_option_Selected='';
$multi_option_value_selected='';
$ingredients_selected='';

if (isset($_GET['id'])){
	if (!$data=Yii::app()->functions->getFoodItem2($_GET['id'])){
		echo "<div class=\"uk-alert uk-alert-danger\">".
		Yii::t("default","Sorry but we cannot find what you are looking for.")."</div>";
		return ;
	}		
	$addon_item=isset($data['addon_item'])?(array)json_decode($data['addon_item']):false;		
	$category=isset($data['category'])?(array)json_decode($data['category']):false;
	$price=isset($data['price'])?(array)json_decode($data['price']):false;	
	$cooking_ref_selected=isset($data['cooking_ref'])?(array)json_decode($data['cooking_ref']):false;
	$happyhours_selected=isset($data['happyhours'])?(array)json_decode($data['happyhours']):false;
	$multi_option_Selected=isset($data['multi_option'])?(array)json_decode($data['multi_option']):false;
	$multi_option_value_selected=isset($data['multi_option_value'])?(array)json_decode($data['multi_option_value']):false;	
	
	$ingredients_selected=isset($data['ingredients'])?(array)json_decode($data['ingredients']):false;
	
	$two_flavors_position=isset($data['two_flavors_position'])?(array)json_decode($data['two_flavors_position']):false;	
	//dump($two_flavors_position);
	
	$require_addon=isset($data['require_addon'])?(array)json_decode($data['require_addon']):false;		
}
?>                                   

<div class="spacer"></div>

<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','CocktailAdd')?>
<?php echo CHtml::hiddenField('id',isset($_GET['id'])?$_GET['id']:"");?>
<?php if (!isset($_GET['id'])):?>
<?php echo CHtml::hiddenField("redirect",Yii::app()->request->baseUrl."/merchant/Cocktail")?>
<?php endif;?>

	<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Cocktail Name")?></label>
	<?php echo CHtml::textField('item_name',
	isset($data['item_name'])?$data['item_name']:""
	,array(
	'class'=>'uk-form-width-large',
	'data-validation'=>"required"
	))?>
	</div>

	<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Description")?></label>
	<?php echo CHtml::textArea('item_description',
	isset($data['item_description'])?$data['item_description']:""
	,array(
	'class'=>'uk-form-width-large big-textarea'	
	))?>
	</div>

	
		<div class="uk-form-row"> 
	 <label class="uk-form-label"><?php echo Yii::t('default',"Featured Image")?></label>
	  <div style="display:inline-table;margin-left:1px;" class="button uk-button" id="photo"><?php echo Yii::t('default',"Browse")?></div>	  
	  <DIV  style="display:none;" class="photo_chart_status" >
		<div id="percent_bar" class="photo_percent_bar"></div>
		<div id="progress_bar" class="photo_progress_bar">
		  <div id="status_bar" class="photo_status_bar"></div>
		</div>
	  </DIV>		  
	</div>

	<?php if (!empty($data['photo'])):?>
	<div class="uk-form-row"> 
	<?php else :?>
	<div class="input_block preview">
	<?php endif;?>
	<label><?php echo Yii::t('default',"Preview")?></label>
	<div class="image_preview">
	 <?php if (!empty($data['photo'])):?>
	 <input type="hidden" name="photo" value="<?php echo $data['photo'];?>">
	 <img class="uk-thumbnail uk-thumbnail-small" src="<?php echo Yii::app()->request->baseUrl."/upload/".$data['photo'];?>?>" alt="" title="">
	 <?php endif;?>
	</div>
	</div>


	<!--GALLERY -->
	<div class="uk-form-row"> 
	 <label class="uk-form-label"><?php echo Yii::t('default',"Gallery Image")?></label>
	  <div style="display:inline-table;margin-left:1px;" class="button uk-button" id="foodgallery"><?php echo Yii::t('default',"Browse")?></div>	  
	  <DIV  style="display:none;" class="foodgallery_chart_status" >
		<div id="percent_bar" class="foodgallery_percent_bar"></div>
		<div id="progress_bar" class="foodgallery_progress_bar">
		  <div id="status_bar" class="foodgallery_status_bar"></div>
		</div>
	  </DIV>		  
	</div>

	<div class="uk-form-row"> 
	  <div class="input_block foodgallery_preview">
	  <?php if (!empty($data['gallery_photo'])): $gallery_photo=json_decode($data['gallery_photo']);?> 
	  <?php if (is_array($gallery_photo) && count($gallery_photo)>=1):?>  
	  <?php foreach ($gallery_photo as $val_gal):  $class_gal = time().Yii::app()->functions->generateRandomKey(10);?>
		<li class="<?php echo $class_gal?>"> 
		  <img class="uk-thumbnail uk-thumbnail-mini" src="<?php echo websiteUrl()."/upload/$val_gal"?>">
		  <?php echo CHtml::hiddenField('gallery_photo[]',$val_gal)?>
		  <p><a href="javascript:rm_foodGallery('<?php echo $class_gal?>')"><?php echo t("Remove image")?></a></p>
		</li>
	  <?php endforeach;?>
	  <?php endif;?>
	  <?php endif;?>
	  </div>
	</div>
	<!--GALLERY -->


<div class="uk-form-row" style="display:none;">
  <label class="uk-form-label"><?php echo t("Category")?></label>
  <input type="" readonly="true" class="uk-form-width-large error" data-validation="required" type="text" value="Cocktail" name="category" id="category" style="border-color: red;">
</div>

	<!--<div class="uk-form-row">
		<label class="uk-form-label"><?php echo t("Ingredients")?></label>
		<?php
		echo CHtml::textField('ingredients',isset($data['ingredients'])?$data['ingredients']:""
		,array(
			'class'=>'uk-form-width-large',
			//'data-validation'=>"required"
		))?>
	</div>-->

	<div class="uk-form-row">
	  <label class="uk-form-label"><?php echo t("Price")?></label>
	  <?php echo CHtml::textField('price[]',
			  isset($price[0])?$price[0]:""
		,array(
		'id'=>"price",
		'class'=>'uk-form-width-medium numeric_only',
		'data-validation'=>"required"
		))?>
	</div>

	<div class="uk-form-row">
	  <label class="uk-form-label"><?php echo t("Quantity")?></label>
	  <?php echo CHtml::textField('quantity',
		isset($data['quantity'])?$data['quantity']:""
		,array(
		'class'=>'uk-form-width-small',
		//'data-validation'=>"required"
		))?>
		<select name="unit">
		  <option value="glass">Glass</option>
		  <option value="pitcher">Pitcher</option>
		</select>
	</div>
	
	<div class="uk-form-row">
	  <label class="uk-form-label"><?php echo t("Status")?></label>
	  <?php echo CHtml::dropDownList('status',
	  isset($data['status'])?$data['status']:"",
	  (array)statusList(),          
	  array(
	  'class'=>'uk-form-width-medium',
	  'data-validation'=>"required"
	  ))?>
	</div>
	
	<div class="uk-form-row">
		<label class="uk-form-label"><?php echo Yii::t("default","Discount (numeric value)")?></label>
		<?php echo CHtml::textField('discount',
		isset($data['discount'])?$data['discount']:""
		,array(
			'id'=>"discount",
			'class'=>'uk-form-width-medium numeric_only', 	
			'onchange'=>"validatediscount();"
		))?>
		<select name="discount_type">
		<?php if($data['discount_type']=='Fixed'){ ?>
		  <option selected value="Fixed">Fixed</option>
		  <option value="Percentage">Percentage</option>
		<?php } else { ?>
			<option value="Fixed">Fixed</option>
			<option selected value="Percentage">Percentage</option>
		<?php } ?>
		</select>
	</div>
			
	<!--<div class="uk-form-row">
	  <label class="uk-form-label uk-h3"><?php echo t("Tax")?></label>
	  <?php echo CHtml::checkBox('non_taxable',
	  $data['non_taxable']==2?true:false
	  ,array(
	   'class'=>"icheck",
	   'value'=>2
	  ))?>	  	  	  
	  <?php echo t("Non taxable")?>
	</div>-->
	
	
	<!--POINTS PROGRAM-->
	<?php /*if (FunctionsV3::hasModuleAddon("pointsprogram")):?>
	<div class="uk-form-row">
	  <label class="uk-form-label uk-h3"><?php echo t("Points earned")?></label>  
	  <?php echo CHtml::textField('points_earned',
	  $data['points_earned']>0?$data['points_earned']:''
	  ,array(
	    'class'=>"numeric_only"
	  ))?>
	</div>  
	<div class="uk-form-row" style="margin-left:200px">
	<?php echo CHtml::checkBox('points_disabled',
	  $data['points_disabled']==2?true:false
	  ,array(
	   'class'=>"icheck",
	   'value'=>2
	  ))?>	  	  	  
	  <?php echo t("Disabled Points on this item")?>	
	</div>
	<?php endif;*/ ?>
	
		

<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>