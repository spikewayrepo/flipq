
<?php if ( Yii::app()->functions->hasMerchantAccess("DashBoard")): ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?ext.js"></script>

<form id="frm_table_list" method="POST" class="report uk-form uk-form-horizontal merchant-dashboard" >
<h3><?php echo Yii::t("default","New Order List For Today")?> 
<?php 
/*$date= date('F d, Y');
$date=Yii::app()->functions->translateDate($date);
echo $date;*/
echo FormatDateTime(date('Y-m-d h:i:s'),false);
?>
</h3>

<input type="hidden" name="action" id="action" value="recentOrder">
<input type="hidden" name="tbl" id="tbl" value="item">
<table id="table_list" class="uk-table uk-table-hover uk-table-striped uk-table-condensed">
  <!--<caption>Merchant List</caption>-->
   <thead>
        <tr> 
            <th width="2%"><?php echo Yii::t('default',"Ref#")?></th>
            <th width="6%"><?php echo Yii::t('default',"Name")?></th>
            <th width="6%"><?php echo Yii::t('default',"Contact#")?></th>
            <th width="3%"><?php echo Yii::t('default',"Item")?></th>            
            <!--<th width="3%"><?php echo Yii::t('default',"TransType")?></th>-->
            <th width="3%"><?php echo Yii::t('default',"Payment Type")?></th>
            <th width="3%"><?php echo Yii::t('default',"Total")?></th>
            <!--<th width="3%"><?php echo Yii::t('default',"Tax")?></th>
            <th width="3%"><?php echo Yii::t('default',"Total W/Tax")?></th>-->
            <th width="3%"><?php echo Yii::t('default',"Status")?></th>
            <th width="3%"><?php echo Yii::t('default',"Date")?></th>
            <th width="3%"></th>
        </tr>
    </thead>
    <tbody>    
    </tbody>
</table>
<div class="clear"></div>
</form>

<hr style="margin-top:20px;margin-bottom:20px;"></hr>
<br>

<?php

$merchant_id=Yii::app()->functions->getMerchantID();
$list=array();
$orders =  "['Date', 'Combo', 'Deals', 'Preorder'],";
if($_GET['m']){
	$month = $_GET['m'];
}else {
	$month = date('m');
}
if($_GET['y']){
	$year = $_GET['y'];
} else {
	$year = date('Y');	
}


for($d=1; $d<=31; $d++)
{
	if($d<10){
		$day="0".$d;
	} else {
		$day=$d;
	}
	$date = $year."-".$month."-".$day;
	$combos = FunctionsV3::getMerchantCombos($merchant_id, $date);
	$deals = FunctionsV3::getMerchantDeals($merchant_id, $date);
	$preorders = FunctionsV3::getMerchantPreOrders($merchant_id, $date);
    $time=mktime(12, 0, 0, $month, $d, $year);          
    if (date('m', $time)==$month) {      
        #$list[]=date('Y-m-d-D', $time);
        $orders .="['".date('d-M', $time)."',".$combos.",".$deals.",".$preorders."],"; 
    }
}
?>

<?php
$listalcohol=array();
$ordersalcohol =  "['Date','Beer','Rum','Whisky','Vodka','Wine','Gin'],";

for($d1=1; $d1<=31; $d1++)
{	if($d1<10)
	{
		$day1="0".$d1;
	} else 
	{
		$day1=$d1;	
	}
	$date1=$year."-".$month."-".$day1;
	$beer = FunctionsV3::getAlcoholQty($merchant_id, $date1, 'BEER');
	$rum = FunctionsV3::getAlcoholQty($merchant_id, $date1, 'RUM');
	$whisky = FunctionsV3::getAlcoholQty($merchant_id, $date1, 'WHISKY');
	$vodka = FunctionsV3::getAlcoholQty($merchant_id, $date1, 'VODKA');
	$wine = FunctionsV3::getAlcoholQty($merchant_id, $date1, 'WINE');
	$gin = FunctionsV3::getAlcoholQty($merchant_id, $date1, 'GIN');
    $time1=mktime(12, 0, 0, $month, $d1, $year);          
    if (date('m', $time1)==$month) {      
        #$list[]=date('Y-m-d-D', $time);
        $ordersalcohol .="['".date('d-M', $time1)."',".$beer.",".$rum.",".$whisky.",".$vodka.",".$wine.",".$gin."],"; 
		//print_r($ordersalcohol);
	}
}
#echo "<pre>";
#print_r($list);
#echo "</pre>";

?>


    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar', 'corechart']});
      google.charts.setOnLoadCallback(drawChart);
	  google.charts.setOnLoadCallback(drawVisualization);
	   
      function drawChart() {
        var data = google.visualization.arrayToDataTable([<?php print $orders ?>]); 
		var options={ 
		                chart: {title: 'Revenue by Order Type' },
						hAxis: {title: "DATE" , direction:-1, slantedText:true, slantedTextAngle:180 }
					}
        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
        chart.draw(data, options);
      }
	  
	  function drawVisualization() {
  // Create and populate the data table.
	var data = google.visualization.arrayToDataTable([<?php print $ordersalcohol; ?>]);

	// Create and draw the visualization.
	var chart = new google.visualization.ColumnChart(document.getElementById('chart'));
      chart.draw( data,
           {title:"Monthly Consumption",
            width:950, height:500,
            vAxis: {title: "Cups"}, isStacked: true,
            hAxis: {title: "Date"}}
		);
}
	  
	  
	  
	  
    </script>

<div id="columnchart_material" style="width: 900px; height: 500px;"></div>

<hr style="margin-top:20px;margin-bottom:20px;"></hr>

<div id="chart"></div>

<!--<div id="total_sales_chart" class="chart"></div>
<div id="total_sales_chart_by_item" class="chart"></div>-->

<?php 
//$this->renderPartial('/front/chart');
?>


<?php else :?>
<h2><?php echo Yii::t("default","Welcome")?></h2>
<?php endif; ?>