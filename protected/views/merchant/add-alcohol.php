
<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Alcohol/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Alcohol" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/RequestAlcohol" class="uk-button"><i class="fa fa-envelope"></i> <?php echo Yii::t("default","Request New Alcohol Brand")?></a>
</div>
<?php 
$addon_item='';
$price='';
$category='Alcohol';
$acategory='';
$subcategory='';
$orderby='';
$cooking_ref_selected='';
$multi_option_Selected='';
$multi_option_value_selected='';
$ingredients_selected='';

if (isset($_GET['id'])){
	if (!$data=Yii::app()->functions->getFoodItem2($_GET['id'])){
		echo "<div class=\"uk-alert uk-alert-danger\">".
		Yii::t("default","Sorry but we cannot find what you are looking for.")."</div>";
		return ;
	}		
	$addon_item=isset($data['addon_item'])?(array)json_decode($data['addon_item']):false;		
	$category=isset($data['category']);
	$acategory=isset($data['alcohol_category'])?(array)json_decode($data['alcohol_category']):false;
	$subcategory=isset($data['alcohol_subcategory'])?(array)json_decode($data['alcohol_subcategory']):false;
	$orderby=isset($data['order_by'])?(array)json_decode($data['order_by']):false;

	$price=isset($data['price'])?(array)json_decode($data['price']):false;	

	$cooking_ref_selected=isset($data['cooking_ref'])?(array)json_decode($data['cooking_ref']):false;
	$happyhours_selected=isset($data['happyhours'])?(array)json_decode($data['happyhours']):false;
	$multi_option_Selected=isset($data['multi_option'])?(array)json_decode($data['multi_option']):false;
	$multi_option_value_selected=isset($data['multi_option_value'])?(array)json_decode($data['multi_option_value']):false;	
	
	$ingredients_selected=isset($data['ingredients'])?(array)json_decode($data['ingredients']):false;
	
	$two_flavors_position=isset($data['two_flavors_position'])?(array)json_decode($data['two_flavors_position']):false;	
	//dump($two_flavors_position);
	
	$require_addon=isset($data['require_addon'])?(array)json_decode($data['require_addon']):false;		
}
?>

<div class="spacer"></div>

<form class="uk-form uk-form-horizontal forms" id="forms" name="addalcohol">
<?php echo CHtml::hiddenField('action','AddAlcoholbyMerchant')?>
<?php echo CHtml::hiddenField('id',isset($_GET['id'])?$_GET['id']:"");?>
<?php if (!isset($_GET['id'])):?>
<?php echo CHtml::hiddenField("redirect",Yii::app()->request->baseUrl."/merchant/Alcohol")?>
<?php endif;?>


<?php if ( Yii::app()->functions->multipleField()==2):?>

<ul data-uk-tab="{connect:'#tab-content'}" class="uk-tab uk-active">
    <li class="uk-active" ><a href="#"><?php echo t("English")?></a></li>
    <?php if ( $fields=Yii::app()->functions->getLanguageField()):?>  
    <?php foreach ($fields as $f_val): ?>
    <li class="" ><a href="#"><?php echo $f_val;?></a></li>
    <?php endforeach;?>
    <?php endif;?>
</ul>

<ul class="uk-switcher" id="tab-content">

  <li class="uk-active">      
  
  <div class="uk-form-row">
   <label class="uk-form-label"><?php echo Yii::t("default","Alcohol Name")?></label>
  <?php echo CHtml::textField('item_name',
  isset($data['item_name'])?json_decode($data['item_name']):""
  ,array(
  'class'=>'uk-form-width-large',
  'data-validation'=>"required"
  ))?>
    </div>    
    
    <div style="height:10px;"></div>
  
   </li>
   
   <?php 
   $item_name_trans=isset($data['item_name_trans'])?json_decode($data['item_name_trans'],true):'';   
   ?>
   
   <?php if (is_array($fields) && count($fields)>=1):?>
   <?php foreach ($fields as $key_f => $f_val): ?>
   <li>
   
   <div class="uk-form-row">
	   <label class="uk-form-label"><?php echo Yii::t("default","Alcohol Name")?></label>
	  <?php echo CHtml::textField("item_name_trans[$key_f]",
	  array_key_exists($key_f,(array)$item_name_trans)?$item_name_trans[$key_f]:''
	  ,array(
	  'class'=>'uk-form-width-large',
	  ))?>  
   </div>    
   
   <div style="height:10px;"></div>
   
   </li>
   <?php endforeach;?>
   <?php endif;?>
</ul>

<?php else :?>

<?php //print_r($data);  ?>


<div class="uk-form-row" >
  <label class="uk-form-label"><?php echo t("Type")?></label>
  <?php $list=Yii::app()->functions->AlcoholCategory(true);?>
  <select name="alcohol_category" id="alcohol_category" required>
  <option value="">Select</option>
  <?php foreach($list as $res){ ?>
  <?php if($data['a_category']==$res){ ?>
  <option  selected value="<?php echo $res;?>"><?php echo $res;?></option>
  <?php }else{?>
    <option value="<?php echo $res;?>"><?php echo $res;?></option>

  <?php } ?>
  <?php }?>
  </select>
		  </div>

<div class="uk-form-row">
  <label class="uk-form-label" id="input" aria-hidden="true"><?php echo t("Manufacturing")?></label>	  
	<?php $list=Yii::app()->functions->AlcoholSubCategory(true);?>

  <select name="alcohol_subcategory" id="alcohol_subcategory" required>
  <option value="">Select</option>
  <?php foreach($list as $res){?>
  <?php if($data['subcategory']==$res){ ?>
  <option  selected value="<?php echo $res;?>"><?php echo $res;?></option>
  <?php }else{?>
    <option value="<?php echo $res;?>"><?php echo $res;?></option>

  <?php } ?>
  <?php }?>
  </select>
		  
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Brand Name")?></label>
  <?php if(isset($data['item_name'])){ ?>
		<select name="alcohol_name" id="alcohol_name" required>
		<option  selected value="<?php echo $data['item_name'];?>"><?php echo $data['item_name'];?></option>
		</select>
	<?php 
	} else { ?>
	  <?php 
	  echo CHtml::dropDownList('alcohol_name',
	  isset($data['item_name']),
	  (array)Yii::app()->functions->Alcohol(true),
	  array(
	  'class'=>"uk-form-width-large",
	  'data-validation'=>"required",
	  'id'=>"alcohol_name",
	  'onchange'=>"checkname();"
	  ));
	}
  ?>
</div>

<!--<div class="uk-form-row"  id="name1" style="display:none; margin-left:200px;">
  <?php 
  echo CHtml::textField('alcohol_name1',
  isset($data['alcohol_name1'])?$data['alcohol_name1']:""
  ,array(
  'class'=>"uk-form-width-large",
  'placeholder'=>"Brand Name..."
  ))
  ?>
</div>-->


<div class="uk-form-row">
  <label class="uk-form-label"><?php echo t("Order By")?></label>
  <?php echo CHtml::dropDownList('order_by',
  isset($data['order_by'])?$data['order_by']:"",
  (array)OrderBy(),          
  array(
  'class'=>'uk-form-width-large',
  'data-validation'=>"required"
  ))?>
</div>

	<div class="uk-form-row">
	  <label class="uk-form-label"><?php echo t("Quantity")?></label>
	  <?php echo CHtml::textField('quantity',
		isset($data['quantity'])?$data['quantity']:""
		,array(
		'class'=>'uk-form-width-small numeric_only',
		'data-validation'=>"required"
		))?>
		<select name="unit">
		  <option value="ml">ml</option>
		  <option value="ltr">ltr</option>
		</select>
	</div>


<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Price")?></label>
  <?php 
  echo CHtml::textField('price[]',
  isset($price[0])?$price[0]:""
  ,array(
  'id'=>'price',
  'class'=>"uk-form-width-large numeric_only",
  'data-validation'=>"required"
  ))
  ?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Description")?></label>
  <?php 
  echo CHtml::textField('item_description',
  isset($data['item_description'])?$data['item_description']:""
  ,array(
  'class'=>"uk-form-width-large",
  'placeholder'=>"Optional"
  ))
  ?>
</div>

<div class="uk-form-row">
	  <label class="uk-form-label"><?php echo t("Status")?></label>
	  <?php echo CHtml::dropDownList('status',
	  isset($data['status'])?$data['status']:"",
	  (array)statusList(),          
	  array(
	  'class'=>'uk-form-width-large',
	  'data-validation'=>"required"
	  ))?>
	</div>

	<div class="uk-form-row">
		<label class="uk-form-label"><?php echo Yii::t("default","Discount")?></label>
		<?php echo CHtml::textField('discount',
		isset($data['discount'])?$data['discount']:""
		,array(
		'id'=>'discount',
		'class'=>'uk-form-width-medium numeric_only',
		'onchange'=>"validatediscount();"
		))?>
		<select name="discount_type">
		<?php if($data['discount_type']=='Fixed'){ ?>
		  <option selected value="Fixed">Fixed</option>
		  <option value="Percentage">Percentage</option>
		<?php } elseif ($data['discount_type']=='Percentage') { ?>
			<option value="Fixed">Fixed</option>
			<option selected value="Percentage">Percentage</option>
		<?php } else { ?>
			<option value="">Please Select</option>
			<option value="Fixed">Fixed</option>
			<option value="Percentage">Percentage</option>
		<?php }?>
		</select>
		
	</div>
			
	<!--<div class="uk-form-row">
	  <label class="uk-form-label uk-h3"><?php echo t("Tax")?></label>    	  
	  <?php echo CHtml::checkBox('non_taxable',
	  $data['non_taxable']==2?true:false
	  ,array(
	   'class'=>"icheck",
	   'value'=>2
	  ))?>	  	  	  
	  <?php echo t("Non taxable")?>
	</div>-->	


        <div class="uk-form-row">
	  <label class="uk-form-label uk-h3"><?php echo t("Extended Happy Hours")?></label>    	  
	  <?php echo CHtml::checkBox('happyhours',
	  $data['happyhours']==1?true:false
	  ,array(
	   'class'=>"icheckss",
	   'value'=>'1',
	  ))?>	  	  	  
	  <?php //echo t("Extended Happy Hours")?>
	</div>

<div class="uk-form-row" id="startdate" <?php echo (!$data['happyhours'])?'style="display:none"':'' ?> >
	<label class="uk-form-label"><?php echo Yii::t("default","Start Date")?></label>
	<?php echo CHtml::textField('happy_start_date',
	isset($data['happy_start_date'])?date('m-d-Y',strtotime($data['happy_start_date'])):""
	,array(
		'class'=>'uk-form-width-large j_date',
		'data-id'=>'start_date'
	))?>
</div>

<div class="uk-form-row" id="enddate" <?php echo (!$data['happyhours'])?'style="display:none"':'' ?>>
  <label class="uk-form-label"><?php echo Yii::t("default","End Date")?></label>
  <?php echo CHtml::textField('happy_end_date',
  isset($data['happy_end_date'])?date('m-d-Y',strtotime($data['happy_end_date'])):""
  ,array(
  'class'=>'uk-form-width-large j_date',
  'data-id'=>'end_date'
  ))?>
</div>

<div class="uk-form-row" id="starttime" <?php echo (!$data['happyhours'])?'style="display:none"':'' ?>>
  <label class="uk-form-label"><?php echo Yii::t("default","Start Time")?></label>

  <?php
$time = array('00:00:00'=>'12:00 AM','00:30:00'=>'12:30 AM','01:00:00'=>'01:00 AM','01:30:00'=>'01:30 AM','02:00:00'=>'02:00 AM',
'02:30:00'=>'02:30 AM','03:00:00'=>'03:00 AM','03:30:00'=>'03:30 AM','04:00:00'=>'04:00 AM',
'04:30:00'=>'04:30 AM','05:00:00'=>'05:00 AM','05:30:00'=>'05:30 AM','06:00:00'=>'06:00 AM','06:30:00'=>'06:30 AM',
'07:00:00'=>'07:00 AM','07:30:00'=>'07:30 AM','08:00:00'=>'08:00 AM','08:30:00'=>'08:30 AM','09:00:00'=>'09:00 AM','09:30:00'=>'09:30 AM',
'10:00:00'=>'10:00 AM','10:30:00'=>'10:30 AM','11:00:00'=>'11:00 AM','11:30:00'=>'11:30 AM','12:00:00'=>'12:00 PM','12:30:00'=>'12:30 PM',
'13:00:00'=>'01:00 PM','13:30:00'=>'01:30 PM','14:00:00'=>'02:00 PM','14:30:00'=>'02:30 PM','15:00:00'=>'03:00 PM','15:30:00'=>'03:30 PM',   
'16:00:00'=>'04:00 PM','16:30:00'=>'04:30 PM','17:00:00'=>'05:00 PM','17:30:00'=>'05:30 PM','18:00:00'=>'06:00 PM','18:30:00'=>'06:30 PM',
'19:00:00'=>'07:00 PM','19:30:00'=>'07:30 PM','20:00:00'=>'08:00 PM','20:30:00'=>'08:30 PM','21:00:00'=>'09:00 PM','21:30:00'=>'09:30 PM',
'22:00:00'=>'10:00 PM','22:30:00'=>'10:30 PM','23:00:00'=>'11:00 PM','23:30:00'=>'11:30 PM');?>
  <?php echo CHtml::dropDownList('happy_start_time',  
  isset($data['happy_start_time'])?$data['happy_start_time']:"",
  (array)$time
  ,array(
  'class'=>'uk-form-width-large',
  'data-id'=>'start_time'
  ))?>
</div>

<div class="uk-form-row" id="endtime" <?php echo (!$data['happyhours'])?'style="display:none"':'' ?>>
  <label class="uk-form-label"><?php echo Yii::t("default","End Time")?></label>
  <?php echo CHtml::dropDownList('happy_end_time',
  isset($data['happy_end_time'])?$data['happy_end_time']:"",
  (array)$time
  ,array(
  'class'=>'uk-form-width-large timepick',
  'data-id'=>'end_time'
  ))?>
</div>

<div class="uk-form-row" id="ext_time" <?php echo (!$data['happyhours'])?'style="display:none"':'' ?>>
  <label class="uk-form-label"><?php echo Yii::t("default","Extended Time")?></label>
  <?php
  $ss = array('0' => '0 Hrs','1'=>'1 Hrs','2'=>'2 Hrs','3'=>'3 Hrs','4'=>'4 Hrs' );
   echo CHtml::dropDownList('ext_time',  
  isset($data['ext_time'])?$data['ext_time']:"",
  (array)$ss
  ,array(
  'class'=>'uk-form-width-large',
  'data-id'=>'ext_time'
  ))?>
</div>

	<!--POINTS PROGRAM-->
	<?php /*if (FunctionsV3::hasModuleAddon("pointsprogram")):?>
	<div class="uk-form-row">
	  <label class="uk-form-label"><?php echo t("Points earned")?></label>  	  	  
	  <?php echo CHtml::textField('points_earned',
	  $data['points_earned']>0?$data['points_earned']:''
	  ,array(
	    'class'=>"numeric_only"
	  ))?>
	</div>  
	<div class="uk-form-row" style="padding-left:200px">  
	  <?php echo CHtml::checkBox('points_disabled',
	  $data['points_disabled']==2?true:false
	  ,array(
	   'class'=>"icheck",
	   'value'=>2
	  ))?>	  	  	  
	  <?php echo t("Disabled Points on this item")?>
	</div>
	
	
<?php endif;*/ ?>	
<?php endif;?>

<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" aria-hidden="true" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>

<script> 
 </script>
