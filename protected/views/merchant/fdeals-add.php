
<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Flashdeals/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Flashdeals" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
</div>

<div class="spacer"></div>

<div id="error-message-wrapper"></div>

<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','addDeals')?>
<?php echo CHtml::hiddenField('id',isset($_GET['id'])?$_GET['id']:"");?>
<?php if (!isset($_GET['id'])):?>
<?php echo CHtml::hiddenField("redirect",Yii::app()->request->baseUrl."/merchant/Flashdeals")?>
<?php endif;?>

<?php 
if (isset($_GET['id'])){
	if (!$data=Yii::app()->functions->getDeals($_GET['id'])){
		echo "<div class=\"uk-alert uk-alert-danger\">".
		Yii::t("default","Sorry but we cannot find what your are looking for.")."</div>";
		return ;
	}	
}
?>                                 

<div class="uk-form-row" style="display: none;">
	<label class="uk-form-label">Deal Type</label>
	<input id="deal_type" value="FLD" name="deal_type" type="text">
</div>

<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Deal Description")?></label>
	<?php echo CHtml::textField('deal_desc',
	isset($data['deal_desc'])?$data['deal_desc']:""
	,array(
		'class'=>'uk-form-width-large',
		'data-validation'=>"required"
	))?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Start Time")?></label>
  <?php echo CHtml::textField('start_time',
  isset($data['start_time'])?$data['start_time']:"",         
  array(
  'class'=>'timepick',
  'data-validation'=>"not required"
  ))?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","End Time")?></label>
  <?php echo CHtml::textField('end_time',
  isset($data['end_time'])?$data['end_time']:"",          
  array(
  'class'=>'timepick',
  'data-validation'=>"not required"
  ))?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Valid From")?></label>
  <?php echo CHtml::hiddenField('valid_from',isset($data['valid_from'])?$data['valid_from']:"")?>
  <?php echo CHtml::textField('valid_from2',
  isset($data['valid_from'])?$data['valid_from']:""
  ,array(
  'class'=>'j_date',
  'data-validation'=>"required",
  'data-id'=>"valid_from"
  ))?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Valid To")?></label>
  <?php echo CHtml::hiddenField('valid_to',isset($data['valid_to'])?$data['valid_to']:"")?>
  <?php echo CHtml::textField('valid_to2',
  isset($data['valid_to'])?$data['valid_to']:""
  ,array(
  'class'=>'j_date',
  'data-validation'=>"required",
  'data-id'=>"valid_to"
  ))?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Status")?></label>
  <?php echo CHtml::dropDownList('status',
  isset($data['status'])?$data['status']:"",
  (array)statusList(),          
  array(
  'class'=>'',
  'data-validation'=>"required"
  ))?>
</div>

<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>
