
<div class="uk-width-1">
<!--<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Alcohol/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>-->
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Alcohol" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
<!--<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/RequestAlcohol" class="uk-button"><i class="fa fa-envelope"></i> <?php echo Yii::t("default","Request New Alcohol Brand")?></a>-->
</div>

<div class="spacer"></div>

<form class="uk-form uk-form-horizontal forms" id="forms" name="request_alcohol">
<?php echo CHtml::hiddenField('action','RequestAlcohol')?>
<?php echo CHtml::hiddenField("redirect",Yii::app()->request->baseUrl."/merchant/Alcohol")?>

<div class="uk-form-row" >
	<label class="uk-form-label"><?php echo t("Type")?></label>
	<?php $list=Yii::app()->functions->AlcoholCategory(true);?>
	<select name="alcohol_category" id="alcohol_category" required>
		<option value="">Select</option>
		<?php foreach($list as $res)
		{?>
			<?php if($data['alcohol_category']==$res){ ?>
			<option  selected value="<?php echo $res;?>"><?php echo $res;?></option>
			<?php 
			} else
			{?>
			<option value="<?php echo $res;?>"><?php echo $res;?></option>
			<?php 
			} ?>
		<?php
		}?>
	</select>
</div>

<div class="uk-form-row">
  <label class="uk-form-label" id="input" aria-hidden="true"><?php echo t("Manufacturing")?></label>	  
	<?php $list=Yii::app()->functions->AlcoholSubCategory(true);?>

  <select name="alcohol_subcategory" id="alcohol_subcategory" required>
  <option value="">Select</option>
  <?php foreach($list as $res){?>
  <?php if($data['alcohol_subcategory']==$res){ ?>
  <option  selected value="<?php echo $res;?>"><?php echo $res;?></option>
  <?php }else{?>
    <option value="<?php echo $res;?>"><?php echo $res;?></option>

  <?php } ?>
  <?php }?>
  </select>
		  
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Brand Name")?></label>
  <?php 
  echo CHtml::textField('alcohol_name',array(
  'class'=>"uk-form-width-large",
  'placeholder'=>"Brand Name"
  ))
  ?>
</div>

<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" aria-hidden="true" value="<?php echo Yii::t("default","Send Request")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>

<script> 
 </script>
