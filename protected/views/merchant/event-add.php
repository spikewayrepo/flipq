<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Event/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Event" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Event/Do/Sort" class="uk-button"><i class="fa fa-sort-alpha-asc"></i> <?php echo Yii::t("default","Sort")?></a>
</div>

<div class="spacer"></div>

<div id="error-message-wrapper"></div>
<?php $infouser = json_decode($_SESSION['kr_merchant_user']);  ?>
<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','AddEvent')?>
<?php echo CHtml::hiddenField('id',isset($_GET['id'])?$_GET['id']:"");?>
<?php echo CHtml::hiddenField('merchant_id',isset($infouser[0]->merchant_id)?$infouser[0]->merchant_id:"");?>
<?php if (!isset($_GET['id'])):?>
<?php echo CHtml::hiddenField("redirect",Yii::app()->request->baseUrl."/merchant/Event")?>
<?php endif;?>

<?php 
if (isset($_GET['id'])){
	if (!$data=Yii::app()->functions->getEvent($_GET['id'])){
		echo "<div class=\"uk-alert uk-alert-danger\">".
		Yii::t("default","Sorry but we cannot find what your are looking for.")."</div>";
		return ;
	}	
}
?>                                 
<?php if ( Yii::app()->functions->multipleField()==2):?>
<?php 
Widgets::multipleFields(array(
  'Event. name'
),array(
  'event_name'
),$data,array(true));
?>
<div class="spacer"></div>
<?php else :?>
<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default"," Event name")?></label>
  <?php echo CHtml::textField('event_name',
  isset($data['event_name'])?$data['event_name']:""
  ,array(
  'class'=>'uk-form-width-large',
  'data-validation'=>"required"
  ))?>
</div>

<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Event Description")?></label>
	<?php echo CHtml::textArea('event_description',
	isset($data['event_description'])?$data['event_description']:""
	,array(
	'class'=>'uk-form-width-large big-textarea'	
	))?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default"," Venue")?></label>
  <?php echo CHtml::textField('venue',
  isset($data['venue'])?$data['venue']:""
  ,array(
  'class'=>'uk-form-width-large',
  'data-validation'=>"required"
  ))?>
</div>

<div class="uk-form-row"> 
 <label class="uk-form-label"><?php echo Yii::t('default',"Upload Photo")?></label>
  <div style="display:inline-table;margin-left:1px;" class="button uk-button" id="spicydish"><?php echo Yii::t('default',"Browse")?></div>	  
  <DIV  style="display:none;" class="spicydish_chart_status" >
	<div id="percent_bar" class="spicydish_percent_bar"></div>
	<div id="progress_bar" class="spicydish_progress_bar">
	  <div id="status_bar" class="spicydish_status_bar"></div>
	</div>
  </DIV>		  
</div>

<?php $spicydish=isset($data['photo'])?$data['photo']:'';?>
<?php if (!empty($spicydish)):?>
<div class="uk-form-row"> 
<?php else :?>
<div class="input_block preview_spicydish">
<?php endif;?>

<label><?php echo Yii::t('default',"Preview")?></label>
<div class="image_preview_spicydish">
 <?php if (!empty($spicydish)):?>
 <input type="hidden" name="spicydish" value="<?php echo $spicydish;?>">
 <img class="uk-thumbnail" src="<?php echo Yii::app()->request->baseUrl."/upload/".$spicydish;?>?>" alt="" title="">
 <p><a href="javascript:rm_spicydish_preview();"><?php echo Yii::t("default","Remove image")?></a></p>
 <?php endif;?>
</div>
</div>


<?php	
	if ($check=Yii::app()->functions->isDJ()){
?>
<?php } else {?>
  <div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default"," Fee")?></label>
  <?php echo CHtml::textField('fees',
  isset($data['fees'])?$data['fees']:""
  ,array(
  'type'=>'number',
  'class'=>'uk-form-width-medium numeric_only'
  ))?>
	</div>
<?php } ?>


<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Start Date")?></label>
  <?php echo CHtml::textField('start_date',''  
  ,array(
  'class'=>'uk-form-width-large j_date',
  'data-id'=>'start_date',
  'data-validation'=>"required"
  ))?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","End Date")?></label>
  <?php echo CHtml::textField('end_date',''  
  ,array(
  'class'=>'uk-form-width-large j_date',
  'data-id'=>'end_date',
  'data-validation'=>"required"
  ))?>
</div>


<?php endif;?>


<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Status")?></label>
  <?php echo CHtml::dropDownList('status',
  isset($data['status'])?$data['status']:"",
  (array)statusList(),          
  array(
  'class'=>'uk-form-width-large',
  'data-validation'=>"required"
  ))?>
</div>

<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>
