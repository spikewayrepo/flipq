
<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Event/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Event" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/Event/Do/Sort" class="uk-button"><i class="fa fa-sort-alpha-asc"></i> <?php echo Yii::t("default","Sort")?></a>
</div>

<form id="frm_table_list" method="POST" >
<input type="hidden" name="action" id="action" value="EventList">
<input type="hidden" name="tbl" id="tbl" value="event">
<input type="hidden" name="clear_tbl"  id="clear_tbl" value="clear_tbl">
<input type="hidden" name="whereid"  id="whereid" value="event_id">
<input type="hidden" name="slug" id="slug" value="Event/Do/Add">
<table id="table_list" class="uk-table uk-table-hover uk-table-striped uk-table-condensed">
   <thead>
        <tr>
			 <th width="1%"><input type="checkbox" id="chk_all" class="chk_all"></th>
			 <th width="5%"><?php echo Yii::t('default',"Name")?></th>			 
			 <th width="5%"><?php echo Yii::t('default',"Photo")?></th>			 
			 <th width="5%"><?php echo Yii::t('default',"Venue")?></th>			 
			 <?php	
				if ($check=Yii::app()->functions->isDJ())
				{
			 ?>		
			 <?php
			 } else {
			 ?>
			 <th width="5%"><?php echo Yii::t('default',"Fees")?></th>
			 <?php } ?>
			 <th width="5%"><?php echo Yii::t('default',"Start Date")?></th>			 
			 <th width="5%"><?php echo Yii::t('default',"End Date")?></th>			 
			 <th width="5%"><?php echo Yii::t('default',"Status")?></th>
        </tr>
    </thead>
    <tbody>    
    </tbody>
</table>
<div class="clear"></div>
</form>
