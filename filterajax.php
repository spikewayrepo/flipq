<?php $html = ''; ?>

<?php $html .= '<div class="row pad-xs-40" id="restralisting">';
			
			if ($res){
				foreach ($res['list'] as $val)
					{
						$merchant_id = $val['merchant_id']; 
						$ratings=Yii::app()->functions->getRatings($merchant_id);
						?>
                                                <?php $html .= '
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        
							<div class="list-card">';
								
								
									$logoo = FunctionsV3::getMerchantLogo($merchant_id);


								
$html .= '
<div class="list-card">
          <div class="image-div1" style="background: rgba(0,0,0,0) url('.$logoo.') no-repeat scroll center center / cover;">
            '.FunctionsV3::merchantOpenTag($merchant_id).'
          </div>
          <div class="list-card-body">
            <p><a href="'.Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'].'?booktable=true').'">'.$val['restaurant_name'].'</a></p><p>';
?>
<?php
$cui_arr = explode(",", FunctionsV3::displayCuisine($val['cuisine']));

	if(count($cui_arr) > 2)
		$html .= "Multicuisine";
	else
		$html .= "".($val['cuisine']!='null')?FunctionsV3::displayCuisine($val['cuisine']):'No Cuisine'."";


									$deals=FunctionsV3::getNoofdeals($merchant_id);
?>
<?php $html .='</p>

            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;'.$val['location'].'</p>
            <a data-toggle="modal" data-target="#dealsModal"><p class="deals"><svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" id="Layer_1"><defs><style>.cls-1,.cls-2{fill:none;stroke:#666;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}.cls-1{fill-rule:evenodd;}</style></defs><title>deal</title><path d="M38,16.3a3.83,3.83,0,1,0-3.84-3.82A3.83,3.83,0,0,0,38,16.3Zm8,8.33L23.94,46.78a2.57,2.57,0,0,1-3.64,0L3,29.55a2.57,2.57,0,0,1,0-3.64L24.62,4.23a5.92,5.92,0,0,1,4.19-1.74l15.69,0a3.22,3.22,0,0,1,3.23,3.21l0,14.76A6,6,0,0,1,46,24.64Z" class="cls-1"/><circle transform="translate(-0.02 0.06) rotate(-0.08)" r="3.83" cy="12.47" cx="37.99" class="cls-2"/></svg>';

  $html .= ($deals)?$deals." Deals":'0 Deals';

  $html .='</p></a> <p><span style="font-family:quick-bold;">₹</span>&nbsp;&nbsp;2300 Cost For 2</p> <div class="rating">


'.$aa.'

          <div class=" rating-stars" data-score="'.$ratings['ratings'].'"></div>
            </div>
          </div>
          <div class="list-card-footer text-center">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <button data-toggle="modal" data-target="#etaModal">Pre-Order</button>
              </div>
              <div class="col-sm-6 col-xs-6">
                <a href="'. Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug']).'"><button>Book-Table</button></a>
              </div>
            </div>
          </div>
        </div>

			
        
							</div>
					
					</div>';


				
				} 
			} 


?>



		
