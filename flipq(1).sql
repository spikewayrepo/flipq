-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 01, 2016 at 05:20 PM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flipq`
--

-- --------------------------------------------------------

--
-- Table structure for table `mt_address_book`
--

CREATE TABLE `mt_address_book` (
  `id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `location_name` varchar(255) NOT NULL,
  `country_code` varchar(3) NOT NULL,
  `as_default` int(1) NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_address_book`
--

INSERT INTO `mt_address_book` (`id`, `client_id`, `street`, `city`, `state`, `zipcode`, `location_name`, `country_code`, `as_default`, `date_created`, `date_modified`, `ip_address`) VALUES
(1, 1, '508/5 govind puri kalka ji delhi', 'delhi', 'delhi', '110019', 'Govind Puri', 'IN', 1, '2016-08-25 18:27:23', '0000-00-00 00:00:00', '182.77.3.127'),
(2, 3, 'C/138, Delhi G.P.O., Tagore Garden Extension', 'Delhi', 'Delhi', '110001', 'hggdgfdfgd', 'Ind', 1, '2016-08-26 19:04:50', '0000-00-00 00:00:00', '122.177.166.76'),
(3, 3, 'C/138, Delhi G.P.O., Tagore Garden Extension', 'Delhi', 'Delhi', '110001', 'hggdgfdfgd', 'Ind', 1, '2016-08-26 19:27:49', '0000-00-00 00:00:00', '122.177.166.76'),
(4, 2, '454', 'uyuigj', 'hkjhkjhkj', '282002', 'hkhuiyiuyui', 'Ind', 2, '2016-09-02 20:04:25', '0000-00-00 00:00:00', '122.177.244.251');

-- --------------------------------------------------------

--
-- Table structure for table `mt_admin_user`
--

CREATE TABLE `mt_admin_user` (
  `admin_id` int(14) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `user_lang` int(14) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `lost_password_code` varchar(255) NOT NULL,
  `session_token` varchar(255) NOT NULL,
  `last_login` datetime NOT NULL,
  `user_access` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_admin_user`
--

INSERT INTO `mt_admin_user` (`admin_id`, `username`, `password`, `first_name`, `last_name`, `role`, `date_created`, `date_modified`, `ip_address`, `user_lang`, `email_address`, `lost_password_code`, `session_token`, `last_login`, `user_access`) VALUES
(1, 'admin', '6af1fb5c5015b1fc56dd047eb4d592ba', 'Admin', 'admin', '', '2016-07-27 09:49:42', '2016-09-01 18:56:19', '171.48.35.109', 0, 'ajay@brownhubsolution.com', '', '56071949150aa53ab9bfc8306956371dc00ccccd42b', '2016-11-01 01:40:11', '["autologin","dashboard","merchant","sponsoredMerchantList","packages","highlights","happyhours","Cuisine","dishes","dishes","dishes","global","OrderStatus","settings","themesettings","zipcode","commisionsettings","voucher","merchantcommission","withdrawal","incomingwithdrawal","withdrawalsettings","emailsettings","emailtpl","customPage","Ratings","ContactSettings","SocialSettings","ManageCurrency","ManageLanguage","Seo","addons","mobileapp","pointsprogram","analytics","customerlist","subscriberlist","reviews","bankdeposit","paymentgatewaysettings","paymentgateway","paypalSettings","stripeSettings","mercadopagoSettings","sisowsettings","payumonenysettings","obdsettings","payserasettings","payondelivery","barclay","epaybg","authorize","sms","smsSettings","smsPackage","smstransaction","smslogs","fax","faxtransaction","faxpackage","faxlogs","faxsettings","reports","rptMerchantReg","rptMerchantPayment","rptMerchanteSales","rptmerchantsalesummary","rptbookingsummary","userList"]');

-- --------------------------------------------------------

--
-- Table structure for table `mt_alcohol`
--

CREATE TABLE `mt_alcohol` (
  `alcohol_id` int(14) NOT NULL,
  `alcohol_name` varchar(255) NOT NULL,
  `sequence` int(14) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_address` varchar(50) NOT NULL,
  `alcohol_name_trans` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `sub_category` varchar(255) DEFAULT NULL,
  `status` enum('Pending','Publish','Draft','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mt_alcohol`
--

INSERT INTO `mt_alcohol` (`alcohol_id`, `alcohol_name`, `sequence`, `date_created`, `date_modified`, `ip_address`, `alcohol_name_trans`, `category`, `sub_category`, `status`) VALUES
(1, 'Black Dog', 0, '2016-10-03 12:49:05', '2016-10-05 01:14:41', '122.177.231.61', '', 'RUM', 'FL', 'Publish'),
(2, 'Signature', 0, '2016-10-03 16:45:43', '2016-10-10 01:59:46', '116.203.74.121', '', 'WINE', 'FL', 'Publish'),
(3, 'Mc Dowells No.1', 0, '2016-10-04 07:19:19', '2016-10-05 01:09:26', '122.177.231.61', NULL, 'WHISKY', 'IMFL', 'Publish');

-- --------------------------------------------------------

--
-- Table structure for table `mt_alcohol_category`
--

CREATE TABLE `mt_alcohol_category` (
  `a_cat_id` int(14) NOT NULL,
  `a_cat_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sequence` int(14) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_address` varchar(255) NOT NULL,
  `a_cat_name_trans` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` enum('Pending','Publish','Draft','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mt_alcohol_category`
--

INSERT INTO `mt_alcohol_category` (`a_cat_id`, `a_cat_name`, `sequence`, `date_created`, `date_modified`, `ip_address`, `a_cat_name_trans`, `status`) VALUES
(1, 'WINE', 6, '2016-10-03 16:47:49', '2016-10-05 12:45:29', '122.177.231.61', '', 'Pending'),
(3, 'BEER', 1, '2016-10-04 12:43:16', '2016-10-05 12:45:22', '122.177.231.61', '', 'Pending'),
(4, 'VODKA', 4, '2016-10-04 12:55:21', '2016-10-05 12:45:15', '122.177.231.61', '', 'Pending'),
(5, 'WHISKY', 5, '2016-10-04 12:55:44', '2016-10-05 12:45:08', '122.177.231.61', '', 'Pending'),
(6, 'RUM', 3, '2016-10-04 12:59:20', '2016-10-05 12:45:00', '122.177.231.61', '', 'Pending'),
(7, 'GIN', 2, '2016-10-04 07:18:12', '2016-10-05 12:44:51', '122.177.231.61', NULL, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `mt_alcohol_subcategory`
--

CREATE TABLE `mt_alcohol_subcategory` (
  `scat_id` int(14) NOT NULL,
  `scat_name` varchar(255) NOT NULL,
  `sequence` int(14) DEFAULT '0',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_address` varchar(50) NOT NULL,
  `scat_name_trans` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mt_alcohol_subcategory`
--

INSERT INTO `mt_alcohol_subcategory` (`scat_id`, `scat_name`, `sequence`, `date_created`, `date_modified`, `ip_address`, `scat_name_trans`) VALUES
(1, 'IMFL', 2, '2016-10-05 11:53:49', '2016-10-05 04:53:49', '122.177.231.61', NULL),
(2, 'FL', 1, '2016-10-05 11:55:17', '2016-10-05 12:09:11', '122.177.231.61', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_bank_deposit`
--

CREATE TABLE `mt_bank_deposit` (
  `id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `branch_code` varchar(100) NOT NULL,
  `date_of_deposit` date NOT NULL,
  `time_of_deposit` varchar(50) NOT NULL,
  `amount` float(14,4) NOT NULL,
  `scanphoto` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'pending',
  `date_created` date NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `transaction_type` varchar(255) NOT NULL DEFAULT 'merchant_signup',
  `client_id` int(14) NOT NULL,
  `order_id` int(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_barclay_trans`
--

CREATE TABLE `mt_barclay_trans` (
  `id` int(14) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `transaction_type` varchar(255) NOT NULL DEFAULT 'signup',
  `date_created` date NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `param1` varchar(255) NOT NULL,
  `param2` text NOT NULL,
  `param3` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_bookingtable`
--

CREATE TABLE `mt_bookingtable` (
  `booking_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `number_guest` int(14) NOT NULL,
  `date_booking` date NOT NULL,
  `booking_time` varchar(50) NOT NULL,
  `booking_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `booking_notes` text NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `viewed` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_category`
--

CREATE TABLE `mt_category` (
  `cat_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL DEFAULT '0',
  `category_name` varchar(255) NOT NULL,
  `category_description` text NOT NULL,
  `photo` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL,
  `sequence` int(14) NOT NULL DEFAULT '0',
  `date_created` varchar(50) NOT NULL,
  `date_modified` varchar(50) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `spicydish` int(2) NOT NULL DEFAULT '1',
  `spicydish_notes` text NOT NULL,
  `dish` text NOT NULL,
  `category_name_trans` text,
  `category_description_trans` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_category`
--

INSERT INTO `mt_category` (`cat_id`, `merchant_id`, `category_name`, `category_description`, `photo`, `status`, `sequence`, `date_created`, `date_modified`, `ip_address`, `spicydish`, `spicydish_notes`, `dish`, `category_name_trans`, `category_description_trans`) VALUES
(10, 0, 'Starter', 'Starter Menu', '1472668272-enhanced-13966-1426688813-29.jpg', 'publish', 0, '2016-09-01T01:31:15+07:00', '2016-10-22 04:21:30', '122.177.241.26', 1, '', '', '', ''),
(12, 0, 'Cocktail', 'Cocktails', '1475309354-Flaming-drinks.jpg', 'publish', 0, '2016-10-01T13:39:19+05:30', '2016-10-22 04:24:45', '122.177.241.26', 1, '', '', '', ''),
(15, 0, 'Alcohol', 'Alcohol', '1477122426-51e9321d5cb70f57c379553dd7d93b78.jpg', 'publish', 0, '2016-10-22 04:14:07', '2016-10-22 04:21:16', '122.177.241.26', 1, '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_client`
--

CREATE TABLE `mt_client` (
  `client_id` int(14) NOT NULL,
  `social_strategy` varchar(100) NOT NULL DEFAULT 'web',
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email_address` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zipcode` varchar(100) NOT NULL,
  `country_code` varchar(3) NOT NULL,
  `location_name` text NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `lost_password_token` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'active',
  `token` varchar(255) NOT NULL,
  `mobile_verification_code` int(14) NOT NULL,
  `mobile_verification_date` datetime NOT NULL,
  `custom_field1` varchar(255) NOT NULL,
  `custom_field2` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `email_verification_code` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_client`
--

INSERT INTO `mt_client` (`client_id`, `social_strategy`, `first_name`, `last_name`, `email_address`, `password`, `street`, `city`, `state`, `zipcode`, `country_code`, `location_name`, `contact_phone`, `lost_password_token`, `date_created`, `date_modified`, `last_login`, `ip_address`, `status`, `token`, `mobile_verification_code`, `mobile_verification_date`, `custom_field1`, `custom_field2`, `avatar`, `email_verification_code`) VALUES
(1, 'web', 'ajay', 'jain', 'jainajay4444@gmail.com', '6af1fb5c5015b1fc56dd047eb4d592ba', '', '', '', '', '', '', '+918285546609', '72a07967346317e3970fc51693039f35', '2016-07-27 11:07:09', '0000-00-00 00:00:00', '2016-09-15 09:39:23', '150.107.8.185', 'active', 'ne2ov2gc2cx3vnhdb4776394b2e7857a46a03e22dd88c13', 0, '0000-00-00 00:00:00', '', '', '', ''),
(2, 'web', 'arpit', 'kumar', 'agrahariarpit@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', '', '', '', '', '', '+919818389740', '', '2016-08-05 22:31:14', '0000-00-00 00:00:00', '2016-09-03 12:57:27', '122.176.113.24', 'active', '9ebz26x81xavfpbceb638232e6ee7d642615b35546b69b6', 222142, '2016-09-02 00:14:10', '', '', '', ''),
(3, 'web', 'Dhawanit', 'Bhatnagar', 'dhawanitbhatnagar@gmail.com', '3c7e6c4db14465b73a62668d886481dd', '', '', '', '', '', '', '+918439765406', '', '2016-08-12 12:52:56', '0000-00-00 00:00:00', '2016-10-26 04:43:08', '182.69.235.34', 'active', '', 410331, '2016-08-12 12:57:36', '', '', '', ''),
(5, 'fb', 'Praveen', 'Kumar', 'prvn24691@yahoo.com', '691332307c248311cd63d22a96c5d3a1', '', '', '', '', '', '', '', '', '2016-08-18 20:23:38', '0000-00-00 00:00:00', '2016-08-18 20:23:38', '1.39.40.118', 'active', '', 0, '0000-00-00 00:00:00', '', '', '', ''),
(6, 'web', 'Preeti ', 'Budhiraja ', 'Preetibudhiraja1980@gmail.com', '283e769aaf5e377589379821213c1797', '', '', '', '', '', '', '+919910997779', '', '2016-08-23 01:39:53', '0000-00-00 00:00:00', '2016-08-23 01:39:53', '59.178.204.108', 'active', '', 0, '0000-00-00 00:00:00', '', '', '', ''),
(7, 'fb', 'Kajal', 'Jain', 'kajal.ajay94@gmail.com', 'baac0225d2ffa5b3ad4f36c9b59b9a57', '', '', '', '', '', '', '', '', '2016-08-30 15:55:53', '0000-00-00 00:00:00', '2016-08-30 15:55:53', '112.196.181.216', 'active', '', 0, '0000-00-00 00:00:00', '', '', '', ''),
(8, 'mobile', 'xyz', 'jan', 'jainajay24444@gmail.com', '6af1fb5c5015b1fc56dd047eb4d592ba', '', '', '', '', '', '', '9278741826', '', '2016-09-01 14:29:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '182.69.215.213', 'active', '6b0y6kk3h7fqwzma31b04764a20211efa47a51ed2566c6e', 0, '0000-00-00 00:00:00', '', '', '', ''),
(9, 'mobile', 'deep', 'singh', 'deep@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', '', '', '', '11344556', '', '2016-09-01 14:47:19', '0000-00-00 00:00:00', '2016-09-03 11:30:09', '203.122.26.101', 'active', '8roixbitbjm1pkr7838bf270a7ac9a6c6ff8db9bc97a636', 0, '0000-00-00 00:00:00', '', '', '', ''),
(10, 'mobile', 'dsf', 'dsfsd', 'dsfsd', '202cb962ac59075b964b07152d234b70', '', '', '', '', '', '', 'dfsdf', '', '2016-09-01 14:55:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '203.122.26.101', 'active', 'e4ttqwardmno2p437e0c86903875b5673b5cd4b03fe8082', 0, '0000-00-00 00:00:00', '', '', '', ''),
(11, 'mobile', 'deep', 'singh', 'deetp@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', '', '', '', '112323', '9356820caeee3c14c42e7074e163d7bd', '2016-09-01 15:00:37', '0000-00-00 00:00:00', '2016-09-01 15:04:22', '203.122.26.101', 'active', '4c2yhzp8p62bpsf8c8805ffcb6bc5f94f6c2f65602f8729', 0, '0000-00-00 00:00:00', '', '', '', ''),
(12, 'web', 'Dhawanit', 'Bhatnagar', 'dpikemon@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', '', '', '', '', '', '+919810912887', '', '2016-09-02 20:10:36', '0000-00-00 00:00:00', '2016-09-02 20:10:36', '122.177.244.251', 'active', '', 0, '0000-00-00 00:00:00', '', '', '', ''),
(13, 'mobile', 'a', 'a', 'a@a.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', '', '', '9812324567', 'c7f613ba18c3dba7183429c8b328a667', '2016-09-03 11:48:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '203.122.26.101', 'active', 'lfyvaclpwnvnyn7d10ca8d11301c2f4993ac2279ce4b930', 0, '0000-00-00 00:00:00', '', '', '', ''),
(14, 'mobile', 'b', 'b', 'b@b.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', '', '', '8877887898', '', '2016-09-03 11:50:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '203.122.26.101', 'active', 'mgrziqcisfb7xj92076105f6efe7c11e285add95f514b9a', 0, '0000-00-00 00:00:00', '', '', '', ''),
(15, 'mobile', 'c', 'c', 'c@c.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', '', '', '9989897898', '', '2016-09-03 11:53:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '203.122.26.101', 'active', 'sphm0bo0ut7kztma6d14de05d7b2c3cf4fae7ae14cfa7f3', 0, '0000-00-00 00:00:00', '', '', '', ''),
(16, 'mobile', 'deep', 'singh', 'deeps@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', '', '', '', '123123123', '', '2016-09-03 12:33:00', '0000-00-00 00:00:00', '2016-09-03 12:33:22', '182.69.232.207', 'active', 'y1wl24hyk83d0oy240971b5c9ad50f8f631c38ee9c79cd2', 0, '0000-00-00 00:00:00', '', '', '', ''),
(17, 'fb', 'Jain', 'Ajay', 'jainmca4444@gmail.com', 'fdc6e369f2a46fd2ce88fdb559b09399', '', '', '', '', '', '', '', '', '2016-09-22 18:53:06', '0000-00-00 00:00:00', '2016-10-27 12:26:35', '122.177.100.160', 'active', '', 0, '0000-00-00 00:00:00', '', '', '', ''),
(18, 'web', ',fak', 'lkjasdha', 'ahdhjag@gmail.com', 'c44a471bd78cc6c2fea32b9fe028d30a', '', '', '', '', '', '', '+919898989898', '', '2016-10-01 13:40:40', '0000-00-00 00:00:00', '2016-10-01 13:40:40', '122.177.251.173', 'active', '', 0, '0000-00-00 00:00:00', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mt_client_cc`
--

CREATE TABLE `mt_client_cc` (
  `cc_id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `card_name` varchar(255) NOT NULL,
  `credit_card_number` varchar(20) NOT NULL,
  `expiration_month` varchar(5) NOT NULL,
  `expiration_yr` varchar(5) NOT NULL,
  `cvv` varchar(20) NOT NULL,
  `billing_address` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_cooking_ref`
--

CREATE TABLE `mt_cooking_ref` (
  `cook_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `cooking_name` varchar(255) NOT NULL,
  `sequence` int(14) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'published',
  `ip_address` varchar(50) NOT NULL,
  `cooking_name_trans` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_cuisine`
--

CREATE TABLE `mt_cuisine` (
  `cuisine_id` int(14) NOT NULL,
  `cuisine_name` varchar(255) NOT NULL,
  `sequence` int(14) DEFAULT '0',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_address` varchar(50) NOT NULL,
  `cuisine_name_trans` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_cuisine`
--

INSERT INTO `mt_cuisine` (`cuisine_id`, `cuisine_name`, `sequence`, `date_created`, `date_modified`, `ip_address`, `cuisine_name_trans`) VALUES
(1, 'American', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(2, 'Deli', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(3, 'Indian', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(4, 'Mediterranean', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(5, 'Sandwiches', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(6, 'Barbeque', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(7, 'Diner', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(8, 'Italian', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(9, 'Mexican', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(10, 'Sushi', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(11, 'Burgers', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(12, 'Greek', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(13, 'Japanese', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(14, 'Middle Eastern', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(15, 'Thai', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(16, 'Chinese', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(17, 'Healthy', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(18, 'Korean', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(19, 'Pizza', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(20, 'Vegetarian', 0, '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173', ''),
(21, 'Lebanese', 0, '2016-08-22 12:20:18', '0000-00-00 00:00:00', '122.177.66.52', ''),
(22, 'North Indian', 0, '2016-08-22 12:20:34', '0000-00-00 00:00:00', '122.177.66.52', ''),
(23, 'Asian', 0, '2016-08-22 12:24:17', '0000-00-00 00:00:00', '122.177.66.52', ''),
(24, 'Continental', 0, '2016-08-22 12:25:41', '2016-10-08 05:46:43', '122.177.111.202', ''),
(25, 'Finger Food', 0, '2016-08-22 14:33:35', '2016-10-08 05:44:51', '122.177.111.202', ''),
(26, 'European', 0, '2016-08-22 15:41:50', '0000-00-00 00:00:00', '122.177.66.52', '');

-- --------------------------------------------------------

--
-- Table structure for table `mt_currency`
--

CREATE TABLE `mt_currency` (
  `currency_code` varchar(3) NOT NULL,
  `currency_symbol` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_currency`
--

INSERT INTO `mt_currency` (`currency_code`, `currency_symbol`, `date_created`, `date_modified`, `ip_address`) VALUES
('AUD', '&#36;', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
('CAD', '&#36;', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
('CNY', '&yen;', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
('EUR', '&euro;', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
('HKD', '&#36;', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
('INR', 'Rs.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '122.177.250.85'),
('JPY', '&yen;', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
('MXN', '&#36;', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
('MYR', '&#82;&#77;', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
('NZD', '&#36;', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
('USD', '&#36;', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173');

-- --------------------------------------------------------

--
-- Table structure for table `mt_custom_page`
--

CREATE TABLE `mt_custom_page` (
  `id` int(14) NOT NULL,
  `slug_name` varchar(255) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `icons` varchar(255) NOT NULL,
  `assign_to` varchar(50) NOT NULL,
  `sequence` int(14) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `open_new_tab` int(11) NOT NULL DEFAULT '1',
  `is_custom_link` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_custom_page`
--

INSERT INTO `mt_custom_page` (`id`, `slug_name`, `page_name`, `content`, `seo_title`, `meta_description`, `meta_keywords`, `icons`, `assign_to`, `sequence`, `status`, `date_created`, `date_modified`, `ip_address`, `open_new_tab`, `is_custom_link`) VALUES
(1, 'how-it-work', 'How IT Work', 'coming soon', '', '', '', 'how-it-work', 'bottom', 1, 'publish', '2016-08-04 12:13:06', '2016-08-04 12:16:43', '182.77.5.227', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mt_deals`
--

CREATE TABLE `mt_deals` (
  `deals_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `offer_percentage` float(14,4) NOT NULL,
  `coupon_code` varchar(255) DEFAULT NULL,
  `offer_price` float(14,4) NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_deals`
--

INSERT INTO `mt_deals` (`deals_id`, `merchant_id`, `offer_percentage`, `coupon_code`, `offer_price`, `valid_from`, `valid_to`, `status`, `date_created`, `date_modified`, `ip_address`) VALUES
(1, 16, 10.0000, '', 4000.0000, '2016-08-01', '2016-12-31', 'publish', '2016-08-30 14:12:36', '2016-08-31 18:12:12', '182.77.8.83'),
(2, 16, 5.0000, '', 2000.0000, '2016-08-01', '2016-12-31', 'publish', '2016-08-30 21:49:45', '2016-08-30 21:53:54', '112.196.181.216'),
(3, 10, 10.0000, NULL, 1500.0000, '2016-10-26', '2016-10-28', 'publish', '2016-10-25 01:35:57', NULL, '122.177.9.0');

-- --------------------------------------------------------

--
-- Table structure for table `mt_dishes`
--

CREATE TABLE `mt_dishes` (
  `dish_id` int(14) NOT NULL,
  `dish_name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_dishes`
--

INSERT INTO `mt_dishes` (`dish_id`, `dish_name`, `photo`, `status`, `date_created`, `date_modified`, `ip_address`) VALUES
(1, 'Chola Bhatura', '1470054805-Chola_bhatura.jpg', 'publish', '2016-08-01 18:03:28', '0000-00-00 00:00:00', '122.177.16.215');

-- --------------------------------------------------------

--
-- Table structure for table `mt_event`
--

CREATE TABLE `mt_event` (
  `event_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `venue` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `photo` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `fees` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `status` enum('draft','pending','publish') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mt_event`
--

INSERT INTO `mt_event` (`event_id`, `merchant_id`, `venue`, `start_date`, `end_date`, `photo`, `date_created`, `date_modified`, `event_name`, `fees`, `ip`, `status`) VALUES
(9, 15, 'allahabad', '2016-08-29 00:00:00', '2016-08-30 00:00:00', '1472038269-4.jpg', '2016-08-24 17:01:53', '0000-00-00 00:00:00', 'Test Event', 500, '122.177.246.111', 'publish'),
(10, 18, 'allahabad', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1472040395-4.jpg', '2016-08-24 17:36:47', '2016-09-09 15:18:45', 'Test Event', 0, '122.177.172.37', 'publish'),
(11, 24, 'Agra', '2016-09-13 00:00:00', '2016-09-17 00:00:00', '1473416607-9F5A3753_1600x770.jpg', '2016-09-09 16:23:02', '0000-00-00 00:00:00', 'Dj Night', 0, '122.177.172.37', 'publish'),
(12, 24, 'Delhi', '2016-09-28 00:00:00', '2016-09-30 00:00:00', '1473419070-9F5A3753_1600x770.jpg', '2016-09-09 16:34:40', '2016-09-09 16:35:09', 'Musical Night', 0, '122.177.172.37', 'publish'),
(13, 10, 'Lord of the Drinks', '2016-09-21 00:00:00', '2016-09-23 00:00:00', '1473503198-maxresdefault.jpg', '2016-09-10 17:27:02', '0000-00-00 00:00:00', 'Sham-E-Ghazal', 200, '182.69.204.186', 'publish');

-- --------------------------------------------------------

--
-- Table structure for table `mt_fax_broadcast`
--

CREATE TABLE `mt_fax_broadcast` (
  `id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `faxno` varchar(50) NOT NULL,
  `recipname` varchar(32) NOT NULL,
  `faxurl` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `jobid` varchar(255) NOT NULL,
  `api_raw_response` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_process` datetime NOT NULL,
  `date_postback` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_fax_package`
--

CREATE TABLE `mt_fax_package` (
  `fax_package_id` int(14) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` float(14,4) NOT NULL,
  `promo_price` float(14,4) NOT NULL,
  `fax_limit` int(14) NOT NULL,
  `sequence` int(14) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_fax_package_trans`
--

CREATE TABLE `mt_fax_package_trans` (
  `id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `fax_package_id` int(14) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `package_price` float(14,4) NOT NULL,
  `fax_limit` int(14) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'pending',
  `payment_reference` varchar(255) NOT NULL,
  `payment_gateway_response` text NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_follow`
--

CREATE TABLE `mt_follow` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `createdat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mt_follow`
--

INSERT INTO `mt_follow` (`id`, `uid`, `rid`, `status`, `createdat`) VALUES
(16, 1, 2, '1', '2016-08-16 16:21:48');

-- --------------------------------------------------------

--
-- Table structure for table `mt_global`
--

CREATE TABLE `mt_global` (
  `global_id` int(11) NOT NULL,
  `global_name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_global`
--

INSERT INTO `mt_global` (`global_id`, `global_name`, `photo`, `status`, `date_created`, `date_modified`, `ip_address`) VALUES
(1, 'Royal Stag', '1471698986-images.jpeg', 'publish', '2016-08-20 19:16:27', '0000-00-00 00:00:00', '122.177.88.34');

-- --------------------------------------------------------

--
-- Table structure for table `mt_group`
--

CREATE TABLE `mt_group` (
  `group_id` int(10) NOT NULL,
  `size_id` int(10) NOT NULL,
  `group_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sequence` int(10) NOT NULL,
  `merchant_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 NOT NULL,
  `item_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `group_name_trans` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mt_happyhours`
--

CREATE TABLE `mt_happyhours` (
  `happy_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `happyhours_name` varchar(255) NOT NULL,
  `sequence` int(14) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'published',
  `ip_address` varchar(50) NOT NULL,
  `happyhours_name_trans` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_happyhours`
--

INSERT INTO `mt_happyhours` (`happy_id`, `merchant_id`, `happyhours_name`, `sequence`, `date_created`, `date_modified`, `status`, `ip_address`, `happyhours_name_trans`) VALUES
(2, 0, 'IMFL', 0, '2016-09-01 19:48:02', '0000-00-00 00:00:00', 'publish', '171.48.35.109', ''),
(3, 0, 'BEER', 0, '2016-09-01 19:48:11', '0000-00-00 00:00:00', 'publish', '171.48.35.109', ''),
(4, 0, 'WINE', 0, '2016-09-01 19:48:21', '0000-00-00 00:00:00', 'publish', '171.48.35.109', ''),
(5, 0, 'FL', 0, '2016-09-01 19:48:32', '0000-00-00 00:00:00', 'publish', '171.48.35.109', ''),
(6, 0, 'SHOTS', 0, '2016-09-01 19:48:42', '0000-00-00 00:00:00', 'publish', '171.48.35.109', '');

-- --------------------------------------------------------

--
-- Table structure for table `mt_highlights`
--

CREATE TABLE `mt_highlights` (
  `h_id` int(10) NOT NULL,
  `h_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `h_sequence` int(10) NOT NULL DEFAULT '0',
  `h_date_created` datetime NOT NULL,
  `h_date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `h_ip_address` varchar(50) CHARACTER SET utf8 NOT NULL,
  `h_name_trans` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mt_highlights`
--

INSERT INTO `mt_highlights` (`h_id`, `h_name`, `h_sequence`, `h_date_created`, `h_date_modified`, `h_ip_address`, `h_name_trans`) VALUES
(4, 'Wheelchair Accessible', 0, '2016-09-02 17:01:42', '2016-09-02 17:57:44', '122.177.244.251', ''),
(5, 'Smoking Area', 0, '2016-09-02 20:23:09', '0000-00-00 00:00:00', '122.177.244.251', ''),
(6, 'Full Bar Available', 0, '2016-09-02 20:23:32', '2016-09-02 20:24:56', '122.177.244.251', ''),
(7, 'Nightlife', 0, '2016-09-02 20:25:19', '2016-09-02 20:26:01', '122.177.244.251', ''),
(8, 'Wifi', 0, '2016-09-02 20:26:09', '2016-09-02 20:26:12', '122.177.244.251', ''),
(9, 'Live Music', 0, '2016-09-02 20:26:44', '0000-00-00 00:00:00', '122.177.244.251', ''),
(10, 'Valet Parking Available', 0, '2016-09-02 20:27:00', '0000-00-00 00:00:00', '122.177.244.251', ''),
(11, 'Paid Parking', 0, '2016-09-02 20:27:22', '0000-00-00 00:00:00', '122.177.244.251', ''),
(12, 'Serves Non Veg', 0, '2016-09-02 20:28:09', '0000-00-00 00:00:00', '122.177.244.251', ''),
(13, 'Outdoor Seating', 0, '2016-09-02 20:28:34', '0000-00-00 00:00:00', '122.177.244.251', ''),
(14, 'Live Sports Screening', 0, '2016-09-02 20:28:50', '0000-00-00 00:00:00', '122.177.244.251', ''),
(15, 'Kid Friendly', 0, '2016-09-02 20:29:15', '0000-00-00 00:00:00', '122.177.244.251', ''),
(16, 'Buffet', 0, '2016-09-02 20:29:32', '2016-10-04 07:16:26', '182.77.4.231', '');

-- --------------------------------------------------------

--
-- Table structure for table `mt_ingredients`
--

CREATE TABLE `mt_ingredients` (
  `ingredients_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `ingredients_name` varchar(255) NOT NULL,
  `sequence` int(14) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'published',
  `ip_address` varchar(50) NOT NULL,
  `ingredients_name_trans` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_ingredients`
--

INSERT INTO `mt_ingredients` (`ingredients_id`, `merchant_id`, `ingredients_name`, `sequence`, `date_created`, `date_modified`, `status`, `ip_address`, `ingredients_name_trans`) VALUES
(1, 1, 'Black Salt', 0, '2016-08-12 16:53:47', '0000-00-00 00:00:00', 'publish', '122.177.199.132', '');

-- --------------------------------------------------------

--
-- Table structure for table `mt_item`
--

CREATE TABLE `mt_item` (
  `item_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `category` text NOT NULL,
  `price` text NOT NULL,
  `addon_item` text NOT NULL,
  `cooking_ref` text NOT NULL,
  `happyhours` varchar(255) NOT NULL,
  `happy_start_date` datetime NOT NULL,
  `happy_end_date` datetime NOT NULL,
  `discount` varchar(14) NOT NULL,
  `multi_option` text NOT NULL,
  `multi_option_value` text NOT NULL,
  `photo` varchar(255) NOT NULL,
  `sequence` int(14) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_address` varchar(50) NOT NULL,
  `ingredients` text NOT NULL,
  `spicydish` int(2) NOT NULL DEFAULT '1',
  `two_flavors` int(2) NOT NULL DEFAULT '0',
  `two_flavors_position` text,
  `require_addon` text,
  `dish` text NOT NULL,
  `item_name_trans` text,
  `item_description_trans` text,
  `non_taxable` int(1) NOT NULL DEFAULT '1',
  `not_available` int(1) NOT NULL DEFAULT '1',
  `gallery_photo` text NOT NULL,
  `points_earned` int(14) NOT NULL,
  `points_disabled` int(1) NOT NULL DEFAULT '1',
  `a_category` varchar(255) DEFAULT NULL,
  `subcategory` varchar(255) DEFAULT NULL,
  `order_by` varchar(255) DEFAULT NULL,
  `quantity` varchar(25) NOT NULL,
  `unit` varchar(20) NOT NULL,
  `starter_category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_item`
--

INSERT INTO `mt_item` (`item_id`, `merchant_id`, `item_name`, `item_description`, `status`, `category`, `price`, `addon_item`, `cooking_ref`, `happyhours`, `happy_start_date`, `happy_end_date`, `discount`, `multi_option`, `multi_option_value`, `photo`, `sequence`, `is_featured`, `date_created`, `date_modified`, `ip_address`, `ingredients`, `spicydish`, `two_flavors`, `two_flavors_position`, `require_addon`, `dish`, `item_name_trans`, `item_description_trans`, `non_taxable`, `not_available`, `gallery_photo`, `points_earned`, `points_disabled`, `a_category`, `subcategory`, `order_by`, `quantity`, `unit`, `starter_category`) VALUES
(24, 16, 'Black Dog', 'jdasd', 'publish', 'Alcohol', '["100"]', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 0, 0, '2016-10-06 05:47:12', '2016-10-07 05:39:08', '122.177.26.185', '', 1, 0, NULL, NULL, '', NULL, NULL, 1, 1, '', 0, 1, 'BEER', 'FL', 'by_glass', '400', 'ml', NULL),
(25, 16, 'virgin marry', 'vodka+Water+oil+ghee<br>', 'publish', 'Cocktail', '["100"]', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 0, 0, '2016-10-06 05:53:06', '2016-10-22 03:29:09', '122.177.241.26', 'vodka+Water+oil+ghee+bab ramdev bhes ka ghee', 1, 0, NULL, NULL, '', NULL, NULL, 1, 1, '', 0, 2, NULL, NULL, NULL, '300', 'ml', ''),
(27, 16, 'Blue lagoon', 'Blue Color<br>', 'publish', 'Cocktail', '["280"]', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 0, 0, '2016-10-06 06:40:04', '2016-10-22 05:07:39', '122.177.241.26', 'vodka+soda+blueberry', 1, 0, NULL, NULL, '', NULL, NULL, 1, 1, '', 0, 2, NULL, NULL, NULL, '400', 'ml', ''),
(28, 16, 'Paneer Tikka', 'Paneer', 'publish', 'Starter', '["200"]', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 0, 0, '2016-10-07 03:41:47', '2016-10-22 03:29:58', '122.177.241.26', '', 1, 0, NULL, NULL, '', NULL, NULL, 1, 1, '', 0, 2, NULL, NULL, NULL, '8', 'plate', ''),
(30, 16, 'Black Dog', '', 'publish', 'Alcohol', '["3000"]', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 0, 0, '2016-10-07 07:08:11', '2016-10-07 13:38:11', '122.177.26.185', '', 1, 0, NULL, NULL, '', NULL, NULL, 1, 1, '', 0, 1, 'RUM', 'FL', 'by_glass', '100', 'ml', NULL),
(33, 16, 'BABA Ramdev Cocktail', 'Ayurdevic Ingredients<br>', 'publish', 'Cocktail', '["600"]', '', '', '', '2016-11-16 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 0, 0, '2016-10-22 05:01:49', '2016-10-22 11:31:49', '122.177.241.26', 'Ashvgandha, Mulethi, Amla', 1, 0, NULL, NULL, '', NULL, NULL, 1, 1, '', 0, 2, NULL, NULL, NULL, '150', 'ml', ''),
(35, 16, '0', 'Fully Ayurvedic', 'publish', 'Alcohol', '["600"]', '', '', '', '2016-11-02 00:00:00', '2016-11-29 00:00:00', '', '', '', '', 0, 0, '2016-10-22 06:03:27', '2016-11-01 10:00:48', '150.107.8.209', '', 1, 0, NULL, NULL, '', NULL, NULL, 1, 1, '', 0, 1, '', '', 'by_glass', '200', 'ml', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_languages`
--

CREATE TABLE `mt_languages` (
  `lang_id` int(14) NOT NULL,
  `country_code` varchar(14) NOT NULL,
  `language_code` varchar(10) NOT NULL,
  `source_text` text NOT NULL,
  `is_assign` int(1) NOT NULL DEFAULT '2',
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_likes`
--

CREATE TABLE `mt_likes` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `createdat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mt_likes`
--

INSERT INTO `mt_likes` (`id`, `uid`, `rid`, `status`, `createdat`) VALUES
(2, 2, 4, '1', '0000-00-00 00:00:00'),
(31, 1, 4, '1', '2016-08-20 13:06:27'),
(40, 1, 1, '1', '2016-08-20 14:44:48'),
(43, 3, 4, '1', '2016-08-20 14:50:45');

-- --------------------------------------------------------

--
-- Table structure for table `mt_merchant`
--

CREATE TABLE `mt_merchant` (
  `merchant_id` int(14) NOT NULL,
  `restaurant_slug` varchar(255) NOT NULL,
  `restaurant_name` varchar(255) NOT NULL,
  `restaurant_phone` varchar(100) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_phone` varchar(100) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `country_code` varchar(3) NOT NULL,
  `street` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `post_code` varchar(100) NOT NULL,
  `cuisine` text,
  `highlights` text,
  `service` varchar(255) DEFAULT NULL,
  `free_delivery` int(1) NOT NULL DEFAULT '2',
  `delivery_estimation` varchar(100) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `activation_key` varchar(50) NOT NULL,
  `activation_token` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_activated` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `ip_address` varchar(50) NOT NULL,
  `package_id` int(14) NOT NULL DEFAULT '0',
  `package_price` float(14,5) NOT NULL DEFAULT '0.00000',
  `membership_expired` date DEFAULT NULL,
  `payment_steps` int(1) NOT NULL DEFAULT '1',
  `is_featured` int(1) NOT NULL DEFAULT '1',
  `is_ready` int(1) NOT NULL DEFAULT '1',
  `is_sponsored` int(2) NOT NULL DEFAULT '1',
  `sponsored_expiration` date DEFAULT NULL,
  `lost_password_code` varchar(50) DEFAULT NULL,
  `user_lang` int(14) DEFAULT NULL,
  `membership_purchase_date` datetime DEFAULT NULL,
  `sort_featured` int(14) DEFAULT NULL,
  `is_commission` int(1) NOT NULL DEFAULT '1',
  `website_address` varchar(255) NOT NULL DEFAULT 'www.flipq.com',
  `abn` varchar(255) NOT NULL,
  `session_token` varchar(255) DEFAULT NULL,
  `commision_type` varchar(50) NOT NULL DEFAULT 'percentage',
  `btype` varchar(255) NOT NULL,
  `rtype` varchar(100) DEFAULT NULL,
  `music_name` text,
  `facebook_page` varchar(255) DEFAULT NULL,
  `twitter_page` varchar(255) DEFAULT NULL,
  `google_page` varchar(255) DEFAULT NULL,
  `instagram_page` varchar(255) DEFAULT NULL,
  `percent_commision` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_merchant`
--

INSERT INTO `mt_merchant` (`merchant_id`, `restaurant_slug`, `restaurant_name`, `restaurant_phone`, `contact_name`, `contact_phone`, `contact_email`, `country_code`, `street`, `city`, `state`, `post_code`, `cuisine`, `highlights`, `service`, `free_delivery`, `delivery_estimation`, `username`, `password`, `activation_key`, `activation_token`, `status`, `date_created`, `date_modified`, `date_activated`, `last_login`, `ip_address`, `package_id`, `package_price`, `membership_expired`, `payment_steps`, `is_featured`, `is_ready`, `is_sponsored`, `sponsored_expiration`, `lost_password_code`, `user_lang`, `membership_purchase_date`, `sort_featured`, `is_commission`, `website_address`, `abn`, `session_token`, `commision_type`, `btype`, `rtype`, `music_name`, `facebook_page`, `twitter_page`, `google_page`, `instagram_page`, `percent_commision`) VALUES
(8, 'my-bar-square', 'My Bar Square', '01133105392', 'My Bar Square', '01133105392', 'mybarsquare@gmail.com', 'IN', 'E - 34 & 35, 1st Floor, Inner Circle, Connaught Place', 'Delhi', 'Delhi', '110001', '["3","16","21","22","23"]', '', '1', 2, '', 'mybarsquare', '25d55ad283aa400af464c76d713c07ad', '331102', 'ad57d2c130046e56a1cad0dd7eb61a0a', 'active', '2016-08-22 12:32:21', '2016-08-22 12:42:30', '0000-00-00 00:00:00', '2016-08-22 12:38:44', '122.177.66.52', 0, 0.00000, '2017-08-30', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '87954881679080b3ef88ab4be9b9de1f540eb644007', 'percentage', 'Restaurant', 'microbrewery_pub', '', '', '', '', '', ''),
(9, 'tamasha', 'Tamasha', '09999477661', 'Tamasha', '09999477661', 'tamasha@gmail.com', 'IN', '28A, Anand House, Kasturba Gandhi Marg, Connaught Place', 'Delhi', 'Delhi', '110001', '["24","25"]', '', '1', 2, '', 'tamasha', '3124d42a928ce84c2df44a03b6a25242', '413412', 'e587adda82a9c6daa8eb113e42716326', 'active', '2016-08-22 14:30:38', '2016-08-22 14:48:21', '0000-00-00 00:00:00', '2016-08-22 14:31:44', '122.177.66.52', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '95112666639080b3ef88ab4be9b9de1f540eb644007', 'percentage', 'Restaurant', 'microbrewery_pub', '', '', '', '', '', ''),
(10, 'lord-of-the-drinks', 'Lord of the Drinks', '09999827144', 'Lord of the Drinks', '09999827144', 'lotd@gmail.com', 'IN', 'G-72, Radial Road Number 2, Connaught Place, New Delhi', 'Delhi', 'Delhi', '110001', '["1","3"]', '["5","6"]', '', 2, '', 'lotd', '51377225077df6eb57a35aaa5a5362a3', '243112', 'aee7c604402279346e363f0dbcd7a93c', 'active', '2016-08-22 14:56:29', '2016-09-10 16:40:38', '0000-00-00 00:00:00', '2016-10-25 12:17:24', '182.69.204.186', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '429610742408aa90d259c45c101fa34126e99aead3c', 'percentage', 'Restaurant', 'pub/bar', '', 'www.facebook.com/lords-of-the-drinks', '', '', '', ''),
(11, 'the-junkyard-cafe', 'The Junkyard Cafe', '09599947643', 'The Junkyard Cafe', '09599947643', 'thejunkyardcafe@gmail.com', 'IN', '91, 2nd Floor, N Block, Connaught Place,', 'New Delhi', 'Delhi', '110001', '["1","8","16","22","26"]', '', '1', 2, '', 'thejunkyardcafe', '25d55ad283aa400af464c76d713c07ad', '020124', '083973a5a03a7a01f315d67dbebb39d6', 'active', '2016-08-22 15:57:17', '2016-08-22 14:31:24', '0000-00-00 00:00:00', '2016-08-26 20:58:37', '122.177.66.52', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '87472332959273f6fa2c962b051200cc13d3947597f', 'percentage', 'Restaurant', 'cafe', '', '', '', '', '', ''),
(12, 'teddy-boy', 'Teddy Boy', '9999082777', 'Teddy Boy', '9999082777', 'teddyboy@gmail.com', 'IN', 'N- 86 Cannaught Place Outer Circle, New Delhi,', 'Delhi', 'Delhi', '110001', '["1","3","6","8","22","23","26"]', '', '1', 2, '', 'teddyboy', '25d55ad283aa400af464c76d713c07ad', '322010', '926f71587de77265ca85a3c69938a08a', 'active', '2016-08-22 16:11:24', '2016-08-22 16:15:10', '0000-00-00 00:00:00', '2016-08-22 16:13:09', '122.177.66.52', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '98325413792080b3ef88ab4be9b9de1f540eb644007', 'percentage', 'Restaurant', 'lounge', '', '', '', '', '', ''),
(13, 'open-house-cafe', 'Open House Cafe', '09999611332', 'Open House Cafe', '09999611332', 'ohc@gmail.com', 'IN', 'C 37, C Block, Connaught Place, New Delhi', 'Delhi', 'Delhi', '110001', '["1","16","23","24"]', '', '1', 2, '', 'openhouse', '25d55ad283aa400af464c76d713c07ad', '423001', '581c7b71f87a9e2b3d6b40ca353caf33', 'active', '2016-08-22 16:20:30', '2016-08-22 14:52:54', '0000-00-00 00:00:00', '2016-08-22 16:22:04', '122.177.66.52', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '00753011680080b3ef88ab4be9b9de1f540eb644007', 'percentage', 'Restaurant', 'sports_bar', '', '', '', '', '', ''),
(14, '38-barracks', '38 Barracks', '9717304097', '38 Barracks', '9717304097', '38barracks@gmail.com', 'IN', '38, Block M, Outer Circle, Connaught Place', 'Delhi', 'Delhi', '110001', '["1","3","8","22","23"]', '["6","7","9"]', '', 2, '', '38barracks', '25d55ad283aa400af464c76d713c07ad', '331341', 'd102f2af362f38c5180ff7285ee12ee2', 'active', '2016-08-22 16:31:23', '2016-09-15 10:53:55', '0000-00-00 00:00:00', '2016-09-15 12:23:12', '122.177.73.34', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '2466525002486071247828b40ccea35c67a3ee11184', 'percentage', 'Restaurant', 'casual_dining', '', '', '', '', '', ''),
(15, 'qba', 'QBA', '01145173333', 'QBA', '01145173333', 'qba@gmail.com', 'IN', 'E-42 & 43, Inner Circle, Connaught Place, New Delhi,', 'Delhi', 'Delhi', '110001', '["14","22","26"]', '', '1', 2, '', 'qba', '25d55ad283aa400af464c76d713c07ad', '224200', 'a83f34b2c7363ded3962bfdbc583f341', 'active', '2016-08-22 16:39:07', '2016-08-22 15:11:47', '0000-00-00 00:00:00', '2016-09-09 16:47:13', '122.177.66.52', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '98872925676ace9a0c02e4d77836cc74cb378c57c4c', 'percentage', 'Restaurant', 'fine_dining', '', '', '', '', '', ''),
(16, 'circus', 'Circus', '09899407151', 'Circus', '09899407151', 'circus@gmail.com', 'IN', 'D-14 , 3rd Floor,, South Extension II, New Delhi', 'Delhi', 'Delhi', '110049', '["1","2","3","4","6","8","10","13"]', '["5","6","8","12","13","16"]', '', 2, '', 'Circus', '25d55ad283aa400af464c76d713c07ad', '322313', '1801afe19360361470c6b755baddc28a', 'active', '2016-08-22 16:50:57', '2016-09-13 16:30:18', '0000-00-00 00:00:00', '2016-11-01 02:29:11', '122.177.77.242', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, 'www.sggfsgasg.com', '', '27176850068aa53ab9bfc8306956371dc00ccccd42b', 'percentage', 'Restaurant', 'pub/bar', '', 'abc.com', '', '', '', ''),
(18, 'altec-electronic-dj', 'Altec Electronic DJ', '011-23863044', 'Altec Electronic DJ', '011-23863044', 'altec@gmail.com', 'IN', '1683/1, J H Building, Bhagirath Palace, Chandni Chowk', 'New Delhi', 'Delhi', '110006', '', '', '', 2, '', 'altec', '25d55ad283aa400af464c76d713c07ad', '214002', '11e77e3db472064b7e7cfc50f417a07b', 'active', '2016-08-23 12:22:09', '2016-09-10 12:20:04', '0000-00-00 00:00:00', '2016-10-22 01:04:13', '122.177.88.89', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '1258375046663349cc1dc9a63569a3f16b0400b7d68', 'percentage', 'DJ', 'CLASSICAL / ART MUSIC', '["8","12","15"]', '', '', '', '', ''),
(19, 'swing', 'Swing', ' 011-45532332, 9811071822 ', 'Swing DJ', ' 011-45532332, 9811071822 ', 'swing@gmail.com', 'IN', 'C-394, Vikaspuri, New Delhi', 'Delhi', 'Delhi', '110018', '["23"]', '', '1', 2, '', 'swing', '25d55ad283aa400af464c76d713c07ad', '003331', 'b0cbd8da4202bc18b1705101eeff1f08', 'active', '2016-08-24 17:10:05', '2016-08-24 16:06:25', '0000-00-00 00:00:00', '2016-10-22 01:11:01', '122.177.246.111', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '6392558400163349cc1dc9a63569a3f16b0400b7d68', 'percentage', 'DJ', 'CLASSICAL / ART MUSIC', '', '', '', '', '', ''),
(20, 'musicam-dj', 'Musicam DJ ', '011-43010803, 9810264991, 9818045551 ', 'Musicam DJ ', '011-43010803, 9810264991, 9818045551 ', 'musicam@gmail.com', 'IN', 'D6/1, Second Floor, Lal Quater, Krishna Nagar,', 'Delhi', 'Delhi', '110051', '', '', '', 2, '', 'musicam', '25d55ad283aa400af464c76d713c07ad', '330032', '42622e71a97ae85bc42f026b3570a459', 'active', '2016-08-24 17:17:45', '2016-08-26 19:31:09', '0000-00-00 00:00:00', '2016-10-22 01:09:37', '122.177.166.76', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '7225346640263349cc1dc9a63569a3f16b0400b7d68', 'percentage', 'DJ', 'ROCK', '["12"]', '', '', '', '', ''),
(21, 'real-vision', 'Real Vision ', '9716187903, 9312417903, 9716017903, 9312417519 ', 'Real Vision', '9716187903, 9312417903, 9716017903, 9312417519', 'real@gmail.com', 'IN', 'House No. 214, Gali No. 1, Sonar Wali Gali, Behind Reliance Petrol Pump, Meerut Road, Harbansh Nagar', 'Ghaziabad', 'Uttar Pradesh', '201001', '["17"]', '', '1', 2, '', 'realvision', '25d55ad283aa400af464c76d713c07ad', '220141', '6478e8cfdc6a44365d7a7fb83f8b4d4d', 'active', '2016-08-24 17:20:48', '2016-08-24 16:05:32', '0000-00-00 00:00:00', '2016-10-22 01:10:17', '122.177.246.111', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '3628684133963349cc1dc9a63569a3f16b0400b7d68', 'percentage', 'DJ', 'ROCK', '', '', '', '', '', ''),
(22, 'handa-dj-party', 'Handa DJ Party ', ' 9891520622, 9211237010 ', 'Mr Deepak Handa', ' 9891520622, 9211237010 ', 'handa@gmail.com', 'IN', 'C/138, Delhi G.P.O., Tagore Garden Extension', 'New Delhi', 'Delhi', '110027', '["13"]', '', '1', 2, '', 'handa', '25d55ad283aa400af464c76d713c07ad', '431220', '7e43462e3f58c1bb2b8a1f36113cda30', 'active', '2016-08-24 17:23:48', '2016-08-24 16:04:41', '0000-00-00 00:00:00', '2016-10-22 01:08:40', '122.177.246.111', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '9079777478463349cc1dc9a63569a3f16b0400b7d68', 'percentage', 'DJ', 'ROCK', '', '', '', '', '', ''),
(23, 'dj-beats', 'DJ BEATS', ' 9899697873, 9811273615', 'DJ BEATS', ' 9899697873, 9811273615', 'djbeats@gmail.com', 'IN', 'B-100, Nr Holi Child School, Tagore Garden', 'New Delhi', 'Delhi', '110027', 'null', '', '', 2, '', 'djbeats', '25d55ad283aa400af464c76d713c07ad', '111220', 'd0db58875964d4020bb728e2d011e631', 'active', '2016-08-24 17:26:21', '2016-09-13 18:17:15', '0000-00-00 00:00:00', '2016-10-22 01:07:55', '122.177.77.242', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '5010017766863349cc1dc9a63569a3f16b0400b7d68', 'percentage', 'DJ', 'PROGRESSIVE HOUSE', '', '', '', '', '', ''),
(24, 'musical-minds', 'Musical Minds', '011-26876123, 9990221333, 9818565498 ', 'Musical Minds', '011-26876123, 9990221333, 9818565498 ', 'musicalminds@gmail.com', 'IN', '2165, Pocket-2, Sector-D, Vasant Kunj', 'New Delhi', 'Delhi', '110070', 'null', '', '', 2, '', 'musicalminds', '25d55ad283aa400af464c76d713c07ad', '431404', '0c0dcd9976cb2cc0e14127ed6a7f827c', 'active', '2016-08-24 17:28:59', '2016-09-15 09:57:22', '0000-00-00 00:00:00', '2016-10-22 01:06:12', '150.107.8.185', 0, 0.00000, '0000-00-00', 3, 2, 2, 1, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 2, '', '', '8163426421163349cc1dc9a63569a3f16b0400b7d68', 'percentage', 'DJ', 'PROGRESSIVE HOUSE', '["8","12","13","16","18","1","3","4"]', '', '', '', '', ''),
(26, 'monkey-bar', 'Monkey Bar', ' 011 33106238', 'Monkey Bar', ' 011 33106238', 'monkeybar@gmail.com', 'IN', 'Plot 11, Upper Ground Floor, LSC, Pocket C-6 & 7, Vasant Kunj', ' New Delhi', 'Delhi', '110004', '["American","Deli","Indian","Mediterranean"]', '["4","5","8","12","16"]', '', 2, NULL, 'monkey', '25d55ad283aa400af464c76d713c07ad', '133130', 'af39c7cf26358c571987259ecfd380c3', 'active', '2016-10-19 04:14:55', '2016-10-19 02:47:03', NULL, NULL, '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '61697700145bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'pub/bar', '', '', '', '', '', ''),
(27, 'bottles-amp-barrels', 'Bottles & Barrels', ' 011 30806288', 'B & B', ' 011 30806288', 'bandb@gmail.com', 'IN', '2nd Floor, Star Tower, Exit 8', 'New Delhi', 'Delhi', '110019', '["American","Indian","Sandwiches","Mexican"]', '["9","10","15","16"]', '', 2, NULL, 'bb', '25d55ad283aa400af464c76d713c07ad', '101030', '50ce6511946b68695a591aa8f9b176ee', 'active', '2016-10-19 04:26:17', '2016-10-19 02:59:47', NULL, '2016-10-22 01:07:32', '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '5896733779863349cc1dc9a63569a3f16b0400b7d68', 'percentage', 'Restaurant', 'microbrewery_pub', '', '', '', '', '', ''),
(28, 'the-potbelly-rooftop-cafe', 'The Potbelly Rooftop Cafe', '011 41612048', 'Potbelly', '011 41612048', 'potbelly@gmail.com', 'IN', '116-C, 4th Floor, Shahpur Jat', 'New Delhi', 'Delhi', '110019', '', '', '', 2, NULL, 'potbelly', '25d55ad283aa400af464c76d713c07ad', '134200', 'a21ff61e7819404b91c2d8fea84a05de', 'active', '2016-10-19 04:39:17', '2016-10-19 04:25:42', NULL, NULL, '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '29612818220bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'cafe', '', '', '', '', '', ''),
(29, 'the-flashback', 'The Flashback', '011 33105309', 'Flashback', '011 33105309', 'flashback@gmail.com', 'IN', '53/5, Opposite Andhra Bank, Old Rajinder Nagar, Rajinder Nagar, New Delhi', 'Delhi', 'Delhi', '110096', '', '', '', 2, NULL, 'flashback', '25d55ad283aa400af464c76d713c07ad', '124102', 'd9f4a03308a42c526c6e248a1377ee7f', 'active', '2016-10-19 04:57:42', '2016-10-19 04:20:45', NULL, NULL, '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '63746961462bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'cafe', '', '', '', '', '', ''),
(30, 'odean-social', 'Odean Social', '+91 7838090132', 'Odean', '+91 7838090132', 'odean@gmail.com', 'IN', '1st Floor, 23, Odeon Building, Radial 5, D Block, Connaught Place', 'New Delhi', 'Delhi', '110001', '', '', '', 2, NULL, 'odean', '25d55ad283aa400af464c76d713c07ad', '421003', 'e5f93026ce247cbb78f5c54ce5e8cd46', 'active', '2016-10-19 05:00:38', '2016-10-19 04:17:16', NULL, NULL, '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '98508414190bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'lounge', '', '', '', '', '', ''),
(31, 'hauj-khas-social', 'Hauj Khas Social', '+91 7838652814', 'Hauj Khas', '+91 7838652814', 'haujkhas@gmail.com', 'IN', '9-A & 12, Hauz Khas Village', 'New Delhi', 'Delhi', '110011', '', '', '', 2, NULL, 'haujkhas', '25d55ad283aa400af464c76d713c07ad', '320043', 'b24911b5b842319d10b14e9d20bb82bc', 'active', '2016-10-19 05:03:07', '2016-10-19 04:16:22', NULL, NULL, '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '51379188984bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'lounge', '', '', '', '', '', ''),
(32, 'underdoggs-sports-bar-amp-grill', ' Underdoggs Sports Bar & Grill', '+91 9818700006', 'underdoggs', '+91 9818700006', 'underdoggs@gmail.com', 'IN', 'T-314, 3rd Floor, Ambience Mall, Vasant Kunj', 'New Delhi', 'Delhi', '110001', '', '', '', 2, NULL, 'underdoggs', '25d55ad283aa400af464c76d713c07ad', '441213', 'e41fbe1d6920a8636b928c4b61a4c2d2', 'active', '2016-10-19 05:05:46', '2016-10-19 04:12:42', NULL, NULL, '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '00441441005bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'sports_bar', '', '', '', '', '', ''),
(33, 'the-sports-club', 'The Sports Club', ' +91 9821897544', 'sports club', ' +91 9821897544', 'sportsclub@gmail.com', 'IN', 'P-15, 1st Floor, Outer Circle, Connaught Place', 'New Delhi', 'Delhi', '110001', '', '', '', 2, NULL, 'sportsclub', '25d55ad283aa400af464c76d713c07ad', '231401', 'f03087c2d8e4803e692eeabc52f99656', 'active', '2016-10-19 05:08:32', '2016-10-19 04:08:37', NULL, NULL, '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '27306268615bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'sports_bar', '', '', '', '', '', ''),
(34, 'gulati', 'Gulati', '011 33105051', 'gulati', '011 33105051', 'gulati@gmail.com', 'IN', '6, Pandara Road Market', 'New Delhi', 'Delhi', '110001', '', '', '', 2, NULL, 'gulati', '25d55ad283aa400af464c76d713c07ad', '112303', 'ca134e6fc1154ac6655a5fe191d23126', 'active', '2016-10-19 05:11:08', '2016-10-19 04:05:55', NULL, NULL, '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '79016552111bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'casual_dining', '', '', '', '', '', ''),
(35, 'kebab-galli', 'Kebab Galli', '011 33105881', 'kebab Galli', '011 33105881', 'kebabgalli@gmail.com', 'IN', '011 33105881', 'New Delhi', 'Delhi', '110028', '', '', '', 2, NULL, 'kebab', '25d55ad283aa400af464c76d713c07ad', '400032', 'dcd2d3a00c9f19e6b416f173dcd96b8c', 'active', '2016-10-19 05:13:23', '2016-10-19 04:04:36', NULL, NULL, '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '03675255891bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'casual_dining', '', '', '', '', '', ''),
(36, 'lodi-the-garden-restaurant', 'Lodi - The Garden Restaurant ', ' 011 33105163', 'Lodi', ' 011 33105163', 'lodi@gmail.com', 'IN', 'Opposite Mausam Bhawan, Near Gate 1, Lodhi Road', 'New Delhi', 'Delhi', '110001', '["Indian","Mediterranean","Sushi","Greek","Chinese","Healthy","Korean","Pizza","Vegetarian"]', '["4","5","6","7","10","12","13","14","15"]', '', 2, NULL, 'lodi', '25d55ad283aa400af464c76d713c07ad', '110302', '2a9c1642ea6d230cc0b6edf790c9114e', 'active', '2016-10-19 05:16:23', '2016-10-19 04:02:09', NULL, NULL, '171.48.37.58', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '25405116349bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'fine_dining', '', '', '', '', '', ''),
(37, 'masala-library', 'Masala Library ', '011 69400005', 'Masala', '011 69400005', 'masala@gmail.com', 'IN', '21 A, Near Le Meridian, Janpath', 'New Delhi', 'Delhi', '110054', '["Indian","Mediterranean","Barbeque"]', '["6","7","8","9"]', '', 2, NULL, 'masala', '25d55ad283aa400af464c76d713c07ad', '323112', '8d6c8de7b531cd6e21d96c19e362d76e', 'active', '2016-10-19 05:18:37', '2016-10-22 12:57:23', NULL, NULL, '122.177.241.26', 0, 0.00000, '2016-08-05', 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '63568353623bcc4c0cd7cec5d715c4567bed3b2a4c8', 'percentage', 'Restaurant', 'fine_dining', '', '', '', '', '', ''),
(40, 'dj-raunak', 'DJ Raunak', '8987657446', 'Raunak', '', 'djraunak@gmail.com', 'IN', 'B-3, Janakpuri', 'Delhi', 'Delhi', '110018', NULL, NULL, NULL, 2, NULL, 'raunak', '25d55ad283aa400af464c76d713c07ad', '204424', '9df7a976652df22cab21842c6d7ad77b', 'active', '2016-10-23 03:20:35', '2016-10-23 03:20:35', NULL, NULL, '47.30.125.118', 0, 0.00000, NULL, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 2, 'www.flipq.com', '', '31422906792d38f299ee1f168fdf49514bb44bc83bb', 'percentage', 'dj', 'PROGRESSIVE HOUSE', NULL, NULL, NULL, NULL, NULL, ''),
(41, 'dj-chetas', 'DJ Chetas', '7868765454', 'Chetas', '7868765454', 'djchetas@gmail.com', 'IN', '116-C, 4th Floor, Shahpur Jat', 'New Delhi', 'Delhi', '110029', '', '', '', 2, NULL, 'chetas', '25d55ad283aa400af464c76d713c07ad', '044211', '70c98cf30d0a2db3b3661c3295b7a229', 'active', '2016-10-23 01:36:50', '2016-10-23 12:53:55', NULL, NULL, '47.30.112.207', 0, 0.00000, NULL, 3, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, 2, '', '', '02522465688f9ea91a26f346a78d88b66128879fa52', 'percentage', 'dj', 'CLASSICAL / ART MUSIC', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mt_merchant_cc`
--

CREATE TABLE `mt_merchant_cc` (
  `mt_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `card_name` varchar(255) NOT NULL,
  `credit_card_number` varchar(20) NOT NULL,
  `expiration_month` varchar(5) NOT NULL,
  `expiration_yr` varchar(5) NOT NULL,
  `cvv` varchar(20) NOT NULL,
  `billing_address` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_merchant_cc`
--

INSERT INTO `mt_merchant_cc` (`mt_id`, `merchant_id`, `card_name`, `credit_card_number`, `expiration_month`, `expiration_yr`, `cvv`, `billing_address`, `date_created`, `ip_address`) VALUES
(1, 2, 'ICICI  Bank', '1234567890111', '01', '2017', '090', '508/5 govind puri', '0000-00-00 00:00:00', '122.177.16.215');

-- --------------------------------------------------------

--
-- Table structure for table `mt_merchant_user`
--

CREATE TABLE `mt_merchant_user` (
  `merchant_user_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_access` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'active',
  `last_login` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `session_token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_mobile_broadcast`
--

CREATE TABLE `mt_mobile_broadcast` (
  `broadcast_id` int(14) NOT NULL,
  `push_title` varchar(255) NOT NULL,
  `push_message` varchar(255) NOT NULL,
  `device_platform` int(14) NOT NULL DEFAULT '1',
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_mobile_push_logs`
--

CREATE TABLE `mt_mobile_push_logs` (
  `id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `device_platform` varchar(100) NOT NULL,
  `device_id` text NOT NULL,
  `push_title` varchar(255) NOT NULL,
  `push_message` varchar(255) NOT NULL,
  `push_type` varchar(100) NOT NULL DEFAULT 'order',
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `json_response` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_process` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `broadcast_id` int(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_mobile_registered`
--

CREATE TABLE `mt_mobile_registered` (
  `id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `device_platform` varchar(255) NOT NULL DEFAULT 'Android',
  `device_id` text NOT NULL,
  `enabled_push` int(1) NOT NULL DEFAULT '1',
  `country_code_set` varchar(3) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `mt_mobile_registered_view`
-- (See below for the actual view)
--
CREATE TABLE `mt_mobile_registered_view` (
`id` int(14)
,`client_id` int(14)
,`device_platform` varchar(255)
,`device_id` text
,`enabled_push` int(1)
,`country_code_set` varchar(3)
,`date_created` datetime
,`date_modified` datetime
,`ip_address` varchar(50)
,`status` varchar(255)
,`client_name` varchar(511)
);

-- --------------------------------------------------------

--
-- Table structure for table `mt_mobile_temp_email`
--

CREATE TABLE `mt_mobile_temp_email` (
  `id` int(14) NOT NULL,
  `order_id` int(14) NOT NULL,
  `client_email` varchar(255) NOT NULL,
  `receipt_sender` varchar(255) NOT NULL,
  `receipt_subject` varchar(255) NOT NULL,
  `tpl` text NOT NULL,
  `email_type` varchar(100) NOT NULL DEFAULT 'client',
  `merchant_id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `gateway` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_music_type`
--

CREATE TABLE `mt_music_type` (
  `music_id` int(11) NOT NULL,
  `music_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sequence` int(11) DEFAULT NULL,
  `music_name_trans` text CHARACTER SET utf8,
  `ip_address` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mt_music_type`
--

INSERT INTO `mt_music_type` (`music_id`, `music_name`, `date_created`, `date_modified`, `sequence`, `music_name_trans`, `ip_address`) VALUES
(1, 'CLASSICAL / ART MUSIC', '2016-08-24 18:00:00', '0000-00-00 00:00:00', 1, '', ''),
(2, 'RHYTHM AND BLUES', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, '', ''),
(3, 'ROCK', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, '', ''),
(4, 'COUNTRY MUSIC ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, '', ''),
(5, 'ELECTRONIC MUSIC', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, '', ''),
(8, 'ELECTRONICA', '2016-08-24 20:58:09', '2016-08-24 20:58:35', 0, '', '122.177.246.111'),
(10, 'HIP HOP / RAP', '2016-08-24 20:59:24', '2016-08-24 20:59:37', 0, '', '122.177.246.111'),
(12, 'HIGH-ENERGY', '2016-08-24 21:00:03', '0000-00-00 00:00:00', 0, '', '122.177.246.111'),
(13, 'DREAM HOUSE', '2016-08-24 21:00:18', '2016-08-24 21:00:31', 0, '', '122.177.246.111'),
(14, 'REGAGE', '2016-08-24 21:00:46', '2016-08-25 17:22:45', 0, '', '182.77.3.127'),
(15, 'ACID JAZZ', '2016-08-24 21:01:19', '2016-08-24 21:01:32', 0, '', '122.177.246.111'),
(16, 'PROGRESSIVE HOUSE', '2016-08-24 21:01:44', '2016-08-24 21:02:09', 0, '', '122.177.246.111'),
(17, 'ENERGY ALTERNATIVE', '2016-08-24 21:02:23', '2016-08-24 21:03:55', 0, '', '122.177.246.111'),
(18, 'AMBIENT', '2016-08-24 21:04:07', '0000-00-00 00:00:00', 0, '', '122.177.246.111'),
(19, 'DUB', '2016-08-25 17:22:07', '2016-08-25 17:22:13', 0, '', '182.77.3.127');

-- --------------------------------------------------------

--
-- Table structure for table `mt_newsletter`
--

CREATE TABLE `mt_newsletter` (
  `id` int(14) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_newsletter`
--

INSERT INTO `mt_newsletter` (`id`, `email_address`, `date_created`, `ip_address`) VALUES
(1, 'jai@aa.com', '2016-08-16 14:57:11', '182.69.226.136');

-- --------------------------------------------------------

--
-- Table structure for table `mt_offers`
--

CREATE TABLE `mt_offers` (
  `offers_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `offer_type` enum('offline','online','offline & online') NOT NULL,
  `offer_percentage` float(14,4) NOT NULL,
  `offer_price` float(14,4) NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_offers`
--

INSERT INTO `mt_offers` (`offers_id`, `merchant_id`, `offer_type`, `offer_percentage`, `offer_price`, `valid_from`, `valid_to`, `status`, `date_created`, `date_modified`, `ip_address`) VALUES
(1, 16, 'offline & online', 10.0000, 120.0000, '2016-08-01', '2016-08-31', 'publish', '2016-08-30 14:12:36', '2016-08-30 19:53:49', '112.196.181.216');

-- --------------------------------------------------------

--
-- Table structure for table `mt_option`
--

CREATE TABLE `mt_option` (
  `id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `option_name` varchar(255) NOT NULL,
  `option_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_option`
--

INSERT INTO `mt_option` (`id`, `merchant_id`, `option_name`, `option_value`) VALUES
(1, 0, 'website_title', 'FLIPQ'),
(2, 0, 'admin_country_set', 'IN'),
(3, 0, 'website_address', '508/5 govind puri'),
(4, 0, 'website_contact_phone', '08285546609'),
(5, 0, 'website_contact_email', 'ajayj@brownhubsolution.com'),
(6, 0, 'admin_currency_set', 'INR'),
(7, 0, 'website_disbaled_auto_cart', ''),
(8, 0, 'website_enabled_mobile_verification', ''),
(9, 0, 'website_date_picker_format', 'mm-dd-yy'),
(10, 0, 'website_time_picker_format', '12'),
(11, 0, 'website_date_format', 'M d,Y'),
(12, 0, 'website_time_format', 'G:i:s'),
(13, 0, 'merchant_sigup_status', 'pending'),
(14, 0, 'merchant_email_verification', ''),
(15, 0, 'merchant_payment_enabled', ''),
(16, 0, 'admin_enabled_paypal', ''),
(17, 0, 'admin_enabled_card', ''),
(18, 0, 'home_search_text', 'Find restaurants near you'),
(19, 0, 'home_search_subtext', ''),
(20, 0, 'home_search_mode', 'address'),
(21, 0, 'website_logo', '1470989294-logo.png'),
(22, 0, 'admin_decimal_place', '0'),
(23, 0, 'admin_use_separators', ''),
(24, 0, 'google_auto_address', ''),
(25, 0, 'home_search_radius', '10'),
(26, 0, 'home_search_unit_type', 'km'),
(27, 0, 'google_default_country', 'yes'),
(28, 0, 'enabled_advance_search', 'yes'),
(29, 0, 'disabled_share_location', ''),
(30, 0, 'enabled_search_map', 'yes'),
(31, 0, 'admin_currency_position', 'left'),
(32, 0, 'merchant_default_country', 'IN'),
(33, 0, 'merchant_specific_country', '[\\"IN\\"]'),
(34, 0, 'map_marker', ''),
(35, 0, 'global_admin_sender_email', 'ajayj@brownhubsolution.com'),
(36, 0, 'merchant_disabled_registration', ''),
(37, 0, 'disabled_subscription', ''),
(38, 0, 'disabled_featured_merchant', ''),
(39, 0, 'merchant_days_can_edit_status', ''),
(40, 0, 'disabled_website_ordering', ''),
(41, 0, 'admin_activated_menu', ''),
(42, 0, 'website_hide_foodprice', ''),
(43, 0, 'disabled_cc_management', 'yes'),
(44, 0, 'merchant_reg_abn', ''),
(45, 0, 'spicydish', ''),
(46, 0, 'website_timezone', 'Asia/Kolkata'),
(47, 0, 'website_admin_mutiple_login', 'yes'),
(48, 0, 'website_merchant_mutiple_login', 'yes'),
(49, 0, 'website_disabled_guest_checkout', 'yes'),
(50, 0, 'website_reviews_actual_purchase', ''),
(51, 0, 'website_terms_merchant', 'yes'),
(52, 0, 'website_terms_merchant_url', ''),
(53, 0, 'website_terms_customer', ''),
(54, 0, 'website_terms_customer_url', ''),
(55, 0, 'admin_thousand_separator', ''),
(56, 0, 'admin_decimal_separator', ''),
(57, 0, 'website_disabled_login_popup', ''),
(58, 0, 'merchant_can_edit_reviews', ''),
(59, 0, 'website_enabled_rcpt', '2'),
(60, 0, 'website_receipt_logo', '1469621187-logo.png'),
(61, 0, 'disabled_cart_sticky', ''),
(62, 0, 'search_result_bydistance', '2'),
(63, 0, 'google_geo_api_key', 'AIzaSyCRSQeWQ67e0Vd4jVv0xHPvsTJOCsv7x9A'),
(64, 0, 'website_enabled_map_address', '2'),
(65, 0, 'client_custom_field_name1', ''),
(66, 0, 'client_custom_field_name2', ''),
(67, 0, 'merchant_days_can_edit_status_basedon', '1'),
(68, 0, 'merchant_status_disabled', ''),
(69, 0, 'view_map_disabled', ''),
(70, 0, 'view_map_default_zoom', ''),
(71, 0, 'view_map_default_zoom_s', ''),
(72, 0, 'receipt_default_subject', ''),
(73, 0, 'merchant_tbl_book_disabled', ''),
(74, 0, 'merchant_changeorder_sms', ''),
(75, 0, 'customer_ask_address', ''),
(76, 0, 'captcha_site_key', '6LcglSgTAAAAAPfzdw7jgp8mzCxUxd2HRI8xq_UJ'),
(77, 0, 'captcha_secret', '6LcglSgTAAAAAPjiUZXcBm6ohz8i2Z6_4-THwHtl'),
(78, 0, 'captcha_lang', 'en'),
(79, 0, 'captcha_customer_signup', '2'),
(80, 0, 'captcha_merchant_signup', '2'),
(81, 0, 'captcha_customer_login', ''),
(82, 0, 'captcha_merchant_login', ''),
(83, 0, 'captcha_admin_login', ''),
(84, 0, 'captcha_order', ''),
(85, 0, 'blocked_email_add', ''),
(86, 0, 'blocked_mobile', ''),
(87, 0, 'admin_zipcode_searchtype', '3'),
(88, 0, 'mobilelogo', '1470989284-logo.png'),
(89, 0, 'theme_enabled_email_verification', ''),
(90, 0, 'google_distance_method', 'straight_line'),
(91, 0, 'google_use_curl', '2'),
(92, 0, 'admin_menu_allowed_merchant', '2'),
(104, 0, 'social_flag', ''),
(105, 0, 'fb_flag', ''),
(106, 0, 'fb_app_id', '838090063001252'),
(107, 0, 'fb_app_secret', '04afe04e6758f68ff42778bb792acf9a'),
(108, 0, 'admin_fb_page', 'https://www.facebook.com/coupontokri/'),
(109, 0, 'admin_twitter_page', ''),
(110, 0, 'admin_google_page', ''),
(111, 0, 'admin_merchant_share', ''),
(112, 0, 'google_client_id', ''),
(113, 0, 'google_client_secret', ''),
(114, 0, 'google_client_redirect_ulr', 'http://ec2-54-254-214-206.ap-southeast-1.compute.amazonaws.com/store/GoogleLogin'),
(115, 0, 'google_login_enabled', ''),
(116, 0, 'default_share_text', '{merchant-name}'),
(117, 0, 'admin_intagram_page', ''),
(118, 0, 'admin_youtube_url', ''),
(119, 0, 'contact_content', ''),
(120, 0, 'contact_map', '1'),
(121, 0, 'map_latitude', ''),
(122, 0, 'map_longitude', ''),
(123, 0, 'contact_email_receiver', 'jainmca4444@gmail.com'),
(124, 0, 'contact_field', '[\\"name\\",\\"email\\",\\"phone\\",\\"message\\"]'),
(125, 0, 'smtp_host', ''),
(126, 0, 'smtp_port', ''),
(127, 0, 'smtp_username', ''),
(128, 0, 'smtp_password', ''),
(129, 0, 'email_provider', 'phpmail'),
(130, 0, 'mandrill_api_key', ''),
(131, 0, 'wd_paypal_minimum', ''),
(132, 0, 'wd_bank_minimum', '100'),
(133, 0, 'wd_days_process', '15'),
(134, 0, 'wd_paypal', ''),
(135, 0, 'wd_paypal_mode', ''),
(136, 0, 'wd_paypal_mode_user', ''),
(137, 0, 'wd_paypal_mode_pass', ''),
(138, 0, 'wd_paypal_mode_signature', ''),
(139, 0, 'wd_bank_deposit', '2'),
(140, 0, 'wd_template_payout', '<p>Hi {merchant-name},</p>\r\n<br/>\r\n<p>We\\\'re just letting you know that we got your request to pay out {payout-amount} via {payment-method} to {account}</p>\r\n<br/> \r\n	\r\n<p>\r\nYou can cancel this request any time before {cancel-date} here:<br/>\r\n{cancel-link}\r\n</p>\r\n\r\n<p>\r\nWe will complete this request on the {process-date} (or the next business day), but it can take up to 7 days to appear in your account. A second confirmation email will be sent at this time.\r\n</p>\r\n\r\n<br/>\r\n<p> Kind Regards</p>'),
(141, 0, 'wd_template_process', '<p>Hi {merchant-name},</p>\r\n<br/>\r\n<p>We just processed your request for {payout-amount} via {payment-method}.</p>\r\n<p>Your payment was sent to {acoount}</p>\r\n<br/> \r\n\r\n<p>Happy Spending!</p>\r\n\r\n<br/>\r\n<p> Kind Regards</p>'),
(142, 0, 'wd_enabled_paypal', ''),
(143, 0, 'wd_payout_disabled', ''),
(144, 0, 'wd_payout_notification', '2'),
(145, 0, 'wd_template_payout_subject', 'Your Request for Withdrawal was Received'),
(146, 0, 'wd_template_process_subject', 'Your Request for Withdrawal has been Processed'),
(147, 0, 'wd_bank_fields', 'default'),
(148, 0, 'admin_commission_enabled', 'yes'),
(149, 0, 'admin_disabled_membership', ''),
(150, 0, 'admin_commision_percent', ''),
(151, 0, 'admin_vat_no', ''),
(152, 0, 'admin_vat_percent', ''),
(153, 0, 'total_commission_status', '[\\"paid\\"]'),
(154, 0, 'admin_commision_ontop', '2'),
(155, 0, 'admin_commision_type', 'percentage'),
(156, 0, 'admin_include_merchant_cod', ''),
(157, 0, 'admin_exclude_cod_balance', '2'),
(158, 0, 'admin_disabled_membership_signup', ''),
(249, 0, 'theme_hide_logo', ''),
(250, 0, 'theme_hide_how_works', ''),
(251, 0, 'theme_hide_cuisine', ''),
(252, 0, 'theme_custom_footer', ''),
(253, 0, 'theme_show_app', ''),
(254, 0, 'theme_app_android', ''),
(255, 0, 'theme_app_ios', ''),
(256, 0, 'theme_app_windows', ''),
(257, 0, 'theme_filter_colapse', ''),
(258, 0, 'theme_list_style', 'listview'),
(259, 0, 'theme_menu_colapse', ''),
(260, 0, 'theme_top_menu', '[\\"browse\\",\\"resto_signup\\",\\"contact\\",\\"signup\\"]'),
(261, 0, 'show_language', ''),
(262, 0, 'theme_promo_tab', ''),
(263, 0, 'theme_hours_tab', ''),
(264, 0, 'theme_reviews_tab', ''),
(265, 0, 'theme_map_tab', ''),
(266, 0, 'theme_info_tab', ''),
(267, 0, 'theme_photos_tab', ''),
(268, 0, 'cookie_law_enabled', '2'),
(269, 0, 'cookie_accept_text', 'save cookie'),
(270, 0, 'cookie_info_text', ''),
(271, 0, 'cookie_msg_text', ''),
(272, 0, 'cookie_info_link', ''),
(273, 0, 'theme_search_merchant_name', ''),
(274, 0, 'theme_search_street_name', ''),
(275, 0, 'theme_search_cuisine', ''),
(276, 0, 'theme_search_foodname', ''),
(277, 0, 'theme_compression', ''),
(278, 0, 'theme_search_merchant_address', ''),
(279, 0, 'theme_lang_pos', 'bottom'),
(280, 0, 'theme_hide_footer_section1', ''),
(281, 0, 'theme_hide_footer_section2', ''),
(282, 0, 'theme_time_pick', '2'),
(304, 8, 'merchant_switch_master_cod', ''),
(305, 8, 'merchant_switch_master_ccr', ''),
(306, 8, 'merchant_switch_master_pyr', ''),
(307, 8, 'merchant_latitude', '28.6314512'),
(308, 8, 'merchant_longtitude', '77.21666720000007'),
(309, 8, 'merchant_information', '<span class=\\"_ZCm\\" data-ved=\\"0ahUKEwjAqYrrsNTOAhXCwI8KHWEhDK4QrTUIICgAMAg\\">Fashionable, brick-walled gastropub with a long beer list and a globe-trotting menu.</span>'),
(310, 8, 'merchant_minimum_order', '250'),
(311, 8, 'merchant_tax', ''),
(312, 8, 'merchant_delivery_charges', ''),
(313, 8, 'stores_open_day', '[\\"monday\\",\\"tuesday\\",\\"wednesday\\",\\"thursday\\",\\"friday\\",\\"saturday\\",\\"sunday\\"]'),
(314, 8, 'stores_open_starts', '{\\"monday\\":\\"12:00\\",\\"tuesday\\":\\"12:00\\",\\"wednesday\\":\\"12:00\\",\\"thursday\\":\\"12:00\\",\\"friday\\":\\"12:00\\",\\"saturday\\":\\"12:00\\",\\"sunday\\":\\"12:00\\"}'),
(315, 8, 'stores_open_ends', '{\\"monday\\":\\"0:00\\",\\"tuesday\\":\\"0:00\\",\\"wednesday\\":\\"0:00\\",\\"thursday\\":\\"0:00\\",\\"friday\\":\\"0:00\\",\\"saturday\\":\\"0:00\\",\\"sunday\\":\\"0:00\\"}'),
(316, 8, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(317, 8, 'merchant_photo', '1471850044-my-bar-square-2.jpg'),
(318, 8, 'merchant_delivery_estimation', ''),
(319, 8, 'merchant_delivery_miles', ''),
(320, 8, 'merchant_photo_bg', '1471849975-my-bar-square-5.jpg'),
(321, 8, 'merchant_disabled_cod', ''),
(322, 8, 'merchant_disabled_ccr', ''),
(323, 8, 'merchant_extenal', 'mybarsquare@gmail.com'),
(324, 8, 'merchant_enabled_voucher', 'yes'),
(325, 8, 'merchant_distance_type', 'mi'),
(326, 8, 'merchant_timezone', 'Indian/Christmas'),
(327, 8, 'merchant_close_msg', ''),
(328, 8, 'merchant_preorder', '1'),
(329, 8, 'merchant_maximum_order', '1500'),
(330, 8, 'merchant_packaging_charge', ''),
(331, 8, 'merchant_close_msg_holiday', ''),
(332, 8, 'merchant_holiday', '[\\"\\"]'),
(333, 8, 'merchant_activated_menu', '3'),
(334, 8, 'spicydish', ''),
(335, 8, 'merchant_required_delivery_time', 'yes'),
(336, 8, 'merchant_close_store', ''),
(337, 8, 'merchant_packaging_increment', ''),
(338, 8, 'merchant_show_time', ''),
(339, 8, 'merchant_enabled_tip', ''),
(340, 8, 'merchant_tip_default', ''),
(341, 8, 'merchant_minimum_order_pickup', '150'),
(342, 8, 'merchant_maximum_order_pickup', '5000'),
(343, 8, 'merchant_disabled_ordering', ''),
(344, 8, 'merchant_tax_charges', '2'),
(345, 8, 'stores_open_pm_start', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(346, 8, 'stores_open_pm_ends', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(347, 8, 'food_option_not_available', '2'),
(348, 8, 'order_verification', '2'),
(349, 8, 'order_sms_code_waiting', ''),
(350, 8, 'disabled_food_gallery', ''),
(351, 8, 'merchant_gallery', '[\\"1471850387-my-bar-square-2.jpg\\",\\"1471850387-my-bar-square-4.jpg\\",\\"1471850389-my-bar-square-1.jpg\\",\\"1471850390-my-bar-square-3.jpg\\",\\"1471850390-my-bar-square-5.jpg\\",\\"1471850392-my-bar-square-6.jpg\\"]'),
(352, 8, 'gallery_disabled', ''),
(353, 9, 'merchant_latitude', '28.6302736'),
(354, 9, 'merchant_longtitude', '77.22215430000006'),
(355, 9, 'merchant_information', 'The bold fusion of curiosity and creativity backed by a tranquil surrounding, make Tamasha one of Delhi\\\'s must-visit nightlife hubs. Our restaurant is spread out over 16000 sq ft. with five distinct seating areas including a courtyard, ground floor, first floor, backyard and an imposing rooftop complete with mist sprinklers, overlooking the busy streets of central Delhi.Tamasha’s eye-catching bar is built to resemble Optimus Prime, a fictional character from the Transformers franchise. A huge banyan tree drapes overhead with a series of white stars hanging from its tendrils, covering much of the outdoor space. Situated to the right of this stunning truck-bar, directly in the courtyard is a placid Sheesha lounge. '),
(356, 9, 'merchant_minimum_order', '250'),
(357, 9, 'merchant_tax', ''),
(358, 9, 'merchant_delivery_charges', ''),
(359, 9, 'stores_open_day', ''),
(360, 9, 'stores_open_starts', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(361, 9, 'stores_open_ends', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(362, 9, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(363, 9, 'merchant_photo', '1471851681-a29f5f4685c66876c13e8910fc4cc6d3.jpg'),
(364, 9, 'merchant_delivery_estimation', ''),
(365, 9, 'merchant_delivery_miles', ''),
(366, 9, 'merchant_photo_bg', '1471851661-Interiors-249-copy.jpg'),
(367, 9, 'merchant_disabled_cod', ''),
(368, 9, 'merchant_disabled_ccr', ''),
(369, 9, 'merchant_extenal', ''),
(370, 9, 'merchant_enabled_voucher', ''),
(371, 9, 'merchant_distance_type', 'mi'),
(372, 9, 'merchant_timezone', ''),
(373, 9, 'merchant_close_msg', ''),
(374, 9, 'merchant_preorder', ''),
(375, 9, 'merchant_maximum_order', '1500'),
(376, 9, 'merchant_packaging_charge', ''),
(377, 9, 'merchant_close_msg_holiday', ''),
(378, 9, 'merchant_holiday', '[\\"\\"]'),
(379, 9, 'merchant_activated_menu', '3'),
(380, 9, 'spicydish', ''),
(381, 9, 'merchant_required_delivery_time', ''),
(382, 9, 'merchant_close_store', ''),
(383, 9, 'merchant_packaging_increment', ''),
(384, 9, 'merchant_show_time', ''),
(385, 9, 'merchant_enabled_tip', ''),
(386, 9, 'merchant_tip_default', ''),
(387, 9, 'merchant_minimum_order_pickup', '150'),
(388, 9, 'merchant_maximum_order_pickup', '5000'),
(389, 9, 'merchant_disabled_ordering', ''),
(390, 9, 'merchant_tax_charges', ''),
(391, 9, 'stores_open_pm_start', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(392, 9, 'stores_open_pm_ends', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(393, 9, 'food_option_not_available', '2'),
(394, 9, 'order_verification', '2'),
(395, 9, 'order_sms_code_waiting', ''),
(396, 9, 'disabled_food_gallery', ''),
(397, 9, 'merchant_gallery', '[\\"1471851799-images.jpg\\",\\"1471851800-1455008090_Tamasha2.jpg\\",\\"1471851825-Interiors-249-copy.jpg\\",\\"1471851826-a29f5f4685c66876c13e8910fc4cc6d3.jpg\\",\\"1471851832-T787_4629_2-a.jpg\\",\\"1471851834-1455008090_Tamasha2.jpg\\",\\"1471851837-tmsha.jpg\\",\\"1471851838-images.jpg\\",\\"1471851847-IMG-20151204-WA0004.jpg\\",\\"1471851856-a29f5f4685c66876c13e8910fc4cc6d3.jpg\\"]'),
(398, 9, 'gallery_disabled', ''),
(399, 9, 'merchant_switch_master_cod', ''),
(400, 9, 'merchant_switch_master_ccr', ''),
(401, 9, 'merchant_switch_master_pyr', ''),
(402, 10, 'merchant_switch_master_cod', ''),
(403, 10, 'merchant_switch_master_ccr', ''),
(404, 10, 'merchant_switch_master_pyr', ''),
(405, 10, 'merchant_latitude', '28.6319097'),
(406, 10, 'merchant_longtitude', '77.2177021'),
(407, 10, 'merchant_minimum_order', '300'),
(408, 10, 'merchant_tax', ''),
(409, 10, 'merchant_delivery_charges', ''),
(410, 10, 'stores_open_day', ''),
(411, 10, 'stores_open_starts', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(412, 10, 'stores_open_ends', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(413, 10, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(414, 10, 'merchant_photo', '1471855606-lotd1.jpg'),
(415, 10, 'merchant_delivery_estimation', ''),
(416, 10, 'merchant_delivery_miles', ''),
(417, 10, 'merchant_photo_bg', '1471855622-watermarked-IMG_20150919_123734.jpg'),
(418, 10, 'merchant_disabled_cod', ''),
(419, 10, 'merchant_disabled_ccr', ''),
(420, 10, 'merchant_extenal', ''),
(421, 10, 'merchant_enabled_voucher', ''),
(422, 10, 'merchant_distance_type', 'mi'),
(423, 10, 'merchant_timezone', 'Indian/Christmas'),
(424, 10, 'merchant_close_msg', ''),
(425, 10, 'merchant_preorder', '1'),
(426, 10, 'merchant_maximum_order', '2000'),
(427, 10, 'merchant_packaging_charge', ''),
(428, 10, 'merchant_close_msg_holiday', ''),
(429, 10, 'merchant_holiday', '[\\"\\"]'),
(430, 10, 'merchant_activated_menu', '3'),
(431, 10, 'spicydish', ''),
(432, 10, 'merchant_required_delivery_time', ''),
(433, 10, 'merchant_close_store', ''),
(434, 10, 'merchant_packaging_increment', ''),
(435, 10, 'merchant_show_time', ''),
(436, 10, 'merchant_enabled_tip', ''),
(437, 10, 'merchant_tip_default', ''),
(438, 10, 'merchant_minimum_order_pickup', '250'),
(439, 10, 'merchant_maximum_order_pickup', '10000'),
(440, 10, 'merchant_disabled_ordering', ''),
(441, 10, 'merchant_tax_charges', ''),
(442, 10, 'stores_open_pm_start', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(443, 10, 'stores_open_pm_ends', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(444, 10, 'food_option_not_available', '2'),
(445, 10, 'order_verification', '2'),
(446, 10, 'order_sms_code_waiting', ''),
(447, 10, 'disabled_food_gallery', ''),
(448, 11, 'merchant_switch_master_cod', ''),
(449, 11, 'merchant_switch_master_ccr', ''),
(450, 11, 'merchant_switch_master_pyr', ''),
(451, 11, 'merchant_latitude', '28.6314512'),
(452, 11, 'merchant_longtitude', '77.21666720000007'),
(453, 11, 'merchant_information', '<span style=\\"color: rgb(68, 68, 68); font-family: Roboto, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: 20px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);\\">The Junkyard Cafe is a luxurious and spacious cafe with junkyard charm combined with plush comfort. Expect barrels, car and truck parts, old radios, TVs and chipped board furniture at this 13,000 sq feet space. There are sofas with chipboards, a live stage with two private VIP zones and a menu that offers healthy grub at reasonable prices. The food menu is meant for health-conscious people who like nutritious meals. We believe that entertainment should be guilt-free. When people step out, they should indulge in food without being worried about the calorie intake. We want to make sure that the food menu is healthy, nutritious and reasonably priced. This place is meant for those who enjoy keeping it casual and going out for a good meal in a funky environment.</span>'),
(454, 11, 'merchant_minimum_order', ''),
(455, 11, 'merchant_tax', ''),
(456, 11, 'merchant_delivery_charges', ''),
(457, 11, 'stores_open_day', ''),
(458, 11, 'stores_open_starts', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(459, 11, 'stores_open_ends', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(460, 11, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(461, 11, 'merchant_photo', '1471856679-Junkyard,-CP-(3).jpg'),
(462, 11, 'merchant_delivery_estimation', ''),
(463, 11, 'merchant_delivery_miles', ''),
(464, 11, 'merchant_photo_bg', '1471856697-p20298-1447661865564991294ad1b-700x700-ido44bab05807ce8.jpg'),
(465, 11, 'merchant_disabled_cod', ''),
(466, 11, 'merchant_disabled_ccr', ''),
(467, 11, 'merchant_extenal', ''),
(468, 11, 'merchant_enabled_voucher', ''),
(469, 11, 'merchant_distance_type', 'mi'),
(470, 11, 'merchant_timezone', ''),
(471, 11, 'merchant_close_msg', ''),
(472, 11, 'merchant_preorder', ''),
(473, 11, 'merchant_maximum_order', ''),
(474, 11, 'merchant_packaging_charge', ''),
(475, 11, 'merchant_close_msg_holiday', ''),
(476, 11, 'merchant_holiday', '[\\"\\"]'),
(477, 11, 'merchant_activated_menu', '3'),
(478, 11, 'spicydish', ''),
(479, 11, 'merchant_required_delivery_time', ''),
(480, 11, 'merchant_close_store', ''),
(481, 11, 'merchant_packaging_increment', ''),
(482, 11, 'merchant_show_time', ''),
(483, 11, 'merchant_enabled_tip', ''),
(484, 11, 'merchant_tip_default', ''),
(485, 11, 'merchant_minimum_order_pickup', ''),
(486, 11, 'merchant_maximum_order_pickup', ''),
(487, 11, 'merchant_disabled_ordering', ''),
(488, 11, 'merchant_tax_charges', ''),
(489, 11, 'stores_open_pm_start', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(490, 11, 'stores_open_pm_ends', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(491, 11, 'food_option_not_available', '2'),
(492, 11, 'order_verification', ''),
(493, 11, 'order_sms_code_waiting', ''),
(494, 11, 'disabled_food_gallery', ''),
(495, 11, 'merchant_gallery', '[\\"1471856730-4efd6e1675f645c637e6176e9e7fbcdc.jpg\\",\\"1471856732-Junkyard-cafe,-Connaught-place,-New-Delhi.jpg\\",\\"1471856732-p23353-1468499617578786a1a2e34-700x700-ido951350fdf46ad.jpg\\",\\"1471856735-p20298-1447661865564991294ad1b-700x700-ido44bab05807ce8.jpg\\",\\"1471856735-p23353-1468499621578786a52011f-700x700-ido4c42f5f67330a.jpg\\",\\"1471856737-p23353-14684996105787869a7f0ee-700x700-idoe386ec7120a97.jpg\\",\\"1471856737-p23353-14684995815787867d0b2e8-700x700-ido54cc00c5d6660.jpg\\",\\"1471856738-p23353-1468499554578786629b442-700x700-idob180918acd8d5.jpg\\",\\"1471856739-Junkyard,-CP-(3).jpg\\",\\"1471856739-p23353-146849957357878675843e3-700x700-ido1795f6b490a03.jpg\\"]'),
(496, 11, 'gallery_disabled', ''),
(497, 12, 'merchant_switch_master_cod', ''),
(498, 12, 'merchant_switch_master_ccr', ''),
(499, 12, 'merchant_switch_master_pyr', ''),
(500, 12, 'merchant_latitude', ''),
(501, 12, 'merchant_longtitude', ''),
(502, 12, 'merchant_minimum_order', ''),
(503, 12, 'merchant_tax', ''),
(504, 12, 'merchant_delivery_charges', ''),
(505, 12, 'stores_open_day', ''),
(506, 12, 'stores_open_starts', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(507, 12, 'stores_open_ends', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(508, 12, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(509, 12, 'merchant_photo', '1471857217-p21480-145941255556fcde4b6c11d-700x700-ido443c4e47398de.jpg'),
(510, 12, 'merchant_delivery_estimation', ''),
(511, 12, 'merchant_delivery_miles', ''),
(512, 12, 'merchant_photo_bg', '1471857216-p21480-145941255556fcde4b8e596-700x700-ido596a132bd07d7.jpg'),
(513, 12, 'merchant_disabled_cod', ''),
(514, 12, 'merchant_disabled_ccr', ''),
(515, 12, 'merchant_extenal', ''),
(516, 12, 'merchant_enabled_voucher', ''),
(517, 12, 'merchant_distance_type', 'mi'),
(518, 12, 'merchant_timezone', ''),
(519, 12, 'merchant_close_msg', ''),
(520, 12, 'merchant_preorder', ''),
(521, 12, 'merchant_maximum_order', ''),
(522, 12, 'merchant_packaging_charge', ''),
(523, 12, 'merchant_close_msg_holiday', ''),
(524, 12, 'merchant_holiday', '[\\"\\"]'),
(525, 12, 'merchant_activated_menu', ''),
(526, 12, 'spicydish', ''),
(527, 12, 'merchant_required_delivery_time', ''),
(528, 12, 'merchant_close_store', ''),
(529, 12, 'merchant_packaging_increment', ''),
(530, 12, 'merchant_show_time', ''),
(531, 12, 'merchant_enabled_tip', ''),
(532, 12, 'merchant_tip_default', ''),
(533, 12, 'merchant_minimum_order_pickup', ''),
(534, 12, 'merchant_maximum_order_pickup', ''),
(535, 12, 'merchant_disabled_ordering', ''),
(536, 12, 'merchant_tax_charges', ''),
(537, 12, 'stores_open_pm_start', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(538, 12, 'stores_open_pm_ends', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(539, 12, 'food_option_not_available', ''),
(540, 12, 'order_verification', '2'),
(541, 12, 'order_sms_code_waiting', ''),
(542, 12, 'disabled_food_gallery', ''),
(543, 12, 'merchant_information', '<span style=\\"color: rgb(68, 68, 68); font-family: Roboto, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: 20px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);\\">Our restaurant is themed similar to the Teddy Boy culture, once prominent in the UK during 1950s. Edwardian era in the West used to be a landmark for men’s fashion. Strongly-moulded greased-up hair, drape jackets, trippy shirts and the thumping rock and roll. Teddy Boy is bringing it all back, that too to Delhi, across decades. Vibrant interiors, dim lights, funky artwork is what you will find in Teddy Boy. The whole place has a rustic decor, with Deer heads on the wall. With vintage walls and leather couches, there is an unbeatable chic element to it. Our newly open Gastro bar serves some lip smacking North Indian and Continental food along with fantastic drinks. Giving a complete make over to Indian dishes with western flavors is something we did in our menu. Our Chef Doyel Sarangi (Master Chef Fame) has a certain style of cooking, which comes from her culture. Infusing international dishes with a touch of Bengali and coastal flavors is Doyel’s forte. With a great influence of Coastal food, the Head Chef has balanced the flavors to create some wonderful gastronomic experiences.</span>'),
(544, 12, 'merchant_gallery', '[\\"1471857267-p21480-145940485056fcc03263ed7-700x700-idofc30f435e990c.jpg\\",\\"1471857268-p21480-145941255556fcde4b7c10b-700x700-idoec076eb700ad1.jpg\\",\\"1471857269-p21480-145941255556fcde4b8e596-700x700-ido596a132bd07d7.jpg\\",\\"1471857269-p21480-145941255556fcde4b6c11d-700x700-ido443c4e47398de.jpg\\",\\"1471857270-p21480-145941255556fcde4bac041-700x700-ido39d0b94b8af3e.jpg\\",\\"1471857271-p21480-145941255556fcde4b9c37f-700x700-ido039124b4c4336.jpg\\",\\"1471857277-p21480-145940485056fcc03284f48-700x700-ido6680b5d0e2684.jpg\\"]'),
(545, 12, 'gallery_disabled', ''),
(546, 13, 'merchant_switch_master_cod', ''),
(547, 13, 'merchant_switch_master_ccr', ''),
(548, 13, 'merchant_switch_master_pyr', ''),
(549, 13, 'merchant_latitude', '28.6314512'),
(550, 13, 'merchant_longtitude', '77.21666720000007'),
(551, 13, 'merchant_information', '<span style=\\"color: rgb(68, 68, 68); font-family: Roboto, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: 20px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);\\">The Open House cafe serves Pan Asian cuisine with a wide range of alcoholic blends. The interiors reflects of a metropolitan cafe with pictures of old CP displayed all around the place. The gorgeous interiors and the chrome plated decor will enchant you with a soothing aura and the luscious food served will form a bond with you. The cafe boast of red and black well lit setting paired with brown leather couches, a gallery lined with cookbooks and crisp table linen. With expansive open space that looks inviting open house is opening soon to love, happiness, excellent food, smashing tunes and good times. The mahogany furniture and warm lighting compliments the mood and is also perfect for romantic dates. Open house will be the first cafe in CP to serve cantonese food with specialities like schezwan, sushi and Cheung fun food. Considering its wonderfully landscaped setting, the open house Cafe promises its customers with an elegant decor, good food and great view to make their experience worth remembering. Brimming with energy and class, it is the perfect place to enjoy a delicious meal with family, friends or a loved one. For a wonderful experience of smell, sight and taste : Open house is the place to be.</span>'),
(552, 13, 'merchant_minimum_order', ''),
(553, 13, 'merchant_tax', ''),
(554, 13, 'merchant_delivery_charges', ''),
(555, 13, 'stores_open_day', ''),
(556, 13, 'stores_open_starts', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(557, 13, 'stores_open_ends', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(558, 13, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(559, 13, 'merchant_photo', '1471857809-p19748-1462114026572616ea5a262-700x700-idof0014c7df1ef1.jpg'),
(560, 13, 'merchant_delivery_estimation', ''),
(561, 13, 'merchant_delivery_miles', ''),
(562, 13, 'merchant_photo_bg', '1471857819-1.jpg'),
(563, 13, 'merchant_disabled_cod', ''),
(564, 13, 'merchant_disabled_ccr', ''),
(565, 13, 'merchant_extenal', ''),
(566, 13, 'merchant_enabled_voucher', ''),
(567, 13, 'merchant_distance_type', 'mi'),
(568, 13, 'merchant_timezone', ''),
(569, 13, 'merchant_close_msg', ''),
(570, 13, 'merchant_preorder', ''),
(571, 13, 'merchant_maximum_order', ''),
(572, 13, 'merchant_packaging_charge', ''),
(573, 13, 'merchant_close_msg_holiday', ''),
(574, 13, 'merchant_holiday', '[\\"\\"]'),
(575, 13, 'merchant_activated_menu', ''),
(576, 13, 'spicydish', ''),
(577, 13, 'merchant_required_delivery_time', ''),
(578, 13, 'merchant_close_store', ''),
(579, 13, 'merchant_packaging_increment', ''),
(580, 13, 'merchant_show_time', ''),
(581, 13, 'merchant_enabled_tip', ''),
(582, 13, 'merchant_tip_default', ''),
(583, 13, 'merchant_minimum_order_pickup', ''),
(584, 13, 'merchant_maximum_order_pickup', ''),
(585, 13, 'merchant_disabled_ordering', ''),
(586, 13, 'merchant_tax_charges', ''),
(587, 13, 'stores_open_pm_start', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(588, 13, 'stores_open_pm_ends', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(589, 13, 'food_option_not_available', ''),
(590, 13, 'order_verification', ''),
(591, 13, 'order_sms_code_waiting', ''),
(592, 13, 'disabled_food_gallery', ''),
(593, 13, 'merchant_gallery', '[\\"1471857854-1.jpg\\",\\"1471857855-p19748-144117721255e69e7c2bb3c-700x700-ido83c86f4d08c9c.jpg\\",\\"1471857856-p19748-144117720655e69e765d43b-700x700-ido88c741e5ced4b.jpg\\",\\"1471857856-p19748-144117721555e69e7f52aea-700x700-ido4c9841807b8d0.jpg\\",\\"1471857857-p19748-144117721855e69e821b643-700x700-idobd5e4a59b601f.jpg\\",\\"1471857858-p19748-1462114026572616ea5a262-700x700-idof0014c7df1ef1.jpg\\",\\"1471857858-p19748-144117722455e69e886cb2e-700x700-ido7a85b6ec3a0b4.jpg\\",\\"1471857859-p19748-1462114026572616ea48f90-700x700-idob252825e0a9aa.jpg\\"]'),
(594, 13, 'gallery_disabled', ''),
(595, 14, 'merchant_switch_master_cod', ''),
(596, 14, 'merchant_switch_master_ccr', ''),
(597, 14, 'merchant_switch_master_pyr', ''),
(598, 14, 'merchant_latitude', '28.6318901'),
(599, 14, 'merchant_longtitude', '77.22280899999998'),
(600, 14, 'merchant_information', '<span style=\\"color: rgb(68, 68, 68); font-family: Roboto, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: 20px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);\\">38 Barracks is a live room featuring the sounds of world music. Operating at two floors, 38 Barracks is the place to come in with friends &amp; family. The ground floor is the place where one can organise Private Disc Parties. It is well equipped with high quality extreme sound systems, laser lights, well stocked bar and DJ Console. It serves world cuisine like falafel, enchiladas, pastas, pizzas, etc. The 1st Floor is young and chic for people with lots of energy to enjoy their day. Loud Music, rounds of tequilas and test tube shots and a menu which leaves you spoilt for choice.</span>'),
(601, 14, 'merchant_minimum_order', ''),
(602, 14, 'merchant_tax', ''),
(603, 14, 'merchant_delivery_charges', ''),
(604, 14, 'stores_open_day', ''),
(605, 14, 'stores_open_starts', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(606, 14, 'stores_open_ends', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(607, 14, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(608, 14, 'merchant_photo', '1471858435-p21171-1467727379577bbe136c0c3-700x700-ido4a5106c71be5e.jpg'),
(609, 14, 'merchant_delivery_estimation', ''),
(610, 14, 'merchant_delivery_miles', ''),
(611, 14, 'merchant_photo_bg', '1471858442-p21171-1467727392577bbe2003bb1-700x700-ido4086d8bf2c599.jpg'),
(612, 14, 'merchant_disabled_cod', ''),
(613, 14, 'merchant_disabled_ccr', ''),
(614, 14, 'merchant_extenal', ''),
(615, 14, 'merchant_enabled_voucher', ''),
(616, 14, 'merchant_distance_type', 'mi'),
(617, 14, 'merchant_timezone', ''),
(618, 14, 'merchant_close_msg', ''),
(619, 14, 'merchant_preorder', ''),
(620, 14, 'merchant_maximum_order', ''),
(621, 14, 'merchant_packaging_charge', ''),
(622, 14, 'merchant_close_msg_holiday', ''),
(623, 14, 'merchant_holiday', '[\\"\\"]'),
(624, 14, 'merchant_activated_menu', ''),
(625, 14, 'spicydish', ''),
(626, 14, 'merchant_required_delivery_time', ''),
(627, 14, 'merchant_close_store', ''),
(628, 14, 'merchant_packaging_increment', ''),
(629, 14, 'merchant_show_time', ''),
(630, 14, 'merchant_enabled_tip', ''),
(631, 14, 'merchant_tip_default', ''),
(632, 14, 'merchant_minimum_order_pickup', ''),
(633, 14, 'merchant_maximum_order_pickup', ''),
(634, 14, 'merchant_disabled_ordering', ''),
(635, 14, 'merchant_tax_charges', ''),
(636, 14, 'stores_open_pm_start', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(637, 14, 'stores_open_pm_ends', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(638, 14, 'food_option_not_available', ''),
(639, 14, 'order_verification', ''),
(640, 14, 'order_sms_code_waiting', ''),
(641, 14, 'disabled_food_gallery', ''),
(642, 14, 'merchant_gallery', '[\\"1471858474-p21171-1467727374577bbe0ef3d7f-700x700-ido2b10b33ab2e6c.jpg\\",\\"1471858476-p21171-1467727379577bbe136c0c3-700x700-ido4a5106c71be5e.jpg\\",\\"1471858476-p21171-1467727370577bbe0aecc4f-700x700-ido291625aa612a7.jpg\\",\\"1471858477-p21171-1467727387577bbe1b64914-700x700-ido9034e1dec3163.jpg\\",\\"1471858478-p21171-1467727383577bbe177da59-700x700-idoef406d2812d55.jpg\\",\\"1471858479-p21171-1467727396577bbe249cab6-700x700-idoaf903ffba0026.jpg\\",\\"1471858480-p21171-1467727404577bbe2c46093-700x700-ido8e0e17fc5ba8f.jpg\\",\\"1471858482-p21171-1467727408577bbe302f4f8-700x700-idofe1aa5b5e6c65.jpg\\",\\"1471858482-p21171-1467727400577bbe287874a-700x700-ido0fd256eb6e8bb.jpg\\",\\"1471858482-p21171-1467727392577bbe2003bb1-700x700-ido4086d8bf2c599.jpg\\"]'),
(643, 14, 'gallery_disabled', ''),
(644, 15, 'merchant_switch_master_cod', ''),
(645, 15, 'merchant_switch_master_ccr', ''),
(646, 15, 'merchant_switch_master_pyr', ''),
(647, 15, 'merchant_latitude', '28.6314512'),
(648, 15, 'merchant_longtitude', '77.21666720000007'),
(649, 15, 'merchant_information', '<span style=\\"color: rgb(68, 68, 68); font-family: Roboto, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: 20px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);\\">QBA Restaurant and Bar has been ranked #1 India, #2 Asia Pacific and #15 World in the World\\\'s Best 50 Bars 2010 in a survey done by Drinks International Magazine, London. It has also been awarded the Best Multi-Cuisine Restaurant in Delhi by Hindustan Times (2010). QBA Restaurant and Bar can organise both indoor and outdoor events (private/corporate) and can accommodate a maximum of 300-400 people indoors and also have an experience in catering till up to 2500 people.</span>'),
(650, 15, 'merchant_minimum_order', ''),
(651, 15, 'merchant_tax', ''),
(652, 15, 'merchant_delivery_charges', ''),
(653, 15, 'stores_open_day', ''),
(654, 15, 'stores_open_starts', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(655, 15, 'stores_open_ends', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(656, 15, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(657, 15, 'merchant_photo', '1471858967-p351-143678170055a38c8439cb9-700x700-idob1f920eb8e411.jpg'),
(658, 15, 'merchant_delivery_estimation', ''),
(659, 15, 'merchant_delivery_miles', ''),
(660, 15, 'merchant_photo_bg', '1471858974-p351-143678170755a38c8bb2245-700x700-ido2376d21d1f00e.jpg'),
(661, 15, 'merchant_disabled_cod', ''),
(662, 15, 'merchant_disabled_ccr', ''),
(663, 15, 'merchant_extenal', ''),
(664, 15, 'merchant_enabled_voucher', ''),
(665, 15, 'merchant_distance_type', 'mi'),
(666, 15, 'merchant_timezone', ''),
(667, 15, 'merchant_close_msg', ''),
(668, 15, 'merchant_preorder', ''),
(669, 15, 'merchant_maximum_order', ''),
(670, 15, 'merchant_packaging_charge', ''),
(671, 15, 'merchant_close_msg_holiday', ''),
(672, 15, 'merchant_holiday', '[\\"\\"]'),
(673, 15, 'merchant_activated_menu', ''),
(674, 15, 'spicydish', ''),
(675, 15, 'merchant_required_delivery_time', ''),
(676, 15, 'merchant_close_store', ''),
(677, 15, 'merchant_packaging_increment', ''),
(678, 15, 'merchant_show_time', ''),
(679, 15, 'merchant_enabled_tip', ''),
(680, 15, 'merchant_tip_default', ''),
(681, 15, 'merchant_minimum_order_pickup', ''),
(682, 15, 'merchant_maximum_order_pickup', ''),
(683, 15, 'merchant_disabled_ordering', ''),
(684, 15, 'merchant_tax_charges', ''),
(685, 15, 'stores_open_pm_start', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(686, 15, 'stores_open_pm_ends', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(687, 15, 'food_option_not_available', ''),
(688, 15, 'order_verification', ''),
(689, 15, 'order_sms_code_waiting', ''),
(690, 15, 'disabled_food_gallery', ''),
(691, 15, 'merchant_gallery', '[\\"1471859013-p351-143678170055a38c8439cb9-700x700-idob1f920eb8e411.jpg\\",\\"1471859014-p351-144732116556445e4dce42a-700x700-ido0d56a697e7470.jpg\\",\\"1471859016-p351-144732117356445e555d047-700x700-idobf01015378342.jpg\\",\\"1471859016-p351-143678170555a38c8968730-700x700-ido3ef4849558b37.jpg\\",\\"1471859016-p351-143678170755a38c8bb2245-700x700-ido2376d21d1f00e.jpg\\"]'),
(692, 15, 'gallery_disabled', ''),
(693, 16, 'merchant_switch_master_cod', ''),
(694, 16, 'merchant_switch_master_ccr', ''),
(695, 16, 'merchant_switch_master_pyr', ''),
(696, 16, 'merchant_latitude', '28.6348765'),
(697, 16, 'merchant_longtitude', '77.09037809999995'),
(698, 16, 'merchant_minimum_order', ''),
(699, 16, 'merchant_tax', ''),
(700, 16, 'merchant_delivery_charges', ''),
(701, 16, 'stores_open_day', '[\\"monday\\",\\"tuesday\\",\\"wednesday\\",\\"thursday\\",\\"friday\\",\\"saturday\\"]'),
(702, 16, 'stores_open_starts', '{\\"monday\\":\\"9:00\\",\\"tuesday\\":\\"9:00\\",\\"wednesday\\":\\"9:00\\",\\"thursday\\":\\"9:00\\",\\"friday\\":\\"9:00\\",\\"saturday\\":\\"9:00\\",\\"sunday\\":false}'),
(703, 16, 'stores_open_ends', '{\\"monday\\":\\"21:00\\",\\"tuesday\\":\\"21:00\\",\\"wednesday\\":\\"21:00\\",\\"thursday\\":\\"21:00\\",\\"friday\\":\\"21:00\\",\\"saturday\\":\\"21:00\\",\\"sunday\\":false}'),
(704, 16, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(705, 16, 'merchant_photo', '1471859572-restaurant120160519132111.jpg'),
(706, 16, 'merchant_delivery_estimation', ''),
(707, 16, 'merchant_delivery_miles', ''),
(708, 16, 'merchant_photo_bg', '1471859578-restaurant020160519132111.jpg'),
(709, 16, 'merchant_disabled_cod', ''),
(710, 16, 'merchant_disabled_ccr', ''),
(711, 16, 'merchant_extenal', ''),
(712, 16, 'merchant_enabled_voucher', ''),
(713, 16, 'merchant_distance_type', ''),
(714, 16, 'merchant_timezone', 'Asia/Kolkata'),
(715, 16, 'merchant_close_msg', ''),
(716, 16, 'merchant_preorder', ''),
(717, 16, 'merchant_maximum_order', ''),
(718, 16, 'merchant_packaging_charge', ''),
(719, 16, 'merchant_close_msg_holiday', ''),
(720, 16, 'merchant_holiday', ''),
(721, 16, 'merchant_activated_menu', ''),
(722, 16, 'spicydish', ''),
(723, 16, 'merchant_required_delivery_time', ''),
(724, 16, 'merchant_close_store', ''),
(725, 16, 'merchant_packaging_increment', ''),
(726, 16, 'merchant_show_time', ''),
(727, 16, 'merchant_enabled_tip', ''),
(728, 16, 'merchant_tip_default', ''),
(729, 16, 'merchant_minimum_order_pickup', ''),
(730, 16, 'merchant_maximum_order_pickup', ''),
(731, 16, 'merchant_disabled_ordering', ''),
(732, 16, 'merchant_tax_charges', ''),
(733, 16, 'stores_open_pm_start', '{\\"monday\\":\\"11:40 PM\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"11:45 PM\\",\\"friday\\":\\"11:45 PM\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(734, 16, 'stores_open_pm_ends', '{\\"monday\\":\\"4:45 AM\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"5:45 AM\\",\\"friday\\":\\"5:45 AM\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(735, 16, 'food_option_not_available', ''),
(736, 16, 'order_verification', ''),
(737, 16, 'order_sms_code_waiting', ''),
(738, 16, 'disabled_food_gallery', ''),
(739, 16, 'merchant_gallery', '[\\"1471859609-restaurant020160519132111.jpg\\",\\"1471859610-restaurant120160519132111.jpg\\",\\"1471859610-restaurant020160519131926.jpg\\",\\"1471859612-restaurant320160519132111.jpg\\",\\"1471859612-restaurant220160519132111.jpg\\"]'),
(740, 16, 'gallery_disabled', ''),
(741, 16, 'merchant_information', ''),
(791, 18, 'merchant_latitude', '28.6565216'),
(792, 18, 'merchant_longtitude', '77.23453719999998'),
(793, 18, 'merchant_information', ''),
(794, 18, 'merchant_read_notice', '1'),
(795, 18, 'merchant_switch_master_cod', ''),
(796, 18, 'merchant_switch_master_ccr', ''),
(797, 18, 'merchant_switch_master_pyr', ''),
(798, 19, 'merchant_switch_master_cod', ''),
(799, 19, 'merchant_switch_master_ccr', ''),
(800, 19, 'merchant_switch_master_pyr', ''),
(801, 19, 'merchant_latitude', '28.635278'),
(802, 19, 'merchant_longtitude', '77.07312300000001'),
(803, 20, 'merchant_switch_master_cod', ''),
(804, 20, 'merchant_switch_master_ccr', ''),
(805, 20, 'merchant_switch_master_pyr', ''),
(806, 20, 'merchant_latitude', '28.6498489'),
(807, 20, 'merchant_longtitude', '77.28348410000001'),
(808, 21, 'merchant_switch_master_cod', ''),
(809, 21, 'merchant_switch_master_ccr', ''),
(810, 21, 'merchant_switch_master_pyr', ''),
(811, 21, 'merchant_latitude', '28.6691565'),
(812, 21, 'merchant_longtitude', '77.45375779999995'),
(813, 24, 'merchant_switch_master_cod', ''),
(814, 24, 'merchant_switch_master_ccr', ''),
(815, 24, 'merchant_switch_master_pyr', ''),
(816, 24, 'merchant_latitude', '28.5254842'),
(817, 24, 'merchant_longtitude', '77.14332489999993'),
(818, 23, 'merchant_switch_master_cod', ''),
(819, 23, 'merchant_switch_master_ccr', ''),
(820, 23, 'merchant_switch_master_pyr', ''),
(821, 23, 'merchant_latitude', '28.648876'),
(822, 23, 'merchant_longtitude', '77.10968290000005'),
(823, 22, 'merchant_switch_master_cod', ''),
(824, 22, 'merchant_switch_master_ccr', ''),
(825, 22, 'merchant_switch_master_pyr', ''),
(826, 22, 'merchant_latitude', '28.6504475'),
(827, 22, 'merchant_longtitude', '77.1111214'),
(828, 20, 'merchant_information', ''),
(829, 24, 'merchant_information', ''),
(830, 23, 'merchant_information', ''),
(831, 22, 'merchant_information', ''),
(832, 21, 'merchant_information', ''),
(833, 19, 'merchant_information', ''),
(834, 0, 'points_enabled', '1'),
(835, 0, 'pts_earning_points', '1'),
(836, 0, 'pts_earning_points_value', '1'),
(837, 0, 'pts_redeeming_point', '1'),
(838, 0, 'pts_redeeming_point_value', '0.25'),
(839, 0, 'pts_expiry', '3'),
(840, 0, 'pts_account_signup', '100'),
(841, 0, 'pts_merchant_reivew', '1'),
(842, 0, 'pts_maximum_points', '10000'),
(843, 0, 'pts_first_order', '10'),
(844, 0, 'points_apply_order_amt', ''),
(845, 0, 'points_minimum', '1'),
(846, 0, 'points_max', '100'),
(847, 0, 'pts_label', 'Order this & earn {points} FlipQ points'),
(848, 0, 'pts_payment_option', '[\\"cod\\",\\"ocr\\",\\"pyp\\",\\"pyr\\",\\"stp\\",\\"mcd\\",\\"payu\\",\\"atz\\",\\"obd\\"]'),
(849, 0, 'pts_label_input', 'FlipQ Points'),
(850, 0, 'pts_label_earn', 'This order earned {points} FlipQ Points'),
(851, 0, 'pts_disabled_redeem', ''),
(852, 0, 'pts_redeem_balance_zero', '1'),
(853, 0, 'mobile_default_image_not_available', '1470989294-logo.png'),
(854, 0, 'mobile_android_push_key', 'AIzaSyBFQ9ErCsZgTvGbMC4mIBtS-HhZ9Jf-Mws'),
(855, 0, 'mobile_country_list', '[\\"IN\\"]'),
(856, 0, 'mobile_push_order_title', 'Update on your order #{order_id}'),
(857, 0, 'mobile_push_order_message', 'Your order #{order_id} status change to {order_status}'),
(858, 0, 'ios_passphrase', ''),
(859, 0, 'ios_push_dev_cer', ''),
(860, 0, 'ios_push_prod_cer', ''),
(861, 0, 'ios_push_mode', 'development'),
(862, 0, 'mobileapp_api_has_key', '0f134caede84bb82ae566165b87d7cd6'),
(863, 10, 'merchant_gallery', '[\\"1472215459-2.jpg\\",\\"1472215460-1.png\\",\\"1472215461-12065985_10153801931186802_8150459514863494354_n.jpg\\",\\"1472215467-FB_IMG_1442324879580-1000x668.jpg\\",\\"1472215479-lotd1.jpg\\",\\"1472215481-watermarked-IMG_20150919_123734.jpg\\",\\"1472215482-0e2348bc069d4034f3e3165d9e7679f3.jpg\\"]'),
(864, 10, 'gallery_disabled', ''),
(865, 10, 'merchant_information', ''),
(866, 18, 'merchant_minimum_order', ''),
(867, 18, 'merchant_tax', ''),
(868, 18, 'merchant_delivery_charges', ''),
(869, 18, 'stores_open_day', ''),
(870, 18, 'stores_open_starts', ''),
(871, 18, 'stores_open_ends', ''),
(872, 18, 'stores_open_custom_text', ''),
(873, 18, 'merchant_photo', '1477116277-unnamed.jpg'),
(874, 18, 'merchant_delivery_estimation', ''),
(875, 18, 'merchant_delivery_miles', ''),
(876, 18, 'merchant_photo_bg', '1477116317-unnamed.jpg'),
(877, 18, 'merchant_disabled_cod', ''),
(878, 18, 'merchant_disabled_ccr', ''),
(879, 18, 'merchant_extenal', ''),
(880, 18, 'merchant_enabled_voucher', ''),
(881, 18, 'merchant_distance_type', ''),
(882, 18, 'merchant_timezone', ''),
(883, 18, 'merchant_close_msg', ''),
(884, 18, 'merchant_preorder', ''),
(885, 18, 'merchant_maximum_order', ''),
(886, 18, 'merchant_packaging_charge', ''),
(887, 18, 'merchant_close_msg_holiday', ''),
(888, 18, 'merchant_holiday', ''),
(889, 18, 'merchant_activated_menu', ''),
(890, 18, 'spicydish', ''),
(891, 18, 'merchant_required_delivery_time', ''),
(892, 18, 'merchant_close_store', ''),
(893, 18, 'merchant_packaging_increment', ''),
(894, 18, 'merchant_show_time', ''),
(895, 18, 'merchant_enabled_tip', ''),
(896, 18, 'merchant_tip_default', ''),
(897, 18, 'merchant_minimum_order_pickup', ''),
(898, 18, 'merchant_maximum_order_pickup', ''),
(899, 18, 'merchant_disabled_ordering', ''),
(900, 18, 'merchant_tax_charges', ''),
(901, 18, 'stores_open_pm_start', ''),
(902, 18, 'stores_open_pm_ends', ''),
(903, 18, 'food_option_not_available', ''),
(904, 18, 'order_verification', ''),
(905, 18, 'order_sms_code_waiting', ''),
(906, 18, 'disabled_food_gallery', ''),
(912, 26, 'merchant_switch_master_cod', ''),
(913, 26, 'merchant_switch_master_ccr', ''),
(914, 26, 'merchant_switch_master_pyr', ''),
(915, 26, 'merchant_latitude', '28.5273352'),
(916, 26, 'merchant_longtitude', '77.15154529999995'),
(917, 26, 'merchant_information', ''),
(918, 26, 'merchant_minimum_order', '700'),
(919, 26, 'merchant_tax', ''),
(920, 26, 'merchant_delivery_charges', ''),
(921, 26, 'stores_open_day', ''),
(922, 26, 'stores_open_starts', ''),
(923, 26, 'stores_open_ends', ''),
(924, 26, 'stores_open_custom_text', ''),
(925, 26, 'merchant_photo', '1476868900-Featured-monkey-bar.jpg');
INSERT INTO `mt_option` (`id`, `merchant_id`, `option_name`, `option_value`) VALUES
(926, 26, 'merchant_delivery_estimation', ''),
(927, 26, 'merchant_delivery_miles', ''),
(928, 26, 'merchant_photo_bg', '1476868906-d988d1fd2f783c73f69077093815f3c9_featured_v2.jpg'),
(929, 26, 'merchant_disabled_cod', ''),
(930, 26, 'merchant_disabled_ccr', ''),
(931, 26, 'merchant_extenal', ''),
(932, 26, 'merchant_enabled_voucher', ''),
(933, 26, 'merchant_distance_type', ''),
(934, 26, 'merchant_timezone', ''),
(935, 26, 'merchant_close_msg', ''),
(936, 26, 'merchant_preorder', ''),
(937, 26, 'merchant_maximum_order', ''),
(938, 26, 'merchant_packaging_charge', ''),
(939, 26, 'merchant_close_msg_holiday', ''),
(940, 26, 'merchant_holiday', ''),
(941, 26, 'merchant_activated_menu', ''),
(942, 26, 'spicydish', ''),
(943, 26, 'merchant_required_delivery_time', ''),
(944, 26, 'merchant_close_store', ''),
(945, 26, 'merchant_packaging_increment', ''),
(946, 26, 'merchant_show_time', ''),
(947, 26, 'merchant_enabled_tip', ''),
(948, 26, 'merchant_tip_default', ''),
(949, 26, 'merchant_minimum_order_pickup', ''),
(950, 26, 'merchant_maximum_order_pickup', ''),
(951, 26, 'merchant_disabled_ordering', ''),
(952, 26, 'merchant_tax_charges', ''),
(953, 26, 'stores_open_pm_start', ''),
(954, 26, 'stores_open_pm_ends', ''),
(955, 26, 'food_option_not_available', ''),
(956, 26, 'order_verification', ''),
(957, 26, 'order_sms_code_waiting', ''),
(958, 26, 'disabled_food_gallery', ''),
(959, 27, 'merchant_latitude', '28.6139391'),
(960, 27, 'merchant_longtitude', '77.20902120000005'),
(961, 27, 'merchant_information', ''),
(962, 27, 'merchant_minimum_order', '1000'),
(963, 27, 'merchant_tax', ''),
(964, 27, 'merchant_delivery_charges', ''),
(965, 27, 'stores_open_day', ''),
(966, 27, 'stores_open_starts', ''),
(967, 27, 'stores_open_ends', ''),
(968, 27, 'stores_open_custom_text', ''),
(969, 27, 'merchant_photo', '1476869531-Bottles-and-Barrels4-62-1447406619.jpg'),
(970, 27, 'merchant_delivery_estimation', ''),
(971, 27, 'merchant_delivery_miles', ''),
(972, 27, 'merchant_photo_bg', '1476869534-5aabca948cd2f779f27e073441432257.jpg'),
(973, 27, 'merchant_disabled_cod', ''),
(974, 27, 'merchant_disabled_ccr', ''),
(975, 27, 'merchant_extenal', ''),
(976, 27, 'merchant_enabled_voucher', ''),
(977, 27, 'merchant_distance_type', ''),
(978, 27, 'merchant_timezone', ''),
(979, 27, 'merchant_close_msg', ''),
(980, 27, 'merchant_preorder', ''),
(981, 27, 'merchant_maximum_order', ''),
(982, 27, 'merchant_packaging_charge', ''),
(983, 27, 'merchant_close_msg_holiday', ''),
(984, 27, 'merchant_holiday', ''),
(985, 27, 'merchant_activated_menu', ''),
(986, 27, 'spicydish', ''),
(987, 27, 'merchant_required_delivery_time', ''),
(988, 27, 'merchant_close_store', ''),
(989, 27, 'merchant_packaging_increment', ''),
(990, 27, 'merchant_show_time', ''),
(991, 27, 'merchant_enabled_tip', ''),
(992, 27, 'merchant_tip_default', ''),
(993, 27, 'merchant_minimum_order_pickup', ''),
(994, 27, 'merchant_maximum_order_pickup', ''),
(995, 27, 'merchant_disabled_ordering', ''),
(996, 27, 'merchant_tax_charges', ''),
(997, 27, 'stores_open_pm_start', ''),
(998, 27, 'stores_open_pm_ends', ''),
(999, 27, 'food_option_not_available', ''),
(1000, 27, 'order_verification', ''),
(1001, 27, 'order_sms_code_waiting', ''),
(1002, 27, 'disabled_food_gallery', ''),
(1003, 37, 'merchant_latitude', '28.6278172'),
(1004, 37, 'merchant_longtitude', '77.21895940000002'),
(1005, 37, 'merchant_information', ''),
(1006, 37, 'merchant_minimum_order', '1500'),
(1007, 37, 'merchant_tax', ''),
(1008, 37, 'merchant_delivery_charges', ''),
(1009, 37, 'stores_open_day', ''),
(1010, 37, 'stores_open_starts', ''),
(1011, 37, 'stores_open_ends', ''),
(1012, 37, 'stores_open_custom_text', ''),
(1013, 37, 'merchant_photo', '1476872809-masala-library.jpg'),
(1014, 37, 'merchant_delivery_estimation', ''),
(1015, 37, 'merchant_delivery_miles', ''),
(1016, 37, 'merchant_photo_bg', '1476872818-SHD-BKC-3.jpg'),
(1017, 37, 'merchant_disabled_cod', ''),
(1018, 37, 'merchant_disabled_ccr', ''),
(1019, 37, 'merchant_extenal', ''),
(1020, 37, 'merchant_enabled_voucher', ''),
(1021, 37, 'merchant_distance_type', ''),
(1022, 37, 'merchant_timezone', ''),
(1023, 37, 'merchant_close_msg', ''),
(1024, 37, 'merchant_preorder', ''),
(1025, 37, 'merchant_maximum_order', ''),
(1026, 37, 'merchant_packaging_charge', ''),
(1027, 37, 'merchant_close_msg_holiday', ''),
(1028, 37, 'merchant_holiday', ''),
(1029, 37, 'merchant_activated_menu', ''),
(1030, 37, 'spicydish', ''),
(1031, 37, 'merchant_required_delivery_time', ''),
(1032, 37, 'merchant_close_store', ''),
(1033, 37, 'merchant_packaging_increment', ''),
(1034, 37, 'merchant_show_time', ''),
(1035, 37, 'merchant_enabled_tip', ''),
(1036, 37, 'merchant_tip_default', ''),
(1037, 37, 'merchant_minimum_order_pickup', ''),
(1038, 37, 'merchant_maximum_order_pickup', ''),
(1039, 37, 'merchant_disabled_ordering', ''),
(1040, 37, 'merchant_tax_charges', ''),
(1041, 37, 'stores_open_pm_start', ''),
(1042, 37, 'stores_open_pm_ends', ''),
(1043, 37, 'food_option_not_available', ''),
(1044, 37, 'order_verification', ''),
(1045, 37, 'order_sms_code_waiting', ''),
(1046, 37, 'disabled_food_gallery', ''),
(1047, 36, 'merchant_latitude', '28.5904477'),
(1048, 36, 'merchant_longtitude', '77.22196589999999'),
(1049, 36, 'merchant_information', ''),
(1050, 36, 'merchant_minimum_order', ''),
(1051, 36, 'merchant_tax', ''),
(1052, 36, 'merchant_delivery_charges', ''),
(1053, 36, 'stores_open_day', ''),
(1054, 36, 'stores_open_starts', ''),
(1055, 36, 'stores_open_ends', ''),
(1056, 36, 'stores_open_custom_text', ''),
(1057, 36, 'merchant_photo', '1476873047-48bbe4764a472593712d273a885ee7bc.jpg'),
(1058, 36, 'merchant_delivery_estimation', ''),
(1059, 36, 'merchant_delivery_miles', ''),
(1060, 36, 'merchant_photo_bg', '1476873042-index.jpg'),
(1061, 36, 'merchant_disabled_cod', ''),
(1062, 36, 'merchant_disabled_ccr', ''),
(1063, 36, 'merchant_extenal', ''),
(1064, 36, 'merchant_enabled_voucher', ''),
(1065, 36, 'merchant_distance_type', ''),
(1066, 36, 'merchant_timezone', ''),
(1067, 36, 'merchant_close_msg', ''),
(1068, 36, 'merchant_preorder', ''),
(1069, 36, 'merchant_maximum_order', ''),
(1070, 36, 'merchant_packaging_charge', ''),
(1071, 36, 'merchant_close_msg_holiday', ''),
(1072, 36, 'merchant_holiday', ''),
(1073, 36, 'merchant_activated_menu', ''),
(1074, 36, 'spicydish', ''),
(1075, 36, 'merchant_required_delivery_time', ''),
(1076, 36, 'merchant_close_store', ''),
(1077, 36, 'merchant_packaging_increment', ''),
(1078, 36, 'merchant_show_time', ''),
(1079, 36, 'merchant_enabled_tip', ''),
(1080, 36, 'merchant_tip_default', ''),
(1081, 36, 'merchant_minimum_order_pickup', ''),
(1082, 36, 'merchant_maximum_order_pickup', ''),
(1083, 36, 'merchant_disabled_ordering', ''),
(1084, 36, 'merchant_tax_charges', ''),
(1085, 36, 'stores_open_pm_start', ''),
(1086, 36, 'stores_open_pm_ends', ''),
(1087, 36, 'food_option_not_available', ''),
(1088, 36, 'order_verification', ''),
(1089, 36, 'order_sms_code_waiting', ''),
(1090, 36, 'disabled_food_gallery', ''),
(1091, 35, 'merchant_latitude', '28.5320832'),
(1092, 35, 'merchant_longtitude', '77.31782499999997'),
(1093, 35, 'merchant_information', ''),
(1094, 35, 'merchant_minimum_order', '1200'),
(1095, 35, 'merchant_tax', ''),
(1096, 35, 'merchant_delivery_charges', ''),
(1097, 35, 'stores_open_day', ''),
(1098, 35, 'stores_open_starts', ''),
(1099, 35, 'stores_open_ends', ''),
(1100, 35, 'stores_open_custom_text', ''),
(1101, 35, 'merchant_photo', '1476873297-main-4957103.jpg'),
(1102, 35, 'merchant_delivery_estimation', ''),
(1103, 35, 'merchant_delivery_miles', ''),
(1104, 35, 'merchant_photo_bg', '1476873304-index.jpg'),
(1105, 35, 'merchant_disabled_cod', ''),
(1106, 35, 'merchant_disabled_ccr', ''),
(1107, 35, 'merchant_extenal', ''),
(1108, 35, 'merchant_enabled_voucher', ''),
(1109, 35, 'merchant_distance_type', ''),
(1110, 35, 'merchant_timezone', ''),
(1111, 35, 'merchant_close_msg', ''),
(1112, 35, 'merchant_preorder', ''),
(1113, 35, 'merchant_maximum_order', ''),
(1114, 35, 'merchant_packaging_charge', ''),
(1115, 35, 'merchant_close_msg_holiday', ''),
(1116, 35, 'merchant_holiday', ''),
(1117, 35, 'merchant_activated_menu', ''),
(1118, 35, 'spicydish', ''),
(1119, 35, 'merchant_required_delivery_time', ''),
(1120, 35, 'merchant_close_store', ''),
(1121, 35, 'merchant_packaging_increment', ''),
(1122, 35, 'merchant_show_time', ''),
(1123, 35, 'merchant_enabled_tip', ''),
(1124, 35, 'merchant_tip_default', ''),
(1125, 35, 'merchant_minimum_order_pickup', ''),
(1126, 35, 'merchant_maximum_order_pickup', ''),
(1127, 35, 'merchant_disabled_ordering', ''),
(1128, 35, 'merchant_tax_charges', ''),
(1129, 35, 'stores_open_pm_start', ''),
(1130, 35, 'stores_open_pm_ends', ''),
(1131, 35, 'food_option_not_available', ''),
(1132, 35, 'order_verification', ''),
(1133, 35, 'order_sms_code_waiting', ''),
(1134, 35, 'disabled_food_gallery', ''),
(1135, 34, 'merchant_latitude', '28.6033235'),
(1136, 34, 'merchant_longtitude', '77.22727650000002'),
(1137, 34, 'merchant_information', ''),
(1138, 34, 'merchant_minimum_order', ''),
(1139, 34, 'merchant_tax', ''),
(1140, 34, 'merchant_delivery_charges', ''),
(1141, 34, 'stores_open_day', ''),
(1142, 34, 'stores_open_starts', ''),
(1143, 34, 'stores_open_ends', ''),
(1144, 34, 'stores_open_custom_text', ''),
(1145, 34, 'merchant_photo', '1476873460-gulati-restaurant.jpg'),
(1146, 34, 'merchant_delivery_estimation', ''),
(1147, 34, 'merchant_delivery_miles', ''),
(1148, 34, 'merchant_photo_bg', '1476873464-images.jpg'),
(1149, 34, 'merchant_disabled_cod', ''),
(1150, 34, 'merchant_disabled_ccr', ''),
(1151, 34, 'merchant_extenal', ''),
(1152, 34, 'merchant_enabled_voucher', ''),
(1153, 34, 'merchant_distance_type', ''),
(1154, 34, 'merchant_timezone', ''),
(1155, 34, 'merchant_close_msg', ''),
(1156, 34, 'merchant_preorder', ''),
(1157, 34, 'merchant_maximum_order', ''),
(1158, 34, 'merchant_packaging_charge', ''),
(1159, 34, 'merchant_close_msg_holiday', ''),
(1160, 34, 'merchant_holiday', ''),
(1161, 34, 'merchant_activated_menu', ''),
(1162, 34, 'spicydish', ''),
(1163, 34, 'merchant_required_delivery_time', ''),
(1164, 34, 'merchant_close_store', ''),
(1165, 34, 'merchant_packaging_increment', ''),
(1166, 34, 'merchant_show_time', ''),
(1167, 34, 'merchant_enabled_tip', ''),
(1168, 34, 'merchant_tip_default', ''),
(1169, 34, 'merchant_minimum_order_pickup', ''),
(1170, 34, 'merchant_maximum_order_pickup', ''),
(1171, 34, 'merchant_disabled_ordering', ''),
(1172, 34, 'merchant_tax_charges', ''),
(1173, 34, 'stores_open_pm_start', ''),
(1174, 34, 'stores_open_pm_ends', ''),
(1175, 34, 'food_option_not_available', ''),
(1176, 34, 'order_verification', ''),
(1177, 34, 'order_sms_code_waiting', ''),
(1178, 34, 'disabled_food_gallery', ''),
(1179, 33, 'merchant_latitude', '28.6310885'),
(1180, 33, 'merchant_longtitude', '77.21711170000003'),
(1181, 33, 'merchant_information', ''),
(1182, 33, 'merchant_minimum_order', ''),
(1183, 33, 'merchant_tax', ''),
(1184, 33, 'merchant_delivery_charges', ''),
(1185, 33, 'stores_open_day', ''),
(1186, 33, 'stores_open_starts', ''),
(1187, 33, 'stores_open_ends', ''),
(1188, 33, 'stores_open_custom_text', ''),
(1189, 33, 'merchant_photo', '1476873635-images.jpg'),
(1190, 33, 'merchant_delivery_estimation', ''),
(1191, 33, 'merchant_delivery_miles', ''),
(1192, 33, 'merchant_photo_bg', '1476873639-caboolture-sports-club-the-bistro-1_lrg.jpg'),
(1193, 33, 'merchant_disabled_cod', ''),
(1194, 33, 'merchant_disabled_ccr', ''),
(1195, 33, 'merchant_extenal', ''),
(1196, 33, 'merchant_enabled_voucher', ''),
(1197, 33, 'merchant_distance_type', ''),
(1198, 33, 'merchant_timezone', ''),
(1199, 33, 'merchant_close_msg', ''),
(1200, 33, 'merchant_preorder', ''),
(1201, 33, 'merchant_maximum_order', ''),
(1202, 33, 'merchant_packaging_charge', ''),
(1203, 33, 'merchant_close_msg_holiday', ''),
(1204, 33, 'merchant_holiday', ''),
(1205, 33, 'merchant_activated_menu', ''),
(1206, 33, 'spicydish', ''),
(1207, 33, 'merchant_required_delivery_time', ''),
(1208, 33, 'merchant_close_store', ''),
(1209, 33, 'merchant_packaging_increment', ''),
(1210, 33, 'merchant_show_time', ''),
(1211, 33, 'merchant_enabled_tip', ''),
(1212, 33, 'merchant_tip_default', ''),
(1213, 33, 'merchant_minimum_order_pickup', ''),
(1214, 33, 'merchant_maximum_order_pickup', ''),
(1215, 33, 'merchant_disabled_ordering', ''),
(1216, 33, 'merchant_tax_charges', ''),
(1217, 33, 'stores_open_pm_start', ''),
(1218, 33, 'stores_open_pm_ends', ''),
(1219, 33, 'food_option_not_available', ''),
(1220, 33, 'order_verification', ''),
(1221, 33, 'order_sms_code_waiting', ''),
(1222, 33, 'disabled_food_gallery', ''),
(1223, 32, 'merchant_latitude', '28.5431686'),
(1224, 32, 'merchant_longtitude', '77.12880610000002'),
(1225, 32, 'merchant_information', ''),
(1226, 32, 'merchant_minimum_order', ''),
(1227, 32, 'merchant_tax', ''),
(1228, 32, 'merchant_delivery_charges', ''),
(1229, 32, 'stores_open_day', ''),
(1230, 32, 'stores_open_starts', ''),
(1231, 32, 'stores_open_ends', ''),
(1232, 32, 'stores_open_custom_text', ''),
(1233, 32, 'merchant_photo', '1476873800-collage-1.jpg'),
(1234, 32, 'merchant_delivery_estimation', ''),
(1235, 32, 'merchant_delivery_miles', ''),
(1236, 32, 'merchant_photo_bg', '1476874676-IMG_8021-719507.JPG'),
(1237, 32, 'merchant_disabled_cod', ''),
(1238, 32, 'merchant_disabled_ccr', ''),
(1239, 32, 'merchant_extenal', ''),
(1240, 32, 'merchant_enabled_voucher', ''),
(1241, 32, 'merchant_distance_type', ''),
(1242, 32, 'merchant_timezone', ''),
(1243, 32, 'merchant_close_msg', ''),
(1244, 32, 'merchant_preorder', ''),
(1245, 32, 'merchant_maximum_order', ''),
(1246, 32, 'merchant_packaging_charge', ''),
(1247, 32, 'merchant_close_msg_holiday', ''),
(1248, 32, 'merchant_holiday', ''),
(1249, 32, 'merchant_activated_menu', ''),
(1250, 32, 'spicydish', ''),
(1251, 32, 'merchant_required_delivery_time', ''),
(1252, 32, 'merchant_close_store', ''),
(1253, 32, 'merchant_packaging_increment', ''),
(1254, 32, 'merchant_show_time', ''),
(1255, 32, 'merchant_enabled_tip', ''),
(1256, 32, 'merchant_tip_default', ''),
(1257, 32, 'merchant_minimum_order_pickup', ''),
(1258, 32, 'merchant_maximum_order_pickup', ''),
(1259, 32, 'merchant_disabled_ordering', ''),
(1260, 32, 'merchant_tax_charges', ''),
(1261, 32, 'stores_open_pm_start', ''),
(1262, 32, 'stores_open_pm_ends', ''),
(1263, 32, 'food_option_not_available', ''),
(1264, 32, 'order_verification', ''),
(1265, 32, 'order_sms_code_waiting', ''),
(1266, 32, 'disabled_food_gallery', ''),
(1267, 31, 'merchant_latitude', '28.5508001'),
(1268, 31, 'merchant_longtitude', '77.2101159'),
(1269, 31, 'merchant_information', ''),
(1270, 31, 'merchant_minimum_order', ''),
(1271, 31, 'merchant_tax', ''),
(1272, 31, 'merchant_delivery_charges', ''),
(1273, 31, 'stores_open_day', ''),
(1274, 31, 'stores_open_starts', ''),
(1275, 31, 'stores_open_ends', ''),
(1276, 31, 'stores_open_custom_text', ''),
(1277, 31, 'merchant_photo', '1476873998-567e9fc3c383b0b5b5404d8c58068df7_featured_v2.jpg'),
(1278, 31, 'merchant_delivery_estimation', ''),
(1279, 31, 'merchant_delivery_miles', ''),
(1280, 31, 'merchant_photo_bg', '1476874004-index.jpg'),
(1281, 31, 'merchant_disabled_cod', ''),
(1282, 31, 'merchant_disabled_ccr', ''),
(1283, 31, 'merchant_extenal', ''),
(1284, 31, 'merchant_enabled_voucher', ''),
(1285, 31, 'merchant_distance_type', ''),
(1286, 31, 'merchant_timezone', ''),
(1287, 31, 'merchant_close_msg', ''),
(1288, 31, 'merchant_preorder', ''),
(1289, 31, 'merchant_maximum_order', ''),
(1290, 31, 'merchant_packaging_charge', ''),
(1291, 31, 'merchant_close_msg_holiday', ''),
(1292, 31, 'merchant_holiday', ''),
(1293, 31, 'merchant_activated_menu', ''),
(1294, 31, 'spicydish', ''),
(1295, 31, 'merchant_required_delivery_time', ''),
(1296, 31, 'merchant_close_store', ''),
(1297, 31, 'merchant_packaging_increment', ''),
(1298, 31, 'merchant_show_time', ''),
(1299, 31, 'merchant_enabled_tip', ''),
(1300, 31, 'merchant_tip_default', ''),
(1301, 31, 'merchant_minimum_order_pickup', ''),
(1302, 31, 'merchant_maximum_order_pickup', ''),
(1303, 31, 'merchant_disabled_ordering', ''),
(1304, 31, 'merchant_tax_charges', ''),
(1305, 31, 'stores_open_pm_start', ''),
(1306, 31, 'stores_open_pm_ends', ''),
(1307, 31, 'food_option_not_available', ''),
(1308, 31, 'order_verification', ''),
(1309, 31, 'order_sms_code_waiting', ''),
(1310, 31, 'disabled_food_gallery', ''),
(1311, 30, 'merchant_latitude', '28.6339192'),
(1312, 30, 'merchant_longtitude', '77.22093559999996'),
(1313, 30, 'merchant_information', ''),
(1314, 30, 'merchant_minimum_order', ''),
(1315, 30, 'merchant_tax', ''),
(1316, 30, 'merchant_delivery_charges', ''),
(1317, 30, 'stores_open_day', ''),
(1318, 30, 'stores_open_starts', ''),
(1319, 30, 'stores_open_ends', ''),
(1320, 30, 'stores_open_custom_text', ''),
(1321, 30, 'merchant_photo', '1476874200-242122.jpg'),
(1322, 30, 'merchant_delivery_estimation', ''),
(1323, 30, 'merchant_delivery_miles', ''),
(1324, 30, 'merchant_photo_bg', '1476874202-index.jpg'),
(1325, 30, 'merchant_disabled_cod', ''),
(1326, 30, 'merchant_disabled_ccr', ''),
(1327, 30, 'merchant_extenal', ''),
(1328, 30, 'merchant_enabled_voucher', ''),
(1329, 30, 'merchant_distance_type', ''),
(1330, 30, 'merchant_timezone', ''),
(1331, 30, 'merchant_close_msg', ''),
(1332, 30, 'merchant_preorder', ''),
(1333, 30, 'merchant_maximum_order', ''),
(1334, 30, 'merchant_packaging_charge', ''),
(1335, 30, 'merchant_close_msg_holiday', ''),
(1336, 30, 'merchant_holiday', ''),
(1337, 30, 'merchant_activated_menu', ''),
(1338, 30, 'spicydish', ''),
(1339, 30, 'merchant_required_delivery_time', ''),
(1340, 30, 'merchant_close_store', ''),
(1341, 30, 'merchant_packaging_increment', ''),
(1342, 30, 'merchant_show_time', ''),
(1343, 30, 'merchant_enabled_tip', ''),
(1344, 30, 'merchant_tip_default', ''),
(1345, 30, 'merchant_minimum_order_pickup', ''),
(1346, 30, 'merchant_maximum_order_pickup', ''),
(1347, 30, 'merchant_disabled_ordering', ''),
(1348, 30, 'merchant_tax_charges', ''),
(1349, 30, 'stores_open_pm_start', ''),
(1350, 30, 'stores_open_pm_ends', ''),
(1351, 30, 'food_option_not_available', ''),
(1352, 30, 'order_verification', ''),
(1353, 30, 'order_sms_code_waiting', ''),
(1354, 30, 'disabled_food_gallery', ''),
(1355, 29, 'merchant_latitude', '28.6404268'),
(1356, 29, 'merchant_longtitude', '77.1858995'),
(1357, 29, 'merchant_information', ''),
(1358, 29, 'merchant_minimum_order', ''),
(1359, 29, 'merchant_tax', ''),
(1360, 29, 'merchant_delivery_charges', ''),
(1361, 29, 'stores_open_day', ''),
(1362, 29, 'stores_open_starts', ''),
(1363, 29, 'stores_open_ends', ''),
(1364, 29, 'stores_open_custom_text', ''),
(1365, 29, 'merchant_photo', '1476874431-f8f1576e0a69554ec47310b1f15a7b78.jpg'),
(1366, 29, 'merchant_delivery_estimation', ''),
(1367, 29, 'merchant_delivery_miles', ''),
(1368, 29, 'merchant_photo_bg', '1476874437-224192_8.jpg'),
(1369, 29, 'merchant_disabled_cod', ''),
(1370, 29, 'merchant_disabled_ccr', ''),
(1371, 29, 'merchant_extenal', ''),
(1372, 29, 'merchant_enabled_voucher', ''),
(1373, 29, 'merchant_distance_type', ''),
(1374, 29, 'merchant_timezone', ''),
(1375, 29, 'merchant_close_msg', ''),
(1376, 29, 'merchant_preorder', ''),
(1377, 29, 'merchant_maximum_order', ''),
(1378, 29, 'merchant_packaging_charge', ''),
(1379, 29, 'merchant_close_msg_holiday', ''),
(1380, 29, 'merchant_holiday', ''),
(1381, 29, 'merchant_activated_menu', ''),
(1382, 29, 'spicydish', ''),
(1383, 29, 'merchant_required_delivery_time', ''),
(1384, 29, 'merchant_close_store', ''),
(1385, 29, 'merchant_packaging_increment', ''),
(1386, 29, 'merchant_show_time', ''),
(1387, 29, 'merchant_enabled_tip', ''),
(1388, 29, 'merchant_tip_default', ''),
(1389, 29, 'merchant_minimum_order_pickup', ''),
(1390, 29, 'merchant_maximum_order_pickup', ''),
(1391, 29, 'merchant_disabled_ordering', ''),
(1392, 29, 'merchant_tax_charges', ''),
(1393, 29, 'stores_open_pm_start', ''),
(1394, 29, 'stores_open_pm_ends', ''),
(1395, 29, 'food_option_not_available', ''),
(1396, 29, 'order_verification', ''),
(1397, 29, 'order_sms_code_waiting', ''),
(1398, 29, 'disabled_food_gallery', ''),
(1399, 28, 'merchant_latitude', '28.5475749'),
(1400, 28, 'merchant_longtitude', '77.2151407'),
(1401, 28, 'merchant_information', ''),
(1402, 28, 'merchant_minimum_order', ''),
(1403, 28, 'merchant_tax', ''),
(1404, 28, 'merchant_delivery_charges', ''),
(1405, 28, 'stores_open_day', ''),
(1406, 28, 'stores_open_starts', ''),
(1407, 28, 'stores_open_ends', ''),
(1408, 28, 'stores_open_custom_text', ''),
(1409, 28, 'merchant_photo', '1476874555-images.jpg'),
(1410, 28, 'merchant_delivery_estimation', ''),
(1411, 28, 'merchant_delivery_miles', ''),
(1412, 28, 'merchant_photo_bg', '1476874559-f4407f2eb3bfd42f725b6714b83613c7.jpg'),
(1413, 28, 'merchant_disabled_cod', ''),
(1414, 28, 'merchant_disabled_ccr', ''),
(1415, 28, 'merchant_extenal', ''),
(1416, 28, 'merchant_enabled_voucher', ''),
(1417, 28, 'merchant_distance_type', ''),
(1418, 28, 'merchant_timezone', ''),
(1419, 28, 'merchant_close_msg', ''),
(1420, 28, 'merchant_preorder', ''),
(1421, 28, 'merchant_maximum_order', ''),
(1422, 28, 'merchant_packaging_charge', ''),
(1423, 28, 'merchant_close_msg_holiday', ''),
(1424, 28, 'merchant_holiday', ''),
(1425, 28, 'merchant_activated_menu', ''),
(1426, 28, 'spicydish', ''),
(1427, 28, 'merchant_required_delivery_time', ''),
(1428, 28, 'merchant_close_store', ''),
(1429, 28, 'merchant_packaging_increment', ''),
(1430, 28, 'merchant_show_time', ''),
(1431, 28, 'merchant_enabled_tip', ''),
(1432, 28, 'merchant_tip_default', ''),
(1433, 28, 'merchant_minimum_order_pickup', ''),
(1434, 28, 'merchant_maximum_order_pickup', ''),
(1435, 28, 'merchant_disabled_ordering', ''),
(1436, 28, 'merchant_tax_charges', ''),
(1437, 28, 'stores_open_pm_start', ''),
(1438, 28, 'stores_open_pm_ends', ''),
(1439, 28, 'food_option_not_available', ''),
(1440, 28, 'order_verification', ''),
(1441, 28, 'order_sms_code_waiting', ''),
(1442, 28, 'disabled_food_gallery', ''),
(1443, 37, 'merchant_switch_master_cod', ''),
(1444, 37, 'merchant_switch_master_ccr', ''),
(1445, 37, 'merchant_switch_master_pyr', ''),
(1446, 24, 'merchant_minimum_order', ''),
(1447, 24, 'merchant_tax', ''),
(1448, 24, 'merchant_delivery_charges', ''),
(1449, 24, 'stores_open_day', ''),
(1450, 24, 'stores_open_starts', ''),
(1451, 24, 'stores_open_ends', ''),
(1452, 24, 'stores_open_custom_text', ''),
(1453, 24, 'merchant_photo', '1477116388-unnamed.jpg'),
(1454, 24, 'merchant_delivery_estimation', ''),
(1455, 24, 'merchant_delivery_miles', ''),
(1456, 24, 'merchant_photo_bg', '1477116395-unnamed.jpg'),
(1457, 24, 'merchant_disabled_cod', ''),
(1458, 24, 'merchant_disabled_ccr', ''),
(1459, 24, 'merchant_extenal', ''),
(1460, 24, 'merchant_enabled_voucher', ''),
(1461, 24, 'merchant_distance_type', ''),
(1462, 24, 'merchant_timezone', ''),
(1463, 24, 'merchant_close_msg', ''),
(1464, 24, 'merchant_preorder', ''),
(1465, 24, 'merchant_maximum_order', ''),
(1466, 24, 'merchant_packaging_charge', ''),
(1467, 24, 'merchant_close_msg_holiday', ''),
(1468, 24, 'merchant_holiday', ''),
(1469, 24, 'merchant_activated_menu', ''),
(1470, 24, 'spicydish', ''),
(1471, 24, 'merchant_required_delivery_time', ''),
(1472, 24, 'merchant_close_store', ''),
(1473, 24, 'merchant_packaging_increment', ''),
(1474, 24, 'merchant_show_time', ''),
(1475, 24, 'merchant_enabled_tip', ''),
(1476, 24, 'merchant_tip_default', ''),
(1477, 24, 'merchant_minimum_order_pickup', ''),
(1478, 24, 'merchant_maximum_order_pickup', ''),
(1479, 24, 'merchant_disabled_ordering', ''),
(1480, 24, 'merchant_tax_charges', ''),
(1481, 24, 'stores_open_pm_start', ''),
(1482, 24, 'stores_open_pm_ends', ''),
(1483, 24, 'food_option_not_available', ''),
(1484, 24, 'order_verification', ''),
(1485, 24, 'order_sms_code_waiting', ''),
(1486, 24, 'disabled_food_gallery', ''),
(1487, 20, 'merchant_minimum_order', ''),
(1488, 20, 'merchant_tax', ''),
(1489, 20, 'merchant_delivery_charges', ''),
(1490, 20, 'stores_open_day', ''),
(1491, 20, 'stores_open_starts', ''),
(1492, 20, 'stores_open_ends', ''),
(1493, 20, 'stores_open_custom_text', ''),
(1494, 20, 'merchant_photo', '1477116419-unnamed.jpg'),
(1495, 20, 'merchant_delivery_estimation', ''),
(1496, 20, 'merchant_delivery_miles', ''),
(1497, 20, 'merchant_photo_bg', '1477116426-unnamed.jpg'),
(1498, 20, 'merchant_disabled_cod', ''),
(1499, 20, 'merchant_disabled_ccr', ''),
(1500, 20, 'merchant_extenal', ''),
(1501, 20, 'merchant_enabled_voucher', ''),
(1502, 20, 'merchant_distance_type', ''),
(1503, 20, 'merchant_timezone', ''),
(1504, 20, 'merchant_close_msg', ''),
(1505, 20, 'merchant_preorder', ''),
(1506, 20, 'merchant_maximum_order', ''),
(1507, 20, 'merchant_packaging_charge', ''),
(1508, 20, 'merchant_close_msg_holiday', ''),
(1509, 20, 'merchant_holiday', ''),
(1510, 20, 'merchant_activated_menu', ''),
(1511, 20, 'spicydish', ''),
(1512, 20, 'merchant_required_delivery_time', ''),
(1513, 20, 'merchant_close_store', ''),
(1514, 20, 'merchant_packaging_increment', ''),
(1515, 20, 'merchant_show_time', ''),
(1516, 20, 'merchant_enabled_tip', ''),
(1517, 20, 'merchant_tip_default', ''),
(1518, 20, 'merchant_minimum_order_pickup', ''),
(1519, 20, 'merchant_maximum_order_pickup', ''),
(1520, 20, 'merchant_disabled_ordering', ''),
(1521, 20, 'merchant_tax_charges', ''),
(1522, 20, 'stores_open_pm_start', ''),
(1523, 20, 'stores_open_pm_ends', ''),
(1524, 20, 'food_option_not_available', ''),
(1525, 20, 'order_verification', ''),
(1526, 20, 'order_sms_code_waiting', ''),
(1527, 20, 'disabled_food_gallery', ''),
(1528, 23, 'merchant_minimum_order', ''),
(1529, 23, 'merchant_tax', ''),
(1530, 23, 'merchant_delivery_charges', ''),
(1531, 23, 'stores_open_day', ''),
(1532, 23, 'stores_open_starts', ''),
(1533, 23, 'stores_open_ends', ''),
(1534, 23, 'stores_open_custom_text', ''),
(1535, 23, 'merchant_photo', '1477116490-unnamed.jpg'),
(1536, 23, 'merchant_delivery_estimation', ''),
(1537, 23, 'merchant_delivery_miles', ''),
(1538, 23, 'merchant_photo_bg', '1477116495-unnamed.jpg'),
(1539, 23, 'merchant_disabled_cod', ''),
(1540, 23, 'merchant_disabled_ccr', ''),
(1541, 23, 'merchant_extenal', ''),
(1542, 23, 'merchant_enabled_voucher', ''),
(1543, 23, 'merchant_distance_type', ''),
(1544, 23, 'merchant_timezone', ''),
(1545, 23, 'merchant_close_msg', ''),
(1546, 23, 'merchant_preorder', ''),
(1547, 23, 'merchant_maximum_order', ''),
(1548, 23, 'merchant_packaging_charge', ''),
(1549, 23, 'merchant_close_msg_holiday', ''),
(1550, 23, 'merchant_holiday', ''),
(1551, 23, 'merchant_activated_menu', ''),
(1552, 23, 'spicydish', ''),
(1553, 23, 'merchant_required_delivery_time', ''),
(1554, 23, 'merchant_close_store', ''),
(1555, 23, 'merchant_packaging_increment', ''),
(1556, 23, 'merchant_show_time', ''),
(1557, 23, 'merchant_enabled_tip', ''),
(1558, 23, 'merchant_tip_default', ''),
(1559, 23, 'merchant_minimum_order_pickup', ''),
(1560, 23, 'merchant_maximum_order_pickup', ''),
(1561, 23, 'merchant_disabled_ordering', ''),
(1562, 23, 'merchant_tax_charges', ''),
(1563, 23, 'stores_open_pm_start', ''),
(1564, 23, 'stores_open_pm_ends', ''),
(1565, 23, 'food_option_not_available', ''),
(1566, 23, 'order_verification', ''),
(1567, 23, 'order_sms_code_waiting', ''),
(1568, 23, 'disabled_food_gallery', ''),
(1569, 22, 'merchant_minimum_order', ''),
(1570, 22, 'merchant_tax', ''),
(1571, 22, 'merchant_delivery_charges', ''),
(1572, 22, 'stores_open_day', ''),
(1573, 22, 'stores_open_starts', ''),
(1574, 22, 'stores_open_ends', ''),
(1575, 22, 'stores_open_custom_text', ''),
(1576, 22, 'merchant_photo', '1477116536-unnamed.jpg'),
(1577, 22, 'merchant_delivery_estimation', ''),
(1578, 22, 'merchant_delivery_miles', ''),
(1579, 22, 'merchant_photo_bg', '1477116540-unnamed.jpg'),
(1580, 22, 'merchant_disabled_cod', ''),
(1581, 22, 'merchant_disabled_ccr', ''),
(1582, 22, 'merchant_extenal', ''),
(1583, 22, 'merchant_enabled_voucher', ''),
(1584, 22, 'merchant_distance_type', ''),
(1585, 22, 'merchant_timezone', ''),
(1586, 22, 'merchant_close_msg', ''),
(1587, 22, 'merchant_preorder', ''),
(1588, 22, 'merchant_maximum_order', ''),
(1589, 22, 'merchant_packaging_charge', ''),
(1590, 22, 'merchant_close_msg_holiday', ''),
(1591, 22, 'merchant_holiday', ''),
(1592, 22, 'merchant_activated_menu', ''),
(1593, 22, 'spicydish', ''),
(1594, 22, 'merchant_required_delivery_time', ''),
(1595, 22, 'merchant_close_store', ''),
(1596, 22, 'merchant_packaging_increment', ''),
(1597, 22, 'merchant_show_time', ''),
(1598, 22, 'merchant_enabled_tip', ''),
(1599, 22, 'merchant_tip_default', ''),
(1600, 22, 'merchant_minimum_order_pickup', ''),
(1601, 22, 'merchant_maximum_order_pickup', ''),
(1602, 22, 'merchant_disabled_ordering', ''),
(1603, 22, 'merchant_tax_charges', ''),
(1604, 22, 'stores_open_pm_start', ''),
(1605, 22, 'stores_open_pm_ends', ''),
(1606, 22, 'food_option_not_available', ''),
(1607, 22, 'order_verification', ''),
(1608, 22, 'order_sms_code_waiting', ''),
(1609, 22, 'disabled_food_gallery', ''),
(1610, 21, 'merchant_minimum_order', ''),
(1611, 21, 'merchant_tax', ''),
(1612, 21, 'merchant_delivery_charges', ''),
(1613, 21, 'stores_open_day', ''),
(1614, 21, 'stores_open_starts', ''),
(1615, 21, 'stores_open_ends', ''),
(1616, 21, 'stores_open_custom_text', ''),
(1617, 21, 'merchant_photo', '1477116636-unnamed.jpg'),
(1618, 21, 'merchant_delivery_estimation', ''),
(1619, 21, 'merchant_delivery_miles', ''),
(1620, 21, 'merchant_photo_bg', '1477116647-unnamed.jpg'),
(1621, 21, 'merchant_disabled_cod', ''),
(1622, 21, 'merchant_disabled_ccr', ''),
(1623, 21, 'merchant_extenal', ''),
(1624, 21, 'merchant_enabled_voucher', ''),
(1625, 21, 'merchant_distance_type', ''),
(1626, 21, 'merchant_timezone', ''),
(1627, 21, 'merchant_close_msg', ''),
(1628, 21, 'merchant_preorder', ''),
(1629, 21, 'merchant_maximum_order', ''),
(1630, 21, 'merchant_packaging_charge', ''),
(1631, 21, 'merchant_close_msg_holiday', ''),
(1632, 21, 'merchant_holiday', ''),
(1633, 21, 'merchant_activated_menu', ''),
(1634, 21, 'spicydish', ''),
(1635, 21, 'merchant_required_delivery_time', ''),
(1636, 21, 'merchant_close_store', ''),
(1637, 21, 'merchant_packaging_increment', ''),
(1638, 21, 'merchant_show_time', ''),
(1639, 21, 'merchant_enabled_tip', ''),
(1640, 21, 'merchant_tip_default', ''),
(1641, 21, 'merchant_minimum_order_pickup', ''),
(1642, 21, 'merchant_maximum_order_pickup', ''),
(1643, 21, 'merchant_disabled_ordering', ''),
(1644, 21, 'merchant_tax_charges', ''),
(1645, 21, 'stores_open_pm_start', ''),
(1646, 21, 'stores_open_pm_ends', ''),
(1647, 21, 'food_option_not_available', ''),
(1648, 21, 'order_verification', ''),
(1649, 21, 'order_sms_code_waiting', ''),
(1650, 21, 'disabled_food_gallery', ''),
(1651, 19, 'merchant_minimum_order', ''),
(1652, 19, 'merchant_tax', ''),
(1653, 19, 'merchant_delivery_charges', ''),
(1654, 19, 'stores_open_day', ''),
(1655, 19, 'stores_open_starts', ''),
(1656, 19, 'stores_open_ends', ''),
(1657, 19, 'stores_open_custom_text', ''),
(1658, 19, 'merchant_photo', '1477116674-unnamed.jpg'),
(1659, 19, 'merchant_delivery_estimation', ''),
(1660, 19, 'merchant_delivery_miles', ''),
(1661, 19, 'merchant_photo_bg', '1477116678-unnamed.jpg'),
(1662, 19, 'merchant_disabled_cod', ''),
(1663, 19, 'merchant_disabled_ccr', ''),
(1664, 19, 'merchant_extenal', ''),
(1665, 19, 'merchant_enabled_voucher', ''),
(1666, 19, 'merchant_distance_type', ''),
(1667, 19, 'merchant_timezone', ''),
(1668, 19, 'merchant_close_msg', ''),
(1669, 19, 'merchant_preorder', ''),
(1670, 19, 'merchant_maximum_order', ''),
(1671, 19, 'merchant_packaging_charge', ''),
(1672, 19, 'merchant_close_msg_holiday', ''),
(1673, 19, 'merchant_holiday', ''),
(1674, 19, 'merchant_activated_menu', ''),
(1675, 19, 'spicydish', ''),
(1676, 19, 'merchant_required_delivery_time', ''),
(1677, 19, 'merchant_close_store', ''),
(1678, 19, 'merchant_packaging_increment', ''),
(1679, 19, 'merchant_show_time', ''),
(1680, 19, 'merchant_enabled_tip', ''),
(1681, 19, 'merchant_tip_default', ''),
(1682, 19, 'merchant_minimum_order_pickup', ''),
(1683, 19, 'merchant_maximum_order_pickup', ''),
(1684, 19, 'merchant_disabled_ordering', ''),
(1685, 19, 'merchant_tax_charges', ''),
(1686, 19, 'stores_open_pm_start', ''),
(1687, 19, 'stores_open_pm_ends', ''),
(1688, 19, 'food_option_not_available', ''),
(1689, 19, 'order_verification', ''),
(1690, 19, 'order_sms_code_waiting', ''),
(1691, 19, 'disabled_food_gallery', ''),
(1692, 40, 'merchant_minimum_order', ''),
(1693, 40, 'merchant_tax', ''),
(1694, 40, 'merchant_delivery_charges', ''),
(1695, 40, 'stores_open_day', ''),
(1696, 40, 'stores_open_starts', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(1697, 40, 'stores_open_ends', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(1698, 40, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(1699, 40, 'merchant_photo', '1477168525-unnamed.jpg'),
(1700, 40, 'merchant_delivery_estimation', ''),
(1701, 40, 'merchant_delivery_miles', ''),
(1702, 40, 'merchant_photo_bg', '1477168534-unnamed.jpg'),
(1703, 40, 'merchant_disabled_cod', ''),
(1704, 40, 'merchant_disabled_ccr', ''),
(1705, 40, 'merchant_extenal', ''),
(1706, 40, 'merchant_enabled_voucher', ''),
(1707, 40, 'merchant_distance_type', ''),
(1708, 40, 'merchant_timezone', ''),
(1709, 40, 'merchant_close_msg', ''),
(1710, 40, 'merchant_preorder', ''),
(1711, 40, 'merchant_maximum_order', ''),
(1712, 40, 'merchant_packaging_charge', ''),
(1713, 40, 'merchant_close_msg_holiday', ''),
(1714, 40, 'merchant_holiday', ''),
(1715, 40, 'merchant_activated_menu', ''),
(1716, 40, 'spicydish', ''),
(1717, 40, 'merchant_required_delivery_time', ''),
(1718, 40, 'merchant_close_store', ''),
(1719, 40, 'merchant_packaging_increment', ''),
(1720, 40, 'merchant_show_time', ''),
(1721, 40, 'merchant_enabled_tip', ''),
(1722, 40, 'merchant_tip_default', ''),
(1723, 40, 'merchant_minimum_order_pickup', ''),
(1724, 40, 'merchant_maximum_order_pickup', ''),
(1725, 40, 'merchant_disabled_ordering', ''),
(1726, 40, 'merchant_tax_charges', ''),
(1727, 40, 'stores_open_pm_start', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(1728, 40, 'stores_open_pm_ends', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(1729, 40, 'food_option_not_available', ''),
(1730, 40, 'order_verification', ''),
(1731, 40, 'order_sms_code_waiting', ''),
(1732, 40, 'disabled_food_gallery', ''),
(1733, 41, 'merchant_latitude', '28.5475749'),
(1734, 41, 'merchant_longtitude', '77.2151407'),
(1735, 41, 'merchant_information', ''),
(1736, 41, 'merchant_minimum_order', ''),
(1737, 41, 'merchant_tax', ''),
(1738, 41, 'merchant_delivery_charges', ''),
(1739, 41, 'stores_open_day', ''),
(1740, 41, 'stores_open_starts', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(1741, 41, 'stores_open_ends', '{\\"monday\\":false,\\"tuesday\\":false,\\"wednesday\\":false,\\"thursday\\":false,\\"friday\\":false,\\"saturday\\":false,\\"sunday\\":false}'),
(1742, 41, 'stores_open_custom_text', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(1743, 41, 'merchant_photo', '1477206732-unnamed.jpg'),
(1744, 41, 'merchant_delivery_estimation', ''),
(1745, 41, 'merchant_delivery_miles', ''),
(1746, 41, 'merchant_photo_bg', '1477206774-unnamed.jpg'),
(1747, 41, 'merchant_disabled_cod', ''),
(1748, 41, 'merchant_disabled_ccr', ''),
(1749, 41, 'merchant_extenal', ''),
(1750, 41, 'merchant_enabled_voucher', ''),
(1751, 41, 'merchant_distance_type', ''),
(1752, 41, 'merchant_timezone', ''),
(1753, 41, 'merchant_close_msg', ''),
(1754, 41, 'merchant_preorder', ''),
(1755, 41, 'merchant_maximum_order', ''),
(1756, 41, 'merchant_packaging_charge', ''),
(1757, 41, 'merchant_close_msg_holiday', ''),
(1758, 41, 'merchant_holiday', ''),
(1759, 41, 'merchant_activated_menu', ''),
(1760, 41, 'spicydish', ''),
(1761, 41, 'merchant_required_delivery_time', ''),
(1762, 41, 'merchant_close_store', ''),
(1763, 41, 'merchant_packaging_increment', ''),
(1764, 41, 'merchant_show_time', ''),
(1765, 41, 'merchant_enabled_tip', ''),
(1766, 41, 'merchant_tip_default', ''),
(1767, 41, 'merchant_minimum_order_pickup', ''),
(1768, 41, 'merchant_maximum_order_pickup', ''),
(1769, 41, 'merchant_disabled_ordering', ''),
(1770, 41, 'merchant_tax_charges', ''),
(1771, 41, 'stores_open_pm_start', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(1772, 41, 'stores_open_pm_ends', '{\\"monday\\":\\"\\",\\"tuesday\\":\\"\\",\\"wednesday\\":\\"\\",\\"thursday\\":\\"\\",\\"friday\\":\\"\\",\\"saturday\\":\\"\\",\\"sunday\\":\\"\\"}'),
(1773, 41, 'food_option_not_available', ''),
(1774, 41, 'order_verification', ''),
(1775, 41, 'order_sms_code_waiting', ''),
(1776, 41, 'disabled_food_gallery', '');

-- --------------------------------------------------------

--
-- Table structure for table `mt_order`
--

CREATE TABLE `mt_order` (
  `prefix` enum('FQO','FQD') NOT NULL DEFAULT 'FQO',
  `order_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `deals_id` int(20) DEFAULT NULL,
  `coupon_code` varchar(255) DEFAULT NULL,
  `json_details` text,
  `trans_type` varchar(100) DEFAULT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `sub_total` float(14,4) NOT NULL DEFAULT '0.0000',
  `tax` float(14,4) NOT NULL DEFAULT '0.0000',
  `taxable_total` decimal(14,4) NOT NULL DEFAULT '0.0000',
  `total_w_tax` float(14,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `stats_id` int(14) DEFAULT NULL,
  `viewed` int(1) NOT NULL DEFAULT '1',
  `delivery_charge` float(14,4) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `delivery_time` varchar(100) DEFAULT NULL,
  `delivery_asap` varchar(14) DEFAULT NULL,
  `delivery_instruction` varchar(255) DEFAULT NULL,
  `voucher_code` varchar(100) DEFAULT NULL,
  `voucher_amount` float(14,4) DEFAULT NULL,
  `voucher_type` varchar(100) DEFAULT NULL,
  `cc_id` int(14) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `ip_address` varchar(50) NOT NULL,
  `order_change` float(14,4) DEFAULT NULL,
  `payment_provider_name` varchar(255) DEFAULT NULL,
  `discounted_amount` float(14,5) DEFAULT NULL,
  `discount_percentage` float(14,5) DEFAULT NULL,
  `percent_commision` float(14,4) DEFAULT NULL,
  `total_commission` float(14,4) DEFAULT NULL,
  `commision_ontop` int(2) NOT NULL DEFAULT '2',
  `merchant_earnings` float(14,4) DEFAULT NULL,
  `packaging` float(14,4) DEFAULT NULL,
  `cart_tip_percentage` float(14,4) DEFAULT NULL,
  `cart_tip_value` float(14,4) DEFAULT NULL,
  `card_fee` float(14,4) DEFAULT NULL,
  `donot_apply_tax_delivery` int(1) NOT NULL DEFAULT '1',
  `order_locked` int(1) NOT NULL DEFAULT '1',
  `request_from` varchar(10) NOT NULL DEFAULT 'web',
  `mobile_cart_details` text,
  `points_discount` float(14,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_order`
--

INSERT INTO `mt_order` (`prefix`, `order_id`, `merchant_id`, `client_id`, `deals_id`, `coupon_code`, `json_details`, `trans_type`, `payment_type`, `sub_total`, `tax`, `taxable_total`, `total_w_tax`, `status`, `stats_id`, `viewed`, `delivery_charge`, `delivery_date`, `delivery_time`, `delivery_asap`, `delivery_instruction`, `voucher_code`, `voucher_amount`, `voucher_type`, `cc_id`, `date_created`, `date_modified`, `ip_address`, `order_change`, `payment_provider_name`, `discounted_amount`, `discount_percentage`, `percent_commision`, `total_commission`, `commision_ontop`, `merchant_earnings`, `packaging`, `cart_tip_percentage`, `cart_tip_value`, `card_fee`, `donot_apply_tax_delivery`, `order_locked`, `request_from`, `mobile_cart_details`, `points_discount`) VALUES
('FQO', 1, 10, 3, NULL, NULL, '[{"currentController":"store","merchant_id":"10","item_id":"4","price":"800","qty":2,"discount":"","notes":"","row":"","two_flavors":"","non_taxable":"1","sub_item":""}]', 'undefined', 'pyp', 1600.0000, 0.0000, '0.0000', 1600.0000, 'initial_order', 0, 1, 0.0000, '2016-08-26', '21:00', '', 'hjgjhgjkgjh', '', 0.0000, '', 0, '2016-08-26 19:04:50', '0000-00-00 00:00:00', '122.177.166.76', 0.0000, '', 0.00000, 0.00000, 0.0000, 0.0000, 2, 1600.0000, 0.0000, 0.0000, 0.0000, 0.0000, 1, 1, 'web', '', 0.0000),
('FQO', 2, 10, 3, NULL, NULL, '{"1":{"item_id":"4","row":"","merchant_id":"10","discount":"","currentController":"store","price":"800","qty":"1","notes":"","require_addon_3":"","sub_item":{"3":["3|160|Momos|","4|250|Paneer Tikka|"]},"addon_qty":{"3":["1","1"]},"two_flavors":"","non_taxable":"1","addon_ids":["3","4"],"cooking_ref":"","ingredients":""}}', 'undefined', 'pyp', 1210.0000, 0.0000, '0.0000', 1210.0000, 'initial_order', 0, 1, 0.0000, '2016-08-26', '21:00', '', 'jgfgdgdfghn', '', 0.0000, '', 0, '2016-08-26 19:27:49', '0000-00-00 00:00:00', '122.177.166.76', 0.0000, '', 0.00000, 0.00000, 0.0000, 0.0000, 2, 1210.0000, 0.0000, 0.0000, 0.0000, 0.0000, 1, 1, 'web', '', 0.0000),
('FQO', 3, 10, 3, NULL, NULL, '{"1":{"item_id":"4","row":"","merchant_id":"10","discount":"","currentController":"store","price":"800","qty":"1","notes":"","require_addon_3":"","sub_item":{"3":["3|160|Momos|","4|250|Paneer Tikka|"]},"addon_qty":{"3":["1","1"]},"two_flavors":"","non_taxable":"1","addon_ids":["3","4"],"cooking_ref":"","ingredients":""}}', 'undefined', 'ide', 1210.0000, 0.0000, '0.0000', 1210.0000, 'initial_order', 0, 1, 0.0000, '2016-08-26', '21:30', '', '', '', 0.0000, '', 0, '2016-08-26 19:28:53', '0000-00-00 00:00:00', '122.177.166.76', 0.0000, '', 0.00000, 0.00000, 0.0000, 0.0000, 2, 1210.0000, 0.0000, 0.0000, 0.0000, 0.0000, 1, 1, 'web', '', 0.0000),
('FQO', 4, 16, 2, NULL, NULL, '[{"currentController":"store","merchant_id":"16","item_id":"5","price":"120","qty":"1","discount":"","notes":"","row":"","two_flavors":"","non_taxable":"2","sub_item":""}]', 'undefined', 'pyp', 120.0000, 0.0000, '0.0000', 120.0000, 'initial_order', 0, 1, 0.0000, '2016-09-02', '20:00', '', '', '', 0.0000, '', 0, '2016-09-02 20:04:25', '0000-00-00 00:00:00', '122.177.244.251', 0.0000, '', 0.00000, 0.00000, 0.0000, 0.0000, 2, 120.0000, 0.0000, 0.0000, 0.0000, 0.0000, 1, 1, 'web', '', 0.0000),
('FQD', 5, 16, 3, 1, '423391', NULL, NULL, NULL, 0.0000, 0.0000, '0.0000', 0.0000, 'sent', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-26 06:53:50', NULL, '182.69.235.34', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, 1, 'web', NULL, NULL),
('FQD', 6, 16, 17, 1, '835041', NULL, NULL, NULL, 0.0000, 0.0000, '0.0000', 0.0000, 'sent', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-27 12:26:53', NULL, '122.177.100.160', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, 1, 'web', NULL, NULL),
('FQD', 7, 16, 17, 1, '962152', NULL, NULL, NULL, 0.0000, 0.0000, '0.0000', 0.0000, 'sent', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-27 12:26:55', NULL, '122.177.100.160', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, 1, 'web', NULL, NULL),
('FQD', 8, 16, 17, 1, '525504', NULL, NULL, NULL, 0.0000, 0.0000, '0.0000', 0.0000, 'sent', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-27 12:26:58', NULL, '122.177.100.160', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, 1, 'web', NULL, NULL),
('FQD', 9, 16, 17, 2, '910355', NULL, NULL, NULL, 0.0000, 0.0000, '0.0000', 0.0000, 'sent', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-27 12:27:02', NULL, '122.177.100.160', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, 1, 'web', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_order_delivery_address`
--

CREATE TABLE `mt_order_delivery_address` (
  `id` int(14) NOT NULL,
  `order_id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `location_name` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `contact_phone` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_order_details`
--

CREATE TABLE `mt_order_details` (
  `id` int(14) NOT NULL,
  `order_id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `item_id` int(14) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `order_notes` text NOT NULL,
  `normal_price` float(14,4) NOT NULL,
  `discounted_price` float(14,4) NOT NULL,
  `size` varchar(255) NOT NULL,
  `qty` int(14) NOT NULL,
  `cooking_ref` varchar(255) NOT NULL,
  `addon` text NOT NULL,
  `ingredients` text NOT NULL,
  `non_taxable` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_order_details`
--

INSERT INTO `mt_order_details` (`id`, `order_id`, `client_id`, `item_id`, `item_name`, `order_notes`, `normal_price`, `discounted_price`, `size`, `qty`, `cooking_ref`, `addon`, `ingredients`, `non_taxable`) VALUES
(1, 1, 3, 4, 'Black Dog', '', 800.0000, 800.0000, '', 2, '', '', '""', 1),
(2, 2, 3, 4, 'Black Dog', '', 800.0000, 800.0000, '', 1, '', '[{"addon_name":"Momos","addon_category":"Snacks","addon_qty":"1","addon_price":"160"},{"addon_name":"Paneer Tikka","addon_category":"Snacks","addon_qty":"1","addon_price":"250"}]', '""', 1),
(3, 3, 3, 4, 'Black Dog', '', 800.0000, 800.0000, '', 1, '', '[{"addon_name":"Momos","addon_category":"Snacks","addon_qty":"1","addon_price":"160"},{"addon_name":"Paneer Tikka","addon_category":"Snacks","addon_qty":"1","addon_price":"250"}]', '""', 1),
(4, 4, 2, 5, 'Old Monk Punch', '', 120.0000, 120.0000, '', 1, '', '', '""', 2);

-- --------------------------------------------------------

--
-- Table structure for table `mt_order_history`
--

CREATE TABLE `mt_order_history` (
  `id` int(14) NOT NULL,
  `order_id` int(14) NOT NULL,
  `status` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_order_sms`
--

CREATE TABLE `mt_order_sms` (
  `id` int(14) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `code` int(4) NOT NULL,
  `session` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_order_status`
--

CREATE TABLE `mt_order_status` (
  `stats_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `description` varchar(200) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_order_status`
--

INSERT INTO `mt_order_status` (`stats_id`, `merchant_id`, `description`, `date_created`, `date_modified`, `ip_address`) VALUES
(1, 0, 'pending', '2016-07-27', '0000-00-00', '122.177.116.173'),
(2, 0, 'cancelled', '2016-07-27', '0000-00-00', '122.177.116.173'),
(3, 0, 'complete', '2016-07-27', '2016-08-17', '112.196.181.219'),
(4, 0, 'paid', '2016-07-27', '0000-00-00', '122.177.116.173');

-- --------------------------------------------------------

--
-- Table structure for table `mt_packages`
--

CREATE TABLE `mt_packages` (
  `package_id` int(14) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` float(14,4) NOT NULL,
  `promo_price` float(14,4) NOT NULL,
  `expiration` int(14) NOT NULL,
  `expiration_type` varchar(50) NOT NULL DEFAULT 'days',
  `unlimited_post` int(1) NOT NULL DEFAULT '2',
  `post_limit` int(14) NOT NULL,
  `sequence` int(14) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `sell_limit` int(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_packages`
--

INSERT INTO `mt_packages` (`package_id`, `title`, `description`, `price`, `promo_price`, `expiration`, `expiration_type`, `unlimited_post`, `post_limit`, `sequence`, `status`, `date_created`, `date_modified`, `ip_address`, `sell_limit`) VALUES
(1, 'delux', 'delux', 2000.0000, 0.0000, 266450, 'year', 2, 10, 0, 'pending', '2016-07-30 17:59:15', '2016-08-12 15:06:26', '122.177.195.151', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mt_package_trans`
--

CREATE TABLE `mt_package_trans` (
  `id` int(14) NOT NULL,
  `package_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `price` float(14,4) NOT NULL,
  `payment_type` varchar(100) NOT NULL,
  `mt_id` int(14) NOT NULL,
  `TOKEN` varchar(255) NOT NULL,
  `membership_expired` date NOT NULL,
  `TRANSACTIONID` varchar(255) NOT NULL,
  `TRANSACTIONTYPE` varchar(100) NOT NULL,
  `PAYMENTSTATUS` varchar(100) NOT NULL,
  `PAYPALFULLRESPONSE` text NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `fee` float(14,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_package_trans`
--

INSERT INTO `mt_package_trans` (`id`, `package_id`, `merchant_id`, `price`, `payment_type`, `mt_id`, `TOKEN`, `membership_expired`, `TRANSACTIONID`, `TRANSACTIONTYPE`, `PAYMENTSTATUS`, `PAYPALFULLRESPONSE`, `status`, `date_created`, `ip_address`, `fee`) VALUES
(1, 1, 2, 2000.0000, 'ocr', 1, '', '2018-08-01', '', '', '', '', 'pending', '2016-08-01 18:09:09', '122.177.16.215', 0.0000);

-- --------------------------------------------------------

--
-- Table structure for table `mt_payment_order`
--

CREATE TABLE `mt_payment_order` (
  `id` int(14) NOT NULL,
  `payment_type` varchar(10) CHARACTER SET utf8 NOT NULL,
  `payment_reference` varchar(255) CHARACTER SET utf8 NOT NULL,
  `order_id` int(14) NOT NULL,
  `raw_response` text CHARACTER SET utf8 NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mt_payment_provider`
--

CREATE TABLE `mt_payment_provider` (
  `id` int(14) NOT NULL,
  `payment_name` varchar(255) NOT NULL,
  `payment_logo` varchar(255) NOT NULL,
  `sequence` int(14) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'publish',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_paypal_checkout`
--

CREATE TABLE `mt_paypal_checkout` (
  `id` int(14) NOT NULL,
  `order_id` int(14) NOT NULL,
  `token` varchar(255) NOT NULL,
  `paypal_request` text NOT NULL,
  `paypal_response` text NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'checkout',
  `date_created` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_paypal_payment`
--

CREATE TABLE `mt_paypal_payment` (
  `id` int(14) NOT NULL,
  `order_id` int(14) NOT NULL,
  `TOKEN` varchar(255) NOT NULL,
  `TRANSACTIONID` varchar(100) NOT NULL,
  `TRANSACTIONTYPE` varchar(100) NOT NULL,
  `PAYMENTTYPE` varchar(100) NOT NULL,
  `ORDERTIME` varchar(100) NOT NULL,
  `AMT` varchar(14) NOT NULL,
  `FEEAMT` varchar(14) NOT NULL,
  `TAXAMT` varchar(14) NOT NULL,
  `CURRENCYCODE` varchar(3) NOT NULL,
  `PAYMENTSTATUS` varchar(100) NOT NULL,
  `CORRELATIONID` varchar(100) NOT NULL,
  `TIMESTAMP` varchar(100) NOT NULL,
  `json_details` text NOT NULL,
  `date_created` varchar(50) NOT NULL,
  `ip_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_points_earn`
--

CREATE TABLE `mt_points_earn` (
  `id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `order_id` int(14) NOT NULL,
  `total_points_earn` int(14) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'inactive',
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `trans_type` varchar(100) NOT NULL DEFAULT 'order',
  `points_type` varchar(50) NOT NULL DEFAULT 'earn'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_points_earn`
--

INSERT INTO `mt_points_earn` (`id`, `client_id`, `merchant_id`, `order_id`, `total_points_earn`, `status`, `date_created`, `ip_address`, `trans_type`, `points_type`) VALUES
(1, 3, 10, 1, 1600, 'inactive', '2016-08-26 19:04:50', '122.177.166.76', 'order', 'earn'),
(2, 3, 0, 0, 10, 'inactive', '2016-08-26 19:04:50', '122.177.166.76', 'first_order', 'earn'),
(3, 3, 10, 2, 800, 'inactive', '2016-08-26 19:27:49', '122.177.166.76', 'order', 'earn'),
(4, 7, 0, 0, 100, 'active', '2016-08-30 15:55:53', '112.196.181.216', 'signup', 'earn'),
(5, 2, 16, 4, 10, 'inactive', '2016-09-02 20:04:25', '122.177.244.251', 'order', 'earn'),
(6, 2, 0, 0, 10, 'inactive', '2016-09-02 20:04:25', '122.177.244.251', 'first_order', 'earn'),
(7, 12, 0, 0, 100, 'active', '2016-09-02 20:10:36', '122.177.244.251', 'signup', 'earn'),
(8, 2, 0, 0, 1000, 'active', '2016-09-02 18:48:23', '', 'adjustment', 'earn'),
(9, 12, 0, 0, 900, 'active', '2016-09-02 19:37:02', '', 'adjustment', 'earn'),
(10, 17, 0, 0, 100, 'active', '2016-09-22 18:53:06', '122.177.5.254', 'signup', 'earn'),
(11, 18, 0, 0, 100, 'active', '2016-10-01 13:40:40', '122.177.251.173', 'signup', 'earn');

-- --------------------------------------------------------

--
-- Table structure for table `mt_points_expenses`
--

CREATE TABLE `mt_points_expenses` (
  `id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `order_id` int(14) NOT NULL,
  `total_points` int(14) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'inactive',
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `total_points_amt` float(14,4) NOT NULL,
  `trans_type` varchar(100) NOT NULL DEFAULT 'order',
  `points_type` varchar(50) NOT NULL DEFAULT 'expenses'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `mt_points_trans`
-- (See below for the actual view)
--
CREATE TABLE `mt_points_trans` (
`id` int(14)
,`client_id` int(14)
,`merchant_id` int(14)
,`order_id` int(14)
,`total_points_earn` int(14)
,`status` varchar(255)
,`date_created` datetime
,`points_type` varchar(50)
,`trans_type` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `mt_rating`
--

CREATE TABLE `mt_rating` (
  `id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `ratings` float(14,1) NOT NULL,
  `client_id` int(14) NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_rating`
--

INSERT INTO `mt_rating` (`id`, `merchant_id`, `ratings`, `client_id`, `date_created`, `ip_address`) VALUES
(1, 1, 4.0, 3, '2016-08-13 13:57:54', '122.177.88.164');

-- --------------------------------------------------------

--
-- Table structure for table `mt_rating_meaning`
--

CREATE TABLE `mt_rating_meaning` (
  `id` int(14) NOT NULL,
  `rating_start` float(14,1) NOT NULL,
  `rating_end` float(14,1) NOT NULL,
  `meaning` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_rating_meaning`
--

INSERT INTO `mt_rating_meaning` (`id`, `rating_start`, `rating_end`, `meaning`, `date_created`, `date_modified`, `ip_address`) VALUES
(1, 1.0, 1.9, 'poor', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
(2, 2.0, 2.9, 'good', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
(3, 3.0, 4.0, 'very good', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173'),
(4, 4.1, 5.0, 'excellent', '2016-07-27 09:49:42', '0000-00-00 00:00:00', '122.177.116.173');

-- --------------------------------------------------------

--
-- Table structure for table `mt_review`
--

CREATE TABLE `mt_review` (
  `id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL,
  `review` text NOT NULL,
  `rating` float(14,1) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'publish',
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `order_id` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_review`
--

INSERT INTO `mt_review` (`id`, `merchant_id`, `client_id`, `review`, `rating`, `status`, `date_created`, `ip_address`, `order_id`) VALUES
(1, 1, 3, 'Very delicious food must try....\r\n', 4.0, 'publish', '2016-08-13 13:57:54', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mt_samplemusic`
--

CREATE TABLE `mt_samplemusic` (
  `id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mt_shipping_rate`
--

CREATE TABLE `mt_shipping_rate` (
  `id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `distance_from` int(14) NOT NULL,
  `distance_to` int(14) NOT NULL,
  `shipping_units` varchar(5) NOT NULL,
  `distance_price` float(14,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_shipping_rate`
--

INSERT INTO `mt_shipping_rate` (`id`, `merchant_id`, `distance_from`, `distance_to`, `shipping_units`, `distance_price`) VALUES
(1, 1, 0, 0, 'mi', 0.0000);

-- --------------------------------------------------------

--
-- Table structure for table `mt_size`
--

CREATE TABLE `mt_size` (
  `size_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `size_name` varchar(255) NOT NULL,
  `sequence` int(14) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'published',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `size_name_trans` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_size`
--

INSERT INTO `mt_size` (`size_id`, `merchant_id`, `size_name`, `sequence`, `status`, `date_created`, `date_modified`, `ip_address`, `size_name_trans`) VALUES
(10, 0, 'Small', 0, 'publish', '2016-09-12 17:09:09', '0000-00-00 00:00:00', '182.77.8.158', ''),
(11, 0, 'Medium', 0, 'publish', '2016-09-12 17:49:08', '0000-00-00 00:00:00', '122.177.150.227', ''),
(12, 0, 'Large', 0, 'publish', '2016-09-12 17:49:29', '0000-00-00 00:00:00', '122.177.150.227', '');

-- --------------------------------------------------------

--
-- Table structure for table `mt_sms_broadcast`
--

CREATE TABLE `mt_sms_broadcast` (
  `broadcast_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `send_to` int(14) NOT NULL,
  `list_mobile_number` text CHARACTER SET utf8 NOT NULL,
  `sms_alert_message` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;

-- --------------------------------------------------------

--
-- Table structure for table `mt_sms_broadcast_details`
--

CREATE TABLE `mt_sms_broadcast_details` (
  `id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL DEFAULT '0',
  `broadcast_id` int(14) NOT NULL,
  `client_id` int(14) NOT NULL DEFAULT '0',
  `client_name` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(50) DEFAULT NULL,
  `sms_message` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `gateway_response` text,
  `date_created` datetime NOT NULL,
  `date_executed` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `gateway` varchar(255) NOT NULL DEFAULT 'twilio',
  `contact_email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_sms_broadcast_details`
--

INSERT INTO `mt_sms_broadcast_details` (`id`, `merchant_id`, `broadcast_id`, `client_id`, `client_name`, `contact_phone`, `sms_message`, `status`, `gateway_response`, `date_created`, `date_executed`, `ip_address`, `gateway`, `contact_email`) VALUES
(1, 0, 0, 0, '', '+919818389740', 'Your verificatio code is 222142', 'Error Code: 20404 - ', '', '2016-08-05 22:31:15', '0000-00-00 00:00:00', '112.196.181.216', '', NULL),
(2, 0, 0, 0, '', '+918439765406', 'Your verificatio code is 410331', 'Error Code: 20404 - ', '', '2016-08-12 12:52:57', '0000-00-00 00:00:00', '122.177.106.61', '', NULL),
(3, 0, 999999999, 0, '', '09899407151', '', 'Error Code: 20404 - ', '', '2016-08-31 19:12:25', '2016-08-31 19:12:25', '122.177.36.229', '', NULL),
(4, 0, 999999999, 0, '', '09899407151', '', 'Error Code: 20404 - ', '', '2016-08-31 19:20:14', '2016-08-31 19:20:14', '122.177.36.229', '', NULL),
(5, 0, 999999999, 0, '', '09899407151', '', 'Error Code: 20404 - ', '', '2016-08-31 19:20:21', '2016-08-31 19:20:21', '122.177.36.229', '', NULL),
(6, 0, 999999999, 0, '', '+918439765406', 'You coupon code is 930304', 'Error Code: 20404 - ', '', '2016-08-31 19:45:03', '2016-08-31 19:45:03', '182.77.8.83', '', NULL),
(7, 0, 999999999, 0, '', '09899407151', 'You have sent coupon code  930304 to customer', 'Error Code: 20404 - ', '', '2016-08-31 19:45:05', '2016-08-31 19:45:05', '182.77.8.83', '', NULL),
(8, 0, 999999999, 0, '', '+918439765406', 'You coupon code is 798874', 'Error Code: 20404 - ', '', '2016-08-31 19:47:16', '2016-08-31 19:47:16', '182.77.8.83', '', NULL),
(9, 0, 999999999, 0, '', '09899407151', 'You have sent coupon code  798874 to customer', 'Error Code: 20404 - ', '', '2016-08-31 19:47:17', '2016-08-31 19:47:17', '182.77.8.83', '', NULL),
(10, 0, 999999999, 0, '', '+918439765406', 'You coupon code is 147655', 'Error Code: 20404 - ', '', '2016-08-31 19:51:47', '2016-08-31 19:51:47', '182.77.8.83', '', NULL),
(11, 0, 999999999, 0, '', '09899407151', 'You have sent coupon code  147655 to customer', 'Error Code: 20404 - ', '', '2016-08-31 19:51:48', '2016-08-31 19:51:48', '182.77.8.83', '', NULL),
(12, 0, 999999999, 0, '', '+918439765406', 'You coupon code is 638332', 'Error Code: 20404 - ', '', '2016-08-31 19:56:01', '2016-08-31 19:56:01', '182.77.8.83', '', NULL),
(13, 0, 999999999, 0, '', '09899407151', 'You have sent coupon code  638332 to customer', 'Error Code: 20404 - ', '', '2016-08-31 19:56:02', '2016-08-31 19:56:02', '182.77.8.83', '', NULL),
(14, 0, 999999999, 0, '', '+918439765406', 'You coupon code is 950392', 'Error Code: 20404 - ', '', '2016-08-31 19:56:03', '2016-08-31 19:56:03', '182.77.8.83', '', NULL),
(15, 0, 999999999, 0, '', '09899407151', 'You have sent coupon code  950392 to customer', 'Error Code: 20404 - ', '', '2016-08-31 19:56:04', '2016-08-31 19:56:04', '182.77.8.83', '', NULL),
(16, 0, 999999999, 0, '', '+918439765406', 'You coupon code is 552191', 'Error Code: 20404 - ', '', '2016-08-31 20:20:58', '2016-08-31 20:20:58', '182.77.8.83', '', NULL),
(17, 0, 999999999, 0, '', '09899407151', 'You have sent coupon code  552191 to customer', 'Error Code: 20404 - ', '', '2016-08-31 20:20:59', '2016-08-31 20:20:59', '182.77.8.83', '', NULL),
(18, 0, 999999999, 0, '', '+918439765406', 'You coupon code is 431142', 'Error Code: 20404 - ', '', '2016-08-31 20:22:42', '2016-08-31 20:22:42', '182.77.8.83', '', NULL),
(19, 0, 999999999, 0, '', '09899407151', 'You have sent coupon code  431142 to customer', 'Error Code: 20404 - ', '', '2016-08-31 20:22:43', '2016-08-31 20:22:43', '182.77.8.83', '', NULL),
(20, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  283226 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:30:37', '2016-10-25 03:30:37', '122.177.16.125', '', NULL),
(21, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  426939 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:30:54', '2016-10-25 03:30:54', '122.177.16.125', '', NULL),
(22, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  487936 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:33:49', '2016-10-25 03:33:49', '122.177.16.125', '', NULL),
(23, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 766003', 'Error Code: 20404 - ', NULL, '2016-10-25 03:37:42', '2016-10-25 03:37:42', '122.177.16.125', '', NULL),
(24, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  766003 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:37:43', '2016-10-25 03:37:43', '122.177.16.125', '', NULL),
(25, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 213870', 'Error Code: 20404 - ', NULL, '2016-10-25 03:40:32', '2016-10-25 03:40:32', '122.177.16.125', '', NULL),
(26, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  213870 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:40:34', '2016-10-25 03:40:34', '122.177.16.125', '', NULL),
(27, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 892141', 'Error Code: 20404 - ', NULL, '2016-10-25 03:46:03', '2016-10-25 03:46:03', '122.177.16.125', '', NULL),
(28, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  892141 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:46:04', '2016-10-25 03:46:04', '122.177.16.125', '', NULL),
(29, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 495919', 'Error Code: 20404 - ', NULL, '2016-10-25 03:46:21', '2016-10-25 03:46:21', '122.177.16.125', '', NULL),
(30, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  495919 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:46:22', '2016-10-25 03:46:22', '122.177.16.125', '', NULL),
(31, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 266803', 'Error Code: 20404 - ', NULL, '2016-10-25 03:48:25', '2016-10-25 03:48:25', '122.177.16.125', '', NULL),
(32, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  266803 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:48:26', '2016-10-25 03:48:26', '122.177.16.125', '', NULL),
(33, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 585175', 'Error Code: 20404 - ', NULL, '2016-10-25 03:51:02', '2016-10-25 03:51:02', '122.177.16.125', '', NULL),
(34, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 120028', 'Error Code: 20404 - ', NULL, '2016-10-25 03:53:04', '2016-10-25 03:53:04', '122.177.16.125', '', NULL),
(35, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  120028 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:53:05', '2016-10-25 03:53:05', '122.177.16.125', '', NULL),
(36, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 251682', 'Error Code: 20404 - ', NULL, '2016-10-25 03:58:07', '2016-10-25 03:58:07', '122.177.16.125', '', NULL),
(37, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  251682 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:58:09', '2016-10-25 03:58:09', '122.177.16.125', '', NULL),
(38, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 986718', 'Error Code: 20404 - ', NULL, '2016-10-25 03:59:33', '2016-10-25 03:59:33', '122.177.16.125', '', NULL),
(39, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  986718 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 03:59:34', '2016-10-25 03:59:34', '122.177.16.125', '', NULL),
(40, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 490225', 'Error Code: 20404 - ', NULL, '2016-10-25 04:03:04', '2016-10-25 04:03:04', '122.177.16.125', '', NULL),
(41, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  490225 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 04:03:05', '2016-10-25 04:03:05', '122.177.16.125', '', NULL),
(42, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 519452', 'Error Code: 20404 - ', NULL, '2016-10-25 04:06:53', '2016-10-25 04:06:53', '122.177.34.83', '', NULL),
(43, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  519452 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 04:06:54', '2016-10-25 04:06:54', '122.177.34.83', '', NULL),
(44, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 229214', 'Error Code: 20404 - ', NULL, '2016-10-25 04:06:55', '2016-10-25 04:06:55', '122.177.34.83', '', NULL),
(45, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  229214 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 04:06:56', '2016-10-25 04:06:56', '122.177.34.83', '', NULL),
(46, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 183291', 'Error Code: 20404 - ', NULL, '2016-10-25 04:10:19', '2016-10-25 04:10:19', '122.177.34.83', '', NULL),
(47, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  183291 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 04:10:20', '2016-10-25 04:10:20', '122.177.34.83', '', NULL),
(48, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 435115', 'Error Code: 20404 - ', NULL, '2016-10-25 04:12:01', '2016-10-25 04:12:01', '122.177.34.83', '', NULL),
(49, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  435115 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 04:12:02', '2016-10-25 04:12:02', '122.177.34.83', '', NULL),
(50, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 710266', 'Error Code: 20404 - ', NULL, '2016-10-25 04:12:11', '2016-10-25 04:12:11', '122.177.34.83', '', NULL),
(51, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  710266 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 04:12:12', '2016-10-25 04:12:12', '122.177.34.83', '', NULL),
(52, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 726624', 'Error Code: 20404 - ', NULL, '2016-10-25 04:13:00', '2016-10-25 04:13:00', '122.177.34.83', '', NULL),
(53, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  726624 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 04:13:01', '2016-10-25 04:13:01', '122.177.34.83', '', NULL),
(54, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 713441', 'Error Code: 20404 - ', NULL, '2016-10-25 04:14:00', '2016-10-25 04:14:00', '122.177.34.83', '', NULL),
(55, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 552165', 'Error Code: 20404 - ', NULL, '2016-10-25 04:14:50', '2016-10-25 04:14:50', '122.177.34.83', '', NULL),
(56, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 234963', 'Error Code: 20404 - ', NULL, '2016-10-25 04:15:51', '2016-10-25 04:15:51', '122.177.34.83', '', NULL),
(57, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 560742', 'Error Code: 20404 - ', NULL, '2016-10-25 04:17:07', '2016-10-25 04:17:07', '122.177.34.83', '', NULL),
(58, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 414923', 'Error Code: 20404 - ', NULL, '2016-10-25 04:20:25', '2016-10-25 04:20:25', '122.177.34.83', 'Error Code: 20404 - ', NULL),
(59, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 615332', 'Error Code: 20404 - ', NULL, '2016-10-25 04:23:55', '2016-10-25 04:23:55', '122.177.34.83', 'Error Code: 20404 - ', NULL),
(60, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 353968', 'Error Code: 20404 - ', NULL, '2016-10-25 04:25:23', '2016-10-25 04:25:23', '122.177.34.83', 'Error Code: 20404 - ', NULL),
(61, 0, 999999999, 0, NULL, NULL, 'You coupon code is 353968', 'Error Code: 20404 - ', NULL, '2016-10-25 04:25:23', '2016-10-25 04:25:23', '122.177.34.83', 'Error Code: 20404 - ', NULL),
(62, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  353968 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 04:25:24', '2016-10-25 04:25:24', '122.177.34.83', 'Error Code: 20404 - ', NULL),
(63, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 214927', 'Error Code: 20404 - ', NULL, '2016-10-25 04:26:29', '2016-10-25 04:26:29', '122.177.34.83', 'Error Code: 20404 - ', NULL),
(64, 0, 999999999, 0, NULL, NULL, 'You coupon code is 214927', 'Error Code: 20404 - ', NULL, '2016-10-25 04:26:29', '2016-10-25 04:26:29', '122.177.34.83', 'Error Code: 20404 - ', NULL),
(65, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  214927 to customer', 'Error Code: 20404 - ', NULL, '2016-10-25 04:26:30', '2016-10-25 04:26:30', '122.177.34.83', 'Error Code: 20404 - ', NULL),
(66, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 234687', 'pending', NULL, '2016-10-25 04:28:28', '2016-10-25 04:28:28', '122.177.34.83', 'Error Code: 20404 - ', NULL),
(67, 0, 999999999, 0, NULL, NULL, 'You coupon code is 234687', 'pending', NULL, '2016-10-25 04:28:28', '2016-10-25 04:28:28', '122.177.34.83', 'Error Code: 20404 - ', 'dhawanitbhatnagar@gmail.com'),
(68, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  234687 to customer', 'pending', NULL, '2016-10-25 04:28:29', '2016-10-25 04:28:29', '122.177.34.83', 'Error Code: 20404 - ', NULL),
(69, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 669051', 'pending', NULL, '2016-10-25 04:30:23', '2016-10-25 04:30:23', '122.177.34.83', 'pending', NULL),
(70, 0, 999999999, 0, NULL, NULL, 'You coupon code is 669051', 'pending', NULL, '2016-10-25 04:30:23', '2016-10-25 04:30:23', '122.177.34.83', 'pending', 'dhawanitbhatnagar@gmail.com'),
(71, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  669051 to customer', 'pending', NULL, '2016-10-25 04:30:24', '2016-10-25 04:30:24', '122.177.34.83', 'pending', NULL),
(72, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  669051 to customer', 'pending', NULL, '2016-10-25 04:30:24', '2016-10-25 04:30:24', '122.177.34.83', 'pending', 'circus@gmail.com'),
(73, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 321835', 'pending', NULL, '2016-10-26 06:22:57', '2016-10-26 06:22:57', '182.69.235.34', 'pending', NULL),
(74, 0, 999999999, 0, NULL, NULL, 'You coupon code is 321835', 'pending', NULL, '2016-10-26 06:22:57', '2016-10-26 06:22:57', '182.69.235.34', 'pending', 'dhawanitbhatnagar@gmail.com'),
(75, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  321835 to customer', 'pending', NULL, '2016-10-26 06:22:58', '2016-10-26 06:22:58', '182.69.235.34', 'pending', NULL),
(76, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  321835 to customer', 'pending', NULL, '2016-10-26 06:22:58', '2016-10-26 06:22:58', '182.69.235.34', 'pending', 'circus@gmail.com'),
(77, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 780002', 'pending', NULL, '2016-10-26 06:23:07', '2016-10-26 06:23:07', '182.69.235.34', 'pending', NULL),
(78, 0, 999999999, 0, NULL, NULL, 'You coupon code is 780002', 'pending', NULL, '2016-10-26 06:23:07', '2016-10-26 06:23:07', '182.69.235.34', 'pending', 'dhawanitbhatnagar@gmail.com'),
(79, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  780002 to customer', 'pending', NULL, '2016-10-26 06:23:08', '2016-10-26 06:23:08', '182.69.235.34', 'pending', NULL),
(80, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  780002 to customer', 'pending', NULL, '2016-10-26 06:23:08', '2016-10-26 06:23:08', '182.69.235.34', 'pending', 'circus@gmail.com'),
(81, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 351883', 'pending', NULL, '2016-10-26 06:39:17', '2016-10-26 06:39:17', '182.69.235.34', 'pending', NULL),
(82, 0, 999999999, 0, NULL, NULL, 'You coupon code is 351883', 'pending', NULL, '2016-10-26 06:39:17', '2016-10-26 06:39:17', '182.69.235.34', 'pending', 'dhawanitbhatnagar@gmail.com'),
(83, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  351883 to customer', 'pending', NULL, '2016-10-26 06:39:18', '2016-10-26 06:39:18', '182.69.235.34', 'pending', NULL),
(84, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  351883 to customer', 'pending', NULL, '2016-10-26 06:39:18', '2016-10-26 06:39:18', '182.69.235.34', 'pending', 'circus@gmail.com'),
(85, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 955734', 'pending', NULL, '2016-10-26 06:41:34', '2016-10-26 06:41:34', '182.69.235.34', 'pending', NULL),
(86, 0, 999999999, 0, NULL, NULL, 'You coupon code is 955734', 'pending', NULL, '2016-10-26 06:41:34', '2016-10-26 06:41:34', '182.69.235.34', 'pending', 'dhawanitbhatnagar@gmail.com'),
(87, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  955734 to customer', 'pending', NULL, '2016-10-26 06:41:35', '2016-10-26 06:41:35', '182.69.235.34', 'pending', NULL),
(88, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  955734 to customer', 'pending', NULL, '2016-10-26 06:41:35', '2016-10-26 06:41:35', '182.69.235.34', 'pending', 'circus@gmail.com'),
(89, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 865801', 'pending', NULL, '2016-10-26 06:42:17', '2016-10-26 06:42:17', '182.69.235.34', 'pending', NULL),
(90, 0, 999999999, 0, NULL, NULL, 'You coupon code is 865801', 'pending', NULL, '2016-10-26 06:42:17', '2016-10-26 06:42:17', '182.69.235.34', 'pending', 'dhawanitbhatnagar@gmail.com'),
(91, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  865801 to customer', 'pending', NULL, '2016-10-26 06:42:18', '2016-10-26 06:42:18', '182.69.235.34', 'pending', NULL),
(92, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  865801 to customer', 'pending', NULL, '2016-10-26 06:42:18', '2016-10-26 06:42:18', '182.69.235.34', 'pending', 'circus@gmail.com'),
(93, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 104265', 'pending', NULL, '2016-10-26 06:43:45', '2016-10-26 06:43:45', '182.69.235.34', 'pending', NULL),
(94, 0, 999999999, 0, NULL, NULL, 'You coupon code is 104265', 'pending', NULL, '2016-10-26 06:43:45', '2016-10-26 06:43:45', '182.69.235.34', 'pending', 'dhawanitbhatnagar@gmail.com'),
(95, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  104265 to customer', 'pending', NULL, '2016-10-26 06:43:47', '2016-10-26 06:43:47', '182.69.235.34', 'pending', NULL),
(96, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  104265 to customer', 'pending', NULL, '2016-10-26 06:43:47', '2016-10-26 06:43:47', '182.69.235.34', 'pending', 'circus@gmail.com'),
(97, 0, 999999999, 0, NULL, '+918439765406', 'You coupon code is 423391', 'pending', NULL, '2016-10-26 06:53:49', '2016-10-26 06:53:49', '182.69.235.34', 'pending', NULL),
(98, 0, 999999999, 0, NULL, NULL, 'You coupon code is 423391', 'pending', NULL, '2016-10-26 06:53:49', '2016-10-26 06:53:49', '182.69.235.34', 'pending', 'dhawanitbhatnagar@gmail.com'),
(99, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  423391 to customer', 'pending', NULL, '2016-10-26 06:53:50', '2016-10-26 06:53:50', '182.69.235.34', 'pending', NULL),
(100, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  423391 to customer', 'pending', NULL, '2016-10-26 06:53:50', '2016-10-26 06:53:50', '182.69.235.34', 'pending', 'circus@gmail.com'),
(101, 0, 999999999, 0, NULL, NULL, 'You coupon code is 835041', 'pending', NULL, '2016-10-27 12:26:52', '2016-10-27 12:26:52', '122.177.100.160', 'pending', 'jainmca4444@gmail.com'),
(102, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  835041 to customer', 'pending', NULL, '2016-10-27 12:26:53', '2016-10-27 12:26:53', '122.177.100.160', 'pending', NULL),
(103, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  835041 to customer', 'pending', NULL, '2016-10-27 12:26:53', '2016-10-27 12:26:53', '122.177.100.160', 'pending', 'circus@gmail.com'),
(104, 0, 999999999, 0, NULL, NULL, 'You coupon code is 962152', 'pending', NULL, '2016-10-27 12:26:54', '2016-10-27 12:26:54', '122.177.100.160', 'pending', 'jainmca4444@gmail.com'),
(105, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  962152 to customer', 'pending', NULL, '2016-10-27 12:26:55', '2016-10-27 12:26:55', '122.177.100.160', 'pending', NULL),
(106, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  962152 to customer', 'pending', NULL, '2016-10-27 12:26:55', '2016-10-27 12:26:55', '122.177.100.160', 'pending', 'circus@gmail.com'),
(107, 0, 999999999, 0, NULL, NULL, 'You coupon code is 525504', 'pending', NULL, '2016-10-27 12:26:57', '2016-10-27 12:26:57', '122.177.100.160', 'pending', 'jainmca4444@gmail.com'),
(108, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  525504 to customer', 'pending', NULL, '2016-10-27 12:26:58', '2016-10-27 12:26:58', '122.177.100.160', 'pending', NULL),
(109, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  525504 to customer', 'pending', NULL, '2016-10-27 12:26:58', '2016-10-27 12:26:58', '122.177.100.160', 'pending', 'circus@gmail.com'),
(110, 0, 999999999, 0, NULL, NULL, 'You coupon code is 910355', 'pending', NULL, '2016-10-27 12:27:01', '2016-10-27 12:27:01', '122.177.100.160', 'pending', 'jainmca4444@gmail.com'),
(111, 0, 999999999, 0, NULL, '09899407151', 'You have sent coupon code  910355 to customer', 'pending', NULL, '2016-10-27 12:27:02', '2016-10-27 12:27:02', '122.177.100.160', 'pending', NULL),
(112, 0, 999999999, 0, NULL, NULL, 'You have sent coupon code  910355 to customer', 'pending', NULL, '2016-10-27 12:27:02', '2016-10-27 12:27:02', '122.177.100.160', 'pending', 'circus@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `mt_sms_package`
--

CREATE TABLE `mt_sms_package` (
  `sms_package_id` int(14) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` float(14,4) NOT NULL,
  `promo_price` float(14,4) NOT NULL,
  `sms_limit` int(14) NOT NULL,
  `sequence` int(14) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_sms_package_trans`
--

CREATE TABLE `mt_sms_package_trans` (
  `id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `sms_package_id` int(14) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `package_price` float(14,4) NOT NULL,
  `sms_limit` int(14) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'pending',
  `payment_reference` varchar(255) NOT NULL,
  `payment_gateway_response` text NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_stripe_logs`
--

CREATE TABLE `mt_stripe_logs` (
  `id` int(14) NOT NULL,
  `order_id` int(14) NOT NULL,
  `json_result` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mt_subcategory`
--

CREATE TABLE `mt_subcategory` (
  `subcat_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `subcategory_name` varchar(255) NOT NULL,
  `subcategory_description` text NOT NULL,
  `discount` varchar(20) NOT NULL,
  `sequence` int(14) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'publish',
  `subcategory_name_trans` text NOT NULL,
  `subcategory_description_trans` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_subcategory`
--

INSERT INTO `mt_subcategory` (`subcat_id`, `merchant_id`, `subcategory_name`, `subcategory_description`, `discount`, `sequence`, `date_created`, `date_modified`, `ip_address`, `status`, `subcategory_name_trans`, `subcategory_description_trans`) VALUES
(6, 0, 'IMFL', 'IMFL', '', 0, '2016-09-01 17:06:13', '0000-00-00 00:00:00', '122.177.250.160', 'publish', '', ''),
(7, 0, 'BEER', 'BEER', '', 0, '2016-09-01 17:07:37', '0000-00-00 00:00:00', '122.177.250.160', 'publish', '', ''),
(8, 0, 'WINE', 'WINE', '', 0, '2016-09-01 17:08:48', '0000-00-00 00:00:00', '122.177.250.160', 'publish', '', ''),
(9, 0, 'FL', 'FL', '', 0, '2016-09-01 17:09:03', '0000-00-00 00:00:00', '122.177.250.160', 'publish', '', ''),
(10, 0, 'SHOTS', 'SHOTS', '', 0, '2016-09-01 17:09:15', '0000-00-00 00:00:00', '122.177.250.160', 'publish', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mt_subcategory_item`
--

CREATE TABLE `mt_subcategory_item` (
  `sub_item_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `sub_item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `category` varchar(255) NOT NULL,
  `price` varchar(15) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `sequence` int(14) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `sub_item_name_trans` text NOT NULL,
  `item_description_trans` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_subcategory_item`
--

INSERT INTO `mt_subcategory_item` (`sub_item_id`, `merchant_id`, `sub_item_name`, `item_description`, `category`, `price`, `photo`, `sequence`, `status`, `date_created`, `date_modified`, `ip_address`, `sub_item_name_trans`, `item_description_trans`) VALUES
(2, 1, 'Coca Cola', '500ml', '["1"]', '37', '1470999179-cocacola_PNG22.png', 0, 'publish', '2016-08-12 16:52:04', '2016-08-12 16:53:12', '122.177.199.132', '', ''),
(3, 10, 'Momos', '8 Pcs', '["3"]', '160', '1472213761-p19748-144117721855e69e821b643-700x700-idobd5e4a59b601f.jpg', 0, 'publish', '2016-08-26 19:16:12', '2016-08-26 19:16:24', '122.177.166.76', '', ''),
(4, 10, 'Paneer Tikka', '8 Pcs', '["3"]', '250', '1472213974-paneer-tikka.jpg', 0, 'publish', '2016-08-26 19:19:35', '2016-08-26 19:19:55', '122.177.166.76', '', '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `mt_view_merchant`
-- (See below for the actual view)
--
CREATE TABLE `mt_view_merchant` (
`merchant_id` int(14)
,`restaurant_slug` varchar(255)
,`restaurant_name` varchar(255)
,`restaurant_phone` varchar(100)
,`contact_name` varchar(255)
,`contact_phone` varchar(100)
,`contact_email` varchar(255)
,`country_code` varchar(3)
,`street` text
,`city` varchar(255)
,`state` varchar(255)
,`post_code` varchar(100)
,`cuisine` text
,`service` varchar(255)
,`free_delivery` int(1)
,`delivery_estimation` varchar(100)
,`username` varchar(100)
,`password` varchar(100)
,`activation_key` varchar(50)
,`activation_token` varchar(255)
,`status` varchar(100)
,`date_created` datetime
,`date_modified` datetime
,`date_activated` datetime
,`last_login` datetime
,`ip_address` varchar(50)
,`package_id` int(14)
,`package_price` float(14,5)
,`membership_expired` date
,`payment_steps` int(1)
,`is_featured` int(1)
,`is_ready` int(1)
,`is_sponsored` int(2)
,`sponsored_expiration` date
,`lost_password_code` varchar(50)
,`user_lang` int(14)
,`membership_purchase_date` datetime
,`sort_featured` int(14)
,`is_commission` int(1)
,`percent_commision` varchar(255)
,`abn` varchar(255)
,`session_token` varchar(255)
,`commision_type` varchar(50)
,`latitude` text
,`lontitude` text
,`delivery_charges` text
,`minimum_order` text
,`ratings` double(22,5)
,`rtype` varchar(100)
,`btype` varchar(255)
,`music_name` text
,`highlights` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `mt_view_order_details`
-- (See below for the actual view)
--
CREATE TABLE `mt_view_order_details` (
`id` int(14)
,`order_id` int(14)
,`client_id` int(14)
,`item_id` int(14)
,`item_name` varchar(255)
,`order_notes` text
,`normal_price` float(14,4)
,`discounted_price` float(14,4)
,`size` varchar(255)
,`qty` int(14)
,`cooking_ref` varchar(255)
,`addon` text
,`ingredients` text
,`non_taxable` int(1)
,`merchant_id` bigint(14)
,`status` varchar(255)
,`date_created` datetime
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `mt_view_ratings`
-- (See below for the actual view)
--
CREATE TABLE `mt_view_ratings` (
`merchant_id` int(14)
,`ratings` double(22,5)
);

-- --------------------------------------------------------

--
-- Table structure for table `mt_voucher`
--

CREATE TABLE `mt_voucher` (
  `voucher_id` int(14) NOT NULL,
  `voucher_name` varchar(255) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `number_of_voucher` int(14) NOT NULL,
  `amount` float NOT NULL,
  `voucher_type` varchar(100) NOT NULL DEFAULT 'fixed amount',
  `status` varchar(100) NOT NULL,
  `date_created` varchar(50) NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_voucher_list`
--

CREATE TABLE `mt_voucher_list` (
  `voucher_id` int(14) NOT NULL,
  `voucher_code` varchar(50) NOT NULL,
  `status` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT 'unused',
  `client_id` int(14) NOT NULL,
  `date_used` varchar(50) NOT NULL,
  `order_id` int(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_voucher_new`
--

CREATE TABLE `mt_voucher_new` (
  `voucher_id` int(14) NOT NULL,
  `voucher_owner` varchar(255) NOT NULL DEFAULT 'merchant',
  `merchant_id` int(14) NOT NULL,
  `joining_merchant` text NOT NULL,
  `voucher_name` varchar(255) NOT NULL,
  `voucher_type` varchar(255) NOT NULL,
  `amount` float(14,4) NOT NULL,
  `expiration` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `used_once` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_withdrawal`
--

CREATE TABLE `mt_withdrawal` (
  `withdrawal_id` int(14) NOT NULL,
  `merchant_id` int(14) NOT NULL,
  `payment_type` varchar(100) NOT NULL,
  `payment_method` varchar(100) NOT NULL,
  `amount` float(14,4) NOT NULL,
  `current_balance` float(14,4) NOT NULL,
  `balance` float(14,4) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `account` varchar(255) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `bank_account_number` varchar(255) NOT NULL,
  `swift_code` varchar(100) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_branch` varchar(255) NOT NULL,
  `bank_country` varchar(50) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `viewed` int(2) NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_to_process` date NOT NULL,
  `date_process` datetime NOT NULL,
  `api_raw_response` text NOT NULL,
  `withdrawal_token` text NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `bank_type` varchar(255) NOT NULL DEFAULT 'default'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_zipcode`
--

CREATE TABLE `mt_zipcode` (
  `zipcode_id` int(14) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `country_code` varchar(5) NOT NULL,
  `city` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `stree_name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `mt_mobile_registered_view`
--
DROP TABLE IF EXISTS `mt_mobile_registered_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mt_mobile_registered_view`  AS  select `a`.`id` AS `id`,`a`.`client_id` AS `client_id`,`a`.`device_platform` AS `device_platform`,`a`.`device_id` AS `device_id`,`a`.`enabled_push` AS `enabled_push`,`a`.`country_code_set` AS `country_code_set`,`a`.`date_created` AS `date_created`,`a`.`date_modified` AS `date_modified`,`a`.`ip_address` AS `ip_address`,`a`.`status` AS `status`,concat(`b`.`first_name`,' ',`b`.`last_name`) AS `client_name` from (`mt_mobile_registered` `a` left join `mt_client` `b` on((`a`.`client_id` = `b`.`client_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `mt_points_trans`
--
DROP TABLE IF EXISTS `mt_points_trans`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mt_points_trans`  AS  select `mt_points_earn`.`id` AS `id`,`mt_points_earn`.`client_id` AS `client_id`,`mt_points_earn`.`merchant_id` AS `merchant_id`,`mt_points_earn`.`order_id` AS `order_id`,`mt_points_earn`.`total_points_earn` AS `total_points_earn`,`mt_points_earn`.`status` AS `status`,`mt_points_earn`.`date_created` AS `date_created`,`mt_points_earn`.`points_type` AS `points_type`,`mt_points_earn`.`trans_type` AS `trans_type` from `mt_points_earn` union select `mt_points_expenses`.`id` AS `id`,`mt_points_expenses`.`client_id` AS `client_id`,`mt_points_expenses`.`merchant_id` AS `merchant_id`,`mt_points_expenses`.`order_id` AS `order_id`,`mt_points_expenses`.`total_points` AS `total_points`,`mt_points_expenses`.`status` AS `status`,`mt_points_expenses`.`date_created` AS `date_created`,`mt_points_expenses`.`points_type` AS `points_type`,`mt_points_expenses`.`trans_type` AS `trans_type` from `mt_points_expenses` ;

-- --------------------------------------------------------

--
-- Structure for view `mt_view_merchant`
--
DROP TABLE IF EXISTS `mt_view_merchant`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mt_view_merchant`  AS  select `a`.`merchant_id` AS `merchant_id`,`a`.`restaurant_slug` AS `restaurant_slug`,`a`.`restaurant_name` AS `restaurant_name`,`a`.`restaurant_phone` AS `restaurant_phone`,`a`.`contact_name` AS `contact_name`,`a`.`contact_phone` AS `contact_phone`,`a`.`contact_email` AS `contact_email`,`a`.`country_code` AS `country_code`,`a`.`street` AS `street`,`a`.`city` AS `city`,`a`.`state` AS `state`,`a`.`post_code` AS `post_code`,`a`.`cuisine` AS `cuisine`,`a`.`service` AS `service`,`a`.`free_delivery` AS `free_delivery`,`a`.`delivery_estimation` AS `delivery_estimation`,`a`.`username` AS `username`,`a`.`password` AS `password`,`a`.`activation_key` AS `activation_key`,`a`.`activation_token` AS `activation_token`,`a`.`status` AS `status`,`a`.`date_created` AS `date_created`,`a`.`date_modified` AS `date_modified`,`a`.`date_activated` AS `date_activated`,`a`.`last_login` AS `last_login`,`a`.`ip_address` AS `ip_address`,`a`.`package_id` AS `package_id`,`a`.`package_price` AS `package_price`,`a`.`membership_expired` AS `membership_expired`,`a`.`payment_steps` AS `payment_steps`,`a`.`is_featured` AS `is_featured`,`a`.`is_ready` AS `is_ready`,`a`.`is_sponsored` AS `is_sponsored`,`a`.`sponsored_expiration` AS `sponsored_expiration`,`a`.`lost_password_code` AS `lost_password_code`,`a`.`user_lang` AS `user_lang`,`a`.`membership_purchase_date` AS `membership_purchase_date`,`a`.`sort_featured` AS `sort_featured`,`a`.`is_commission` AS `is_commission`,`a`.`percent_commision` AS `percent_commision`,`a`.`abn` AS `abn`,`a`.`session_token` AS `session_token`,`a`.`commision_type` AS `commision_type`,`b`.`option_value` AS `latitude`,`c`.`option_value` AS `lontitude`,`d`.`option_value` AS `delivery_charges`,`e`.`option_value` AS `minimum_order`,`f`.`ratings` AS `ratings`,`a`.`rtype` AS `rtype`,`a`.`btype` AS `btype`,`a`.`music_name` AS `music_name`,`a`.`highlights` AS `highlights` from (((((`mt_merchant` `a` left join `mt_option` `b` on(((`a`.`merchant_id` = `b`.`merchant_id`) and (`b`.`option_name` = 'merchant_latitude')))) left join `mt_option` `c` on(((`a`.`merchant_id` = `c`.`merchant_id`) and (`c`.`option_name` = 'merchant_longtitude')))) left join `mt_option` `d` on(((`a`.`merchant_id` = `d`.`merchant_id`) and (`d`.`option_name` = 'merchant_delivery_charges')))) left join `mt_option` `e` on(((`a`.`merchant_id` = `e`.`merchant_id`) and (`e`.`option_name` = 'merchant_minimum_order')))) left join `mt_view_ratings` `f` on((`a`.`merchant_id` = `f`.`merchant_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `mt_view_order_details`
--
DROP TABLE IF EXISTS `mt_view_order_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mt_view_order_details`  AS  select `a`.`id` AS `id`,`a`.`order_id` AS `order_id`,`a`.`client_id` AS `client_id`,`a`.`item_id` AS `item_id`,`a`.`item_name` AS `item_name`,`a`.`order_notes` AS `order_notes`,`a`.`normal_price` AS `normal_price`,`a`.`discounted_price` AS `discounted_price`,`a`.`size` AS `size`,`a`.`qty` AS `qty`,`a`.`cooking_ref` AS `cooking_ref`,`a`.`addon` AS `addon`,`a`.`ingredients` AS `ingredients`,`a`.`non_taxable` AS `non_taxable`,(select `mt_order`.`merchant_id` from `mt_order` where (`mt_order`.`order_id` = `a`.`order_id`) limit 0,1) AS `merchant_id`,(select `mt_order`.`status` from `mt_order` where (`mt_order`.`order_id` = `a`.`order_id`) limit 0,1) AS `status`,(select `mt_order`.`date_created` from `mt_order` where (`mt_order`.`order_id` = `a`.`order_id`) limit 0,1) AS `date_created` from `mt_order_details` `a` ;

-- --------------------------------------------------------

--
-- Structure for view `mt_view_ratings`
--
DROP TABLE IF EXISTS `mt_view_ratings`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mt_view_ratings`  AS  select `mt_rating`.`merchant_id` AS `merchant_id`,(sum(`mt_rating`.`ratings`) / count(0)) AS `ratings` from `mt_rating` group by `mt_rating`.`merchant_id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mt_address_book`
--
ALTER TABLE `mt_address_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_admin_user`
--
ALTER TABLE `mt_admin_user`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `mt_alcohol`
--
ALTER TABLE `mt_alcohol`
  ADD PRIMARY KEY (`alcohol_id`);

--
-- Indexes for table `mt_alcohol_category`
--
ALTER TABLE `mt_alcohol_category`
  ADD PRIMARY KEY (`a_cat_id`);

--
-- Indexes for table `mt_alcohol_subcategory`
--
ALTER TABLE `mt_alcohol_subcategory`
  ADD PRIMARY KEY (`scat_id`);

--
-- Indexes for table `mt_bank_deposit`
--
ALTER TABLE `mt_bank_deposit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_barclay_trans`
--
ALTER TABLE `mt_barclay_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_bookingtable`
--
ALTER TABLE `mt_bookingtable`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indexes for table `mt_category`
--
ALTER TABLE `mt_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `mt_client`
--
ALTER TABLE `mt_client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `mt_client_cc`
--
ALTER TABLE `mt_client_cc`
  ADD PRIMARY KEY (`cc_id`);

--
-- Indexes for table `mt_cooking_ref`
--
ALTER TABLE `mt_cooking_ref`
  ADD PRIMARY KEY (`cook_id`);

--
-- Indexes for table `mt_cuisine`
--
ALTER TABLE `mt_cuisine`
  ADD PRIMARY KEY (`cuisine_id`);

--
-- Indexes for table `mt_currency`
--
ALTER TABLE `mt_currency`
  ADD PRIMARY KEY (`currency_code`);

--
-- Indexes for table `mt_custom_page`
--
ALTER TABLE `mt_custom_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_deals`
--
ALTER TABLE `mt_deals`
  ADD PRIMARY KEY (`deals_id`);

--
-- Indexes for table `mt_dishes`
--
ALTER TABLE `mt_dishes`
  ADD PRIMARY KEY (`dish_id`);

--
-- Indexes for table `mt_event`
--
ALTER TABLE `mt_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `mt_fax_broadcast`
--
ALTER TABLE `mt_fax_broadcast`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_fax_package`
--
ALTER TABLE `mt_fax_package`
  ADD PRIMARY KEY (`fax_package_id`);

--
-- Indexes for table `mt_fax_package_trans`
--
ALTER TABLE `mt_fax_package_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_follow`
--
ALTER TABLE `mt_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_global`
--
ALTER TABLE `mt_global`
  ADD PRIMARY KEY (`global_id`);

--
-- Indexes for table `mt_group`
--
ALTER TABLE `mt_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `mt_happyhours`
--
ALTER TABLE `mt_happyhours`
  ADD PRIMARY KEY (`happy_id`);

--
-- Indexes for table `mt_highlights`
--
ALTER TABLE `mt_highlights`
  ADD PRIMARY KEY (`h_id`);

--
-- Indexes for table `mt_ingredients`
--
ALTER TABLE `mt_ingredients`
  ADD PRIMARY KEY (`ingredients_id`);

--
-- Indexes for table `mt_item`
--
ALTER TABLE `mt_item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `mt_languages`
--
ALTER TABLE `mt_languages`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `mt_likes`
--
ALTER TABLE `mt_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_merchant`
--
ALTER TABLE `mt_merchant`
  ADD PRIMARY KEY (`merchant_id`);

--
-- Indexes for table `mt_merchant_cc`
--
ALTER TABLE `mt_merchant_cc`
  ADD PRIMARY KEY (`mt_id`);

--
-- Indexes for table `mt_merchant_user`
--
ALTER TABLE `mt_merchant_user`
  ADD PRIMARY KEY (`merchant_user_id`);

--
-- Indexes for table `mt_mobile_broadcast`
--
ALTER TABLE `mt_mobile_broadcast`
  ADD PRIMARY KEY (`broadcast_id`);

--
-- Indexes for table `mt_mobile_push_logs`
--
ALTER TABLE `mt_mobile_push_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `device_platform` (`device_platform`),
  ADD KEY `push_type` (`push_type`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `mt_mobile_registered`
--
ALTER TABLE `mt_mobile_registered`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `enabled_push` (`enabled_push`),
  ADD KEY `device_platform` (`device_platform`);

--
-- Indexes for table `mt_mobile_temp_email`
--
ALTER TABLE `mt_mobile_temp_email`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `mt_music_type`
--
ALTER TABLE `mt_music_type`
  ADD PRIMARY KEY (`music_id`);

--
-- Indexes for table `mt_newsletter`
--
ALTER TABLE `mt_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_offers`
--
ALTER TABLE `mt_offers`
  ADD PRIMARY KEY (`offers_id`);

--
-- Indexes for table `mt_option`
--
ALTER TABLE `mt_option`
  ADD PRIMARY KEY (`id`),
  ADD KEY `merchant_id` (`merchant_id`);

--
-- Indexes for table `mt_order`
--
ALTER TABLE `mt_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `merchant_id` (`merchant_id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `mt_order_delivery_address`
--
ALTER TABLE `mt_order_delivery_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `mt_order_details`
--
ALTER TABLE `mt_order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_order_history`
--
ALTER TABLE `mt_order_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `mt_order_sms`
--
ALTER TABLE `mt_order_sms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session` (`session`),
  ADD KEY `code` (`code`);

--
-- Indexes for table `mt_order_status`
--
ALTER TABLE `mt_order_status`
  ADD PRIMARY KEY (`stats_id`);

--
-- Indexes for table `mt_packages`
--
ALTER TABLE `mt_packages`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `mt_package_trans`
--
ALTER TABLE `mt_package_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_payment_order`
--
ALTER TABLE `mt_payment_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_payment_provider`
--
ALTER TABLE `mt_payment_provider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_paypal_checkout`
--
ALTER TABLE `mt_paypal_checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_paypal_payment`
--
ALTER TABLE `mt_paypal_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_points_earn`
--
ALTER TABLE `mt_points_earn`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `total_points_earn` (`total_points_earn`),
  ADD KEY `trans_type` (`trans_type`),
  ADD KEY `status` (`status`),
  ADD KEY `merchant_id` (`merchant_id`);

--
-- Indexes for table `mt_points_expenses`
--
ALTER TABLE `mt_points_expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `merchant_id` (`merchant_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `status` (`status`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `trans_type` (`trans_type`),
  ADD KEY `total_points` (`total_points`);

--
-- Indexes for table `mt_rating`
--
ALTER TABLE `mt_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_rating_meaning`
--
ALTER TABLE `mt_rating_meaning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_review`
--
ALTER TABLE `mt_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_samplemusic`
--
ALTER TABLE `mt_samplemusic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_shipping_rate`
--
ALTER TABLE `mt_shipping_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_size`
--
ALTER TABLE `mt_size`
  ADD PRIMARY KEY (`size_id`);

--
-- Indexes for table `mt_sms_broadcast`
--
ALTER TABLE `mt_sms_broadcast`
  ADD PRIMARY KEY (`broadcast_id`);

--
-- Indexes for table `mt_sms_broadcast_details`
--
ALTER TABLE `mt_sms_broadcast_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_sms_package`
--
ALTER TABLE `mt_sms_package`
  ADD PRIMARY KEY (`sms_package_id`);

--
-- Indexes for table `mt_sms_package_trans`
--
ALTER TABLE `mt_sms_package_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_stripe_logs`
--
ALTER TABLE `mt_stripe_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_subcategory`
--
ALTER TABLE `mt_subcategory`
  ADD PRIMARY KEY (`subcat_id`);

--
-- Indexes for table `mt_subcategory_item`
--
ALTER TABLE `mt_subcategory_item`
  ADD PRIMARY KEY (`sub_item_id`);

--
-- Indexes for table `mt_voucher`
--
ALTER TABLE `mt_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `mt_voucher_new`
--
ALTER TABLE `mt_voucher_new`
  ADD PRIMARY KEY (`voucher_id`),
  ADD KEY `voucher_name` (`voucher_name`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `mt_withdrawal`
--
ALTER TABLE `mt_withdrawal`
  ADD PRIMARY KEY (`withdrawal_id`);

--
-- Indexes for table `mt_zipcode`
--
ALTER TABLE `mt_zipcode`
  ADD PRIMARY KEY (`zipcode_id`),
  ADD KEY `country_code` (`country_code`),
  ADD KEY `area` (`area`),
  ADD KEY `stree_name` (`stree_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mt_address_book`
--
ALTER TABLE `mt_address_book`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mt_admin_user`
--
ALTER TABLE `mt_admin_user`
  MODIFY `admin_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_alcohol`
--
ALTER TABLE `mt_alcohol`
  MODIFY `alcohol_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mt_alcohol_category`
--
ALTER TABLE `mt_alcohol_category`
  MODIFY `a_cat_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mt_alcohol_subcategory`
--
ALTER TABLE `mt_alcohol_subcategory`
  MODIFY `scat_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mt_bank_deposit`
--
ALTER TABLE `mt_bank_deposit`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_barclay_trans`
--
ALTER TABLE `mt_barclay_trans`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_bookingtable`
--
ALTER TABLE `mt_bookingtable`
  MODIFY `booking_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_category`
--
ALTER TABLE `mt_category`
  MODIFY `cat_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `mt_client`
--
ALTER TABLE `mt_client`
  MODIFY `client_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `mt_client_cc`
--
ALTER TABLE `mt_client_cc`
  MODIFY `cc_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_cooking_ref`
--
ALTER TABLE `mt_cooking_ref`
  MODIFY `cook_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_cuisine`
--
ALTER TABLE `mt_cuisine`
  MODIFY `cuisine_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `mt_custom_page`
--
ALTER TABLE `mt_custom_page`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_deals`
--
ALTER TABLE `mt_deals`
  MODIFY `deals_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mt_dishes`
--
ALTER TABLE `mt_dishes`
  MODIFY `dish_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_event`
--
ALTER TABLE `mt_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `mt_fax_broadcast`
--
ALTER TABLE `mt_fax_broadcast`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_fax_package`
--
ALTER TABLE `mt_fax_package`
  MODIFY `fax_package_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_fax_package_trans`
--
ALTER TABLE `mt_fax_package_trans`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_follow`
--
ALTER TABLE `mt_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `mt_global`
--
ALTER TABLE `mt_global`
  MODIFY `global_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_group`
--
ALTER TABLE `mt_group`
  MODIFY `group_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_happyhours`
--
ALTER TABLE `mt_happyhours`
  MODIFY `happy_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mt_highlights`
--
ALTER TABLE `mt_highlights`
  MODIFY `h_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `mt_ingredients`
--
ALTER TABLE `mt_ingredients`
  MODIFY `ingredients_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_item`
--
ALTER TABLE `mt_item`
  MODIFY `item_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `mt_languages`
--
ALTER TABLE `mt_languages`
  MODIFY `lang_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_likes`
--
ALTER TABLE `mt_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `mt_merchant`
--
ALTER TABLE `mt_merchant`
  MODIFY `merchant_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `mt_merchant_cc`
--
ALTER TABLE `mt_merchant_cc`
  MODIFY `mt_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_merchant_user`
--
ALTER TABLE `mt_merchant_user`
  MODIFY `merchant_user_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_mobile_broadcast`
--
ALTER TABLE `mt_mobile_broadcast`
  MODIFY `broadcast_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_mobile_push_logs`
--
ALTER TABLE `mt_mobile_push_logs`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_mobile_registered`
--
ALTER TABLE `mt_mobile_registered`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_mobile_temp_email`
--
ALTER TABLE `mt_mobile_temp_email`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_music_type`
--
ALTER TABLE `mt_music_type`
  MODIFY `music_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `mt_newsletter`
--
ALTER TABLE `mt_newsletter`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_offers`
--
ALTER TABLE `mt_offers`
  MODIFY `offers_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_option`
--
ALTER TABLE `mt_option`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1777;
--
-- AUTO_INCREMENT for table `mt_order`
--
ALTER TABLE `mt_order`
  MODIFY `order_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mt_order_delivery_address`
--
ALTER TABLE `mt_order_delivery_address`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_order_details`
--
ALTER TABLE `mt_order_details`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mt_order_history`
--
ALTER TABLE `mt_order_history`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_order_sms`
--
ALTER TABLE `mt_order_sms`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_order_status`
--
ALTER TABLE `mt_order_status`
  MODIFY `stats_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mt_packages`
--
ALTER TABLE `mt_packages`
  MODIFY `package_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_package_trans`
--
ALTER TABLE `mt_package_trans`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_payment_order`
--
ALTER TABLE `mt_payment_order`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_payment_provider`
--
ALTER TABLE `mt_payment_provider`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_paypal_checkout`
--
ALTER TABLE `mt_paypal_checkout`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_paypal_payment`
--
ALTER TABLE `mt_paypal_payment`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_points_earn`
--
ALTER TABLE `mt_points_earn`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `mt_points_expenses`
--
ALTER TABLE `mt_points_expenses`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_rating`
--
ALTER TABLE `mt_rating`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_rating_meaning`
--
ALTER TABLE `mt_rating_meaning`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mt_review`
--
ALTER TABLE `mt_review`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_samplemusic`
--
ALTER TABLE `mt_samplemusic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_shipping_rate`
--
ALTER TABLE `mt_shipping_rate`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_size`
--
ALTER TABLE `mt_size`
  MODIFY `size_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `mt_sms_broadcast`
--
ALTER TABLE `mt_sms_broadcast`
  MODIFY `broadcast_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_sms_broadcast_details`
--
ALTER TABLE `mt_sms_broadcast_details`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `mt_sms_package`
--
ALTER TABLE `mt_sms_package`
  MODIFY `sms_package_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_sms_package_trans`
--
ALTER TABLE `mt_sms_package_trans`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_stripe_logs`
--
ALTER TABLE `mt_stripe_logs`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_subcategory`
--
ALTER TABLE `mt_subcategory`
  MODIFY `subcat_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mt_subcategory_item`
--
ALTER TABLE `mt_subcategory_item`
  MODIFY `sub_item_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mt_voucher`
--
ALTER TABLE `mt_voucher`
  MODIFY `voucher_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_voucher_new`
--
ALTER TABLE `mt_voucher_new`
  MODIFY `voucher_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_withdrawal`
--
ALTER TABLE `mt_withdrawal`
  MODIFY `withdrawal_id` int(14) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_zipcode`
--
ALTER TABLE `mt_zipcode`
  MODIFY `zipcode_id` int(14) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
